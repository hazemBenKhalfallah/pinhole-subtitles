projectFilters.filter('i18n', ['$rootScope',
    function ($rootScope) {
        return function (input) {
            'use strict';
            var translations = {
                    'fr': {
                        //DO NOT REMOVE- THIS IS FOR UNIT TESTS//
                        'unit-test': 'fr'
                        },
                    'en': {
                        //DO NOT REMOVE- THIS IS FOR UNIT TESTS//
                        'unit-test': 'en'
                    }
                },
                currentLanguage = $rootScope.currentLanguage || 'fr';
            return translations[currentLanguage][input];
        }
    }
]);