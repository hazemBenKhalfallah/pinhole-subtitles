'use strict';

projectServices.factory('$logger', ['$log', '$location', function ($log, $location) {
    return {
        isProdHost: function () {
            var host = $location.host().match(/\.tn/);
            return host !== null && angular.isDefined(host);
        },
        info: function (message) {
            var self = this;
            if (self.isProdHost()) {
                return;
            }
            $log.info(message);
        },
        warn: function (message) {
            var self = this;

            if (self.isProdHost()) {
                return;
            }
            $log.warn(message);
        },
        error: function (message) {
            var self = this;
            if (self.isProdHost()) {
                return;
            }
            $log.error(message);
        },
        log: function (message) {
            var self = this;
            if (self.isProdHost()) {
                return;
            }
            $log.log(message);
        }
    }
}]);

projectServices.factory('serviceSample', ['$http', '$logger',
    function ($http, $logger) {
        return {
            testPost: function () {
                $http.post(
                    '/test',
                    {
                        a: 'value of a',
                        b: 'value of b'
                    }
                ).success(function (data) {
                        if(data.error){
                            $logger.info("error");
                        }else{
                            $logger.info("success");
                        }
                    }).error(function () {
                    });

            },
            testGet: function () {
                $http.get(
                    '/test'
                ).success(function (data) {
                        console.log(data.age);
                    }).error(function () {
                        //
                    });

            }
        }
    }]);

