var projectDirectives = angular.module('project.directives', ['ui.bootstrap']);
var projectFilters = angular.module('project.filters', []);
var projectServices = angular.module('project.services', []);
var projectApp = angular.module('projectApp', ['ngResource', 'ngRoute', 'ngSanitize', 'project.directives', 'project.filters', 'project.services']);

var appConfig = function ($routeProvider, $locationProvider, $httpProvider) {
    // ROUTING
    $routeProvider.
        when('/', {
            templateUrl: 'assets/views/home.html'
        }).
        when('/404', {
            templateUrl: 'assets/views/404.html'
        }).
        when('/500', {
            templateUrl: 'assets/views/500.html'
        }).
        otherwise({
            redirectTo: '/404'
        });

    // DO NOT REMOVE !handles hashbangs! //
    $locationProvider.html5Mode(true);

    // HTTP INTERCEPTOR
    var $http,
        interceptor = ['$q', '$injector', '$location',
            function ($q, $injector, $location) {

                function success(response) {
                    if (angular.isDefined(response)) {
                        return response;
                    }
                }

                function error(response) {
                    if (angular.isDefined(response)) {
                        if (response.status === 404) {
                            $location.path('/404').replace();
                            return $q.reject(response);
                        }
                        if (response.status === 500) {
                            $location.path('/500').replace();
                            return $q.reject(response);
                        }
                    }
                }

                return function (promise) {
                    if (angular.isDefined(promise)) {
                        return promise.then(success, error);
                    }
                }
            }
        ];

    $httpProvider.responseInterceptors.push(interceptor);

};
appConfig.$inject = ["$routeProvider", "$locationProvider", "$httpProvider"];

projectApp.config(appConfig).run(['$rootScope','$location',
    function ($rootScope, $location) {

        //ROUTE CHANGE ERRORS
        $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
            //change this code to handle the error somehow
            $location.path('/404').replace();
        });

        $rootScope.$on("$routeChangeStart", function () {
        });
    }


]);
