'use strict';
function exampleController($scope, serviceSample) {
    $scope.post = function () {
        serviceSample.testPost();
    }

    $scope.get = function () {
        serviceSample.testGet();
    }
}

exampleController.$inject = ['$scope', 'serviceSample'];