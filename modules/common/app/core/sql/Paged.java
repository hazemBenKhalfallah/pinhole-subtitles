/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package core.sql;

import com.avaje.ebean.SqlRow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ayassinov
 */
public class Paged<T extends Serializable> implements Serializable {

    public List<T> results = new ArrayList<T>();
    public boolean hasMore = false;
    public int next = 0;
    public int count = 0;



    public void addResult(T item, int maxRowCount, int nextPage){
        if(this.results.size() >= maxRowCount){
            this.hasMore = true;
            this.next = nextPage + maxRowCount;
        }else{
            this.results.add(item);
        }
    }

    public static <T extends Serializable> Paged<T> paged(List<SqlRow> rows, int maxRowCount, int nextPage) {
        final Paged<T> paged = new Paged<T>();
        if (rows.size() > maxRowCount) {
            rows.remove(maxRowCount);
            paged.hasMore = true;
            paged.next = nextPage + maxRowCount;
        } else {
            paged.hasMore = false;
        }
        return paged;
    }

    public static <T extends  Serializable> Paged<T> pagedFromItemsList(List<T> items, int maxRowCount, int nextPage){
        final Paged<T> paged = new Paged<T>();
        if (items.size() > maxRowCount) {
            items.remove(maxRowCount);
            paged.hasMore = true;
            paged.next = nextPage + maxRowCount;
        } else {
            paged.hasMore = false;
        }
        return paged;
    }


}


