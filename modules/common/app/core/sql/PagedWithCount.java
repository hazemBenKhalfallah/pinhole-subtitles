package core.sql;


import java.io.Serializable;

public class PagedWithCount<T extends Serializable> extends  Paged<T>{
    public int totalPages = 0;
    public int count = 0;
    public int pageSize = 0;

    public static <T extends Serializable> PagedWithCount<T> paged(int maxRowCount, int currentPage, int count){
        final PagedWithCount <T> paged = new PagedWithCount<T>();
        final int totalPages = count  % maxRowCount == 0 ? count/maxRowCount : (int)Math.ceil(count/maxRowCount) + 1;
        paged.pageSize = maxRowCount;
        paged.hasMore = totalPages > 0 && currentPage < totalPages;
        paged.next = totalPages > 0 && currentPage < totalPages ? currentPage * maxRowCount : 0;
        paged.totalPages = totalPages;
        paged.count = count;
        return paged;
    }
}
