package core.search;

import com.google.common.base.Optional;
import core.StringUtils;
import core.log.Log;
import core.parameter.ApplicationParameter;
import play.libs.Json;
import play.libs.WS;

/**
 * @author ayassinov
 */
public class ElasticSearch {

    public static class Document {
        public static final String CATEGORY = "category";
        public static final String CHANNEL = "channel";
        public static final String EVENT = "event";
    }

    public static String getElasticSearchUrl(String docType, String id) {
        return ApplicationParameter.EC2.esUrl + String.format("/search/%s/%s", docType, id);
    }

    /**
     * creates an index on a object t
     *
     * @param t   object to index
     * @param id  identifier
     * @param <T> models.view.channel.Channel, models.view.event.card.Category or models.view.event.card.EventCardSmall;
     */
    public static <T> void index(T t, String document, String id) {
        if (t == null)
            return;

        if (StringUtils.isNullOrEmpty(document))
            return;

        try {
            deleteIndex(document, id);
            WS.url(getElasticSearchUrl(document, id))
                    .post(Json.toJson(t))
                    .get();
        } catch (Exception ex) {
            Log.error(Log.Type.SEARCH, ex, "Error while creating new index with type=%s, id=%s", document, id);
        }
    }

    /**
     * delete from elasticSearch all documents related to a given type
     *
     * @param document (models.event.Event, models.event.Channel, models.category.Category)
     */
    public static void deleteIndex(String document) {
        if (StringUtils.isNullOrEmpty(document))
            return;

        try {
            WS.url(ApplicationParameter.EC2.esUrl + "/search/" + document)
                    .delete()
                    .get();
        } catch (Exception ex) {
            Log.error(Log.Type.SEARCH, ex, "Error while deleting all index with type=%s", document);
        }
    }

    /**
     * delete a document from elasticSearch
     *
     * @param document class (models.event.Event, models.event.Channel, models.category.Category)
     * @param id       document identifier
     */
    public static void deleteIndex(String document, String id) {
        if (StringUtils.isNullOrEmpty(document))
            return;

        try {
            WS.url(getElasticSearchUrl(document, id))
                    .delete()
                    .get();
        } catch (Exception ex) {
            Log.error(Log.Type.SEARCH, ex, "Exception while deleting an index with type=%s, id=%s", document, id);
        }
    }

    public static WS.Response doSearch(String query, Optional<String> document) {
        final String url = !document.isPresent()
                ? String.format("%s/search/_search", ApplicationParameter.EC2.esUrl)  //auto complete
                : String.format("%s/search/%s/_search", ApplicationParameter.EC2.esUrl, document.get()); //search

        return WS.url(url).post(query).get();
    }
}
