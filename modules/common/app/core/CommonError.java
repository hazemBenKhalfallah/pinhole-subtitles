package core;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author ayassinov
 */
public class CommonError {

    private final static Map<Integer, String> EXCEPTIONS = new ImmutableMap.Builder<Integer, String>()
            .put(Codes.INVALID_JSON_TEXT, "error when parsing the request body as json object")
            .put(Codes.UPDATE_FRIENDS_STATUS_EXCEPTION, "Error while changing status of invited friends in database")
            .put(Codes.LIST_FRIENDS_EXCEPTION, "Error while getting facebook friends")
            .put(Codes.SAVE_SESSION_EXCEPTION, "Exception when saving the new session")
            .put(Codes.FACEBOOK_COMMUNICATION_EXCEPTION, "Communication exception with facebook")
            .put(Codes.TWITTER_COMMUNICATION_EXCEPTION, "Communication exception with twitter")
            .put(Codes.SAVE_NEW_ACCOUNT_EXCEPTION, "Exception on creation the new user account")
            .put(Codes.NO_SOCIAL_ACCOUNT, "social account was not found in database")
            .put(Codes.SENDING_EMAIL_EXCEPTION, "Communication error on email sending")
            .put(Codes.PASSWORD_RECOVERING_EXCEPTION, "Exception on recovering password")
            .put(Codes.REQUIRED_VALUE, "value is absent")
            .put(Codes.NOT_VALID_VALUE, "value is not valid")
            .put(Codes.NOT_VALID_PASSWORD_PATTERN, "password pattern is not respected")
            .put(Codes.INVALID_PASSWORD_CONFIRMATION, "password and confirmation are different")
            .put(Codes.NO_USER_FOUND, "no user with this email")
            .put(Codes.USER_WITH_NO_PASSWORD, "user don't have a password")
            .put(Codes.INVALID_CREDENTIALS, "email and password don't match")
            .put(Codes.EMAIL_SIGN_IN_BLOCKED, "you sign up with a social account you can't sign in with email now !")
            .put(Codes.USER_NOT_FOUND_IN_FACEBOOK, "user was not found in facebook !")
            .put(Codes.USER_NOT_FOUND_IN_TWITTER, "user was not found in twitter !")
            .put(Codes.NOT_VALID_FB_ID, "user id in the request is not valid !")
            .put(Codes.NOT_VALID_TWITTER_ID, "user id in the request is not valid !")
            .put(Codes.NOT_AVAILABLE_EMAIL, "email is not available")
            .put(Codes.INVALID_OLD_PASSWORD, "old password is not valid")
            .put(Codes.EXPIRED_TOKEN, "password token expired")
            .put(Codes.NOT_FOUND_TOKEN, "password token not found")
            .put(Codes.USER_NOT_MEMBER, "user is not a member")
            .put(Codes.LOGIN_TYPE_IS_NOT_EMAIL, "login type is not an email, user can't do this action")
            .put(Codes.AT_LEAST_ONE_SET, "At least one field should be set should be set")
            .put(Codes.USER_IS_MEMBER_WITH_SOCIAL_ACCOUNT, "user is a member with social account and cant be sign up with email. He can link his account instead")
            .put(Codes.USER_USER_CAN_LINK_SOCIAL_ACCOUNT, "user can link his email account with his social account.")
            .put(Codes.SUBSCRIPTION_EXCEPTION, "an error on saving or updating user subscription.")
            .put(Codes.BLOCK_PINHOLE_FRIEND_EXCEPTION, "Exception while blocking Pinhole friends.")
            .put(Codes.ALREADY_EXISTS, "The data already exist")
            .build();

    public static String getMessage(int code) {
        return EXCEPTIONS.get(code);
    }

    public static class Codes {
        //generic code begins from 600
        public static final int INVALID_JSON_TEXT = 600;
        public static final int REQUIRED_VALUE = 601;
        public static final int NOT_VALID_VALUE = 603;
        public static final int NOT_VALID_PASSWORD_PATTERN = 605;
        public static final int NOT_FOUND = 606;
        public static final int EXCEPTION = 607;
        public static final int ALREADY_EXISTS = 608;

        //facabook
        public static final int FACEBOOK_COMMUNICATION_EXCEPTION = 700;
        public static final int TWITTER_COMMUNICATION_EXCEPTION = 705;


        //friends
        public static final int UPDATE_FRIENDS_STATUS_EXCEPTION = 710;
        public static final int LIST_FRIENDS_EXCEPTION = 711;
        public static final int NO_SOCIAL_ACCOUNT = 712;
        public static final int BLOCK_PINHOLE_FRIEND_EXCEPTION = 713;

        //recover account / change password
        public static final int SENDING_EMAIL_EXCEPTION = 720;
        public static final int PASSWORD_RECOVERING_EXCEPTION = 721;
        public static final int INVALID_PASSWORD_CONFIRMATION = 722;
        public static final int INVALID_OLD_PASSWORD = 723;
        public static final int EXPIRED_TOKEN = 724;
        public static final int NOT_FOUND_TOKEN = 725;
        public static final int USER_NOT_MEMBER = 726;
        public static final int LOGIN_TYPE_IS_NOT_EMAIL = 727;

        //sign in / sign up
        public static final int NO_USER_FOUND = 730;
        public static final int USER_WITH_NO_PASSWORD = 731;
        public static final int INVALID_CREDENTIALS = 732;
        public static final int EMAIL_SIGN_IN_BLOCKED = 733;
        public static final int USER_NOT_FOUND_IN_FACEBOOK = 734;
        public static final int NOT_VALID_FB_ID = 735;
        public static final int NOT_AVAILABLE_EMAIL = 736;
        public static final int SAVE_NEW_ACCOUNT_EXCEPTION = 737;
        public static final int SAVE_SESSION_EXCEPTION = 738;
        public static final int USER_IS_MEMBER_WITH_SOCIAL_ACCOUNT = 739;
        public static final int USER_USER_CAN_LINK_SOCIAL_ACCOUNT = 740;

        public static final int USER_NOT_FOUND_IN_TWITTER = 41;
        public static final int NOT_VALID_TWITTER_ID = 742;

        public static final int SUBSCRIPTION_EXCEPTION = 750;

        //comments
        public static final int LIST_COMMENTS_EXCEPTION = 760;
        public static final int SAVE_COMMENTS_EXCEPTION = 761;
        public static final int LIKE_COMMENTS_EXCEPTION = 762;
        public static final int REPORT_COMMENTS_EXCEPTION = 763;
        public static final int DELETE_COMMENTS_EXCEPTION = 764;
        public static final int COMMENT_CANNOT_BE_DELETED = 765;
        public static final int AT_LEAST_ONE_SET = 766;

        //Polls
        public static final int USER_ALREADY_VOTED = 770;
        public static final int TOO_MANY_CHOICES_SELECTED = 771;

        //Slides
        public static final int TOO_MANY = 781;
        public static final int NOT_ENOUGH = 782;
    }


}
