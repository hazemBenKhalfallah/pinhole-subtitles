/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package core;

import com.google.common.base.Optional;
import core.parameter.ApplicationParameter;
import play.libs.Json;

import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Util to manipulate strings
 *
 * @author ayassinov
 */
public class StringUtils {

    private final static String ARABIC_PATTERN = "\\u0600-\\u06FF\\u0750-\\u077F\\uFB50-\\uFDFF\\uFE70-\\uFEFF";
    private final static Charset CHARSET = Charset.forName("UTF-8");

    final static Pattern REGEX_EMAIL = Pattern.compile("\\b[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*\\b");
    public final static Pattern REGEX_PASSWORD = Pattern.compile("^(?=.*[a-zA-Z])(?=.*[0-9])\\S{6,}$");

    public static Boolean hasLatinLetters(String value) {
        Pattern patter = Pattern.compile("[\\pL&&\\p{L1}]");
        final Matcher arabicLettersMatcher = patter.matcher(value);
        return arabicLettersMatcher.find();
    }

    public static Boolean isRtl(String value) {
        if (StringUtils.isNullOrEmpty(value))
            return false;
        if (hasLatinLetters(value))
            return false;

        return hasArabicLetters(value);


    }

    /**
     * remove # from the string if it exists
     *
     * @param tag the tag to format
     * @return a text without #
     */
    public static String formatTwitterTag(String tag) {
        if (StringUtils.isNullOrEmpty(tag))
            return "";
        tag = tag.trim();
        if (tag.startsWith("#"))
            tag = tag.replaceFirst("#", "");
        return tag;
    }

    /**
     * Cette methode permet de remplacer tous les characteres spéciaux par leurs correspondents dans l alphabet
     * arabe ou latin ou par un '-'
     *
     * @param value string value to be simplified
     * @return simplified string
     */
    public static String simplifyString(String value) {
        if (value == null) {
            return null;
        }

        if (value.trim().isEmpty()) {
            return "";
        }

        value = value.toLowerCase()
                .replaceAll("[éèêë]", "e")   //replace 'e' based letters
                .replaceAll("[àâä]", "a")    //replace 'a' based letters
                .replaceAll("[ùûü]", "u")    //replace 'u' based letters
                .replaceAll("[ïî]", "i")     //replace 'i' based letters
                .replaceAll("[ôö]", "o")     //replace 'o' based letters
                .replaceAll("[ŷÿ]", "y")     //replace 'y' based letters
                .replaceAll("[ç]", "c")      //replace 'c' based letters
                .replaceAll("[^a-zA-Z0-9" + ARABIC_PATTERN + "]", "-")
                .replaceAll("[\\-]+", "-");

        if (value.startsWith("-")) {
            value = value.substring(1, value.length());
        }

        if (value.endsWith("-")) {
            value = value.substring(0, value.length() - 1);
        }

        return value;
    }

    /**
     * Verify if the given value has arabic letters.
     *
     * @param value value to be verified
     * @return true if the value has arabic letters
     */
    public static Boolean hasArabicLetters(String value) {
        final Pattern arabicLettersPattern = Pattern.compile("[" + ARABIC_PATTERN + "]",
                Pattern.UNICODE_CASE | Pattern.CANON_EQ | Pattern.CASE_INSENSITIVE);
        final Matcher arabicLettersMatcher = arabicLettersPattern.matcher(value);
        return arabicLettersMatcher.find();
    }

    /**
     * Check if a string is null or empty
     *
     * @param value the string value to verify
     * @return true if the string value is null or empty
     */
    public static boolean isNullOrEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }

    public static boolean isPasswordComplex(String password) {
        return !StringUtils.isNullOrEmpty(password) && REGEX_PASSWORD.matcher(password).matches();
    }

    public static boolean isEmailValid(String email) {
        return REGEX_EMAIL.matcher(email).matches();
    }

    /**
     * converts a list of primitives or java wrappers to an sql array
     *
     * @param ids list of primitives or java wrappers
     * @return string containing representing an array String
     */
    public static String toSqlArray(Collection ids) {
        if (ids == null || ids.isEmpty()) return "";
        final String jsonString = Json.toJson(ids).toString().replace("\"", "'"); // replace " by '
        final StringBuffer sb = new StringBuffer(jsonString);
        sb.deleteCharAt(0).deleteCharAt(sb.length() - 1);     //remove [ and ]
        return "(" + sb.toString() + ")";
    }

    public static String convertListToString(List l) {
        if (l == null || l.isEmpty())
            return "";

        final StringBuilder sb = new StringBuilder();
        for (Object o : l) {
            sb.append("\"").append(o.toString()).append("\"").append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    /**
     * Check if the user agent match any search bot from the config file.
     *
     * @param userAgent the user agent name
     * @return true if the user agent match any search bot that we support
     */
    public static boolean isSearchBot(final String userAgent) {
        return !StringUtils.isNullOrEmpty(userAgent) && ApplicationParameter.SEO.pattern.matcher(userAgent).matches();
    }

    /**
     * Check if user-agent match the pinhole bot name
     *
     * @param userAgent the user agent name
     * @return true if the user agent match the pinhole bot name
     */
    public static boolean isPinholeBot(final String userAgent) {
        return !StringUtils.isNullOrEmpty(userAgent) && ApplicationParameter.SEO.pinholeName.compareToIgnoreCase(userAgent) == 0;
    }

    /**
     * Check if the path is to the admin module or the box module.
     * Generally we test if the path begin with /admin or /box
     *
     * @param path a string representing the path of the route
     * @return true if the path goes to the admin or box module
     */
    public static boolean isPathAdminApp(String path) {
        if (isNullOrEmpty(path))
            return false;
        path = path.toLowerCase();
        return path.startsWith("/admin") || path.startsWith("/box") ||
                path.contains("/css") || path.contains("/stylesheets") ||
                path.contains("/font") || path.contains("/fonts");
    }

    public static boolean isPathAdsApp(String path) {
        if (isNullOrEmpty(path))
            return false;
        path = path.toLowerCase();
        return path.startsWith("/ads") || path.contains("/css") ||
                path.contains("/stylesheets") || path.contains("/font") ||
                path.contains("/fonts");
    }

    /**
     * Get a page Id to use it in SEO fetching snapshots from the current url path
     * for the / it will return 'index'
     * for the /:vanityUrl it will return 'vanityUrl'
     * for the /:vanityUrl/:videoId it will return 'videoId'
     *
     * @param path the path of the url
     * @return return the page id if the path value is not null and supported
     */
    public static Optional<String> getPageIdFromPath(String path) {
        //path = path.toLowerCase();
        if (path.compareToIgnoreCase("/") == 0) {
            return Optional.of("index");
        }

        //event and channel vanity urls or video id  /:vanityUrl
        String[] pathParts = path.split("/");
        if (pathParts.length > 0) {
            final StringBuilder stringBuilder = new StringBuilder(pathParts[pathParts.length - 1]);
            if (stringBuilder.indexOf("?") > 0)
                stringBuilder.delete(stringBuilder.indexOf("?"), stringBuilder.length());
            return Optional.of(stringBuilder.toString());
        }

        //none found
        return Optional.absent();
    }

    public static String shortenText(String text, int maxLength) {
        final int length = text.trim().length();
        final String t = text.trim();
        return length <= maxLength ? t : t.substring(0, maxLength - 3).concat("...");
    }

    public static boolean isNumber(String text) {
        return text.matches("[0-9]+");
    }
}
