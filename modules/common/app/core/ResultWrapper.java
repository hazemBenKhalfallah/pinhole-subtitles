/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package core;

import play.libs.F;
import play.mvc.SimpleResult;

/**
 * @author : hazem
 */
public class ResultWrapper {

    public static F.Promise<SimpleResult> promise(final SimpleResult simpleResult) {
        return F.Promise.promise(
                new F.Function0<SimpleResult>() {
                    @Override
                    public SimpleResult apply() {
                        return simpleResult;
                    }
                }
        );
    }
}
