/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package core;

import core.log.Log;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Utility to hashPassword passwords
 *
 * @author ayassinov
 */
public class HashUtils {

    public static String hashPassword(String text) {
        return DigestUtils.sha256Hex(text);
    }

    public static String hashUID(String text) {
        return DigestUtils.sha1Hex(text);
    }

    public static String getFileCheckSum(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            return DigestUtils.md5Hex(fis);
        } catch (IOException ex) {
            Log.error(Log.Type.UTILS, ex, "Exception on getting a checksum from a file");
            return "";
        }
    }

    /**
     * to rebuild the file name form the given url
     *
     * @param md5 :
     * @return :
     */
    public static String MD5(String md5) {
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(md5.getBytes(Charset.forName("UTF8")));
            final byte[] resultByte = messageDigest.digest();
            return new String(Hex.encodeHex(resultByte));
        } catch (NoSuchAlgorithmException ex) {
            Log.error(Log.Type.UTILS, ex, "Could not find md5 algorithm to digest the message=%s", md5);
        }
        return null;
    }

}
