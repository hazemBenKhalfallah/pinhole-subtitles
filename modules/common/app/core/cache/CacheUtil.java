package core.cache;

import com.google.common.base.Optional;
import core.StringUtils;
import core.log.Log;
import core.parameter.ApplicationParameter;
import org.apache.commons.codec.binary.Base64;
import play.Play;
import play.cache.Cache;
import play.libs.Akka;
import play.libs.Crypto;
import play.libs.Json;
import play.libs.WS;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * *
 *
 * @author ayassinov
 */
public class CacheUtil {

    private static final String EVICT_CACHE_URL = "/app/cache/evict";

    @SuppressWarnings("unchecked")
    public static <T> Optional<T> get(final String key) {
        if (StringUtils.isNullOrEmpty(key))
            return Optional.absent();

        final Object object = Cache.get(key);
        if (object != null)
            return Optional.of((T) object);
        return Optional.absent();
    }


    public static <T> void cache(final String key, final T tobject, final int seconds) {
        Akka.future(new Callable<Object>() {  //make asynchronous call
            public Object call() {
                Cache.set(key, tobject, seconds);
                return null;
            }
        });
    }

    //@Deprecated
    public static void removeFromCache(final String key) {
        Akka.future(new Callable<Integer>() {  //make asynchronous call
            public Integer call() {
                Cache.remove(key);
                return 0;
            }
        });
    }

    /**
     * evicts cache for frontend app
     *
     * @param keys cache keys
     */
    public static void evict(String... keys) {
        if (keys == null || keys.length == 0)
            return;

        final CacheKeys cacheKeys = new CacheKeys();
        cacheKeys.add(keys);

        try {
            final WS.WSRequestHolder holder = WS.url(String.format("%s%s", ApplicationParameter.APP.host, EVICT_CACHE_URL))
                    .setHeader("signature", Crypto.sign(StringUtils.toSqlArray(cacheKeys.keys)));

            if (Play.isProd() && !ApplicationParameter.APP.isProdHost) {
                final String auth = String.format("%s:%s", ApplicationParameter.APP.userName, ApplicationParameter.APP.password);
                holder.setHeader("Authorization", String.format("Basic %s", new Base64().encodeAsString(auth.getBytes("UTF-8"))));
            }

            final WS.Response response = holder
                    .post(Json.toJson(cacheKeys))
                    .get();
            Log.info(Log.Type.CACHE, "Evict request for object with key=%s responded with=%s", StringUtils.toSqlArray(cacheKeys.keys), response.getStatus());
        } catch (Exception ex) {
            Log.error(Log.Type.CACHE, ex, "Error while evicting cache with key=%s", StringUtils.toSqlArray(cacheKeys.keys));
        }
    }

    /**
     * wrapper for the internal cache plugin, without future as this will be called from an actor !
     *
     * @param key the key of the cache element
     */
    public static void remove(String key) {
        Cache.remove(key);
    }

    public static class Keys {
        //Video
        public static final String NEW_EPISODES = "NEW_EPISODES";
        public static final String VIDEO_WATCHED_BY = "VIDEO_WATCHED_BY";

        //Category
        public static final String FEATURED_CATEGORIES = "FEATURED_CATEGORIES";
        public static final String SUBCATEGORIES = "SUBCATEGORIES_%s_%s";
        public static final String IN_BROWSE_CATEGORIES = "IN_BROWSE_CATEGORIES_";
        public static final String CATEGORIES_LIST = "CATEGORIES_LIST_";
        public static final String POPULAR_CATEGORY = "POPULAR_CATEGORY_";

        //Event
        public static final String FEATURED_SHOWS = "FEATURED_SHOWS";
        public static final String POPULAR_SHOWS = "POPULAR_SHOWS_";
        public static final String RELATED_EVENTS = "RELATED_EVENTS_";
        public static final String SLIDES = "SLIDES";

        //Channel
        public static final String FEATURED_CHANNEL = "FEATURED_CHANNEL";
        public static final String POPULAR_CHANNEL = "POPULAR_CHANNEL_";
        public static final String CHANNELS = "CHANNELS";
        public static final String IN_BROWSE_CHANNELS = "IN_BROWSE_CHANNELS";
        public static final String SIMILAR_CHANNELS = "SIMILAR_CHANNELS_";

        //Parameter
        public static final String CACHE_PARAM = "PARAM_%s_%s";
        public static final String CACHE_PARAM_SECTION = "PARAM_SECTION_";
        public static final String CACHE_ALL_PARAMS = "PARAM_ALL";
        public static final String PARAM_STARTUP = "PARAM_STARTUP";

        //User
        public static final String USER_SEARCH_BOT = "USER_SEARCH_BOT";
        public static final String USER_SUBSCRIPTION = "USER_SUBSCRIPTION_";
        public static final String USER_NEXT_VIDEO_WATCH = "USER_NEXT_VIDEO_WATCH_";
        public static final String USER_NEXT_VIDEO_SUGGEST = "USER_NEXT_VIDEO_SUGGEST_";

        //Other
        public static final String SEARCH = "SEARCH_";
        public static final String AFFILIATE_INVITATION = "AFF_INVITATION";
        public static final String CONTENT_ITEM = "CONTENT_ITEM_%s";
        public static final String VANITY_URL = "VANITY_URL";
        public static final String i18_MESSAGE = "i18_MESSAGE_";
        public static final String PLANNING_BY_SYSTEM = "PLAN_BY_SYS_";
    }

    public static class Period {
        public static final int ETERNITY = 0; //0 means eternity
        public static final int VERY_LONG = 7200; //2h
        public static final int LONG = 3600; //1h
        public static final int MEDIUM = 900; //15mn
        public static final int SHORT = 180; //3mn
    }

}


class CacheKeys {
    public List<String> keys = new ArrayList<String>();

    public CacheKeys add(String... keys) {
        if (keys == null || keys.length == 0)
            return this;

        int i = 0;
        while (i < keys.length) {
            this.keys.add(keys[i++]);
        }
        return this;
    }
}

