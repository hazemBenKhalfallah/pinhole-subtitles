package core.email.builder;

import core.email.model.Message;
import core.email.model.MessageTemplate;
import core.url.UrlBuilder;
import models.user.User;

/**
 * Hold all email template string information
 */
public class EmailTemplate {

    public static class SignUp {
        public static final String TEMPLATE_NAME = "signup";
        public static final String TEMPLATE_VAR_NAME = "FNAME";
        public static final String RECIPIENT_METADATA_ID = "user_id";

        public static MessageTemplate getMessage(final User user) {
            final MessageTemplate signUpMessageTemplate = new MessageTemplate(TEMPLATE_NAME);
            signUpMessageTemplate.set(RecipientBuilder.create(user.name, user.email)
                    .addMergeVar(TEMPLATE_VAR_NAME, user.name)
                    .addMetadata(RECIPIENT_METADATA_ID, user.id.toString())
            );
            return signUpMessageTemplate;
        }
    }

    public static class AccountRecover {

        public static MessageTemplate getRecoverMessage(final User user, final String token) {
            final MessageTemplate signUpMessageTemplate = new MessageTemplate("account_recover");
            signUpMessageTemplate.set(RecipientBuilder.create(user.name, user.email)
                    .addMergeVar("FRECOVERURL", UrlBuilder.getAccountRecoverURL(user.uid, token))
                    .addMetadata("user_id", user.id.toString())
            );
            return signUpMessageTemplate;
        }

        public static Message getPasswordUpdatedMessage(final User user) {
            final MessageTemplate signUpMessageTemplate = new MessageTemplate("password_updated");
            signUpMessageTemplate.set(RecipientBuilder.create(user.name, user.email)
                    .addMetadata("user_id", user.id.toString())
            );
            return signUpMessageTemplate;
        }
    }
}

