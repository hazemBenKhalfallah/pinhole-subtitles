package core.email.builder;

import com.google.common.base.Optional;
import core.email.model.MergeVar;
import core.email.model.Recipient;
import core.email.model.RecipientMetadata;

/**
 * Convenient class to create a recipient with all specific information
 *
 * @author ayassinov
 */
public class RecipientBuilder {

    private Recipient recipient;
    private MergeVar mergeVar;
    private RecipientMetadata recipientMetadata;


    public static RecipientBuilder create(String name, String email) {
        return new RecipientBuilder(name, email);
    }

    private RecipientBuilder(String name, String email) {
        this.recipient = new Recipient(name, email);
        this.recipientMetadata = new RecipientMetadata(email);
        this.mergeVar = new MergeVar(email);
    }

    public RecipientBuilder addMergeVar(String key, String value) {
        this.mergeVar.addVar(key, value);
        return this;
    }

    public RecipientBuilder addMetadata(String key, String value) {
        this.recipientMetadata.addMetaData(key, value);
        return this;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public Optional<MergeVar> getMergeVar() {
        if (mergeVar == null || mergeVar.getVars().size() == 0)
            Optional.absent();
        return Optional.of(mergeVar);
    }

    public Optional<RecipientMetadata> getRecipientMetadata() {
        if (recipientMetadata == null || recipientMetadata.getValues().size() == 0)
            Optional.absent();
        return Optional.of(recipientMetadata);
    }
}
