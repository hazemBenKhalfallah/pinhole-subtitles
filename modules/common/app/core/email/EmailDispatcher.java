package core.email;

import core.email.mandrill.MandrillMessageSender;
import core.email.model.Message;
import core.email.model.MessageTemplate;

/**
 * @author ayassinov
 */
public class EmailDispatcher {

    private static final MessageSender sender;

    static {
        sender = new MandrillMessageSender(); //mandrill is default implementation
    }

    private EmailDispatcher() {
    }

    public static boolean send(Message message) {
        try {
            MessageResponse response;
            if (message instanceof MessageTemplate) {
                response = sender.send((MessageTemplate) message);
            } else {
                response = sender.send(message);
            }

            return response.isSuccessful(message.getTo().get(0).getEmail());
        } catch (Exception ex) {
            return false;
        }
    }
}
