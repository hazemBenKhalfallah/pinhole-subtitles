package core.email.smtp;

import com.typesafe.plugin.MailerAPI;
import com.typesafe.plugin.MailerPlugin;
import core.email.MessageRequest;
import core.email.MessageResponse;
import core.email.MessageSender;
import core.email.model.Message;
import core.email.model.MessageTemplate;
import core.log.Log;

/**
 *
 */
public class SMTPMessageSender implements MessageSender {
    private static final MailerAPI mail = play.Play.application().plugin(MailerPlugin.class).email();

    @Override
    public MessageResponse send(Message message) {
        try {
            mail.setSubject(message.getSubject());
            mail.addRecipient(message.getFormattedRecipients());
            mail.addFrom(message.getFormattedSender());
            mail.send(message.getText(), message.getHtml());
            onSuccess(null, null);
            return new MessageResponse() {
                @Override
                public boolean isSuccessful(String email) {
                    return true;
                }
            };
        } catch (Exception ex) {
            Log.error(Log.Type.EMAIL, ex, "Exception when sending an email to=%s", message.getFormattedRecipients().toString());
            onFailure(null, null);
            return new MessageResponse() {
                @Override
                public boolean isSuccessful(String email) {
                    return false;
                }
            };
        }
    }

    @Override
    public MessageResponse send(MessageTemplate message) {
        return new MessageResponse() {
            @Override
            public boolean isSuccessful(String email) {
                return false;
            }
        };
    }

    @Override
    public void onSuccess(MessageRequest request, MessageResponse response) {
    }

    @Override
    public void onFailure(MessageRequest request, MessageResponse response) {
    }
}
