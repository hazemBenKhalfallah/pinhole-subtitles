package core.email.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MergeVar implements Serializable {

    @JsonProperty(value = "rcpt")
    private String email;
    private List<TemplateContent> vars = new ArrayList<TemplateContent>();

    public MergeVar(String email) {
        this.email = email;
    }

    public MergeVar(String email, TemplateContent... content) {
        this.email = email;
        this.vars = Arrays.asList(content);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<TemplateContent> getVars() {
        return vars;
    }

    public void setVars(List<TemplateContent> vars) {
        this.vars = vars;
    }

    public MergeVar addVar(String name, String value) {
        this.vars.add(new TemplateContent(name, value));
        return this;
    }
}
