package core.email.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Optional;
import core.StringUtils;
import core.email.builder.RecipientBuilder;
import core.parameter.ApplicationParameter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * base message object
 */
public class Message implements Serializable {

    private String html;
    private String text;
    private String subject;
    @JsonProperty(value = "from_email")
    private String fromEmail;
    @JsonProperty(value = "from_name")
    private String fromName;
    @JsonProperty(value = "preserve_recipients")
    private boolean preserveRecipients = false;
    private List<Recipient> to = new ArrayList<Recipient>();
    private Map<String, String> headers = new HashMap<String, String>();
    private List<String> tags = new ArrayList<String>();
    @JsonProperty(value = "global_merge_vars")
    private List<TemplateContent> globalMergeVars = new ArrayList<TemplateContent>();
    @JsonProperty(value = "merge_vars")
    private List<MergeVar> mergeVars = new ArrayList<MergeVar>();
    private Map<String, String> metadata = new HashMap<String, String>();
    @JsonProperty(value = "recipient_metadata")
    private List<RecipientMetadata> recipientsMetadata = new ArrayList<RecipientMetadata>();

    public Message() {
    }

    /**
     * Create a message with Html and a Subject, defaults sender email address will be used
     *
     * @param html    the full HTML content to be sent
     * @param subject the message subject
     */
    public Message(String html, String subject) {
        this(subject, null, null, html, null);
    }

    /**
     * Create a new message
     *
     * @param subject   the message subject
     * @param fromEmail the sender email address.
     * @param fromName  optional from name to be used
     * @param html      the full HTML content to be sent
     * @param text      optional full text content to be sent
     */
    public Message(String subject, String fromEmail, String fromName, String html, String text) {
        this.subject = subject;
        this.fromEmail = StringUtils.isNullOrEmpty(fromEmail) ? ApplicationParameter.EMAIL.senderEmail : fromEmail;
        this.fromName = StringUtils.isNullOrEmpty(fromName) ? ApplicationParameter.EMAIL.senderName : fromName;
        this.html = html;
        this.text = text;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public boolean isPreserveRecipients() {
        return preserveRecipients;
    }

    public void setPreserveRecipients(boolean preserveRecipients) {
        this.preserveRecipients = preserveRecipients;
    }

    public List<Recipient> getTo() {
        return to;
    }

    public void setTo(List<Recipient> to) {
        this.to = to;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<TemplateContent> getGlobalMergeVars() {
        return globalMergeVars;
    }

    public void setGlobalMergeVars(List<TemplateContent> globalMergeVars) {
        this.globalMergeVars = globalMergeVars;
    }

    public List<MergeVar> getMergeVars() {
        return mergeVars;
    }

    public void setMergeVars(List<MergeVar> mergeVars) {
        this.mergeVars = mergeVars;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    public List<RecipientMetadata> getRecipientsMetadata() {
        return recipientsMetadata;
    }

    public void setRecipientsMetadata(List<RecipientMetadata> recipientsMetadata) {
        this.recipientsMetadata = recipientsMetadata;
    }

    public Optional<RecipientMetadata> getRecipientsMetadata(String email) {
        for (final RecipientMetadata recipientMetadata : recipientsMetadata) {
            if (recipientMetadata.getEmail().equals(email))
                return Optional.of(recipientMetadata);
        }
        return Optional.absent();
    }

    /**
     * Add a reply to email
     *
     * @param replyToEmail the email to reply to
     */
    public void addReplyToHeader(String replyToEmail) {
        headers.put("Reply-To", replyToEmail);
    }

    /**
     * Add to the recipient list
     *
     * @param name  the recipient name
     * @param email the recipient email
     */
    public void addRecipient(String name, String email) {
        to.add(new Recipient(name, email));
    }

    /**
     * Add to the recipient list
     *
     * @param email the recipient email
     */
    public void addRecipient(String email) {
        to.add(new Recipient(email));
    }

    public void addTag(String tag) {
        this.tags.add(tag);
    }

    public void addGlobalMergeVar(String name, String value) {
        this.globalMergeVars.add(new TemplateContent(name, value));
    }

    public void addMergeVar(MergeVar mergeVar) {
        if (mergeVar != null)
            this.mergeVars.add(mergeVar);
    }

    public void addMetadata(String key, String value) {
        this.metadata.put(key, value);
    }

    public void addRecipientMetadata(RecipientMetadata recipientMetadata) {
        if (recipientMetadata != null)
            this.recipientsMetadata.add(recipientMetadata);
    }

    public Message set(RecipientBuilder recipientBuilder) {
        this.to.add(recipientBuilder.getRecipient());
        this.addMergeVar(recipientBuilder.getMergeVar().orNull());
        this.addRecipientMetadata(recipientBuilder.getRecipientMetadata().orNull());
        return this;
    }

    @JsonIgnore
    public String[] getFormattedRecipients() {
        final String[] result = new String[to.size()];
        int index = 0;
        for (final Recipient recipient : to) {
            result[index++] = recipient.toString();
        }
        return result;
    }

    @JsonIgnore
    public String getFormattedSender() {
        return StringUtils.isNullOrEmpty(fromName) ? fromEmail : fromName + "<" + fromEmail + ">";
    }
}
