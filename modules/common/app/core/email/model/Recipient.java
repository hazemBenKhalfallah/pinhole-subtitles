package core.email.model;

import core.StringUtils;

import java.io.Serializable;

/**
 * The message recipient
 */
public class Recipient implements Serializable {

    public static final String TYPE_TO = "to";
    public static final String TYPE_CC = "cc";
    public static final String TYPE_BCC = "bcc";

    private String name;
    private String email;
    private String type;


    /**
     * Create recipient's information.
     *
     * @param name  the optional display name to use for the recipient
     * @param email the email address of the recipient required
     * @param type  the header type to use for the recipient, defaults to "to" if not provided
     */
    public Recipient(String name, String email, String type) {
        this.name = name;
        this.email = email;
        this.type = StringUtils.isNullOrEmpty(type) ? TYPE_TO : type;
    }

    /**
     * Create recipient's information with the name and the email
     *
     * @param name  the optional display name to use for the recipient
     * @param email the email address of the recipient required
     */
    public Recipient(String name, String email) {
        this(name, email, null);
    }

    /**
     * Create recipient's information with only the email
     *
     * @param email the email address of the recipient required
     */
    public Recipient(String email) {
        this(null, email, null);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return StringUtils.isNullOrEmpty(name) ? email : name + "<" + email + ">";
    }
}
