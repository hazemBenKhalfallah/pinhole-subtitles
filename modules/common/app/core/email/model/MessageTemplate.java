package core.email.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ayassinov
 */
public class MessageTemplate extends Message {

    private String templateName;
    private List<TemplateContent> templateContents = new ArrayList<TemplateContent>();

    public MessageTemplate() {
    }

    public MessageTemplate(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public List<TemplateContent> getTemplateContents() {
        return templateContents;
    }

    public void setTemplateContents(List<TemplateContent> templateContents) {
        this.templateContents = templateContents;
    }

    /**
     * Add to the recipient list
     *
     * @param name  the tempalte section name
     * @param value the value of the template section
     */
    public void addTemplateContent(String name, String value) {
        templateContents.add(new TemplateContent(name, value));
    }
}