package core.email.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Meta for a specific recipient
 */
public class RecipientMetadata implements Serializable {

    private String email;
    private Map<String, String> values = new HashMap<String, String>();

    public RecipientMetadata(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, String> getValues() {
        return values;
    }

    public void setValues(Map<String, String> values) {
        this.values = values;
    }

    public RecipientMetadata addMetaData(String key, String value) {
        values.put(key, value);
        return this;
    }
}
