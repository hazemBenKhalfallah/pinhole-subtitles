package core.email;

import java.io.Serializable;

/**
 *
 */
public abstract class MessageResponse implements Serializable {

    public abstract boolean isSuccessful(String email);
}
