package core.email;

import core.email.model.Message;
import core.email.model.MessageTemplate;

/**
 * @author ayassinov
 */
public interface MessageSender {

    /**
     * Send a message
     *
     * @param message the message to send
     */
    MessageResponse send(Message message);

    /**
     * Send a message using a template
     *
     * @param message the message to send
     */
    MessageResponse send(MessageTemplate message);

    void onSuccess(final MessageRequest request, final MessageResponse response);

    void onFailure(final MessageRequest request, final MessageResponse response);
}
