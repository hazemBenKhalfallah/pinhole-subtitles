package core.email.mandrill;

import core.email.MessageRequest;
import core.email.MessageResponse;
import core.email.MessageSender;
import core.email.mandrill.request.MandrillMessageRequest;
import core.email.mandrill.request.MandrillMessageTemplateRequest;
import core.email.mandrill.request.MandrillRequest;
import core.email.mandrill.response.MandrillErrorResponse;
import core.email.mandrill.response.MandrillMessageResponse;
import core.email.mandrill.response.MandrillResponse;
import core.email.model.Message;
import core.email.model.MessageTemplate;
import core.log.Log;
import data.BaseEmailTrackingData;
import json.JsonSerializer;
import json.Response;
import models.email.EmailTracking;
import play.libs.WS;

/**
 *
 */
public class MandrillMessageSender implements MessageSender {

    @Override
    public MessageResponse send(Message message) {
        final MandrillMessageRequest request = new MandrillMessageRequest(message);
        return doJsonPostRequest("messages/send.json", request);
    }

    @Override
    public MessageResponse send(MessageTemplate message) {
        final MandrillMessageTemplateRequest request = new MandrillMessageTemplateRequest(message);
        return doJsonPostRequest("messages/send-template.json", request);
    }

    @Override
    public void onSuccess(final MessageRequest request, final MessageResponse response) {
        BaseEmailTrackingData.save(EmailTracking.from((MandrillMessageResponse) response, (MandrillMessageRequest) request));
    }

    @Override
    public void onFailure(final MessageRequest request, final MessageResponse response) {
        final MandrillErrorResponse errorResponse = (MandrillErrorResponse) response;
        //BaseEmailTrackingData.save(EmailTracking.from(errorResponse)); // only log
        Log.error(Log.Type.EMAIL, "Error on sending emails with cause %s and message %s", errorResponse.getName(), errorResponse.getMessage());
    }


    private MessageResponse doJsonPostRequest(final String url, final MandrillRequest request) {
        final WS.Response response = WS.url("https://mandrillapp.com/api/1.0/" + url)
                .setHeader("Content-Type", Response.JSON_RESPONSE_CHARSET)
                .post(JsonSerializer.serialize(request)).get();

        if ((response.getStatus() == 200)) {
            final MandrillMessageResponse mandrillMessageResponse = new MandrillMessageResponse();
            final String body = response.getBody();
            mandrillMessageResponse.setResponses(JsonSerializer.toListObject(body, MandrillResponse.class));
            mandrillMessageResponse.setActualResponse(body);
            Log.info(Log.Type.EMAIL, "%s emails sent with this details: \n\r %s", mandrillMessageResponse.getResponses().size(), mandrillMessageResponse.getActualResponse());
            onSuccess(request, mandrillMessageResponse);
            return mandrillMessageResponse;
        } else {
            final MandrillErrorResponse errorResponse = JsonSerializer.toObject(response.getBody(), MandrillErrorResponse.class);
            errorResponse.setRecipients(((MandrillMessageRequest) request).getMessage().getTo());
            onFailure(request, errorResponse);
            return errorResponse;
        }
    }
}
