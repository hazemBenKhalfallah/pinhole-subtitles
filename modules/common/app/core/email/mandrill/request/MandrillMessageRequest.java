package core.email.mandrill.request;

import core.email.model.Message;

/**
 * Request body to send an email
 */
public class MandrillMessageRequest extends MandrillRequest {

    private Message message;

    public MandrillMessageRequest(Message message) {
        this.message = message;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
