package core.email.mandrill.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import core.email.model.MessageTemplate;
import core.email.model.TemplateContent;

import java.util.ArrayList;
import java.util.List;

/**
 * Request body to send an email with a template
 */
public class MandrillMessageTemplateRequest extends MandrillMessageRequest {

    @JsonProperty(value = "template_name")
    private String templateName;
    @JsonProperty(value = "template_content")
    private List<TemplateContent> templateContents = new ArrayList<TemplateContent>();

    public MandrillMessageTemplateRequest(MessageTemplate messageTemplate) {
        super(messageTemplate);
        this.templateName = messageTemplate.getTemplateName();
        this.templateContents = messageTemplate.getTemplateContents();
    }

    @Override
    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public List<TemplateContent> getTemplateContents() {
        return templateContents;
    }

    public void setTemplateContents(List<TemplateContent> templateContents) {
        this.templateContents = templateContents;
    }
}
