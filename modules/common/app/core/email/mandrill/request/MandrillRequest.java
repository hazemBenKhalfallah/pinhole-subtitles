package core.email.mandrill.request;

import core.email.MessageRequest;
import core.parameter.ApplicationParameter;

/**
 */
public class MandrillRequest extends MessageRequest {

    private final String key;

    public MandrillRequest() {
        key = ApplicationParameter.EMAIL.mandrillKey;
    }

    public String getKey() {
        return key;
    }
}
