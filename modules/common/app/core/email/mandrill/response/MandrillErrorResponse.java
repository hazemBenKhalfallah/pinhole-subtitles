package core.email.mandrill.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import core.email.MessageResponse;
import core.email.model.Recipient;

import java.util.List;

/**
 */
@JsonIgnoreProperties
public class MandrillErrorResponse extends MessageResponse {

    private String status;
    private int code;
    private String name;
    private String message;
    private List<Recipient> recipients;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Recipient> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<Recipient> recipients) {
        this.recipients = recipients;
    }

    @Override
    @JsonIgnore
    public boolean isSuccessful(String email) {
        return false;
    }
}
