package core.email.mandrill.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import core.email.MessageResponse;

import java.util.List;

/**
 */
public class MandrillMessageResponse extends MessageResponse {

    private List<MandrillResponse> responses;
    private String actualResponse;

    public List<MandrillResponse> getResponses() {
        return responses;
    }

    public void setResponses(List<MandrillResponse> responses) {
        this.responses = responses;
    }

    @JsonIgnore
    public String getActualResponse() {
        return actualResponse;
    }

    public void setActualResponse(String actualResponse) {
        this.actualResponse = actualResponse;
    }

    @Override
    @JsonIgnore
    public boolean isSuccessful(String email) {
        for (final MandrillResponse response : responses) {
            if (response.getEmail().equalsIgnoreCase(email.trim())) {
                return response.getStatus().equalsIgnoreCase(MandrillResponse.STATUS_SENT);
            }
        }

        return false;
    }


}
