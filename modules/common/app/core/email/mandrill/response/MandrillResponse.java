package core.email.mandrill.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 */
public class MandrillResponse {

    public static final String STATUS_SENT = "sent";
    public static final String STATUS_QUEUED = "queued";
    public static final String STATUS_SCHEDULED = "scheduled";
    public static final String STATUS_REJECTED = "rejected";
    public static final String STATUS_INVALID = "invalid";

    private String status;
    private String email;

    @JsonProperty(value = "reject_reason")
    private String rejectReason;

    @JsonProperty(value = "_id")
    private String id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
