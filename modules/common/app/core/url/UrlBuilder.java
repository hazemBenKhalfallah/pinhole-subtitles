package core.url;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import com.google.common.base.Optional;
import core.StringUtils;
import core.log.Log;
import core.parameter.ApplicationParameter;
import models.media.ImageModel;
import models.media.MediaSize;
import play.Play;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ayassinov
 */
public class UrlBuilder {

    //vanity urls
    public static final String URL_TYPE_EVENT = "Event";
    public static final String URL_TYPE_CHANNEL = "Channel";


    public static final String SIZE_LARGE = "large";
    public static final String SIZE_NORMAL = "normal";
    public static final String SIZE_SMALL = "small";
    public static final String SIZE_MINI = "mini";

    public static final String JPG = "jpg";

    /**
     * Default url image that will be uploaded to the S3
     */
    public static String getEventDefaultImage(Long id) {
        return getImageUrl(id, UrlBuilder.SIZE_LARGE, MediaTypesEnum.EVENT);
    }

    /**
     * Image url for the different objects
     *
     * @param id   the identifier of the object
     * @param size the size of the image
     * @param type use the internal enum in UrlBuilder
     */
    public static String getImageUrl(Long id, String size, MediaTypesEnum type) {
        return String.format("%s/%s", ApplicationParameter.S3.url, getImageRelativeUrl(id, size, type));
    }

    public static String getAdsImageUrl(String relativePath) {
        return String.format("%s/%s", ApplicationParameter.S3.url, relativePath);
    }

    /**
     * Get a relative path the the image
     *
     * @param id   the identifier of the object
     * @param size the size of the image
     * @param type the type of the object
     * @return a relative path of the image hosted in s3
     */
    public static String getImageRelativeUrl(Long id, String size, MediaTypesEnum type) {
        if (type.equals(MediaTypesEnum.HERO)) {
            //    events/id/hero_size.jpg
            return String.format("%s/%s/hero_%s.%s", MediaTypesEnum.EVENT.getValue(), id, size, JPG);
        } else if (type.equals(MediaTypesEnum.EVENT)) {
            //http://amazon/events/id/id_size.jpg
            return String.format("%s/%s/%s_%s.%s", type.getValue(), id, id, size, JPG);
        } else if (type.equals(MediaTypesEnum.SPLASH)) {
            //http://amazon/events/id/id_size.jpg
            return String.format("extra/%s_splash.%s", id, JPG);
        } else {
            //http://amazon/categories/id_size.jpg
            return String.format("%s/%s_%s.%s", type.getValue(), id, size, JPG);
        }
    }

    /**
     * Get a specific size info using image type and size
     *
     * @param type the type of the image
     * @param size the size of the image
     */
    public static MediaSize getByObjectTypeAndSize(String type, String size) {
        final SqlRow row = Ebean.createSqlQuery("select * from pin_media_sizes where media_class = :mediaClass and media_label = :mediaLabel")
                .setParameter("mediaClass", type)
                .setParameter("mediaLabel", size)
                .findUnique();
        if (row == null || row.isEmpty())
            return null;

        return new MediaSize(row);
    }

    /**
     * List of sizes
     *
     * @param type the type of objects
     */
    public static List<MediaSize> listByObjectType(String type) {
        return MediaSize.from(Ebean.createSqlQuery("select * from pin_media_sizes where media_class = :mediaClass").setParameter("mediaClass", type).findList());
    }

    /**
     * List of images urls with infos
     *
     * @param id         the identifier object
     * @param objectType the object type
     */
    public static List<ImageModel> listImages(Long id, String objectType) {
        final List<MediaSize> sizes = listByObjectType(objectType);
        final ArrayList<ImageModel> result = new ArrayList<ImageModel>();
        for (final MediaSize mediaSize : sizes) {
            final ImageModel imageModel = new ImageModel();
            imageModel.url = UrlBuilder.getImageUrl(id, mediaSize.label, MediaTypesEnum.getType(objectType));
            imageModel.label = mediaSize.label;
            imageModel.height = mediaSize.height.toString();
            imageModel.width = mediaSize.width.toString();
            result.add(imageModel);
        }
        return result;
    }

    /**
     * Get the vanity url
     *
     * @param url     the original url
     * @param urlType url type
     * @return the original url
     */
    public static Optional<String> getActualUrl(final String url, String urlType) {
        if (StringUtils.isNullOrEmpty(url) || StringUtils.isNullOrEmpty(urlType))
            return Optional.absent();

        final String sql = "select v.van_url as original_url, r.van_url as new_url" +
                " from pin_vanity_url v " +
                " left join pin_vanity_url r on r.van_id = v.van_redirect" +
                " where v.van_url = :eventUrl and v.van_type = :urlType ";

        final SqlRow row = Ebean.createSqlQuery(sql)
                .setParameter("eventUrl", url.trim().toLowerCase())
                .setParameter("urlType", urlType)
                .findUnique();

        if (row == null || row.isEmpty()) {
            Log.warn(Log.Type.URL, "Event not found with url=%s", url);
            return Optional.absent();
        }

        if (row.getString("new_url") != null) { //redirect to the nex URL
            return Optional.of(row.getString("new_url"));
        }

        return Optional.fromNullable(row.getString("original_url"));
    }

    /**
     * check if the image exist
     *
     * @param id           the identifier of the object
     * @param imageUrlType image url type
     * @return true if exists
     */
    public static boolean isUrlImageExist(Long id, String imageUrlType) {
        final SqlRow row = Ebean.createSqlQuery("select ma.pin_media_id from pin_media_assets ma " +
                "left join pin_media_sizes ms on ms.media_type_id = ma.media_size " +
                "where ma.media_object_id = :mediaId " +
                "and ms.media_class = :mediaSize " +
                "limit 1")
                .setParameter("mediaId", id + "")
                .setParameter("mediaSize", imageUrlType)
                .findUnique();
        return !(row == null || row.isEmpty()) && row.getString("pin_media_id") != null;
    }

    /**
     * Get the full url to the html page for in s3 for SEO
     *
     * @param id the name of the page
     * @return the relative url to the html file generally '/snapshots/index.html'
     */
    public static String getRelativeHtmlFilePath(String id) {
        //http://pinholesocialtv.s3.amazon/snapshots/index.html
        return String.format("/%s/%s.html", ApplicationParameter.SEO.snapshotFolderName, id);
    }

    public static String getPrerenderUrl(String path) {
        //http://prerender.com/http://pinhole-host.com/path-to-page
        return ApplicationParameter.SEO.prerenderHost + "/" + ApplicationParameter.SEO.productionHost + path;
    }

    /**
     * Get the file path to the site map xml file. generally  <b>./public/sitemap.xml</b>
     *
     * @return the full file path to the site map
     */
    public static String getSiteMapFilePath() {
        return ApplicationParameter.SEO.publicFolder + "/sitemap.xml";
    }

    /**
     * @param urlPath the url path, should begin with '/' like <i>/fariha</i> or <i>/happy-ness/1252</i>
     * @return the host with the path given like   <i>http://www.pinhole.tn/happy-ness</i>
     */
    public static String getSiteMapElementUrl(String urlPath) {
        //http://www.pinhole.tn/any-path
        return ApplicationParameter.APP.host + urlPath;
    }

    /**
     * Get an link to recover the user account
     *
     * @param uid   the user uid
     * @param token the token generated for the recover
     * @return the full URL to the recover application route
     */
    public static String getAccountRecoverURL(String uid, String token) {
        return String.format("%s/recover/%s/%s", ApplicationParameter.APP.host, uid, token);
    }

    /**
     * Return the full url to the assets file
     *
     * @param relativePath the relative and file name like img/icon.jpg
     * @return the url to the file url
     */
    public static String getAssetsFileUrl(String relativePath) {
        if (Play.isDev()) { //for dev return assets from public folder
            return "/assets/" + relativePath;
        }

        if (relativePath.contains("css") || relativePath.contains("font"))
            return "/assets/" + relativePath;

        return String.format("%s/assets/%s/%s", ApplicationParameter.S3.securedUrl, ApplicationParameter.APP.version, relativePath);
    }

    /**
     * get url for facebook notification.
     * If number of episodes in the notification is more than one, return home page uri.
     * Else return video page uri
     *
     * @param episodesCount number of episode in the notification
     * @param extraId       extra id
     * @param vanityUrl     event url
     * @return notification uri
     */
    public static String getFaceBookNotificationUrl(int episodesCount, String vanityUrl, Long extraId) {
        if (episodesCount > 1)
            return "#";
        else
            return vanityUrl + "/" + extraId;
    }

    public static String getAdAssetsFileFullPath(String key, String extension) {
        final String defaultFolder = "ads/images";
        return String.format("%s/%s.%s", defaultFolder, key, extension);
    }
}
