package core.url;


import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author Zied
 *         Media types
 */
public enum MediaTypesEnum {

    EVENT("events"),
    CHANNEL("channels"),
    EXTRA("extras"),
    CATEGORY("categories"),
    HERO("hero"),
    SPLASH("splash"),
    UNKNOWN("");

    private String value;

    private static final Map<String, MediaTypesEnum> TYPES = ImmutableMap.<String, MediaTypesEnum>builder()
            .put(MediaTypesEnum.EVENT.value, MediaTypesEnum.EVENT)
            .put(MediaTypesEnum.CHANNEL.value, MediaTypesEnum.CHANNEL)
            .put(MediaTypesEnum.EXTRA.value, MediaTypesEnum.EXTRA)
            .put(MediaTypesEnum.CATEGORY.value, MediaTypesEnum.CATEGORY)
            .put(MediaTypesEnum.HERO.value, MediaTypesEnum.HERO)
            .put(MediaTypesEnum.SPLASH.value, MediaTypesEnum.SPLASH)
            .build();

    private MediaTypesEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static MediaTypesEnum getType(String value) {
        value = value.toLowerCase();
        if (TYPES.containsKey(value))
            return TYPES.get(value);
        else
            return UNKNOWN;
    }
}

