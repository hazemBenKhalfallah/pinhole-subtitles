/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package core;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.google.common.base.Strings;
import com.google.common.io.Files;
import core.log.Log;
import core.parameter.ApplicationParameter;
import json.Response;
import play.Logger;
import play.libs.F;
import play.libs.WS;
import play.mvc.SimpleResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static play.mvc.Results.notFound;

/***
 *
 */
public class FileUtils {

    public final static AmazonS3 amazonS3;

    static {
        amazonS3 = new AmazonS3Client(
                new BasicAWSCredentials(ApplicationParameter.S3.accessKey, ApplicationParameter.S3.secretKey)
        );
        amazonS3.setBucketAcl(ApplicationParameter.S3.bucketName, CannedAccessControlList.PublicRead);
    }

    public static boolean uploadFile(File file, String fileRelativePath) {
        try {
            return uploadFile(Files.newInputStreamSupplier(file).getInput(), fileRelativePath);
        } catch (IOException ex) {
            Logger.error("Exception on getting the file as stream", ex);
            return false;
        }
    }

    /**
     * @param stream           the file stream
     * @param fileRelativePath relative path with the file name in format myforlder/myfile.extentions
     * @return true if the file was uploaded
     */
    public static boolean uploadFile(FileInputStream stream, String fileRelativePath) {
        try {
            amazonS3.putObject(ApplicationParameter.S3.bucketName, fileRelativePath, stream, new ObjectMetadata()); // upload file
            return true;
        } catch (Exception ex) {
            Logger.error("S3_UPLOAD: error uploading file to s3", ex);
            return false;
        }
    }

    /**
     * Get a file from S3
     *
     * @param url the relative url to the file like '/sitemap.xml' or /snapshots/index.html
     * @return the future with the file if exists
     */
    public static F.Promise<SimpleResult> getFileFromS3AsResult(final String url, final SimpleResult defaultAction) {
        try {
            final F.Promise<WS.Response> response = WS.url(url).get();
            return response.map(new F.Function<WS.Response, SimpleResult>() {
                @Override
                public SimpleResult apply(WS.Response response) throws Throwable {
                    if (response.getStatus() == 200) {
                        return Response.with(response.getBody(),
                                Response.headers()
                                        .setAccessControl()
                                        .setContentType(response.getHeader(Response.Head.CONTENT_TYPE))
                                        .setEtag(response.getHeader(Response.Head.HEAD_ETAG))
                                        .setLastModified(response.getHeader(Response.Head.LAST_MODIFIED))
                                        .setDefaultCache()
                        );
                    }
                    Log.info(Log.Type.ASSETS, "File not found in url=%s", url);
                    return defaultAction;
                }
            });

        } catch (Exception ex) {
            Log.error(Log.Type.ASSETS, ex, "Exception on getting file from url=%s", url);
            return ResultWrapper.promise(defaultAction);
        }
    }

    public static F.Promise<SimpleResult> getFileFromS3AsResultOrNotFound(String url) {
        return getFileFromS3AsResult(url, notFound());
    }

    public static String getFileExtension(String fullFileName, String defaultExtension) {
        final String extension = Files.getFileExtension(fullFileName);
        return Strings.isNullOrEmpty(extension) ? defaultExtension : extension;
    }
}
