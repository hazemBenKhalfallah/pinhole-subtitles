package core.log;

import com.google.common.base.Optional;
import play.Logger;

/**
 * @author ayassinov
 */
public class Log {

    public enum Type {
        ADS, ACCOUNT, TRACKING, SEO, SEARCH, MONGO, CACHE, PARAMETER, BOOTSTRAP, SECURITY, VIDEO, EMAIL, COOKIE, UTILS, ASSETS, URL, NEXT_VIDEO, NOTIFICATION, YOUTUBE;

        @Override
        public String toString() {
            return String.format("tn.pinhole.%s", super.toString().toLowerCase());    //To change body of overridden methods use File | Settings | File Templates.
        }

    }

    private enum Level {
        DEBUG, INFO, WARN, ERROR
    }

    public static void debug(Type logType, String message, Object... args) {
        log(Level.DEBUG, logType, Optional.<Throwable>absent(), message, args);
    }

    public static void warn(Type logType, String message, Object... args) {
        log(Level.WARN, logType, Optional.<Throwable>absent(), message, args);
    }

    public static void info(Type logType, String message, Object... args) {
        log(Level.INFO, logType, Optional.<Throwable>absent(), message, args);
    }

    public static void error(Type logType, Throwable ex) {
        log(Level.ERROR, logType, Optional.of(ex), "");
    }

    public static void error(Type logType, String message, Object... args) {
        log(Level.ERROR, logType, Optional.<Throwable>absent(), message, args);
    }

    public static void error(Type logType, Throwable ex, String message, Object... args) {
        log(Level.ERROR, logType, Optional.of(ex), message, args);
    }

    private static void log(Level level, Type logType, Optional<Throwable> ex, String message, Object... args) {
        try {
            final String formattedMessage = formatMessage(message, args);
            switch (level) {
                case DEBUG:
                    if (Logger.isDebugEnabled())
                        Logger.of(logType.toString()).debug(formattedMessage);
                    break;
                case INFO:
                    if (Logger.isInfoEnabled())
                        Logger.of(logType.toString()).info(formattedMessage);
                    break;
                case WARN:
                    if (Logger.isWarnEnabled())
                        Logger.of(logType.toString()).warn(formattedMessage);
                    break;
                case ERROR:
                    if (ex.isPresent())
                        Logger.of(logType.toString()).error(formattedMessage, ex.get());
                    else
                        Logger.of(logType.toString()).error(formattedMessage);
                    break;
            }
        } catch (Exception e) {
            Logger.error("LOG exception on logging data", e);
        }
    }

    private static String formatMessage(String message, Object... args) {
        try {
            return String.format(message, args);
        } catch (Exception ex) {
            Logger.error("LOG error on formatting the message=" + message, ex);
            return "";
        }
    }
}
