/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package core;

import core.log.Log;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import org.joda.time.*;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * Util class used to manipulate date and time
 *
 * @author ayassinov
 */
public class DateTimeUtils {

    public static final String DEFAULT_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    public static final String COOKIE_TRACKING_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String BOX_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm";

    public static final String SITEMAP_DATE_TIME_FORMAT = "yyyy-MM-dd";
    public static final String DEFAULT_LOCALE = "FR-fr";
    private static final int USER_CONNECTION_TIMEOUT = 35; //seconds


    /**
     * Get current date without seconds and milliseconds,
     * used when comparing date and ignoring seconds and milliseconds
     *
     * @return a current date time object without seconds and milliseconds
     */
    public static Date currentDateWithoutSeconds() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * Get a current time
     *
     * @return an instance of Date reprenting the current date time on the system timezone
     */
    public static Date now() {
        return Calendar.getInstance().getTime();
    }

    public static Date now(String timezone) {
        return convertFromSystemTimeZone(now(), timezone);
    }

    public static Date getDateWithoutTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * truncate a given date and remove time information
     *
     * @param date the date that will be truncated
     * @return an instance of date without time in the system timezone
     */
    public static Date getDateWithoutTime(Date date) {
        Calendar calendar = getCalendar(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * A minimum date is a date at 01/01/1970 00:00. We can use it to avoid testing with null
     *
     * @return a minimum date.
     */
    public static Date getMinDate() {
        return new Date(0L);
    }

    /**
     * truncate a given date and remove time information
     *
     * @param date the date that will be truncated
     * @return an instance of date without time in the system timezone
     */
    public static Date getTime(Date date) {
        return DateTimeUtils.getCompiledDateAndTime(getMinDate(), date);
    }

    /**
     * Convert a date from time zone to  system time zone
     *
     * @param date     : date to convert
     * @param timeZone : current time zone
     * @return date in the system time zone. If date is null this function return a null
     */
    public static Date convertToSystemTimeZone(Date date, String timeZone) {
        if (date == null)
            return null;
        //set the date and time zone
        DateTime srcDateTime = new DateTime(date).withZoneRetainFields(DateTimeZone.forID(timeZone.replace("GMT", "")));
        // convert to reference time zone
        DateTime dstDateTime = srcDateTime.withZone(DateTimeZone.getDefault()); //todo check system time zone

        return dstDateTime.toLocalDateTime().toDateTime().toDate();
    }

    /**
     * Convert a date from system time zone to another time zone
     *
     * @param date         : date to convert
     * @param timeZoneDest : destination time zone
     * @return date in the timeZoneDest time zone. If date is null this function return a null
     */
    public static Date convertFromSystemTimeZone(Date date, String timeZoneDest) {
        if (date == null) //no date return null
            return null;

        if (timeZoneDest == null || timeZoneDest.isEmpty()) //we return the same date if timezone is null
            return date;

        DateTime scrDateTime = new DateTime(date).withZoneRetainFields(DateTimeZone.getDefault());
        DateTime destDateTime = scrDateTime.withZone(DateTimeZone.forID(timeZoneDest.replace("GMT", "")));
        return destDateTime.toLocalDateTime().toDateTime().toDate();
    }

    public static String convertTimeZoneAndFormat(Date date) {
        date = convertFromSystemTimeZone(date, TimeZoneMap.DEFAULT);
        return format(date, DEFAULT_DATE_FORMAT);
    }

    /**
     * Return a string that represent a date time in the current format and local
     * If date or code or format is null or empty we return an empty string
     * If code is bad formatted we return an empty string
     *
     * @param date         the date to format
     * @param localeString a code in sting format like fr or fr-FR
     * @param format       a date time format like dd/MM/yy
     * @return formatted date time.
     */
    public static String format(Date date, String localeString, String format) {
        if (date == null || localeString == null || localeString.isEmpty() || format == null || format.isEmpty())
            return "";

        final Optional<Locale> locale = parseLocale(localeString);
        if (locale.isPresent()) {
            return new SimpleDateFormat(format, DateFormatSymbols.getInstance(locale.get())).format(date);
        }
        return "";
    }

    /**
     * Format a date using default Locale FR-fr and default format dd/MM/yyyy HH:mm
     *
     * @param date the date to format
     * @return formatted date time.
     */
    public static String format(Date date) {
        return format(date, DEFAULT_LOCALE, DEFAULT_DATE_TIME_FORMAT);
    }

    /**
     * Format a date using default Locale FR-fr any format in form of dd/MM/yyyy HH:mm
     *
     * @param date the date to format
     * @return formatted date time.
     */
    public static String format(Date date, String format) {
        return format(date, DEFAULT_LOCALE, format);
    }

    /**
     * Format a date to parse it from momentJS (yyyy-MM-dd HH:mm:ss Z)
     *
     * @param date the date time to parse
     * @return the formatted date or an empty string if the date is null
     */
    public static String formatToMomentJs(Date date, String timeZone) {
        if (date == null)
            return ""; // we return an empty string
        final String formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        final String formattedTime = new SimpleDateFormat("HH:mm:ss").format(date);
        return String.format("%sT%s", formattedDate, formattedTime);
    }

    /**
     * Format the date for the box communication
     *
     * @param date the date to format
     * @return a string representing the formatted date
     */
    public static String formatToBox(Date date) {
        return (long) (date.getTime() / 1000) + "";
    }

    /**
     * @param date the date to check
     * @return true if the date is after or equal current date
     */
    public static boolean isInTheFuture(Date date) {
        return isAfter(date, now(), true);
    }

    /**
     * @param date the date to check
     * @return true if the date is before or equal current date
     */
    public static boolean isInThePast(Date date) {
        return isBefore(date, now(), true);
    }

    /**
     * Compare two date. If the first date is after or equal the second one.
     *
     * @param firstDate     the first date
     * @param secondDate    the second date
     * @param checkEquality need to check the date for equality
     * @return true if the first date after the second date or if the checkEquality param is true we return true if the two date are equal.
     */
    public static boolean isAfter(final Date firstDate, final Date secondDate, boolean checkEquality) {
        final DateTime firstDateTime = new DateTime(firstDate);
        final DateTime secondDateTime = new DateTime(secondDate);
        return (checkEquality && firstDateTime.isEqual(secondDateTime)) || firstDateTime.isAfter(secondDateTime);
    }

    /**
     * Compare two date. If the first date is before or equal the second one.
     *
     * @param firstDate     the first date
     * @param secondDate    the second date
     * @param checkEquality need to check the date for equality
     * @return true if the first date before the second date or if the checkEquality param is true we return true if the two date are equal.
     */
    public static boolean isBefore(final Date firstDate, final Date secondDate, boolean checkEquality) {
        final DateTime firstDateTime = new DateTime(firstDate);
        final DateTime secondDateTime = new DateTime(secondDate);
        return (checkEquality && firstDateTime.isEqual(secondDateTime)) || firstDateTime.isBefore(secondDateTime);
    }

    /**
     * Check if the two dates are equal
     *
     * @param firstDate  the first date
     * @param secondDate the first date
     * @return return true if the two dates are equals
     */
    public static boolean isEqual(Date firstDate, Date secondDate) {
        final DateTime firstDateTime = new DateTime(firstDate);
        final DateTime secondDateTime = new DateTime(secondDate);
        return firstDateTime.isEqual(secondDateTime);
    }

    /**
     * Check if the date are between two other dates
     *
     * @param date          the date to check
     * @param dateFrom      the date from
     * @param dateTo        the date to
     * @param checkEquality true if we need to check the equality
     * @return true if the date are between the two dates
     */
    public static boolean isBetween(Date date, Date dateFrom, Date dateTo, boolean checkEquality) {
        return isAfter(date, dateFrom, checkEquality) && isBefore(date, dateTo, checkEquality);
    }

    /**
     * Check if a first begin/end time are within another begin/end time
     *
     * @param firstDateFrom  the first date from
     * @param firstDateTo    the first date to
     * @param secondDateFrom the second date from
     * @param secondDateTo   the second date to
     * @param checkEquality  true if we need to check the equality
     * @return true if the first date is between the second date or the second date is within the first date
     */
    public static boolean isWithin(Date firstDateFrom, Date firstDateTo, Date secondDateFrom, Date secondDateTo, boolean checkEquality) {
        return isAfter(firstDateFrom, secondDateFrom, checkEquality) && isBefore(firstDateTo, secondDateTo, checkEquality) ||
                isAfter(secondDateFrom, firstDateFrom, checkEquality) && isBefore(secondDateTo, firstDateTo, checkEquality);
    }

    /**
     * Check if the date is a minimal date
     *
     * @param date the date to check
     * @return true if the date is minimal
     */
    public static boolean isMinimal(Date date) {
        return isEqual(getDateWithoutTime(date), getDateWithoutTime(getMinDate()));
    }

    public static boolean isSpanMultipleDay(Date beginDateTime, Date endDateTime) {
        if (beginDateTime == null || endDateTime == null)
            return false;
        final DateTime firstDateTime = new DateTime(beginDateTime);
        final DateTime secondDateTime = new DateTime(endDateTime);
        int da1 = firstDateTime.getDayOfWeek();
        int da2 = secondDateTime.getDayOfWeek();
        return da1 != da2;
    }

    /**
     * Set a time to a date instance ignoring the seconds and milliseconds
     *
     * @param date the date portion
     * @param time the time portion
     * @return a new instance with the given date and time on the system timezone and without seconds and milliseconds
     */
    public static Date getCompiledDateAndTime(Date date, Date time) {
        final Calendar c = getCalendar(time);
        return new DateTime(date).withTime(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), 0, 0).toDate();
    }

    /**
     * Get the hour from a date
     *
     * @param now an instance of date
     * @return an number representing the hour of a date
     */
    public static int getHours(Date now) {
        return getCalendar(now).get(Calendar.HOUR_OF_DAY);
    }

    /**
     * return a future date on the system time zone with the number of defined hours
     *
     * @param hours number of hours to add
     * @return the future date without seconds and milliseconds
     */
    public static Date addHoursFromNow(int hours) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * Set a minutes to a date
     *
     * @param date    an instance of a date
     * @param minutes number of minutes to set
     * @return an instance of a date with the minutes added
     */
    public static Date addMinutes(Date date, int minutes) {
        final Calendar c = getCalendar(date);
        c.add(Calendar.MINUTE, minutes);
        return c.getTime();
    }

    /**
     * Set a seconds to a date
     *
     * @param date    an instance of a date
     * @param seconds number of seconds to set
     * @return an instance of a date with the seconds added
     */
    public static Date addSeconds(Date date, int seconds) {
        final Calendar c = getCalendar(date);
        c.add(Calendar.SECOND, seconds);
        return c.getTime();
    }

    /**
     * Set minutes to a current date/time
     *
     * @param minutes number of seconds to set
     * @return an instance of a date with the minutes added
     */
    public static Date addMinutesFromNow(int minutes) {
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, minutes);
        return c.getTime();
    }

    /**
     * Set a seconds to a current date/time
     *
     * @param seconds number of seconds to set
     * @return an instance of a date with the seconds added
     */
    public static Date addSecondsFromNow(int seconds) {
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.SECOND, seconds);
        return c.getTime();
    }

    public static Date addDaysFromNow(int days) {
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, days);
        return c.getTime();
    }

    public static Date addMonthsNow(int months) {
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, months);
        return c.getTime();
    }

    /**
     * Set days to a date
     *
     * @param date an instance of a date
     * @param days number of days to set
     * @return an instance of a date with the days added
     */
    public static Date addDays(Date date, int days) {
        final Calendar c = getCalendar(date);
        c.add(Calendar.DAY_OF_MONTH, days);
        return c.getTime();
    }

    public static int getDifferenceInDays(Date beginDate, Date endDate) {
        final DateTime beginDateTime = new DateTime(beginDate);
        final DateTime endDateTime = new DateTime(endDate);
        return Days.daysBetween(beginDateTime, endDateTime).getDays();
    }

    public static int getDifferenceInMinutes(Date beginDate, Date endDate) {
        final DateTime beginDateTime = new DateTime(beginDate);
        final DateTime endDateTime = new DateTime(endDate);
        final Period period = new Period(beginDateTime, endDateTime);
        return period.toStandardMinutes().getMinutes();
    }

    public static int getDifferenceInSeconds(Date beginDate, Date endDate) {
        final DateTime beginDateTime = new DateTime(beginDate);
        final DateTime endDateTime = new DateTime(endDate);
        final Period period = new Period(beginDateTime, endDateTime);
        return period.toStandardSeconds().getSeconds();
    }

    /**
     * return the day of week number for a given date.
     * ex: for 06/03/2013 (06 march 2013) returns 4
     *
     * @param date
     * @return
     */
    public static int getDayOfWeekIndex(Date date) {
        DateTime dt = new DateTime(date);
        return dt.dayOfWeek().get();
    }

    /**
     * Convert a date to GMT+1 date and return a string in format yyyy,MM,dd,HH,mm
     *
     * @param date the date to format
     * @return a string in format of yyyy,MM,dd,HH,mm
     */
    public static String convertToTimeLine(Date date) {
        return formatToMomentJs(DateTimeUtils.convertFromSystemTimeZone(date, TimeZoneMap.DEFAULT), TimeZoneMap.listJS().get(TimeZoneMap.DEFAULT));
    }

    /**
     * Return a date from a text and a format
     *
     * @param textDate the date in text format
     * @param format   the format of the date
     * @return return an instance of date if the parse was successful or null if an exception was raised
     */
    public static Date parse(String textDate, String format) {
        final DateFormat formatter = new SimpleDateFormat(format);
        return parse(textDate, formatter).orNull();
    }

    public static Optional<Date> parse(String textDate, DateFormat dateFormat) {
        try {
            final Date res = dateFormat.parse(textDate);
            return Optional.of(res);
        } catch (ParseException e) {
            Log.error(Log.Type.UTILS, e, "Exception on parsing text to date. textDate=%s, format=%s", textDate, dateFormat);
            return Optional.absent();
        }
    }

    /**
     * Set a date to a Calender instance and return it
     *
     * @param date the date to set on a calender
     * @return an instance of a calender with the given date
     */
    private static Calendar getCalendar(Date date) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * Convert String to java.util.Locale
     *
     * @param locale : code as string to convert formatted like us-US or fr-FR
     * @return code from
     */
    public static Optional<Locale> parseLocale(String locale) {
        String[] tab = locale.split("-");
        if (tab.length == 1) //language
            return Optional.of(new Locale(tab[0]));
        if (tab.length == 2) //language and country
            return Optional.of(new Locale(tab[0], tab[1]));
        return Optional.absent(); //bad format
    }

    /**
     * Get the number of second between now and the hours and minutes that are given
     * used in planning a job on startup in a fixed date
     *
     * @param hour         the hour of a day between 0-23
     * @param minute       the minute of an hour between 0-59
     * @param jobFrequency scheduler frequency
     * @return the number of second to that time of the day
     */
    public static int getNextExecutionInSeconds(int hour, int minute, int jobFrequency) {
        return Seconds.secondsBetween(
                new DateTime(now()),
                nextExecution(hour, minute, jobFrequency)
        ).getSeconds();
    }

    /**
     * Get the target date time based on hour of a day and minute of a day
     *
     * @param hour         the hour of a day between 0-23
     * @param minute       the minute of an hour between 0-59
     * @param jobFrequency Job frequency
     * @return the date time if it's before now we add 'jobFrequency' hours
     */
    private static DateTime nextExecution(int hour, int minute, int jobFrequency) {
        DateTime next = new DateTime()
                .withHourOfDay(hour)
                .withMinuteOfHour(minute)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);

        return (next.isBeforeNow())
                ? next.plusHours(jobFrequency)
                : next;
    }

    /**
     * @author ayassinov
     */
    public static class TimeZoneMap {

        public static final String DEFAULT = "GMT+01:00";
        public static final String DEFAULT_JS = "+0000";
        private static final Map<String, String> GMT_TIMEZONE;
        private static final Map<String, String> GMT_JS_TIMEZONE;

        static {
            GMT_TIMEZONE = new ImmutableMap.Builder<String, String>()
                    .put("GMT-12:00", "GMT-12")
                    .put("GMT-11:00", "GMT-11")
                    .put("GMT-10:00", "GMT-10")
                    .put("GMT-09:00", "GMT-9")
                    .put("GMT-08:00", "GMT-8")
                    .put("GMT-07:00", "GMT-7")
                    .put("GMT-06:00", "GMT-6")
                    .put("GMT-05:00", "GMT-5")
                    .put("GMT-04:00", "GMT-4")
                    .put("GMT-03:00", "GMT-3")
                    .put("GMT-02:00", "GMT-2")
                    .put("GMT-01:00", "GMT-1")
                    .put("UTC", "GMT")
                    .put("GMT+01:00", "GMT+1")
                    .put("GMT+02:00", "GMT+2")
                    .put("GMT+03:00", "GMT+3")
                    .put("GMT+04:00", "GMT+4")
                    .put("GMT+05:00", "GMT+5")
                    .put("GMT+06:00", "GMT+6")
                    .put("GMT+07:00", "GMT+7")
                    .put("GMT+08:00", "GMT+8")
                    .put("GMT+09:00", "GMT+9")
                    .put("GMT+10:00", "GMT+10")
                    .put("GMT+11:00", "GMT+11")
                    .put("GMT+12:00", "GMT+12")
                    .put("GMT+13:00", "GMT+13")
                    .put("GMT+14:00", "GMT+14")
                    .build();

            GMT_JS_TIMEZONE = new ImmutableMap.Builder<String, String>()
                    .put("GMT-12:00", "-1200")
                    .put("GMT-11:00", "-1100")
                    .put("GMT-10:00", "-1000")
                    .put("GMT-09:00", "-0900")
                    .put("GMT-08:00", "-0800")
                    .put("GMT-07:00", "-0700")
                    .put("GMT-06:00", "-0600")
                    .put("GMT-05:00", "-0500")
                    .put("GMT-04:00", "-0400")
                    .put("GMT-03:00", "-0300")
                    .put("GMT-02:00", "-0200")
                    .put("GMT-01:00", "-0100")
                    .put("UTC", "+0000")
                    .put("GMT+01:00", "+0100")
                    .put("GMT+02:00", "+0200")
                    .put("GMT+03:00", "+0300")
                    .put("GMT+04:00", "+0400")
                    .put("GMT+05:00", "+0500")
                    .put("GMT+06:00", "+0600")
                    .put("GMT+07:00", "+0700")
                    .put("GMT+08:00", "+0800")
                    .put("GMT+09:00", "+0900")
                    .put("GMT+10:00", "+1000")
                    .put("GMT+11:00", "+1100")
                    .put("GMT+12:00", "+1200")
                    .put("GMT+13:00", "+1300")
                    .put("GMT+14:00", "+1400")
                    .build();
        }

        public static Map<String, String> list() {
            return GMT_TIMEZONE;
        }

        public static Map<String, String> listJS() {
            return GMT_JS_TIMEZONE;
        }

        public static String getJsTz(String key) {
            return GMT_JS_TIMEZONE.get(key);
        }
    }
}
