/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package core.user;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.TxCallable;
import com.avaje.ebean.TxScope;
import com.google.common.base.Optional;
import cookies.CookieUtils;
import cookies.VisitorCookie;
import core.StringUtils;
import core.cache.CacheUtil;
import core.log.Log;
import core.parameter.ApplicationParameter;
import models.Language;
import models.user.Location;
import models.user.User;
import models.user.UserStatus;
import play.mvc.Http;
import services.LanguageService;
import services.LocationService;

/**
 * @author ayassinov
 */
public class BaseUserService {

    public static final String SQL_SELECT_USER = "select u.usr_id, u.usr_uid, u.usr_session_id, u.usr_name, u.usr_account_type, " +
            "       u.usr_timezone, u.usr_islogged, u.usr_isregistercomplete, u.usr_email_opting, u.usr_email,  u.usr_img_url, u.version as usr_version, u.usr_password," +
            "       l.loc_id, l.version as loc_version, l.loc_name, l.loc_timezone, l.loc_dst, l.loc_default, " +
            "       a.lng_id, a.lng_name, a.lng_locale, a.lng_default," +
            "       i.inf_id, i.version as inf_version, i.inf_country_code, i.inf_country, i.inf_timezone, i.inf_timezone, " +
            "       s.sts_id, s.version as sts_version, s.sts_became_member, s.sts_join_date, s.sts_last_makeit_social_prompt, s.sts_madeit_social, " +
            "       p.uvp_id,  p.uvp_volume, " +
            "       COALESCE((select sf.soc_id is not null has_fb from pin_usr_social sf where sf.usr_id = u.usr_id " +
            "                and sf.soc_provider = 'facebook'), false) as has_fb ," +
            "       COALESCE((select st.soc_id is not null has_tw from pin_usr_social st where st.usr_id = u.usr_id" +
            "                and st.soc_provider = 'twitter'), false) as has_tw," +
            "       (select count(f.usr_friend_id) as friends from pin_user_friend f where f.usr_id = u.usr_id) as friends" +
            " from pin_user u" +
            " left join pin_location l on l.loc_id = u.loc_id" +
            " left join pin_language a on a.lng_id = u.lng_id" +
            " left join pin_usr_video_pref p on  u.usr_id = p.usr_id" +
            " left join pin_usr_info i on i.usr_id = u.usr_id" +
            " left join pin_usr_status s on s.usr_id = u.usr_id  ";

    public static User getMinBySessionId(String sessionId) {
        if (StringUtils.isNullOrEmpty(sessionId))
            return null;

        final SqlRow row = Ebean.createSqlQuery("select u.usr_id, u.usr_uid, u.usr_session_id, u.usr_name, u.usr_account_type, u.usr_isadmin from pin_user u where u.usr_session_id = :usrSessionId")
                .setParameter("usrSessionId", sessionId).findUnique();

        if (row == null || row.isEmpty())
            return null;

        return new User(row);
    }

    public static Optional<UserStatus> getUserStatus(User user) {
        final SqlRow row = Ebean.createSqlQuery("select * from pin_usr_status where usr_id = :usrId")
                .setParameter("usrId", user.id)
                .findUnique();
        if (row == null || row.isEmpty()) {
            return Optional.absent();
        }

        return Optional.of(new UserStatus(user, row));
    }

    /**
     * Get user using the cookies uid values. If the user is a search bot we return a specific user.
     * If no cookies is found we assume that it's a new visitor and we create a new user
     * and set the visitor cookie
     *
     * @return User instance (visitor, member, bot user)
     */
    public static User getUserOrCreate() { //todo make not static
        final Optional<User> optionalUser = getUser();
        if (optionalUser.isPresent()) //user exist
            return optionalUser.get();

        //not found? so it's first /time here. We create the user
        //get language from the browser
        final Language language = LanguageService.getOrDefault(Http.Context.current().lang().code());
        //get the location from ip address
        final Location location = LocationService.getByIpAddressOrDefault();
        final User user = saveUserVisitor(language, location);
        CookieUtils.setUserIdToCurrentContext(user.id);
        return user;
    }

    public static Optional<User> getUser() {
        //test if it's a bot //=> return bot user
        final String userAgent = Http.Context.current().request().getHeader(ApplicationParameter.SEO.userAgentHeaderKey);
        if (StringUtils.isSearchBot(userAgent) || StringUtils.isPinholeBot(userAgent)) {
            Log.info(Log.Type.SEO, "Search bot detected with agent=%s", userAgent);
            return Optional.of(getBotUser());
        }

        //read session cookie
        final String sessionId = CookieUtils.getSessionId();
        final User member = getBySessionId(sessionId);
        if (member != null) {
            CookieUtils.setUserIdToCurrentContext(member.id);
            return Optional.of(member);
        }
        //read visitor cookie
        final VisitorCookie visitorCookie = CookieUtils.getVisitorCookie();
        if (visitorCookie != null && !StringUtils.isNullOrEmpty(visitorCookie.uid)) {
            final User visitor = getByUid(visitorCookie.uid).orNull();
            if (visitor != null) {
                CookieUtils.setUserIdToCurrentContext(visitor.id);
                return Optional.of(visitor);
            }
        }
        return Optional.absent();
    }

    /**
     * To call only when we are sure to have a sessionId
     *
     * @param sessionId the user session Id
     * @return an instance of a user
     */
    private static User getBySessionId(String sessionId) {
        if (sessionId == null || sessionId.trim().isEmpty())
            return null;

        final String sql = SQL_SELECT_USER + " where u.usr_session_id = :usrSessionId";
        final SqlRow row = Ebean.createSqlQuery(sql)
                .setParameter("usrSessionId", sessionId).findUnique();

        if (row == null || row.isEmpty())
            return null;

        return new User(row);
    }

    private static User getBotUser() {
        final Optional<User> phantomJsUser = CacheUtil.get(CacheUtil.Keys.USER_SEARCH_BOT);
        if (phantomJsUser.isPresent()) {
            return phantomJsUser.get();
        } else {
            final User user = getByUid(ApplicationParameter.SEO.userUid).orNull();
            if (user == null) {
                Log.error(Log.Type.SEO, "Pinhole search bot user with uid=%s should be present on the database", ApplicationParameter.SEO.userUid);
                return null;
            }
            CacheUtil.cache(CacheUtil.Keys.USER_SEARCH_BOT, user, CacheUtil.Period.VERY_LONG);
            return user;
        }
    }

    private static Optional<User> getByUid(String uid) {
        if (uid == null || uid.trim().isEmpty())
            return Optional.absent();

        final String sql = SQL_SELECT_USER + " where u.usr_uid = :usrUid";
        final SqlRow row = Ebean.createSqlQuery(sql)
                .setParameter("usrUid", uid).findUnique();

        if (row == null || row.isEmpty())
            return Optional.absent();

        //final models.view.user.User userJson = new models.view.user.User(UserStatusService.refreshUserStatus(user), withFriends);
        return Optional.of(new User(row));
    }

    private static User saveUserVisitor(final Language language, final Location location) {
        final User visitorUser = Ebean.execute(TxScope.required(), new TxCallable<User>() {
            @Override
            public User call() {
                final User savedUser = new User(language, location);
                Ebean.save(savedUser);
                Ebean.save(savedUser.userInfo);
                Ebean.save(savedUser.status);
                Ebean.save(savedUser.videoPreferences);
                return savedUser;
            }
        });

        if (visitorUser != null)
            CookieUtils.saveVisitorCookie(new VisitorCookie(visitorUser));

        return visitorUser;
    }
}
