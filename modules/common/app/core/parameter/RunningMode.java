/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package core.parameter;

/**
 * The running mode of the application : Full, administration only, pinhole only , ads only
 *
 * @author ayssinov
 */
public enum RunningMode {
    PINHOLE, ADMIN, ADS, FULL;

    public boolean isAdsMode(){
        return this.equals(ADS);
    }

    public boolean isAdminMode() {
        return this.equals(ADMIN);
    }

    public boolean isPinholeMode() {
        return this.equals(PINHOLE);
    }

    public boolean isFullMode() {
        return this.equals(FULL);
    }
}
