package core.parameter;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import core.cache.CacheUtil;
import core.log.Log;
import models.Parameter;
import models.ParameterSection;
import play.Play;
import play.db.ebean.Model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author ayassinov
 */
public class ApplicationParameter {

    private static Model.Finder<Long, Parameter> find = new Model.Finder<Long, Parameter>(Long.class, Parameter.class);
    private static Model.Finder<Long, ParameterSection> findSection = new Model.Finder<Long, ParameterSection>(Long.class, ParameterSection.class);

    public final static Seo SEO = new Seo();
    public final static S3 S3 = new S3();
    public final static Ec2 EC2 = new Ec2();
    public final static Locale LOCALE = new Locale();
    public final static Cookie COOKIE = new Cookie();
    public final static App APP = new App();
    public final static Email EMAIL = new Email();
    public final static Youtube Youtube = new Youtube();


    public static boolean validate() {
        return false;
    }

    /**
     * get a parameter from cache and parse it as Integer
     *
     * @param sectionName   section name
     * @param parameterName parameter name
     * @param defaultValue  default value
     * @return Integer
     */
    public static Integer getAsIntOrElse(String sectionName, String parameterName, Integer defaultValue) {
        return getAsTypeOrElse(sectionName, parameterName, defaultValue);
    }

    /**
     * get a parameter from cache and parse it as Boolean
     *
     * @param sectionName   section name
     * @param parameterName parameter name
     * @param defaultValue  default value
     * @return Boolean
     */
    public static Boolean getAsBooleanOrElse(String sectionName, String parameterName, Boolean defaultValue) {
        return getAsTypeOrElse(sectionName, parameterName, defaultValue);
    }

    /**
     * get a parameter from cache and parse it as Boolean
     *
     * @param sectionName   section name
     * @param parameterName parameter name
     * @param defaultValue  default value
     * @return Boolean
     */
    public static String getAsTextOrElse(String sectionName, String parameterName, String defaultValue) {
        return getAsTypeOrElse(sectionName, parameterName, defaultValue);
    }


    public static List<Integer> getAsIntListOrElse(String sectionName, String parameterName, List<Integer> defaultValue) {
        return getAsTypeListOrElse(sectionName, parameterName, defaultValue, Integer.class);
    }

    public static List<Boolean> getAsBooleanListOrElse(String sectionName, String parameterName, List<Boolean> defaultValue) {
        return getAsTypeListOrElse(sectionName, parameterName, defaultValue, Boolean.class);
    }

    public static List<String> getAsTextListOrElse(String sectionName, String parameterName, List<String> defaultValues) {
        return getAsTypeListOrElse(sectionName, parameterName, defaultValues, String.class);
    }

    /**
     * get a parameter from cache and parse it as T
     *
     * @param sectionName   section name
     * @param parameterName parameter name
     * @param defaultValue  default value
     * @param <T>           primitive type T
     * @return instance of T
     */
    @SuppressWarnings("unchecked")
    private static <T> T getAsTypeOrElse(String sectionName, String parameterName, T defaultValue) {
        return (T) getAsOrElse(sectionName, parameterName, defaultValue);
    }

    /**
     * get a parameter from cache and parse it as T
     *
     * @param sectionName   section name
     * @param parameterName parameter name
     * @param defaultValues List of default values
     * @param <T>           primitive type T
     * @return instance of T
     */
    @SuppressWarnings("unchecked")
    private static <T> List<T> getAsTypeListOrElse(String sectionName, String parameterName, List<T> defaultValues, Class clazz) {
        return (List<T>) getAsListOrElse(sectionName, parameterName, defaultValues, clazz);
    }

    /**
     * get a parameter from cache and parse it as Object
     *
     * @param sectionName   section name
     * @param parameterName parameter name
     * @param defaultValue  default value
     * @return Boolean
     */
    private static Object getAsOrElse(String sectionName, String parameterName, Object defaultValue) {
        try {
            final Optional<String> parameterOptional = getFromCache(sectionName, parameterName);

            if (parameterOptional.isPresent()) {
                if (defaultValue instanceof Integer)
                    return Integer.valueOf(parameterOptional.get());
                if (defaultValue instanceof Long)
                    return Long.valueOf(parameterOptional.get());
                if (defaultValue instanceof Boolean)
                    return Boolean.valueOf(parameterOptional.get());
                if (defaultValue instanceof String)
                    return parameterOptional.get();
            } else
                Log.error(Log.Type.PARAMETER, "Configuration parameter not found ! section=%s, name=%s", sectionName, parameterName);
        } catch (Exception ex) {
            Log.error(Log.Type.PARAMETER, ex, "Error while getting the parameter with section=%s, name=%s", sectionName, parameterName);
        }
        return defaultValue;
    }

    /**
     * get a parameter from cache and parse it as Object
     *
     * @param sectionName   section name
     * @param parameterName parameter name
     * @param defaultValues default values
     * @return Boolean
     */
    private static <T> Object getAsListOrElse(String sectionName, String parameterName, Object defaultValues, final T clazz) {
        try {
            final Optional<String> parameterOptional = getFromCache(sectionName, parameterName);

            if (parameterOptional.isPresent()) {
                final String delimiters = "[ ,]+";
                final List<String> stringValues = Arrays.asList(parameterOptional.get().split(delimiters));
                return Lists.transform(stringValues, new Function<String, Object>() {
                    public Object apply(String s) {
                        if (clazz.equals(Integer.class))
                            return Integer.valueOf(s);
                        if (clazz.equals(Long.class))
                            return Long.valueOf(s);
                        if (clazz.equals(Boolean.class))
                            return Boolean.valueOf(s);
                        if (clazz.equals(String.class))
                            return s;

                        return null;
                    }
                });

            } else
                Log.error(Log.Type.PARAMETER, "Configuration parameter not found ! section=%s, name=%s", sectionName, parameterName);
        } catch (Exception ex) {
            Log.error(Log.Type.PARAMETER, ex, "Error while getting the parameter with section=%s, name=%s", sectionName, parameterName);
        }
        return defaultValues;
    }

    public static Map<String, String> list() {
        return new ImmutableMap.Builder<String, String>()
                .putAll(getAllMap())
                .build();
    }

    public static Map<String, String> getAllMap() {
        final Map<String, String> resultMap = new HashMap<String, String>();
        final List<ParameterSection> sections = findSection.all();
        for (ParameterSection section : sections) {
            resultMap.put(section.id.toString(), section.name);
        }
        return resultMap;
    }

    /**
     * Gets parameter from cache. If not found, retrieves it from database and sets it in cache
     *
     * @param paramName   parameter name
     * @param sectionName section name
     * @return Optional<Parameter>
     */
    private static Optional<String> getFromCache(String sectionName, String paramName) throws Exception {
        try {
            final Optional<String> cachedParameter = CacheUtil.get(String.format(CacheUtil.Keys.CACHE_PARAM, sectionName, paramName));
            if (cachedParameter.isPresent()) // parameter exist on cache
                return cachedParameter;


            final Optional<Parameter> parameterOptional = getBy(sectionName, paramName);
            if (!parameterOptional.isPresent()) { // parameter does not exist
                return Optional.absent();
            } else {
                Log.debug(Log.Type.PARAMETER, "Parameter section=%s, name=%s not in cache. Loading it...", sectionName, paramName);

                final Parameter parameter = parameterOptional.get();
                CacheUtil.cache(String.format(CacheUtil.Keys.CACHE_PARAM, parameter.section.name, parameter.name), parameter.value, CacheUtil.Period.VERY_LONG);
                return Optional.of(parameter.value);
            }
        } catch (Exception ex) {
            Log.error(Log.Type.PARAMETER, ex, "Error while getting the parameter section=%s, name=%s", sectionName, paramName);
            throw new Exception("Error while getting an application parameter by its section and name");
        }
    }

    /**
     * Get a parameter by its names
     *
     * @param sectionName section name
     * @param paramName   parameter name
     * @return Optional<Parameter>
     */
    private static Optional<Parameter> getBy(String sectionName, String paramName) {
        try {
            return Optional.fromNullable(
                    find.where()
                            .eq("section.name", sectionName)
                            .eq("name", paramName)
                            .findUnique()
            );
        } catch (Exception ex) {
            Log.error(Log.Type.PARAMETER, ex, "Error while getting the parameter with section=%s, name=%s", sectionName, paramName);
            return Optional.absent();
        }
    }

    /**
     * Configuration class for s3
     */
    public static class S3 {
        public final String bucketName;
        public final String url;
        public final String accessKey;
        public final String securedUrl;
        public final String secretKey;

        private S3() {
            bucketName = Play.application().configuration().getString("aws.s3.bucket");
            accessKey = Play.application().configuration().getString("aws.access.key");
            secretKey = Play.application().configuration().getString("aws.secret.key");
            securedUrl = String.format("//%s.s3.amazonaws.com", bucketName);
            url = String.format("https://%s.s3.amazonaws.com", bucketName);
        }
    }

    /**
     * Location and languages configuration
     */
    public static class Locale {
        public final String defaultLocationCode;
        public final String defaultLanguageCode;

        public Locale() {
            defaultLocationCode = Play.application().configuration().getString("application.default.location");
            defaultLanguageCode = Play.application().configuration().getString("application.default.language");
        }
    }

    /**
     * Cookies configuration
     */
    public static class Cookie {
        private static final int VISITOR_DEFAULT_MAX_AGE = 2419200; //  28 days
        private static final String VISITOR_DEFAULT_NAME = "pin_uid"; //  28 days
        private static final int AD_COOKIE_DEFAULT_MAX_AGE = 259200; // 3 days
        private static final String AD_COOKIE_DEFAULT_NAME = "pin_ad_uid"; //  28 days

        public final String visitorName;
        public final int visitorMaxAge;
        public final int logoutMaxAge;
        public final int trackingMaxAge;

        public Cookie() {
            visitorName = Play.application().configuration().getString("visitor.cookieName", VISITOR_DEFAULT_NAME);
            visitorMaxAge = Play.application().configuration().getInt("visitor.maxAge", VISITOR_DEFAULT_MAX_AGE);
            logoutMaxAge = Play.application().configuration().getInt("logout.maxAge", VISITOR_DEFAULT_MAX_AGE);
            trackingMaxAge = Play.application().configuration().getInt("tracking.maxAge", VISITOR_DEFAULT_MAX_AGE);
        }
    }

    /**
     * Application parameters, hostname, temp folder,...
     */
    public static class App {
        public static final int PINHOLE_ACTIVATED_CODE = 0;
        public static final int ADMIN_ACTIVATED_CODE = 1;
        public static final int ADS_ACTIVATED_CODE = 2;

        public final RunningMode runningMode;
        public final String host;
        public final String tempFolder;
        public final boolean isProdHost;
        public final String version;
        public final String userName;
        public final String password;

        public App() {
            final int runCode = Play.application().configuration().getInt("application.admin");
            //load running mode
            if (runCode == ADMIN_ACTIVATED_CODE)
                runningMode = RunningMode.ADMIN;
            else if (runCode == ADS_ACTIVATED_CODE)
                runningMode = RunningMode.ADS;
            else
                runningMode = RunningMode.PINHOLE;

            host = Play.application().configuration().getString("application.host");
            tempFolder = Play.application().configuration().getString("application.tempfolder");
            isProdHost = Play.application().configuration().getBoolean("application.isProdHost");
            version = Play.application().configuration().getString("application.version");
            userName = Play.application().configuration().getString("application.userName");
            password = Play.application().configuration().getString("application.password");
        }
    }

    /**
     * Amazon EC2 configuration mongo db and elastic search
     */
    public static class Ec2 {
        public final String esUrl;

        public Ec2() {
            esUrl = Play.application().configuration().getString("ec2.es.url");
        }
    }

    /**
     * Seo related configuration
     */
    public static class Seo {
        public final String userAgentHeaderKey = "User-Agent";
        public final String snapshotFolderName;
        public final String publicFolder;
        public final String prerenderHost;
        public final String pinholeName;
        public final Pattern pattern;
        public final String userUid;
        public final String productionHost;

        public Seo() {
            snapshotFolderName = Play.application().configuration().getString("seo.snapshot.folder");
            publicFolder = Play.application().configuration().getString("seo.public.folder");
            prerenderHost = Play.application().configuration().getString("seo.prerender.host");
            //bot config
            final String botNames = Play.application().configuration().getString("seo.bot.names");
            pattern = Pattern.compile(String.format("(?i)^(.*)(%s)(.*)$", botNames));
            pinholeName = Play.application().configuration().getString("seo.bot.pinhole.name");
            userUid = Play.application().configuration().getString("seo.bot.user.uid");
            productionHost = Play.application().configuration().getString("seo.production.host");
        }
    }

    /**
     * Email parameters and mandrill specific configuration
     */
    public static class Email {
        public final String senderEmail;
        public final String senderName;
        public final String mandrillKey;

        private Email() {
            senderEmail = Play.application().configuration().getString("email.sender.email");
            senderName = Play.application().configuration().getString("email.sender.name");
            mandrillKey = Play.application().configuration().getString("email.mandrill.key");
        }
    }

    /**
     * Youtube api parameters
     */
    public static class Youtube {
        public final String apiKey;

        private Youtube() {
            apiKey = Play.application().configuration().getString("youtube.api.key");
        }
    }
}


