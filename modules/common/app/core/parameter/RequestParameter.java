/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package core.parameter;

import com.google.common.base.Optional;
import core.log.Log;
import play.mvc.Http;

/**
 * @author ayassinov
 */
public class RequestParameter {

    //url param util
    public static Integer getNextPageParam(Http.Request request) {
        Integer lastExtraId = null;
        try {
            if (request.queryString().containsKey("next"))
                lastExtraId = Integer.valueOf(request.queryString().get("next")[0]);
            return lastExtraId;
        } catch (Exception ex) {
            Log.error(Log.Type.PARAMETER, ex, "Exception on getting request parameter with name=next");
            return null;
        }
    }

    public static <T extends Enum<T>> Optional<T> getEnum(Class<T> c, Http.Request request, String key) {
        if (!request.queryString().containsKey(key))
            return Optional.absent();
        try {
            final String requestValue = request.queryString().get(key)[0];
            final T t = Enum.valueOf(c, requestValue.trim().toUpperCase());
            return Optional.of(t);
        } catch (Exception ex) {
            Log.error(Log.Type.PARAMETER, ex, "Error on getting enum value from request parameters");
            return Optional.absent();
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T get(Http.Request request, String key, T defaultValue) {
        try {
            if (!request.queryString().containsKey(key))
                return defaultValue;

            final String requestValue = request.queryString().get(key)[0];
            if (defaultValue instanceof Integer)
                return (T) Integer.valueOf(requestValue);

            else if (defaultValue instanceof String)
                return (T) requestValue;

            else if (defaultValue instanceof Boolean)
                return (T) Boolean.valueOf(requestValue);

            else if (defaultValue instanceof Long)
                return (T) Long.valueOf(requestValue);

            return defaultValue;
        } catch (Exception ex) {
            Log.error(Log.Type.PARAMETER, ex, "Exception on getting request parameter with name=%s", key);
            return defaultValue;
        }
    }
}
