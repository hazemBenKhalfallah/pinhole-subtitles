package data;

import com.avaje.ebean.Ebean;
import models.email.EmailMetadata;
import models.email.EmailTracking;

import java.util.List;

/**
 * Track emails
 *
 * @author ayassinov
 */
public class BaseEmailTrackingData {


    public static void save(List<EmailTracking> emailsTracking) {
        final String sql = "INSERT INTO pin_email_tracking (etr_code, etr_email, etr_template, etr_status, " +
                " etr_error_message, etr_creationDate) VALUES (?,?,?,?,?,?)";

        final String sqlMetaData = "INSERT INTO pin_email_metadata (etr_code, emd_key, emd_value, emd_isglobal)" +
                " VALUES (?,?,?,?)";

        for (final EmailTracking emailTracking : emailsTracking) {
            Ebean.createSqlUpdate(sql).setLabel(emailTracking.code)
                    .setParameter(1, emailTracking.code)
                    .setParameter(2, emailTracking.email)
                    .setParameter(3, emailTracking.template)
                    .setParameter(4, emailTracking.status)
                    .setParameter(5, emailTracking.errorMessage)
                    .setParameter(6, emailTracking.creationDateTime).execute();
            for (final EmailMetadata emailMetadata : emailTracking.emailMetadatas) {
                Ebean.createSqlUpdate(sqlMetaData).setLabel(emailMetadata.code)
                        .setParameter(1, emailMetadata.code)
                        .setParameter(2, emailMetadata.key)
                        .setParameter(3, emailMetadata.value)
                        .setParameter(4, emailMetadata.isGlobal)
                        .execute();
            }
        }
    }

    /*public static void save(List<EmailTracking> emailsTracking) {
        Ebean.createSqlQuery()

        final String sql = "INSERT INTO pin_email_tracking (etr_code, etr_email, etr_template, etr_status, " +
                " etr_error_message, etr_creationDate) VALUES (?,?,?,?,?,?)";

        final String sqlMetaData = "INSERT INTO pin_email_metadata (etr_code, emd_key, emd_value, emd_isglobal)" +
                " VALUES (?,?,?,?)";



        Connection cnx = null;
        PreparedStatement statement = null;
        Statement statementMetadata = null;
        try {
            cnx = DB.getConnection();
            cnx.setAutoCommit(false);
            statement = cnx.prepareStatement(sql);
            statementMetadata = cnx.createStatement();

            for (final EmailTracking emailTracking : emailsTracking) {
                statement.setString(1, emailTracking.code);
                statement.setString(2, emailTracking.email);
                if (StringUtils.isNullOrEmpty(emailTracking.template))
                    statement.setNull(3, Types.CHAR);
                statement.setString(3, emailTracking.template);
                statement.setString(4, emailTracking.status);
                if (StringUtils.isNullOrEmpty(emailTracking.errorMessage))
                    statement.setNull(5, Types.CHAR);
                statement.setString(5, emailTracking.errorMessage);
                statement.setTimestamp(6, new java.sql.Timestamp(emailTracking.creationDateTime.getTime()));
                //for(emailTracking.emailMetadatas)
                statement.addBatch();
            }

            statement.executeBatch();
            cnx.commit();
        } catch (SQLException ex) {
            Logger.error("SEO error on updating the prerendred status", ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {
                }
            }

            if (cnx != null)
                try {
                    cnx.close();
                } catch (SQLException ignored) {
                }
        }
    } */

  /*  public static void save(List<EmailMetadata> emailsMetadata) {

    }
*/
}
