package services.oauth;

import core.StringUtils;
import core.log.Log;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

/**
 * @author ayassinov
 */
public class FacebookProvider {

    private static final String URL_USER_INFO = "https://graph.facebook.com/me?fields=id,name,hometown,username,email,gender,updated_time,picture";
    private static final String URL_ACCESS_KEY = "https://graph.facebook.com/oauth/access_token";
    private static final String URL_NOTIFICATION_POST = "https://graph.facebook.com/%s/notifications";

    public static String facebookAccessKey = "";
    private static OAuthService service;
    public static String facebookKey = "";
    public static String facebookSecret = "";

    public static void init(String key, String secret) {
        facebookKey = key;
        facebookSecret = secret;
        service = new ServiceBuilder()
                .provider(FacebookApi.class)
                .apiKey(key)
                .apiSecret(secret)
                .build();
    }

    public static FacebookResponse getSocialAccount(String token) {
        try {
            final OAuthRequest request = new OAuthRequest(Verb.GET, URL_USER_INFO);
            service.signRequest(new Token(token, ""), request);
            final Response response = request.send();
            return new FacebookResponse(response, token);
        } catch (Exception ex) {
            Log.error(Log.Type.ACCOUNT, ex, "Error when getting social account information from facebook token=%s", token);
            return null;
        }
    }

    public static Response sendNotification(String fbId, String message, String url) {
        final OAuthRequest request = new OAuthRequest(Verb.POST, String.format(URL_NOTIFICATION_POST, fbId));
        request.addQuerystringParameter("template", message);
        request.addQuerystringParameter("href", url);
        request.addQuerystringParameter("access_token", getAccessKey());
        return request.send();
    }

    private static String getAccessKey() {
        if (!StringUtils.isNullOrEmpty(facebookAccessKey))
            return facebookAccessKey;

        final OAuthRequest request = new OAuthRequest(Verb.GET, URL_ACCESS_KEY);
        request.addQuerystringParameter("client_id", facebookKey);
        request.addQuerystringParameter("client_secret", facebookSecret);
        request.addQuerystringParameter("grant_type", "client_credentials");
        final Response response = request.send();
        final String value = response.getBody(); //=> access_token=xxxxxxx
        if (response.isSuccessful() && value.contains("=")) {
            facebookAccessKey = value.split("=")[1];
        } else {
            Log.error(Log.Type.ACCOUNT, "Could not get application access key=%s", value);
        }
        return facebookAccessKey;
    }
}