package services.oauth;/*
 * @author ayassinov
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Optional;
import json.JsonSerializer;
import models.user.ProviderEnum;
import models.user.SocialAccount;
import org.scribe.model.Response;

/**
 * @author ayassinov
 */
public class FacebookResponse {

    public boolean isSuccessful;
    public SocialAccount socialAccount;
    public String error;

    public FacebookResponse(final Response response, String token) {
        final Optional<JsonNode> jsonBody = JsonSerializer.parse(response.getBody());
        if (jsonBody.isPresent()) { //body where parsed
            this.isSuccessful = response.isSuccessful(); //HTTP 200
            if (this.isSuccessful) {
                this.socialAccount = getSocialAccount(jsonBody.get());
                this.socialAccount.token = token;
            } else {
                this.error = jsonBody.get().path("error").path("message").asText();
            }
        } else {
            this.isSuccessful = false;
            this.error = "Error on paring the facebook body to a JSON object";
        }
    }

    public static SocialAccount getSocialAccount(JsonNode json) {
        final SocialAccount socialAccount = new SocialAccount();
        socialAccount.provider = ProviderEnum.FACEBOOK;
        socialAccount.userId = json.path("id").asText();
        socialAccount.fullName = json.path("name").asText();
        socialAccount.firstName = json.path("first_name").asText();
        socialAccount.lastName = json.path("last_name").asText();
        socialAccount.userName = json.path("username").asText();
        socialAccount.email = json.path("email").asText();
        socialAccount.gender = SocialAccount.GenderEnum.fromFacebook(json.path("gender").asText());
        return socialAccount;
    }
}
