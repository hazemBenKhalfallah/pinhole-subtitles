/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package services;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import com.google.common.base.Optional;
import core.cache.CacheUtil;
import models.resources.SqlColumn;

import static models.Language.LanguageCodesEnum;

/**
 * @author : hazem
 */
public class MessageService {

    public static String getMessage(String messageCode, LanguageCodesEnum languageCode) {
        //get from cache
        final Optional<String> messageCached = CacheUtil.get(getCacheKey(messageCode, languageCode));
        if (messageCached.isPresent()) {
            return messageCached.get();
        }
        final SqlRow row = Ebean.createSqlQuery("select m.* from pin_message m  " +
                " left join pin_language l on l.lng_id = m.lng_id " +
                " where m.msg_code = :code and l.lng_locale = :lngId ")
                .setParameter("code", messageCode)
                .setParameter("lngId", languageCode.getValue())
                .findUnique();

        //cache result
        CacheUtil.cache(getCacheKey(messageCode, languageCode), row.getString(SqlColumn.Message.VALUE), CacheUtil.Period.LONG); //1 hour
        return row.getString(SqlColumn.Message.VALUE);
    }

    private static String getCacheKey(String messageCode, LanguageCodesEnum languageCode) {
        return CacheUtil.Keys.i18_MESSAGE + languageCode.getValue() + "_" + messageCode;
    }
}
