package services;

import com.avaje.ebean.Ebean;
import com.google.common.base.Optional;
import core.parameter.ApplicationParameter;
import models.user.Location;
import play.mvc.Http;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ayassinov
 */
public class LocationService {

    private static Map<String, Location> locations = new HashMap<String, Location>();

    public static void init() {
        locations = Ebean.find(Location.class).findMap("code", String.class);
    }

    public static List<Location> listAll(){
          return Ebean.find(Location.class).findList();
    }

    public static Location getByIpAddressOrDefault() {
        final String ipAddress = Http.Context.current().request().remoteAddress();
        final Optional<String> codeFromIpAddress = getLocationCodeFromIpAddress(ipAddress);
        //code exist from ip address get it from the repo
        if (codeFromIpAddress.isPresent()) {
            final Location location = locations.get(codeFromIpAddress.get().toUpperCase());
            if (location != null)
                return location;
        }

        return locations.get(ApplicationParameter.LOCALE.defaultLocationCode.toUpperCase());
    }

    /**
     * Get a location code base on the ip address
     *
     * @param ipAddress the ip address
     * @return the location code if found
     */
    private static Optional<String> getLocationCodeFromIpAddress(String ipAddress) {
        // final String ipAddress  = Http.Context.current().request().remoteAddress();
        //todo: ayassinov Amal we need to get the location code from an Ip address.
        return Optional.of(ApplicationParameter.LOCALE.defaultLocationCode);
    }


}
