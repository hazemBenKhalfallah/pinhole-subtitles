/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package services;

/**
 * @author ayassinov
 */
public class BoxMessageFactory {

    /**
     * singleton instance
     */
    public static BoxMessageFactory ref;

    /**
     * internal dispatcher instance
     */
    private IBoxMessageDispatcher dispatcher;

    /**
     * private constructor
     */
    private BoxMessageFactory(IBoxMessageDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    /**
     * init the dispatcher
     *
     * @param dispatcher the implementation of the dispatcher
     */
    public static void init(IBoxMessageDispatcher dispatcher) {
        ref = new BoxMessageFactory(dispatcher);
    }

    /**
     * get the wrapped dispatcher
     */
    public IBoxMessageDispatcher getDispatcher() {
        return dispatcher;
    }
}
