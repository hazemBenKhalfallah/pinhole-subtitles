package services;

import com.avaje.ebean.Ebean;
import com.google.common.collect.Lists;
import core.StringUtils;
import core.parameter.ApplicationParameter;
import models.Language;
import play.mvc.Http;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Zied
 */
public class LanguageService {

    private static Map<String, Language> languages = new HashMap<String, Language>();

    public static void init() {
        languages = Ebean.find(Language.class).findMap("code", String.class);
    }

    /**
     * Get a language by the code like "fr, en"
     *
     * @param code the code of the language
     * @return return the default language if not found
     */
    public static Language getOrDefault(final String code) {
        if (!StringUtils.isNullOrEmpty(code)) {
            final Language language = languages.get(code);
            if (language != null)
                return language;
        }

        return languages.get(ApplicationParameter.LOCALE.defaultLanguageCode.toLowerCase());
    }

    public static Language getFromRequestOrDefault() {
        final String code = Http.Context.current().lang().code();
        return getOrDefault(code);
    }


    /**
     * @return : the list of supported languages within the application
     */
    public static List<Language> listAll() {
        return Lists.newArrayList(languages.values());
    }


}
