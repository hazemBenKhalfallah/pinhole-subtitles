/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.event;


import com.avaje.ebean.annotation.Sql;
import com.fasterxml.jackson.annotation.JsonIgnore;
import models.user.User;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

/**
 * doc:hazem
 *
 * @author : hazem
 */
@Entity
@Sql
public class RecommendationAggregate {
    private static final String SQL_SELECT = "select u.usr_id, count(r.*) > 0 as alreadyRecommended, r.rcm_creationdate\n" +
            "from pin_user_friend f\n" +
            "left join pin_user u on u.usr_id = f.usr_friend_id\n" +
            "left join pin_recommendation r on r.frd_id = f.usr_friend_id and r.usr_id = :userId and r.evt_id = :eventId \n" +
            "where f.usr_id = :userId \n" +
            "group by u.usr_id,r.rcm_creationdate;";

    @OneToOne
    @JsonIgnore
    public User user;
/*
    @JsonUnwrapped
    public UserJson userJson;*/

    @SuppressWarnings("unused")
    public Boolean alreadyRecommended;

    public Date date;

    @SuppressWarnings("unused")
    public RecommendationAggregate() {
    }


    /**
     * doc:hazem
     *
     * @param recommendationFriends recommendationFriends
     * @return Query<RecommendationAggregate>
     */
/*    public static Query<RecommendationAggregate> getQuery(RecommendationFriends recommendationFriends) {
        final String queryText = buildQuery(recommendationFriends);

        final RawSql rawSql = RawSqlBuilder
                .unparsed(queryText)
                .columnMapping("u.usr_id", "user.id")
                .columnMapping("alreadyRecommended", "alreadyRecommended")
                .columnMapping("r.rcm_creationdate", "date")
                .create();

        //create configured query
        return Ebean.find(RecommendationAggregate.class)
                .setRawSql(rawSql);
    }*/

/*    private static String buildQuery(RecommendationFriends recommendationFriends) {
        final String queryBuilder = SQL_SELECT
                .replaceAll(":userId", "" + recommendationFriends.userInSession.id)
                .replaceFirst(":eventId", "" + recommendationFriends.eventId);
        return queryBuilder;
    }*/
/*
    *//**
     * used to convert User to  UserJson
     *//*
    final public static Function<RecommendationAggregate, RecommendationAggregate> convertUserToUserJson = new Function<RecommendationAggregate, RecommendationAggregate>() {
        @Nullable
        @Override
        public RecommendationAggregate apply(@Nullable RecommendationAggregate recommendationAggregate) {
            recommendationAggregate.userJson = new UserJson((recommendationAggregate.user));
            return recommendationAggregate;
        }
    };*/


}