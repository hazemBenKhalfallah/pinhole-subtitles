/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.event;

import models.category.Category;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * An Event that is related to another event that will be broadcasted in the future and share at least one category
 *
 * @author ayassinov
 */
@Table(name = "pin_related_event")
@Entity
public class RelatedEvent extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "evt_id", insertable = false, updatable = false)
    public Event event;

    @ManyToOne
    @JoinColumn(name = "evt_related_id", insertable = false, updatable = false)
    public Event relatedEvent;

    @ManyToOne
    @JoinColumn(name = "cat_id", nullable = false)
    public Category category;

    /**
     * Default constructor
     */
    public RelatedEvent() {
    }

    /**
     * Constructor with Mandatory fields
     *
     * @param event        the event
     * @param relatedEvent the event to be related
     * @param category     the category that link the two events
     */
    public RelatedEvent(Event event, Event relatedEvent, Category category) {
        this();
        this.id = new Id(event, relatedEvent);
        this.event = event;
        this.relatedEvent = relatedEvent;
        this.category = category;
        this.event.relatedEvents.add(this);
        this.relatedEvent.relatedEvents.add(this);
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "evt_id")
        public Long eventId;

        @Column(name = "evt_related_id")
        public Long relatedEventId;

        public Id() {
        }

        public Id(Event event, Event relatedEvent) {
            this.eventId = event.id;
            this.relatedEventId = relatedEvent.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (eventId != null ? !eventId.equals(id.eventId) : id.eventId != null) return false;
            //noinspection RedundantIfStatement
            if (relatedEventId != null ? !relatedEventId.equals(id.relatedEventId) : id.relatedEventId != null)
                return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = eventId != null ? eventId.hashCode() : 0;
            result = 31 * result + (relatedEventId != null ? relatedEventId.hashCode() : 0);
            return result;
        }
    }
}
