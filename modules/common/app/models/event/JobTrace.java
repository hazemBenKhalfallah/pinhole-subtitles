/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.event;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Class representing a tracing job
 */
@Entity
@Table(name = "pin_trace")
public class JobTrace extends Model {
    //select trc_id, trc_obj, trc_txt, trc_date from pin_trace
    @Id
    @Column(name = "trc_id")
    public Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "evt_id")
    public Event event;

    @Column(name = "trc_txt", nullable = false, columnDefinition = "TEXT")
    public String message;

    @Column(name = "trc_date", nullable = false)
    public Date operationDate;

    /**
     * Default constructor
     */
    public JobTrace() {

    }

}
