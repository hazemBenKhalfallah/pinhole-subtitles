/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.event;

import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.annotation.JsonIgnore;
import core.DateTimeUtils;
import models.box.TvBoxWatch;
import models.extra.Ads;
import models.media.HasMedia;
import models.resources.*;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represent a TV channel. The a unique identifier is a DVB Triplet.
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_channel")
public class Channel extends Model implements HasMedia {

    @Id
    @Column(name = SqlColumn.Channel.CHANNEL_ID)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = SqlColumn.Channel.NAME, nullable = false, length = FieldLength.Channel.NAME)
    @Constraints.MaxLength(FieldLength.Channel.NAME)
    @Constraints.Required
    public String name;

    @Column(name = SqlColumn.Channel.DVB_TRIPLET, nullable = false, unique = true, length = FieldLength.Channel.DVB_TRIPLET)
    @Constraints.MaxLength(FieldLength.Channel.DVB_TRIPLET)
    @Constraints.Required
    public String dvbTriplet;

    @Constraints.Required
    @Column(name = SqlColumn.Channel.IMAGE_ALT, nullable = false, length = FieldLength.Channel.IMAGE_ALT)
    public String altText;

    // image used for events when there is no image
    @Column(name = SqlColumn.Channel.URL_FRAGMENT, nullable = false, length = FieldLength.Channel.URL_FRAGMENT, columnDefinition = "default '/channels/'")
    public String defaultUrlFragment;


    @Constraints.Required
    @Column(name = SqlColumn.Channel.VANITY_URL, nullable = false, length = FieldLength.Channel.VANITY_URL)
    public String vanityUrl;

    @Column(name = SqlColumn.Channel.IMAGE_EXISTS, nullable = false, columnDefinition = "default false")
    @JsonIgnore
    public Boolean imageExists = false;

    @Column(name = SqlColumn.Channel.IS_FEATURED, nullable = false, columnDefinition = "default false")
    public boolean isFeatured;

    @Column(name = SqlColumn.Channel.FEATURED_ORDER, nullable = false, columnDefinition = "default 1")
    public int featuredOrder;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.Channel.LAST_MODIFIED_TIME, nullable = false, columnDefinition = "default current_timestamp")
    public Date lastModifiedTime;

    @Column(name= SqlColumn.Channel.IN_BROWSE, nullable =false , columnDefinition = "default false")
    public boolean inBrowse;

    @Column(name= SqlColumn.Channel.YOUTUBE_USER)
    public String youtubeUser;

    @Column(name= SqlColumn.Channel.YOUTUBE_ID)
    public String youtubeId;

    @Column(name= SqlColumn.Channel.YOUTUBE_ENABLED , columnDefinition = "default false")
    public boolean youtubeEnabled;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = SqlColumn.Channel.YOUTUBE_PUBLISHED_AFTER)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm:ss")
    public Date youtubePublishAfter;

    @OneToMany(mappedBy = "channel", fetch = FetchType.LAZY)
    public List<Event> events = new ArrayList<Event>();

    @OneToMany(mappedBy = "channel", fetch = FetchType.LAZY)
    public List<Ads> adses = new ArrayList<Ads>();

    @OneToMany(mappedBy = "channel")
    public List<TvBoxWatch> watchedChannels = new ArrayList<TvBoxWatch>();

    /**
     * Default constructor
     */
    public Channel() {
        this.isFeatured = false;
        this.featuredOrder = 1;
        this.lastModifiedTime = DateTimeUtils.currentDateWithoutSeconds();
    }

    /**
     * A partial constructor for box communication without image URL
     *
     * @param name       the channel name
     * @param dvbTriplet the dvb triplet
     */
    public Channel(String name, String dvbTriplet) {
        this();
        this.name = name;
        this.altText = name;
        this.dvbTriplet = dvbTriplet;
    }


    public Channel(Long id) {
        this();
        this.id = id;
    }

    public Channel(SqlRow row) {
        this();
        this.id = row.getLong(SqlColumn.Channel.CHANNEL_ID);
        this.name = row.getString(SqlColumn.Channel.NAME);
        this.youtubeId = row.getString(SqlColumn.Channel.YOUTUBE_ID);
        this.youtubePublishAfter = row.getDate(SqlColumn.Channel.YOUTUBE_PUBLISHED_AFTER);
    }

    /**
     * Split the the string representing dvb triplet into an array of string
     *
     * @return an array of string representing respectively sid, tsid, onid
     */
    public String[] splitDvbTriplet() {
        return dvbTriplet.split("_");
    }
}
