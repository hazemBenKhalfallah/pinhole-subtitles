/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.event;

import com.avaje.ebean.SqlRow;
import com.avaje.ebean.annotation.EnumValue;
import com.avaje.ebean.annotation.Formula;
import core.DateTimeUtils;
import core.StringUtils;
import models.category.Category;
import models.category.CategoryEvent;
import models.extra.Extra;
import models.media.HasMedia;
import models.planning.Plan;
import models.planning.PlanningEvent;
import models.planning.ShowTime;
import models.poll.Poll;
import models.resources.FieldLength;
import models.resources.SqlColumn;
import models.user.UserEventSubscription;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represent a TV event that occur on a channel at a given date for a given period.
 *
 * @author ayassinov
 * @author hazem
 */
@Entity
@Table(name = "pin_event")
public class Event extends Model implements HasMedia {

    @Id
    @Column(name = SqlColumn.Event.EVENT_ID)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = SqlColumn.Event.TITLE, nullable = false, length = FieldLength.Event.TITLE)
    @Constraints.MaxLength(FieldLength.Event.TITLE)
    @Constraints.Required
    public String title;

    @Column(name = SqlColumn.Event.TITLE_BOX, nullable = false, length = FieldLength.Event.TITLE_BOX)
    @Constraints.MaxLength(FieldLength.Event.TITLE_BOX)
    public String titleBox;

    @Constraints.Required
    @Constraints.MaxLength(FieldLength.Event.SHORT_DESCRIPTION)
    @Column(name = SqlColumn.Event.SHORT_DESCRIPTION, nullable = false, length = FieldLength.Event.SHORT_DESCRIPTION)
    public String shortDescription;

    @Column(name = SqlColumn.Event.DESCRIPTION, nullable = false, columnDefinition = "TEXT")
    @Constraints.Required
    public String description;

    @Column(name = SqlColumn.Event.TIMEZONE, length = FieldLength.Event.TIMEZONE, nullable = false)
    public String timeZone;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = SqlColumn.Event.BEGIN_TIME, nullable = false)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date beginTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = SqlColumn.Event.END_TIME, nullable = false)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date endTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.Event.START_DATE_TIME_GMT, nullable = false)
    public Date beginTimeSystem;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.Event.END_DATE_TIME_GMT, nullable = false)
    public Date endTimeSystem;

    @Constraints.MaxLength(FieldLength.Event.HASHTAG)
    @Constraints.Required
    @Column(name = SqlColumn.Event.HASHTAG, nullable = false, length = FieldLength.Event.HASHTAG)
    public String twitterTag;

    @Column(name = SqlColumn.Event.URL, nullable = false, unique = true)
    public String vanityUrl;

    @ManyToOne
    @JoinColumn(name = SqlColumn.ShowTime.SHOW_TIME_ID, nullable = true)
    public ShowTime currentShowTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SqlColumn.Channel.CHANNEL_ID, nullable = false)
    @Constraints.Required
    public Channel channel;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.Event.LAST_MODIFIED, nullable = false, columnDefinition = "default current_timestamp")
    public Date lastModifiedTime;

    @Column(name = SqlColumn.Event.IS_FEATURED, nullable = false, columnDefinition = "default false")
    public boolean isFeatured;

    @Column(name = SqlColumn.Event.ENABLE_TWITTER, columnDefinition = "boolean default false")
    public Boolean enableTwitter;

    @OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
    public List<Plan> plans;

    @OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
    public List<UserEventWatch> watchedUsers = new ArrayList<UserEventWatch>();

    @OneToMany(mappedBy = "event", fetch = FetchType.EAGER)
    public List<CategoryEvent> categories = new ArrayList<CategoryEvent>();

    @OneToMany(mappedBy = "event", fetch = FetchType.EAGER)
    public List<RelatedEvent> relatedEvents = new ArrayList<RelatedEvent>();

    @OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
    public List<Recommendation> recommendations = new ArrayList<Recommendation>();

    @OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
    public List<Poll> polls = new ArrayList<Poll>();

    @OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
    public List<Extra> extras = new ArrayList<Extra>();

    @OneToMany(mappedBy = "event", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    public List<ShowTime> showTimes = new ArrayList<ShowTime>();

    @OneToMany(mappedBy = "event")
    public List<UserEventSubscription> subscriptions = new ArrayList<UserEventSubscription>();

    @Formula(select = "COALESCE(ns.min_show = ${ta}.show_id, false) as isNew", join = "left outer join (select min(show_id) min_show, evt_id from pin_showtime s left join pin_planning_event e on e.plan_evt_id = s.plan_evt_id where e.plan_evt_type <> 3 group by evt_id) as ns on ns.evt_id = ${ta}.evt_id")
    public boolean isNew = false;

    @Formula(select = "COALESCE(bz.score, 0) as commentPerMinute", join = "left outer join (select evt_id, score from pin_view_buzz) as bz on bz.evt_id = ${ta}.evt_id")
    public Double score;

    //HasEpisodes
    @Formula(select = "COALESCE(show.showCount, 0) as showCount", join = "left outer join (select evt_id, count(show_id) as showCount from pin_showtime group by evt_id) as show on show.evt_id = ${ta}.evt_id")
    public int episodesCount;

    @Formula(select = "COALESCE(sb.subscribers, 0) as subscribersCount", join = "left outer join (select count(s.usr_id) as subscribers, s.evt_id from pin_usr_event_subs s where s.evs_enddate is null group by s.evt_id) as sb on sb.evt_id = ${ta}.evt_id")
    public int subscribers = 0;

    @Formula(select = "COALESCE(sh.shareCount, 0) as shareCount", join = "LEFT JOIN (select count(usr_id) as shareCount, evt_id, show_id FROM pin_usr_evt_share group by usr_id, evt_id, show_id) sh on sh.evt_id = ${ta}.evt_id and sh.show_id = ${ta}.show_id")
    public int shareCount = 0;

    @Transient
    public boolean hasImage = false;

    @Transient
    public boolean hasHeroImage = false;

    @Enumerated(EnumType.STRING)
    @Column(name = SqlColumn.Event.HERO_STATUS, nullable = false, columnDefinition = "default none")
    public HeroStatusEnum heroStatusEnum;

    public Event() {
        final Date initDate = DateTimeUtils.getMinDate(); //minimal date
        this.timeZone = DateTimeUtils.TimeZoneMap.DEFAULT;
        this.beginTime = initDate;
        this.endTime = initDate;
        this.beginTimeSystem = initDate;
        this.endTimeSystem = initDate;
        this.lastModifiedTime = initDate;
        this.isFeatured = false;
        this.heroStatusEnum = HeroStatusEnum.NONE;
    }

    public Event(long id) {
        this();
        this.id = id;
    }


    public Event(SqlRow row) {
        this();
        this.id = row.getLong(SqlColumn.Event.EVENT_ID);
        this.title = row.getString(SqlColumn.Event.TITLE);
        this.currentShowTime = new ShowTime();
        this.currentShowTime.id = row.getLong(SqlColumn.ShowTime.SHOW_TIME_ID);
        this.channel = new Channel();
        this.channel.id = row.getLong(SqlColumn.Channel.CHANNEL_ID);
        this.channel.name = row.getString(SqlColumn.Channel.NAME);
        this.channel.dvbTriplet = row.getString(SqlColumn.Channel.DVB_TRIPLET);
        this.channel.id = row.getLong(SqlColumn.Channel.CHANNEL_ID);
    }

    /**
     * Check if a category already linked to the event.
     *
     * @return true if the category exists
     */
    public boolean isCategoryExist(Category category) {
        for (CategoryEvent categoryEvent : categories) {
            if (categoryEvent.category.id.equals(category.id))
                return true;
        }
        return false;
    }

    public List<String> getTags() {
        List<String> node = new ArrayList<String>();
        if (this.currentShowTime != null && this.currentShowTime.planningEvent != null) {
            if (this.currentShowTime.planningEvent.type.equals(PlanningEvent.PlanningTypeEnum.LIVE.getValue()))
                node.add("live");

            else if (this.currentShowTime.planningEvent.type.equals(PlanningEvent.PlanningTypeEnum.REPLAY.getValue()))
                node.add("replay");
        }

        if (isFeatured)
            node.add("featured");

        if (isNew)
            node.add("new");

        return node;
    }

    /**
     * Call this method before any save or update on the event.
     */
    public Event updateFieldsBeforeSave() {
        //vanity url
        if (StringUtils.isNullOrEmpty(this.vanityUrl)) {  //no vanity url info copy from title
            this.vanityUrl = StringUtils.simplifyString(this.title);
        } else {
            this.vanityUrl = StringUtils.simplifyString(vanityUrl); //make vanity url conform
        }

        //title box
        if (StringUtils.isNullOrEmpty(this.titleBox)) {
            this.titleBox = this.title;
        } else {
            this.titleBox = this.titleBox.trim();
        }

        //twitter
        if (this.enableTwitter == null) {
            this.enableTwitter = false;
        }

        //twitter tag
        this.twitterTag = StringUtils.formatTwitterTag(this.twitterTag);
        return this;
    }

    public enum HeroStatusEnum {
        @EnumValue(value = "none")
        NONE,
        @EnumValue(value = "both")
        BOTH,
        @EnumValue(value = "small")
        SMALL
    }
}
