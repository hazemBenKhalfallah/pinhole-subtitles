
/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.event;


import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Sql;
import com.fasterxml.jackson.annotation.JsonIgnore;
import core.DateTimeUtils;
import core.StringUtils;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;
import java.util.List;

/**
 * doc:hazem
 *
 * @author : hazem
 */
@Entity
@Sql
public class TrackedEventAggregate {
    private static final String SQL_SELECT = "SELECT  e.evt_id as id, e.show_id as showId , e.evt_twitterTag as hashtag , e.evt_sysbegintime as beginDate, e.evt_sysEndTime as endDate\n" +
            "FROM    pin_event e \n" +
            "WHERE " +
            "1 = 1" +
            "and e.evt_enableTwitter = true\n" +
            "and e.show_id is not null \n" +
            "%s \n" +
            "order by e.evt_id";

    private static final String SQL_EXCLUDED_TWITTER_ENABLED_EVENTS = "AND NOT EXISTS ( SELECT  ee.evt_id \n" +
            "FROM    pin_event ee \n" +
            "WHERE   ee.evt_id in  %s \n" +
            "AND ee.evt_id = e.evt_id )";

    @OneToOne
    @JsonIgnore
    public Event event;


    @SuppressWarnings("unused")
    public TrackedEventAggregate() {
    }

/*    *//**
     * doc:hazem
     *
     * @param trackedEventAggregates List of trackedEventAggregate
     * @param periodBeforeStart      periodBeforeStart
     * @return List of TrackedEvent
     *//*
    public static List<TrackedEvent> extract(List<TrackedEventAggregate> trackedEventAggregates, int periodBeforeStart) {
        final List<TrackedEvent> trackedEvents = new ArrayList<TrackedEvent>();
        for (TrackedEventAggregate trackedEventAggregate : trackedEventAggregates) {
            trackedEvents.add(new TrackedEvent(trackedEventAggregate.event, periodBeforeStart));
        }
        return trackedEvents;
    }*/


    /**
     * doc:hazem
     */
    public static Query<TrackedEventAggregate> getQuery(List<Long> excludedIds, Date endDate, int beginTwitterTrackingDuration) {

        final String queryText = buildQuery(excludedIds);

        final RawSql rawSql = RawSqlBuilder
                .parse(queryText)
                .columnMapping("e.evt_id", "event.id")
                .columnMapping("e.show_id", "event.currentShowTime.id")
                .columnMapping("e.evt_twitterTag", "event.twitterTag")
                .columnMapping("e.evt_sysbegintime", "event.beginTimeSystem")
                .columnMapping("e.evt_sysEndTime", "event.endTimeSystem")
                .create();

        //create configured query
        return Ebean.find(TrackedEventAggregate.class)
                .setRawSql(rawSql)
                .where()
                .le("e.evt_sysbegintime", DateTimeUtils.addMinutes(endDate, beginTwitterTrackingDuration))
                .gt("e.evt_sysbegintime", endDate)
                .query();
    }

    private static String buildQuery(List<Long> excludedIds) {
        final String filter = excludedIds.size() > 0 ? String.format(SQL_EXCLUDED_TWITTER_ENABLED_EVENTS, StringUtils.toSqlArray(excludedIds)) : "";
        return String.format(SQL_SELECT, filter);
    }

}