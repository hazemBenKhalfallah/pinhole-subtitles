/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.event;

import core.DateTimeUtils;
import models.planning.ShowTime;
import models.user.User;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Represent a period of time a user watched an event
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_user_watch")
public class UserEventWatch extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JoinColumn(name = "evt_id", insertable = false, updatable = false)
    public Event event;

    @ManyToOne
    @JoinColumn(name = "show_id", insertable = false, updatable = false)
    public ShowTime showTime;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "uwt_endDate")
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date endDate;

    /**
     * Default constructor
     */
    public UserEventWatch() {
    }

    /**
     * A Constructor representing a user watching an event in the current time
     *
     * @param user     the user
     * @param showTime the current showTime
     */
    public UserEventWatch(User user, ShowTime showTime) {
        this(user, showTime, DateTimeUtils.now());
    }

    /**
     * A Constructor representing a user watching an event starting from a given date
     *
     * @param user      the user
     * @param showTime  the showTime
     * @param beginDate the date when user start watching
     */
    public UserEventWatch(User user, ShowTime showTime, Date beginDate) {
        this();
        this.id = new Id(user, showTime, beginDate);
        this.user = user;
        this.showTime = showTime;
        this.event = showTime.event;

        this.user.watchedEvents.add(this);
        this.event.watchedUsers.add(this);
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "usr_id")
        public Long userId;

        @Column(name = "evt_id")
        public Long eventId;

        @Column(name = "show_id")
        public Long showTimeId;

        @Temporal(value = TemporalType.DATE)
        @Column(name = "uwt_beginDate")
        @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
        public Date beginDate;

        public Id() {
        }

        public Id(User user, ShowTime showTime, Date beginDate) {
            this();
            this.userId = user.id;
            this.eventId = showTime.event.id;
            this.showTimeId = showTime.id;
            this.beginDate = beginDate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (beginDate != null ? !beginDate.equals(id.beginDate) : id.beginDate != null) return false;
            if (eventId != null ? !eventId.equals(id.eventId) : id.eventId != null) return false;
            if (showTimeId != null ? !showTimeId.equals(id.showTimeId) : id.showTimeId != null) return false;
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = userId != null ? userId.hashCode() : 0;
            result = 31 * result + (eventId != null ? eventId.hashCode() : 0);
            result = 31 * result + (showTimeId != null ? showTimeId.hashCode() : 0);
            result = 31 * result + (beginDate != null ? beginDate.hashCode() : 0);
            return result;
        }
    }
}
