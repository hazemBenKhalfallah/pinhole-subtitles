package models.event;

import models.category.Category;

/**
 * @author hazem
 */
public enum VanityUrlTypeEnum {
    EVENT("Event"),
    CATEGORY("Category"),
    CHANNEL("Channel");
    private final String value;

    VanityUrlTypeEnum(String value) {
        this.value = value;
    }

    /**
     * Returns the Type of the vanity url related to the beanType
     *
     * @param beanType type of the vanity url (Event.class, Category.class or Channel.class)
     * @return type of bean
     */
    public static String getValue(Class<?> beanType) {
        if (beanType.equals(Event.class))
            return VanityUrlTypeEnum.EVENT.getValue();
        if (beanType.equals(Category.class))
            return VanityUrlTypeEnum.CATEGORY.getValue();
        if (beanType.equals(Channel.class))
            return VanityUrlTypeEnum.CHANNEL.getValue();
        else return "";
    }

    public String getValue() {
        return value;
    }
}
