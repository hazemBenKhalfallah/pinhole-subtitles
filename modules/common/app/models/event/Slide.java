/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import core.DateTimeUtils;
import core.StringUtils;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_slide")
public class Slide extends Model {
    @Id
    @Column(name = "sld_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    public Long id;

    @Version
    @JsonIgnore
    public Long version;

    @Column(name = "sld_title", nullable = false, length = 80)
    @Constraints.MaxLength(80)
    public String title;

    @Constraints.MaxLength(150)
    @Column(name = "sld_description", nullable = false, length = 150)
    public String description;

    @Column(name = "sld_priority")
    @Constraints.Required
    @JsonIgnore
    public int priority;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sld_creationDate", nullable = false)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @JsonIgnore
    public Date creationDate;

    @Column(name = "sld_inactive", nullable = false, columnDefinition = "boolean default false")
    @JsonIgnore
    public Boolean inactive = false;

    @Transient
    public int order = 0;

    @ManyToOne
    @JoinColumn(name = "evt_id", nullable = false)
    @JsonIgnore
    public Event event;

    @Transient
    public Boolean isRtlTitle = false;
    @Transient
    public Boolean isRtlDescription = false;

    public Slide() {
        this.creationDate = DateTimeUtils.now();
    }

    public void updateRtlFields() {
        if (!StringUtils.isNullOrEmpty(this.title)) {
            isRtlTitle = StringUtils.isRtl(this.title);
        }
        if (!StringUtils.isNullOrEmpty(this.description)) {
            isRtlDescription = StringUtils.isRtl(this.description);
        }
    }
}
