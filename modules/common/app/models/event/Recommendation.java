/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.event;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import core.DateTimeUtils;
import models.user.User;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A recommendation of an event from a user to his friend.
 * It's can be also a recommendation of an event from pinhole to a user based on his categories
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_recommendation")
public class Recommendation extends Model {

    @EmbeddedId
    @javax.persistence.Id
    @JsonIgnore
    public Id id = new Id();


    @Column(name = "rcm_uid", nullable = false, unique = true)
    @JsonIgnore
    public String uniqueId;

    @Version
    @JsonIgnore
    public Long version;

    @Column(name = "rcm_comment")
    public String comment;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    @JsonBackReference
    public User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "frd_id", insertable = false, updatable = false)
    @JsonBackReference
    public User friend;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "evt_id", insertable = false, updatable = false)
    @JsonBackReference
    public Event event;

    @Column(name = "rcm_creationDate", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    public Date dateCreation;

    @Column(name = "rcm_seenDate")
    @Temporal(value = TemporalType.TIMESTAMP)
    public Date dateSeen;

    @Column(name = "rcm_sendDate")
    @Temporal(value = TemporalType.TIMESTAMP)
    public Date sendDate;

    @Column(name = "rcm_keepNotification", nullable = false)
    public boolean keepNotification;

    @Column(name = "rcm_isDirty", nullable = false)
    public boolean isDirty;

    /**
     * Represent a list of user how sent the same recommendation to the current user
     * This used to avoid duplication on the UI when listing all recommendation.
     */
    @Transient
    public List<User> groupedUsers = new ArrayList<User>();

    /**
     * Default constructor
     */
    public Recommendation() {
        this.isDirty = false;
        this.keepNotification = false;
        this.dateCreation = DateTimeUtils.now();
    }

    /**
     * Constructor with mandatory fields
     *
     * @param user   the user how sent the recommendation
     * @param friend the friend to be notified about the recommendation
     * @param event  the event
     */
    public Recommendation(User user, User friend, Event event) {
        this();
        this.id = new Id(user, friend, event);
        this.user = user;
        this.friend = friend;
        this.event = event;
        this.uniqueId = String.format("%s%s%s", event.id, user.id, friend.id);
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "usr_id")
        public Long userId;

        @Column(name = "frd_id")
        public Long frdId;

        @Column(name = "evt_id")
        public Long eventId;

        public Id() {
        }

        public Id(User user, User friend, Event event) {
            this.userId = user.id;
            this.frdId = friend.id;
            this.eventId = event.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (eventId != null ? !eventId.equals(id.eventId) : id.eventId != null) return false;
            if (frdId != null ? !frdId.equals(id.frdId) : id.frdId != null) return false;
            //noinspection RedundantIfStatement
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = userId != null ? userId.hashCode() : 0;
            result = 31 * result + (frdId != null ? frdId.hashCode() : 0);
            result = 31 * result + (eventId != null ? eventId.hashCode() : 0);
            return result;
        }
    }

    /**
     * Check if a recommendation is already sent by another user
     *
     * @param user the current user
     * @return true if grouped user exist
     *         todo:hazem we may use an sql request to group by recommendation for different user.
     */
    public boolean isGroupedUserExist(User user) {
        for (User u : groupedUsers) {
            if (u.id.equals(user.id))
                return true;
        }

        return false;
    }
}
