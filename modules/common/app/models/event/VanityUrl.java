package models.event;

import models.resources.*;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_vanity_url")
public class VanityUrl extends Model {

    @Id
    @Column(name = SqlColumn.VanityUrl.VANITY_URL_ID)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = SqlColumn.VanityUrl.URL, unique = true, nullable = false)
    @Constraints.MaxLength(250)
    public String url;

    @Column(name = SqlColumn.VanityUrl.TYPE, nullable = false, columnDefinition = "default ''")
    public String type;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SqlColumn.VanityUrl.REDIRECT_ID)
    public VanityUrl redirect;

    @OneToMany(mappedBy = "redirect", fetch = FetchType.LAZY)
    public List<VanityUrl> oldVanityUrls = new ArrayList<VanityUrl>();

    public VanityUrl() {
    }

    public VanityUrl(String url, Class beanType) {
        this();
        this.url = url;
        this.type = VanityUrlTypeEnum.getValue(beanType);
    }
}
