package models.event;

import core.DateTimeUtils;
import models.user.User;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_grid_view")
public class GridView extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "grd_id", nullable = false)
    public Long id;

    @Version
    public Long version;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usr_id")
    public User user;

    //@OneToMany(mappedBy = "gridView")
    public List<Event> events;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "grd_create_date", nullable = false)
    public Date createDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "grd_update_date", nullable = false)
    public Date updateDate;

    public GridView() {
        this.createDate = DateTimeUtils.now();
        this.updateDate = DateTimeUtils.now();
    }

    public GridView(User user, List<Event> events, Date createDate) {
        this.user = user;
        this.events = events;
        this.createDate = createDate;
    }
}
