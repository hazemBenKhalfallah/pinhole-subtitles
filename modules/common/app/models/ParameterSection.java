/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_param_section")
public class ParameterSection extends Model {
    @Id
    @Column(name = "sec_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
    @Version
    public Long version;

    @Column(name = "sec_name", nullable = false, unique = true, length = 30)
    @Constraints.Required
    public String name;

    @Column(name = "sec_description", columnDefinition = "TEXT")
    @Constraints.Required
    public String description;

}
