/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.planning;

import com.avaje.ebean.Ebean;
import core.DateTimeUtils;
import core.log.Log;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author hazem
 */
@Entity
@Table(name = "pin_planning_Day")
public class PlanningDay extends Model {
    @Id
    @Column(name = "plan_day_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "plan_day_dow", nullable = false)
    @Constraints.Required
    public Integer dayOfWeek;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "plan_day_sysbegindate", nullable = false)
    @Formats.DateTime(pattern = "HH:mm")
    @Constraints.Required
    public Date sysBeginTime;

    @Column(name = "plan_day_duration", nullable = false)
    @Constraints.Required
    @Constraints.Min(1)
    public Integer duration;

    @ManyToOne
    @JoinColumn(name = "plan_evt_id", nullable = false)
    public PlanningEvent planningEvent;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "planningDay")
    public PlanningReplay planningReplay;

    @Transient
    public Date sysEndTime;

    @Transient
    public List<PlanningDay> overlapDays = new ArrayList<PlanningDay>();

    /**
     * Default Constructor
     */
    public PlanningDay() {
        this.dayOfWeek = DayOfWeek.MONDAY;
    }

    /**
     * Constructor using the identifier
     *
     * @param id the identifier of the planning day
     */
    public PlanningDay(Long id) {
        this();
        this.id = id;
    }

    /**
     * Constructor using planning event
     *
     * @param planningEvent the planning event instance
     */
    public PlanningDay(PlanningEvent planningEvent) {
        this();
        this.planningEvent = planningEvent;
    }


    public PlanningDay(Integer dayOfWeek, Date sysBeginTime, Integer duration, PlanningEvent planningEvent) {
        this.dayOfWeek = dayOfWeek;
        this.sysBeginTime = sysBeginTime;
        this.duration = duration;
        this.planningEvent = planningEvent;
    }

    public PlanningDay convertDatesToUserTimezone(String timeZone) {
        this.sysBeginTime = DateTimeUtils.convertFromSystemTimeZone(this.sysBeginTime, timeZone);
        return this;
    }

    public PlanningDay convertToSystemTimeZone(String timeZone) {
        this.sysBeginTime = DateTimeUtils.convertToSystemTimeZone(this.sysBeginTime, timeZone);
        return this;
    }

    @Transient
    public Boolean replayExists() {  //todo:yassine refactor this
        try {
            if (this.id == null || this.planningReplay == null || this.planningReplay.id == null)
                return false;
            Ebean.refresh(this.planningReplay);
            final PlanningEvent planningEvent = this.planningReplay.planningEvent;
            return !(planningEvent.planningDays == null || planningEvent.planningDays.isEmpty());
        } catch (Exception ex) {
            Log.error(Log.Type.UTILS, ex, "Error while verifying existence of replays in PlanningDay.replayExists");
            return false;
        }
    }

    public boolean areOverlapsed(PlanningDay that) {
        //calculate end time
        this.sysEndTime = DateTimeUtils.addMinutes(this.sysBeginTime, this.duration);
        that.sysEndTime = DateTimeUtils.addMinutes(that.sysBeginTime, that.duration);

        //begin in the same time
        if (DateTimeUtils.isEqual(this.sysBeginTime, that.sysBeginTime)) {
            return true;
        }

        //begin date between the begin and end date of the other (we permit two days to end begin in the same instant)
        if (DateTimeUtils.isBetween(this.sysBeginTime, that.sysBeginTime, that.sysEndTime, false)) {
            return true;
        }

        //end date between the begin and end date of the other (we permit two days to end begin in the same instant)
        if (DateTimeUtils.isBetween(this.sysEndTime, that.sysBeginTime, that.sysEndTime, false)) {
            return true;
        }

        //is that within this or is this within that
        //noinspection RedundantIfStatement
        if (DateTimeUtils.isWithin(this.sysBeginTime, this.sysEndTime, that.sysBeginTime, that.sysEndTime, true))
            return true;
        return false;
    }
}
