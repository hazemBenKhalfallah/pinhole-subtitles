/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.planning;

import com.avaje.ebean.SqlRow;
import com.avaje.ebean.annotation.Formula;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import core.DateTimeUtils;
import models.comment.Comment;
import models.event.Event;
import models.extra.Extra;
import models.extra.UserExtraWatch;
import models.media.HasMedia;
import models.poll.Poll;
import models.resources.SqlColumn;
import models.user.UserEventShare;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author hazem
 *         todo:hazem refactor this
 */
@Entity
@Table(name = "pin_showTime")
public class ShowTime extends Model implements Comparable<ShowTime>, HasMedia {

    @Id
    @Column(name = "show_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "show_ref", nullable = false)
    @Constraints.Required
    @Constraints.Min(0)
    public Integer showTimeRef;

    @Column(name = "show_title", length = 80)
    @Constraints.MaxLength(80)
    public String title;

    @Constraints.MaxLength(150)
    @Column(name = "show_shortDescription", length = 150)
    public String shortDescription;

    @Column(name = "show_description", columnDefinition = "TEXT")
    public String description;

    @Column(name = "show_imageUrl", length = 250)
    @Constraints.MaxLength(250)
    public String imageUrl;

    @Column(name = "show_smallImageUrl", length = 250)
    @Constraints.MaxLength(250)
    public String imageSmallUrl;

    @Column(name = "show_url_fragment", length = 255)
    @Constraints.MaxLength(255)
    public String showUrlFragment;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "show_sysBeginTime", nullable = false)
    @Constraints.Required
    public Date beginTimeSystem;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "show_sysEndTime", nullable = false)
    @Constraints.Required
    public Date endTimeSystem;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "evt_id", nullable = false)
    @Constraints.Required
    @JsonIgnore
    public Event event;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "show_live_id", nullable = true)
    @JsonIgnore
    public ShowTime liveShowTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ext_id", nullable = true)
    public Extra extra;

    @OneToMany(mappedBy = "liveShowTime", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JsonIgnore
    public List<ShowTime> replayShowTimes = new ArrayList<ShowTime>();

    @OneToMany(mappedBy = "showtime")
    @JsonManagedReference
    public List<Comment> comments = new ArrayList<Comment>();

    @OneToMany(mappedBy = "showtime", fetch = FetchType.EAGER)
    @JsonManagedReference
    public List<Extra> extras = new ArrayList<Extra>();

    @OneToMany(mappedBy = "showTime")
    @JsonManagedReference
    public List<UserExtraWatch> userExtraWatches = new ArrayList<UserExtraWatch>();

    @OneToMany(mappedBy = "showTime")
    @JsonManagedReference
    public List<UserEventShare> shares = new ArrayList<UserEventShare>();

    @OneToMany(mappedBy = "showtime")
    @JsonManagedReference
    public List<Poll> polls = new ArrayList<Poll>();

    @ManyToOne
    @JoinColumn(name = "plan_evt_id", nullable = false)
    @JsonIgnore
    public PlanningEvent planningEvent;

    @Formula(select = "COALESCE(cmt_${ta}.commentCount, 0) as commentCount", join = "LEFT JOIN (SELECT count(cmt_id) AS commentCount, show_id as showId FROM pin_comment GROUP BY show_id) cmt_${ta} ON cmt_${ta}.showId = ${ta}.show_id")
    public int commentCount;

    @Formula(select = "COALESCE(vs_${ta}.nb_viewers, 0) AS viewersCount", join = "LEFT JOIN pin_event_viewers vs_${ta} ON vs_${ta}.show_id = ${ta}.show_id")
    public int viewersCount;

    @Formula(select = "COALESCE(sh_${ta}.shareCount, 0) as shareCount", join = "LEFT JOIN (select count(usr_id) as shareCount, evt_id, show_id FROM pin_usr_evt_share group by usr_id, evt_id, show_id) sh_${ta} on sh_${ta}.show_id = ${ta}.show_id")
    public int shareCount;

    @Formula(select = "COALESCE(ext_${ta}.extraCount, 0) AS extraCount", join = "LEFT JOIN (select count(ext_id) as extraCount, show_id from pin_extra group by show_id) ext_${ta} ON ext_${ta}.show_id = ${ta}.show_id")
    public int clipsCount;

    public ShowTime() {
        this.showTimeRef = 1;
        this.beginTimeSystem = DateTimeUtils.currentDateWithoutSeconds();
        this.endTimeSystem = DateTimeUtils.addMinutes(this.beginTimeSystem, 60);
    }


    public ShowTime(Integer showTimeRef, Date beginTimeSystem, Date endTimeSystem, Event event, PlanningEvent planningEvent) {
        this.showTimeRef = showTimeRef;
        this.beginTimeSystem = beginTimeSystem;
        this.endTimeSystem = endTimeSystem;
        this.event = event;
        this.planningEvent = planningEvent;
    }

    public ShowTime(Integer showTimeRef, Date beginTimeSystem, Date endTimeSystem, Event event, PlanningEvent planningEvent, ShowTime liveShowTime) {
        this.showTimeRef = showTimeRef;
        this.beginTimeSystem = beginTimeSystem;
        this.endTimeSystem = endTimeSystem;
        this.event = event;
        this.planningEvent = planningEvent;
        this.liveShowTime = liveShowTime;
    }

    /**
     * Create a live show time. panningLive should have event and planningEvent values.
     * Be aware the dates will be converted to system timezone
     *
     * @param planningLive   the planning for live showtime
     * @param currentEpisode the current episode
     * @param beginDate      the begin date
     * @param endDate        the end date
     */
    public ShowTime(PlanningLive planningLive, Event event, int currentEpisode, Date beginDate, Date endDate) {
        this.event = event;
        this.planningEvent = planningLive.planningEvent;
        this.showTimeRef = currentEpisode;
        this.beginTimeSystem = beginDate;
        this.endTimeSystem = endDate;
    }

    /**
     * Create a replay showtime associated to a live showtime. Event should be present in liveShowTime
     * and planningEvent should be present in planningReplay
     * Be aware the dates will be converted to system timezone
     *
     * @param planningReplay the planning of the replay
     * @param liveShowTime   the live show time to be associated with
     * @param beginDate      the begin date
     * @param endDate        the end date
     */
    public ShowTime(PlanningReplay planningReplay, ShowTime liveShowTime, Date beginDate, Date endDate) {
        this.event = liveShowTime.event;
        this.planningEvent = planningReplay.planningEvent;
        this.beginTimeSystem = beginDate;
        this.endTimeSystem = endDate;
        this.showTimeRef = liveShowTime.showTimeRef;
    }

    public ShowTime(SqlRow row) {
        this.id = row.getLong(SqlColumn.ShowTime.SHOW_TIME_ID);
        this.beginTimeSystem = row.getDate(SqlColumn.ShowTime.BEGIN_DATE);
        this.endTimeSystem = row.getDate(SqlColumn.ShowTime.END_DATE);
    }

    public ShowTime convertDatesToUserTimezone(String timeZone) {
        this.beginTimeSystem = DateTimeUtils.convertFromSystemTimeZone(this.beginTimeSystem, timeZone);
        this.endTimeSystem = DateTimeUtils.convertFromSystemTimeZone(this.endTimeSystem, timeZone);
        return this;
    }

    public ShowTime convertToSystemTimeZone(String timeZone) {
        this.beginTimeSystem = DateTimeUtils.convertToSystemTimeZone(this.beginTimeSystem, timeZone);
        this.endTimeSystem = DateTimeUtils.convertToSystemTimeZone(this.endTimeSystem, timeZone);
        return this;
    }

    public void sortReplayShowtimes() {
        Collections.sort(this.replayShowTimes);
    }

    @Override
    public String toString() {
        return "ShowTime{" +
                "showTimeRef=" + showTimeRef +
                ", beginTimeSystem=" + beginTimeSystem +
                ", endTimeSystem=" + endTimeSystem +
                '}';
    }

    @Override
    public int compareTo(ShowTime o) {
        return this.beginTimeSystem.compareTo(o.beginTimeSystem);
    }


}
