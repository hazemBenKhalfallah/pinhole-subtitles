/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.planning;

import javax.persistence.*;

/**
 * User: hazem
 */
@Entity
@Table(name = "pin_planning_replay", uniqueConstraints = @UniqueConstraint(columnNames = {"plan_day_id"}))
public class PlanningReplay implements IPlanning {

    @Id
    @Column(name = "plan_repl_id")
    public Long id;

    @Version
    public Long version;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "plan_evt_id", nullable = false)
    public PlanningEvent planningEvent;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "plan_day_id", nullable = false, unique = true)
    public PlanningDay planningDay;

    public PlanningReplay(PlanningDay planningDay) {
        this.planningDay = planningDay;
    }


    @Override
    public void setId(PlanningEvent planningEvent) {
        if (planningEvent == null) return;
        this.id = planningEvent.id;
        this.planningEvent = planningEvent;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public PlanningEvent getPlanningEvent() {
        return this.planningEvent;
    }
}
