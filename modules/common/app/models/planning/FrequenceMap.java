/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.planning;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author ayassinov
 */
public class FrequenceMap {

    private static final Map<String, String> FREQUENCE;

    static {
        FREQUENCE = new ImmutableMap.Builder<String, String>()
                .put("1", "Une seule fois")
                .put("2", "chaque semaine")
                .put("3", "chaque 2 semaines")
                .put("4", "chaque 3 semaines")
                .put("5", "chaque mois")
                .build();
    }

    public static Map<String, String> list() {
        return FREQUENCE;
    }

}
