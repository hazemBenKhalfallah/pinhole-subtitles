/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.planning;


import models.user.User;

import javax.persistence.*;

/**
 * @author: hazem
 */
@Entity
@Table(name = "pin_planning_user", uniqueConstraints = @UniqueConstraint(columnNames = {"usr_id"}))
public class PlanningUser implements IPlanning {
    @Id
    @Column(name = "plan_usr_id")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "usr_id")
    public User user;

    @Version
    public Long version;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "plan_evt_id", nullable = false)
    public PlanningEvent planningEvent;

    public PlanningUser(User user) {
        this.user = user;
    }

    @Override
    public void setId(PlanningEvent planningEvent) {
        if (planningEvent == null) return;
        this.id = planningEvent.id;
        this.planningEvent = planningEvent;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public PlanningEvent getPlanningEvent() {
        return this.planningEvent;
    }
}
