/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.planning;

import core.DateTimeUtils;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.Date;

/**
 * @author hazem
 */
@Entity
@Table(name = "pin_planning_live")
public class PlanningLive implements IPlanning {

    @Id
    @Column(name = "plan_live_id")
    public Long id;

    @Version
    public Long version;

    @Column(name = "plan_live_totalEps")
    //@Constraints.Min(1)
    public Integer totalEpisodes;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "plan_live_sysbegindate", nullable = false)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Constraints.Required
    public Date sysBeginApplyDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "plan_live_sysenddate")
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date sysEndApplyDate;

    @Column(name = "plan_live_freq", nullable = false)
    @Constraints.Required
    @Constraints.Min(1)
    public Integer frequency;

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "plan_evt_id", nullable = false)
    public PlanningEvent planningEvent;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "plan_last_generation", nullable = true)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date lastGenerationDate;

    /**
     * Constructor with default values
     */
    public PlanningLive() {
        this.totalEpisodes = 30;
        this.sysBeginApplyDate = DateTimeUtils.currentDateWithoutSeconds();
        this.sysEndApplyDate = null;
        this.lastGenerationDate = null;
    }

    public PlanningLive convertDatesToUserTimezone(String timeZone) {
        this.sysBeginApplyDate = DateTimeUtils.convertFromSystemTimeZone(this.sysBeginApplyDate, timeZone);
        if (this.sysEndApplyDate != null)
            this.sysEndApplyDate = DateTimeUtils.convertFromSystemTimeZone(this.sysEndApplyDate, timeZone);
        return this;
    }

    public PlanningLive convertToSystemTimeZone(String timeZone) {
        this.sysBeginApplyDate = DateTimeUtils.convertToSystemTimeZone(this.sysBeginApplyDate, timeZone);
        if (this.sysEndApplyDate != null)
            this.sysEndApplyDate = DateTimeUtils.convertToSystemTimeZone(this.sysEndApplyDate, timeZone);
        return this;
    }

    /**
     * Exist if any overlaps exist on this planning
     *
     * @return true if overlaps exists
     */
    @Transient
    public boolean isOverlapsExists() {
        if (this.planningEvent == null || this.planningEvent.planningDays.size() == 0)
            return false;

        for (final PlanningDay day : this.planningEvent.planningDays) {
            if (day.overlapDays.size() > 0)
                return false;
            if (day.planningReplay != null && day.planningReplay.planningEvent != null) {
                for (final PlanningDay replayDay : day.planningReplay.planningEvent.planningDays)
                    if (replayDay.overlapDays.size() > 0)
                        return false;
            }
        }

        return true;
    }

    @Override
    public void setId(PlanningEvent planningEvent) {
        if (planningEvent == null)
            return;
        this.id = planningEvent.id;
        this.planningEvent = planningEvent;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public PlanningEvent getPlanningEvent() {
        return this.planningEvent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlanningLive that = (PlanningLive) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
