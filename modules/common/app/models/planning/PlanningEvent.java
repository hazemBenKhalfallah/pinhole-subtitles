/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.planning;

import javax.persistence.*;
import java.util.List;

/**
 * @author hazem
 */
@Entity
@Table(name = "pin_planning_event")
public class PlanningEvent {

    @Id
    @Column(name = "plan_evt_id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pin_planning_event_seq")
    public Long id;

    @Version
    public Long version;

    @Column(name = "plan_evt_type", nullable = false, columnDefinition = "smallint default 1")
    public Integer type;

    @OneToOne(mappedBy = "planningEvent", fetch = FetchType.EAGER)
    //should not be changed to eager or else errors will be seen when adding showtimes manually
    public Plan plan;

    @OneToMany(mappedBy = "planningEvent", fetch = FetchType.LAZY)
    public List<PlanningDay> planningDays;

    @OneToMany(mappedBy = "planningEvent", fetch = FetchType.LAZY)
    public List<ShowTime> showTimes;

    public PlanningEvent() {
    }

    public PlanningEvent(PlanningTypeEnum type) {
        this.type = type.value;
    }

    public static enum PlanningTypeEnum {
        LIVE(1),
        REPLAY(2),
        USER(3);

        private final Integer value;

        PlanningTypeEnum(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }
    }

}
