/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.planning;

import com.google.common.collect.ImmutableMap;
import org.joda.time.LocalDate;

import java.util.Locale;
import java.util.Map;

/**
 * @author ayassinov
 */
public class DayOfWeek {

    private static final Locale DEFAULT_LOCALE = Locale.FRANCE;
    private static final Map<String, String> DAYS;


    public static final int MONDAY = 1;
    public static final int TUESDAY = 2;
    public static final int WEDNESDAY = 3;
    public static final int THURSDAY = 4;
    public static final int FRIDAY = 5;
    public static final int SATURDAY = 6;
    public static final int SUNDAY = 7;

    static {
        DAYS = new ImmutableMap.Builder<String, String>()
                .put(MONDAY + "", "Lun")
                .put(TUESDAY + "", "Mar")
                .put(WEDNESDAY + "", "Mer")
                .put(THURSDAY + "", "Jeu")
                .put(FRIDAY + "", "Ven")
                .put(SATURDAY + "", "Sam")
                .put(SUNDAY + "", "Dim")
                .build();
    }

    public static Map<String, String> list() {
        return DAYS;
    }

    public static String getName(Integer dayOfWeek) {
        return getName(dayOfWeek, DEFAULT_LOCALE);
    }

    public static String getName(Integer dayOfWeek, Locale locale) {
        return new LocalDate().withDayOfWeek(dayOfWeek).dayOfWeek().getAsText(locale);
    }
}
