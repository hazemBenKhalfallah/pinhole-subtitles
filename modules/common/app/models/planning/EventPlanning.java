/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.planning;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author hazem
 *         todo:hazem refactor this
 */
@Entity
@Table(name = "pin_event_plan")
public class EventPlanning extends Model {

    @Id
    @Column(name = "plan_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "plan_totalEps", nullable = true)
    @Constraints.Min(1)
    public Integer totalEpisodes;

    @Column(name = "plan_isLimited", nullable = false, columnDefinition = "boolean default true")
    @Constraints.Required
    public Boolean isLimited;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "plan_applyDate", nullable = false)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Constraints.Required
    public Date applyDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "plan_stopDate")
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date stopDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "plan_beginTime", nullable = false)
    @Formats.DateTime(pattern = "HH:mm")
    @Constraints.Required
    public Date beginTime;

    @Column(name = "plan_duration", nullable = false)
    @Constraints.Required
    @Constraints.Min(0)
    public Integer duration;

    @Column(name = "plan_weekDays", nullable = false, length = 7)
    @Constraints.Required
    public String weekDays;

    @Transient
    public List<Integer> daysList;

    public EventPlanning() {
        isLimited = true;
        daysList = new ArrayList<Integer>();
    }

    public EventPlanning(Long id) {
        this();
        this.id = id;
    }

    public boolean validateDuration() {
        return duration != null && duration > 0;
    }

    public boolean validateTotalEpisodes() {
        return totalEpisodes != null && totalEpisodes > 0;
    }

    public void parseDays() {
        for (final char aChar : weekDays.toCharArray()) {
            daysList.add(Character.getNumericValue(aChar));
        }
        Collections.sort(daysList);
    }
}
