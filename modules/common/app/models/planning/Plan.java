/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.planning;

import models.event.Event;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author hazem
 */

@Entity
@Table(name = "pin_plan")
public class Plan extends Model {
    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "evt_id", insertable = false, updatable = false)
    public Event event;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "plan_evt_id", insertable = false, updatable = false)
    public PlanningEvent planningEvent;

    public Plan() {
    }

    public Plan(Event event, PlanningEvent planningEvent) {
        this.id = new Id(event, planningEvent);
        this.event = event;
        this.planningEvent = planningEvent;
        this.event.plans.add(this);
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "evt_id")
        public Long eventId;

        @Column(name = "plan_evt_id")
        public Long planId;

        public Id() {
        }

        public Id(Event event, PlanningEvent planningEvent) {
            this.eventId = event.id;
            this.planId = planningEvent.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (eventId != null ? !eventId.equals(id.eventId) : id.eventId != null) return false;
            //noinspection RedundantIfStatement
            if (planId != null ? !planId.equals(id.planId) : id.planId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = eventId != null ? eventId.hashCode() : 0;
            result = 31 * result + (planId != null ? planId.hashCode() : 0);
            return result;
        }
    }


}
