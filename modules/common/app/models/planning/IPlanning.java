/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.planning;

/**
 * The interface for all the planning type (panningLive, planningReplay, planningXML)
 *
 * @author hazem
 */
public interface IPlanning {
    public void setId(PlanningEvent planningEvent);

    public Long getId();

    public PlanningEvent getPlanningEvent();

}
