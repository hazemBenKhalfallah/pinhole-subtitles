package models.enums.ads;

import com.avaje.ebean.annotation.EnumValue;

public enum AdType {
    @EnumValue(value = "none")
    NONE("none"),
    @EnumValue(value = "player_skin")
    PLAYER_SKIN("player_skin"),
    @EnumValue(value = "video")
    VIDEO("video"),
    @EnumValue(value = "tv")
    TV("tv");

    private static final AdType[] values = AdType.values();

    private final String value;

    private AdType(String value) {
        this.value = value;
    }

    public static AdType fromValue(String value) {
        for (AdType u : values) {
            if (u.value.equalsIgnoreCase(value))
                return u;
        }

        throw new IllegalArgumentException("No enum found for " + value);
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.value.replace("_", " ");
    }
}
