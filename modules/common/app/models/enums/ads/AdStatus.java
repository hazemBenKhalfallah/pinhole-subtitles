package models.enums.ads;

import com.avaje.ebean.annotation.EnumValue;

public enum AdStatus {
    @EnumValue(value = "none")
    NONE("none"),
    @EnumValue(value = "pending")
    PENDING("pending"),
    @EnumValue(value = "active")
    ACTIVE("active"),
    @EnumValue(value = "on_hold")
    ON_HOLD("on_hold");

    private static final AdStatus[] values = AdStatus.values();

    private String value;

    AdStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static AdStatus fromValue(String value) {
        for (AdStatus s : values) {
            if (s.value.equalsIgnoreCase(value))
                return s;
        }

        throw new IllegalArgumentException("No enum found for " + value);
    }

    @Override
    public String toString() {
        return this.value;
    }
}
