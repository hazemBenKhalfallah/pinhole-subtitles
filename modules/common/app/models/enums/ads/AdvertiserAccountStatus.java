package models.enums.ads;

import com.avaje.ebean.annotation.EnumValue;

public enum AdvertiserAccountStatus {
    @EnumValue(value = "none")
    none("none"),
    @EnumValue(value = "pending")
    pending("pending"),
    @EnumValue(value = "active")
    active("active"),
    @EnumValue(value = "on_hold")
    on_hold("on_hold");


    private static final AdvertiserAccountStatus[] values = AdvertiserAccountStatus.values();

    private final String value;

    private AdvertiserAccountStatus(String value) {
        this.value = value;
    }

    public static AdvertiserAccountStatus fromValue(String value) {
        for (AdvertiserAccountStatus u : values) {
            if (u.value.equalsIgnoreCase(value))
                return u;
        }

        throw new IllegalArgumentException("No enum found for " + value);
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.value;
    }

}
