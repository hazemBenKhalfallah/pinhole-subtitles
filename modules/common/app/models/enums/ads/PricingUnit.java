package models.enums.ads;

import com.avaje.ebean.annotation.EnumValue;

public enum PricingUnit {

    @EnumValue(value = "none")
    NONE("none"),
    @EnumValue(value = "cpm")
    CPM("cpm"),
    @EnumValue(value = "cpc")
    CPC("cpc"),
    @EnumValue(value = "day")
    DAY("day");
    private final String value;

    private static final PricingUnit[] values = PricingUnit.values();

    private PricingUnit(String value) {
        this.value = value;
    }

    public static PricingUnit fromValue(String value) {
        for (PricingUnit u : values) {
            if (u.value.equalsIgnoreCase(value))
                return u;
        }

        throw new IllegalArgumentException("No enum found for " + value);
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
