package models.enums.ads;

import com.avaje.ebean.annotation.EnumValue;


public enum DiscountType {
    @EnumValue(value = "none")
    NONE("none"),
    @EnumValue(value = "amount_off")
    AMOUNT_OFF("amount_off"),
    @EnumValue(value = "percentage_off")
    PERCENTAGE_OFF("percentage_off");

    private String value;

    private static final DiscountType[] values = DiscountType.values();


    private DiscountType(String value) {
        this.value = value;
    }

    public static DiscountType fromValue(String value) {
        for (DiscountType u : values) {
            if (u.value.equalsIgnoreCase(value))
                return u;
        }

        throw new IllegalArgumentException("No enum found for " + value);
    }

    @Override
    public String toString() {
        return value.replace("_", " ");
    }
}
