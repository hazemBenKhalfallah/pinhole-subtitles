package models.enums.ads;

import com.avaje.ebean.annotation.EnumValue;

/**
 * @author: akhezami
 */
public enum AdTrackType {
    @EnumValue(value = "view")
    VIEW,
    @EnumValue(value = "click")
    CLICK
}
