/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.enums;

/**
 * The Job internal type. This enum make easy for us to differentiate the result of the job on the trace.
 *
 * @author hazem
 */
public enum JobIdEnum {
    REFRESH_EVENT_DATES(1),
    REFRESH_ADS(2),
    DAILY_SYS_RECOMMENDATION(3),
    CALCULATE_BUZZ(4),
    REFRESH_RELATED_EVENTS(5);

    private final Integer value;

    JobIdEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
