package models.enums;

import com.avaje.ebean.annotation.EnumValue;

/**
 * Created by khezamian on 23/12/13.
 */
public enum PromptType {

    @EnumValue(value = "none")
    NONE("none"),
    @EnumValue(value = "fb_join")
    FB_JOIN("fb_join"),
    @EnumValue(value = "video_preroll_join")
    VIDEO_PREROLL_JOIN("video_preroll_join");

    private static final PromptType[] values = PromptType.values();

    private String value;

    PromptType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static PromptType fromValue(String value) {
        for (PromptType s : values) {
            if (s.value.equalsIgnoreCase(value))
                return s;
        }

        throw new IllegalArgumentException("No enum found for " + value);
    }

    @Override
    public String toString() {
        return this.value;
    }

}
