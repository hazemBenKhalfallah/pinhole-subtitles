package models.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author James
 */
public enum ImageType {
    JPG,
    GIF,
    PNG,
    UNKNOWN;

    private static final Map<String, ImageType> extensionMap = new HashMap<String, ImageType>();
    private static final Map<ImageType, String> imageTypeMap = new HashMap<ImageType, String>();

    static {
        extensionMap.put("jpg", ImageType.JPG);
        extensionMap.put("jpeg", ImageType.JPG);
        extensionMap.put("gif", ImageType.GIF);
        extensionMap.put("png", ImageType.PNG);
    }

    static {
        imageTypeMap.put(ImageType.JPG, "jpg");
        imageTypeMap.put(ImageType.GIF, "gif");
        imageTypeMap.put(ImageType.PNG, "png");
    }

    public static ImageType getType(String ext) {
        ext = ext.toLowerCase();
        if (extensionMap.containsKey(ext))
            return extensionMap.get(ext);
        else
            return UNKNOWN;
    }

    public static String getExtension(ImageType type) {

        if (imageTypeMap.containsKey(type))
            return imageTypeMap.get(type);
        else
            return "";
    }
}
