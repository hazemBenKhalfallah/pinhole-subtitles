/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.enums;

import models.extra.Extra;

import java.util.HashMap;
import java.util.Map;

/**
 * document types in ElasticSearch
 *
 * @author : hazem
 */
public enum DocumentTypeEnum {
    EVENT,
    CHANNEL,
    CATEGORY,
    CLIP,
    EPISODE,
    UNKNOWN;

    private static final Map<String, DocumentTypeEnum> TypesMap = new HashMap<String, DocumentTypeEnum>();
    private static final Map<DocumentTypeEnum, String> ValuesMap = new HashMap<DocumentTypeEnum, String>();

    static {
        TypesMap.put("event", DocumentTypeEnum.EVENT);
        TypesMap.put("channel", DocumentTypeEnum.CHANNEL);
        TypesMap.put("category", DocumentTypeEnum.CATEGORY);
        TypesMap.put("clip", DocumentTypeEnum.CLIP);
        TypesMap.put("episode", DocumentTypeEnum.EPISODE);
    }

    static {
        ValuesMap.put(DocumentTypeEnum.EVENT, "event");
        ValuesMap.put(DocumentTypeEnum.CHANNEL, "channel");
        ValuesMap.put(DocumentTypeEnum.CATEGORY, "category");
        ValuesMap.put(DocumentTypeEnum.CLIP, "clip");
        ValuesMap.put(DocumentTypeEnum.EPISODE, "episode");
    }

    public static DocumentTypeEnum getType(String value) {
        value = value.toLowerCase();
        if (TypesMap.containsKey(value))
            return TypesMap.get(value);
        else
            return UNKNOWN;
    }

    public static DocumentTypeEnum getType(Extra.TypeEnum type) {
        if (type.equals(Extra.TypeEnum.EPISODE))
            return DocumentTypeEnum.EPISODE;
        else
            return DocumentTypeEnum.CLIP;
    }


    public static String getValue(DocumentTypeEnum type) {
        return ValuesMap.get(type);
    }

    public String getValue() {
        return ValuesMap.get(this);
    }
}
