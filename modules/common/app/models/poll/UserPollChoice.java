/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.poll;

import core.DateTimeUtils;
import models.user.User;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * todo:hazem refactor this
 */
@Entity
@Table(name = "pin_user_poll_choice")
public class UserPollChoice extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JoinColumn(name = "choice_id", insertable = false, updatable = false)
    public PollChoice choice;

    @ManyToOne
    @JoinColumn(name = "poll_id", nullable = false)
    public Poll poll;

    @Temporal(TemporalType.DATE)
    @Column(name = "upc_vote_date", nullable = false)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date voteDate;

    public UserPollChoice() {
    }

    public UserPollChoice(User user, PollChoice choice, Date creationDate) {
        this.user = user;
        this.choice = choice;
        this.poll = choice.poll;
        this.voteDate = creationDate;
        this.id = new Id(user.id, choice.id);
    }

    public UserPollChoice(User user, PollChoice choice) {
        this.user = user;
        this.choice = choice;
        this.poll = choice.poll;
        this.voteDate = DateTimeUtils.currentDateWithoutSeconds();
        this.id = new Id(user.id, choice.id);
    }

    @Embeddable
    public static class Id {
        @Column(name = "usr_id")
        public Long userId;

        @Column(name = "choice_id")
        public Long choiceId;

        public Id() {
        }

        public Id(Long userId, Long choiceId) {
            this.userId = userId;
            this.choiceId = choiceId;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Id)) return false;

            Id id = (Id) o;

            if (choiceId != null ? !choiceId.equals(id.choiceId) : id.choiceId != null) return false;
            //noinspection RedundantIfStatement
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = userId.hashCode();
            result = 31 * result + choiceId.hashCode();
            return result;
        }
    }

}


