/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.poll;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import core.DateTimeUtils;
import models.event.Event;
import models.planning.ShowTime;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * todo:hazem refactor this
 */
@Entity
@Table(name = "pin_poll")
public class Poll extends Model {

    @Id
    @Column(name = "poll_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    @JsonIgnore
    public Long version;

    @Constraints.Required
    @Constraints.MaxLength(150)
    @Column(name = "poll_question", nullable = false, length = 150)
    public String question;

    @Column(name = "poll_multi_choice", nullable = false)
    public boolean multiChoice;

    @Column(name = "poll_rtl", nullable = false)
    public boolean rtl;

    @Column(name = "poll_inactive", nullable = false)
    public boolean inactive;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "poll_sysBeginTime", nullable = false)
    public Date beginTimeSystem;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "poll_sysEndTime", nullable = false)
    public Date endTimeSystem;

    @OneToMany(mappedBy = "poll", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnore
    public List<PollChoice> choices = new ArrayList<PollChoice>();

    @ManyToOne
    @JoinColumn(name = "evt_id")
    @JsonBackReference
    public Event event;

    @ManyToOne
    @JoinColumn(name = "show_id")
    @JsonBackReference
    public ShowTime showtime;

    public Poll() {
        this.inactive = false;
    }

    public Poll(ShowTime showtime) {
        this.showtime = showtime;
    }

    public Poll(String question, boolean multiChoice, boolean rtl, Date beginTimeSystem, Date endTimeSystem, ShowTime showtime) {
        this();
        this.question = question;
        this.multiChoice = multiChoice;
        this.rtl = rtl;
        this.showtime = showtime;
        this.event = showtime.event;
        this.beginTimeSystem = beginTimeSystem;
        this.endTimeSystem = endTimeSystem;
    }

    public boolean displayPoll() {
        final Date currentDate = DateTimeUtils.currentDateWithoutSeconds();
        if (this.beginTimeSystem.after(currentDate) && this.endTimeSystem.after(currentDate)) {
            return false;
        }
        return !(this.inactive);
    }

    @JsonProperty("choices")
    public List<PollChoice> sortedChoices() {
        Collections.sort(this.choices);
        return this.choices;
    }


}
