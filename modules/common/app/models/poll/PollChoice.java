/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.poll;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Function;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * todo:hazem refactor this
 */
@Entity
@Table(name = "pin_poll_choice")
public class PollChoice extends Model implements Comparable<PollChoice> {

    @Id
    @Column(name = "choice_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    @JsonIgnore
    public Long version;

    @Column(name = "choice_label", nullable = false, length = 80)
    @Constraints.MaxLength(80)
    @Constraints.Required
    public String label;

    @Column(name = "choice_rank", nullable = false)
    @Constraints.Min(0)
    public int rank;

    @ManyToOne
    @JoinColumn(name = "poll_id")
    @JsonBackReference
    public Poll poll;

    @Transient
    public boolean selected;

    @OneToMany(mappedBy = "choice")
    @JsonIgnore
    public List<UserPollChoice> users = new ArrayList<UserPollChoice>();


    public PollChoice(String label, int rank, Poll poll) {
        this.label = label;
        this.rank = rank;
        this.poll = poll;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PollChoice)) return false;
        if (!super.equals(o)) return false;

        PollChoice that = (PollChoice) o;

        return id.equals(that.id);
    }

    @Override
    public int compareTo(PollChoice o) {
        return (o.rank > this.rank ? -1 : (o.rank == this.rank ? 0 : 1));
    }

    final public static Function<PollChoice, Long> retrieveIds = new Function<PollChoice, Long>() {
        @Nullable
        @Override
        public Long apply(@Nullable PollChoice pollChoice) {
            return pollChoice.id;
        }
    };
}
