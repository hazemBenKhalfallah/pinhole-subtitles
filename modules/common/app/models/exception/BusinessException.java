/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.exception;

/**
 * A base class for all business exception
 *
 * @author ayassinov
 */
public class BusinessException extends RuntimeException {

    private Exception originalException;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Exception originalException) {
        super(message);
        this.originalException = originalException;
    }

    public Exception getOriginalException() {
        return originalException;
    }
}
