/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.exception;

/**
 * Exception generated when the planning did not generate any showtime
 *
 * @author ayassinov
 */
public class NoShowTimeGeneratedException extends BusinessException {
    public NoShowTimeGeneratedException(String message) {
        super(message);
    }

    public NoShowTimeGeneratedException(String message, Exception originalException) {
        super(message, originalException);
    }
}
