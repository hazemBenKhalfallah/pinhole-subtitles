/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models;

import core.DateTimeUtils;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_notification_trace")
public class NotificationTrace extends Model {

    /**
     * An Enum for the notification's sending way
     */
    public enum TypeEnum {
        FACEBOOK(1),
        EMAIL(2),
        BOX(3),
        SMS(4),
        PINHOLE(5);
        private Integer value;

        private TypeEnum(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return this.value;
        }
    }

    @Id
    @Column(name = "trc_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;


    @Column(name = "trc_type", nullable = false)
    public Integer type;

    @Column(name = "trc_date", nullable = false)
    public Date date;

    @Column(name = "trc_group", nullable = false)
    public String group;

    @ManyToOne
    @JoinColumn(name = "ntf_id", nullable = false)
    public Notification notification;

    public NotificationTrace(Long notificationId, TypeEnum type, String group){
        this.notification = new Notification(notificationId);
        this.type = type.getValue();
        this.date = DateTimeUtils.currentDateWithoutSeconds();
        this.group = group;
    }

}
