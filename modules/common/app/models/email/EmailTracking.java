package models.email;

import com.google.common.base.Optional;
import core.DateTimeUtils;
import core.email.mandrill.request.MandrillMessageRequest;
import core.email.mandrill.response.MandrillErrorResponse;
import core.email.mandrill.response.MandrillMessageResponse;
import core.email.mandrill.response.MandrillResponse;
import core.email.model.Recipient;
import core.email.model.RecipientMetadata;
import models.resources.SqlColumn;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "pin_email_tracking")
public class EmailTracking extends Model {
    @Id
    @Column(name = SqlColumn.EmailTracking.CODE)
    public String code;

    @Column(name = SqlColumn.EmailTracking.EMAIL, nullable = false)
    public String email;

    @Column(name = SqlColumn.EmailTracking.TEMPLATE)
    public String template;

    @Column(name = SqlColumn.EmailTracking.STATUS, nullable = false)
    public String status;

    @Column(name = SqlColumn.EmailTracking.ERROR_MESSAGE)
    public String errorMessage;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = SqlColumn.EmailTracking.DATETIME_CREATION)
    public Date creationDateTime;

    @Transient
    public List<EmailMetadata> emailMetadatas = new ArrayList<EmailMetadata>();

    public EmailTracking() {
        this.creationDateTime = DateTimeUtils.now();
    }

    public EmailTracking(String email, String status, String errorMessage) {
        this();
        this.email = email;
        this.status = status;
        this.errorMessage = errorMessage;
    }

    public EmailTracking(String code, String email, String status, String errorMessage, Optional<RecipientMetadata> recipientMetadataOptional, Map<String, String> globalMetadata, String template) {
        this();
        this.code = code;
        this.email = email;
        this.status = status;
        this.errorMessage = errorMessage;
        this.emailMetadatas = EmailMetadata.from(code, recipientMetadataOptional, globalMetadata);
        this.template = template;
    }

    public static List<EmailTracking> from(MandrillErrorResponse errorResponse) {
        final List<EmailTracking> emailsTracking = new ArrayList<EmailTracking>();
        for (final Recipient recipient : errorResponse.getRecipients()) {
            emailsTracking.add(new EmailTracking(recipient.getEmail(), errorResponse.getStatus(), errorResponse.getMessage()));
        }
        return emailsTracking;
    }

    public static List<EmailTracking> from(MandrillMessageResponse mandrillMessageResponse, MandrillMessageRequest request) {
        final List<EmailTracking> emailsTracking = new ArrayList<EmailTracking>();
        for (final MandrillResponse mandrillResponse : mandrillMessageResponse.getResponses()) {
            emailsTracking.add(
                    new EmailTracking(
                            mandrillResponse.getId(),
                            mandrillResponse.getEmail(),
                            mandrillResponse.getStatus(),
                            mandrillResponse.getRejectReason(),
                            request.getMessage().getRecipientsMetadata(mandrillResponse.getEmail()),
                            request.getMessage().getMetadata(),
                            request.getTemplateName()
                    )
            );
        }
        return emailsTracking;
    }
}
