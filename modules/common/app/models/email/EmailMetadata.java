package models.email;

import com.google.common.base.Optional;
import core.email.model.RecipientMetadata;
import models.resources.SqlColumn;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Store the email metadata
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_email_metadata")
public class EmailMetadata extends Model {

    @Id
    @Column(name = SqlColumn.EmailTracking.CODE, nullable = false)
    public String code;

    @Column(name = SqlColumn.EmailMetadata.KEY)
    public String key;

    @Column(name = SqlColumn.EmailMetadata.VALUE)
    public String value;

    @Column(name = SqlColumn.EmailMetadata.IS_GLOBAL)
    public boolean isGlobal;

    public EmailMetadata() {
        this.isGlobal = false;
    }

    public EmailMetadata(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public EmailMetadata(String key, String value, boolean global) {
        this.key = key;
        this.value = value;
        isGlobal = global;
    }

    public EmailMetadata(String code, String key, String value) {
        this.code = code;
        this.key = key;
        this.value = value;
    }

    public EmailMetadata(String code, String key, String value, boolean global) {
        this.code = code;
        this.key = key;
        this.value = value;
        isGlobal = global;
    }


    public static List<EmailMetadata> from(final String code, final Optional<RecipientMetadata> recipientMetadataOptional, final Map<String, String> globalMetadata) {

        final List<EmailMetadata> emailMetadata = new ArrayList<EmailMetadata>();
        //global metadata
        if (globalMetadata != null && globalMetadata.size() > 0) {
            for (String key : globalMetadata.keySet())
                emailMetadata.add(new EmailMetadata(code, key, globalMetadata.get(key), true));
        }

        //user specific
        if (recipientMetadataOptional.isPresent()) {
            final Map<String, String> recipientMap = recipientMetadataOptional.get().getValues();
            for (String key : recipientMap.keySet()) {
                emailMetadata.add(new EmailMetadata(code, key, recipientMap.get(key), false));
            }
        }

        return emailMetadata;
    }

}
