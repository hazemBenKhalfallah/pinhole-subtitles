package models.resources;

public class SqlColumn {
    public static class Event {
        public static final String EVENT_ID = "evt_id";
        public static final String TITLE = "evt_title";
        public static final String TITLE_BOX = "evt_title_box";
        public static final String SHORT_DESCRIPTION = "evt_shortDescription";
        public static final String DESCRIPTION = "evt_description";
        public static final String TIMEZONE = "evt_timeZone";
        public static final String HASHTAG = "evt_twitterTag";
        public static final String VANITY_URL = "evt_url";
        public static final String HERO_STATUS = "evt_hero_status";
        public static final String START_DATE_TIME_GMT = "evt_sysBeginTime";
        public static final String END_DATE_TIME_GMT = "evt_sysEndTime";
        public static final String BEGIN_TIME = "evt_beginTime";
        public static final String END_TIME = "evt_endTime";

        public static final String URL = "evt_url";
        public static final String LAST_MODIFIED = "evt_last_modif_time";
        public static final String IS_FEATURED = "evt_is_featured";
        public static final String ENABLE_TWITTER = "evt_enableTwitter";
        public static final String SLIDE_DESCRIPTION = "sld_description";
        public static final String SLIDE_TITLE = "sld_title";

        public static final String EPISODE_COUNT = "episodeCount";

    }

    public static class Channel {
        public static final String CHANNEL_ID = "cha_id";
        public static final String NAME = "cha_name";
        public static final String DVB_TRIPLET = "cha_dvbtriplet";
        public static final String VANITY_URL = "cha_url";
        public static final String IMAGE_ALT = "cha_img_alt";
        public static final String URL_FRAGMENT = "cha_url_fragment";
        public static final String IMAGE_EXISTS = "cha_img_exists";
        public static final String IS_FEATURED = "cha_is_featured";
        public static final String FEATURED_ORDER = "cha_featured_order";
        public static final String LAST_MODIFIED_TIME = "cha_last_modif_time";
        public static final String IN_BROWSE = "cha_inbrowse";
        public static final String EVENT_COUNT = "eventCount";
        public static final String VIEW_COUNT = "viewers";
        public static final String YOUTUBE_USER = "cha_ytb_user";
        public static final String YOUTUBE_ID = "cha_ytb_id";
        public static final String YOUTUBE_ENABLED = "cha_ytb_enabled";
        public static final String YOUTUBE_PUBLISHED_AFTER = "cha_ytb_published_after";
    }

    public static class ShowTime {
        public static final String SHOW_TIME_ID = "show_id";
        public static final String BEGIN_DATE = "show_sysbegintime";
        public static final String END_DATE = "show_sysendtime";
    }

    public static class VanityUrl {
        public static final String VANITY_URL_ID = "van_id";
        public static final String URL = "van_url";
        public static final String TYPE = "van_type";
        public static final String REDIRECT_ID = "van_redirect";
        public static final String REDIRET_URL = "van_redirect_url";
    }

    public static class Category {
        public static final String CATEGORY_ID = "cat_id";
        public static final String VANITY_URL = "cat_url";
        public static final String TERM = "term";
        public static final String EVENT_COUNT = "eventCount";
        public static final String VIEW_COUNT = "viewers";
    }

    public static class LocalizedTerm {
        public static final String LANGUAGE_CODE = "lng_locale";
        public static final String TERM = "term";
    }

    public static class Video {
        public static final String VIDEO_ID = "ext_id";
        public static final String SPLASH = "ext_splash";
        public static final String URL = "ext_content";
        public static final String TITLE = "ext_title";
        public static final String LENGTH = "ext_length";
        public static final String TYPE = "ext_type";
        public static final String AIR_DATE = "airDate";
        public static final String PROVIDER = "ext_provider";
        public static final String BEGIN_DATE = "ext_beginTime";
    }

    public static class User {
        public static final String ID = "usr_id";
    }

    public static class EmailTracking {
        public static final String CODE = "etr_code";
        public static final String TEMPLATE = "etr_template";
        public static final String STATUS = "etr_status";
        public static final String ERROR_MESSAGE = "etr_error_message";
        public static final String DATETIME_CREATION = "etr_creationDate";
        public static final String EMAIL = "etr_email";
    }

    public class EmailMetadata {
        public static final String KEY = "emd_key";
        public static final String VALUE = "emd_value";
        public static final String IS_GLOBAL = "emd_isglobal";
    }

    public static class Message {
        public static final String ID = "msg_id";
        public static final String LANGUAGE = "lng_id";
        public static final String CODE = "msg_code";
        public static final String VALUE = "msg_value";
    }

    public class Account {
        public static final String ACCOUNT_ID = "acc_id";
        public static final String COMPANY = "acc_company";
        public static final String PRIMARY_PHONE = "acc_primary_phone";
        public static final String SECONDARY_PHONE = "acc_secondary_phone";
        public static final String ACCOUNT_STATUS = "acc_status";
        public static final String VALIDATED_DATE_TIME = "ad_acc_validated_timestamp";
    }

    public class AdUser {
        public static final String AD_USER_ID = "ad_user_id";
        public static final String AD_USER_UID = "ad_user_uid";
        public static final String SESSION_ID = "ad_user_session_id";
        public static final String EMAIL = "ad_user_email";
        public static final String PASSWORD = "ad_user_password";
        public static final String FIRST_NAME = "ad_user_first_name";
        public static final String LAST_NAME = "ad_user_last_name";
        public static final String IS_ADMIN = "ad_user_isadmin";
        public static final String CREATED_DATE_TIME = "ad_user_created_date_time";
        public static final String LANGUAGE = "lng_id";
        public static final String LOCATION = "loc_id";
    }

    public class AdPrice {
        public static final String AD_PRICE_ID = "ad_pricing_id";
        public static final String AD_TYPE = "ad_type";
        public static final String UNIT = "ad_pricing_unit";
        public static final String VALUE = "ad_price";
        public static final String START_DATE_TIME = "ad_price_start_date_time";
        public static final String END_DATE_TIME = "ad_price_end_date_time";
        public static final String DEPENDENT_ADS = "dependentAds";
    }

    public class AdDiscount {
        public static final String AD_DISCOUNT_ID = "ad_discount_id";
        public static final String DISCOUNT_AMOUNT = "ad_discount_amount";
        public static final String AD_TYPE = "ad_type";
        public static final String DISCOUNT_TYPE = "ad_discount_type";
        public static final String START_DATE_TIME = "ad_discount_start_date_time";
        public static final String END_DATE_TIME = "ad_discount_end_date_time";
    }


    public class AdCampaign {
        public static final String AD_CAMPAIGN_ID = "camp_id";
        public static final String CAMPAIGN_NAME = "camp_name";
        public static final String START_DATE_TIME = "camp_start_date_time";
        public static final String END_DATE_TIME = "camp_end_date_time";
        public static final String CREATED_DATE_TIME = "camp_created_date_time";
        public static final String CREATED_BY_ID = "created_by_uid";
    }

    public class Ad {
        public static final String AD_ID = "ad_id";
        public static final String AD_NAME = "ad_name";
        public static final String PREVIEW_KEY = "ad_preview_key";
        public static final String AD_STATUS = "ad_status";
        public static final String START_DATE_TIME = "ad_start_date_time";
        public static final String END_DATE_TIME = "ad_end_date_time";
        public static final String ACTIVATE_DATE_TIME = "ad_activate_date_time";
        public static final String CREATED_DATE_TIME = "ad_created_date_time";
        public static final String BUDGET = "ad_budget";
        public static final String EXTERNAL_URL = "ad_external_url";
        public static final String TWITTER_URL = "ad_twitter_url";
        public static final String FB_URL = "ad_fb_url";
        public static final String ENABLE_FB = "ad_enable_fb";
        public static final String ENABLE_TWITTER = "ad_enable_twitter";
        public static final String ALLOWANCE = "ad_allowance";
        public static final String CREATED_BY_ID = "created_by_uid";
        public static final String Ad_TYPE = "ad_type";
        public static final String AD_URL = "ad_url";
        public static final String AD_ALTERNATE_URL = "ad_alternate_url";
        public static final String AD_CLICKS = "ad_clicks";
        public static final String AD_VIEWS = "ad_views";
    }

    public class AdTrack {
        public static final String TYPE = "ad_track_type";
        public static final String DATE_TIME = "ad_track_timestamp";
    }

    public class YoutubeKeyword {
        public static final String ID = "ytb_id";
        public static final String KEYWORD = "ytb_keyword";
    }

    public class YoutubeSetting {
        public static final String ID = "ys_id";
        public static final String MIN_LENGTH = "ys_min_length";
        public static final String MAX_LENGTH = "ys_max_length";
        public static final String HAS_DATE = "ys_has_date";
        public static final String HAS_PARTS = "ys_has_parts";
    }

    public class YoutubeRegex{
        public static final String ID = "rg_id";
        public static final String LABEL = "rg_label";
        public static final String VALUE = "rg_value";
        public static final String TYPE = "rg_type";
        public static final String DATE_LOCAL = "rg_date_local";
        public static final String DATE_FORMAT = "rg_date_format";
    }
}

