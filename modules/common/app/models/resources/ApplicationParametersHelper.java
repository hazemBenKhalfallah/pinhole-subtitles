package models.resources;

import core.parameter.ApplicationParameter;

/**
 * fetches the application parameters by section with their defaults if they are null
**/
public class ApplicationParametersHelper {

    // section names

    private static class ApplicationParameterSection {
        public static final String EVENT_INTERACTION = "EventInteraction";
        public static final String SEARCH = "Search";
        public static final String JOIN_CAMPAIGN = "JoinCampaign";
    }

    // TODO: akhezami refactor by removing all default values and the rest of constant strings into a central location
    public static class EventInteraction {
        public static Boolean ActivateRecentlyAdded(){
            return ApplicationParameter.getAsBooleanOrElse(ApplicationParameterSection.EVENT_INTERACTION, "ActivateRecentlyAdded", true);
        }

        public static Integer MaxNbHomePage(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.EVENT_INTERACTION, "MaxNbHomePage", 10);
        }

        public static Integer MaxNbRelated(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.EVENT_INTERACTION, "MaxNbRelated", 10);
        }

        public static Integer MaxNbFriendOnPopular(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.EVENT_INTERACTION, "MaxNbFriendOnPopular", 4);
        }

        public static Integer MaxPopularViewsChannelPage(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.EVENT_INTERACTION, "MaxPopularViewsChannelPage", 5);
        }

        public static Integer DaysNewEpisodesChannelPage(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.EVENT_INTERACTION, "DaysNewEpisodesChannelPage", 2);
        }

        public static Integer MaxNbUser(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.EVENT_INTERACTION, "MaxNbUser", 6);
        }

        public static Integer MaxNbVideo(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.EVENT_INTERACTION, "MaxNbVideo", 10);
        }

        public static Integer DaysNewEpisodesHomePage(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.EVENT_INTERACTION, "DaysNewEpisodesHomePage", 4);
        }

        public static Integer MaxPopularViewsHomePage(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.EVENT_INTERACTION, "MaxPopularViewsHomePage", 10);
        }
    }

    public static class Search {

        public static Integer autoCompleteMinLength(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.SEARCH, "autoCompleteMinLength", 1);
        }

        public static Integer autoCompleteSize(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.SEARCH, "autoCompleteSize", 10);
        }

        public static Integer searchSize(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.SEARCH, "searchSize", 10);
        }

        public static Integer EventEpisodesSize(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.SEARCH, "EventEpisodesSize", 10);
        }
    }

    public static class JoinCampaign{
        public static Integer PopupVideosThreshold(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.JOIN_CAMPAIGN, "PopupSiteVideosSeenThreshold", 1);
        }

        public static Integer PopupWaitHours(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.JOIN_CAMPAIGN, "BetweenPopupsWaitInHours", 24);
        }

        public static Integer PreRollPerShowVideoThreshold(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.JOIN_CAMPAIGN, "PreRollVideosPerShowThreshold", 2);
        }

        public static Integer PreRollLengthInSeconds(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.JOIN_CAMPAIGN, "PrerollLengthInSeconds", 20);
        }

        public static Integer PreRollWaitHours(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.JOIN_CAMPAIGN, "BetweenPrerollsWaitInHours", 24);
        }

        public static Integer EventRecommendations(){
            return ApplicationParameter.getAsIntOrElse(ApplicationParameterSection.JOIN_CAMPAIGN, "PopupEventRecommendations", 3);
        }
    }
}
