package models.resources;

public class Format {
    public static class DateTimeFormat {
        public static String DEFAULT_SHORT_DATE_TIME = "dd/MM/yyyy HH:mm";
        public static String SHORT_TIME = "HH:mm";
    }
}
