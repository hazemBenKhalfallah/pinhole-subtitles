package models.resources;

public class FieldLength {
    public static class Event{
        public final static int TITLE = 80;
        public final static int TITLE_BOX = 80;
        public final static int SHORT_DESCRIPTION = 150;
        public final static int TIMEZONE = 10;
        public final static int IMAGE_URL = 250;
        public final static int IMAGE_SMALL_URL = 250;
        public final static int IMAGE_ALT = 250;
        public final static int HASHTAG = 16;
    }

    public static class Channel{
        public final static int NAME = 80;
        public final static int DVB_TRIPLET = 50;
        public final static int IMAGE_ALT = 250;
        public final static int URL_FRAGMENT = 255;
        public final static int VANITY_URL = 255;

    }

    public static class Account{
        public final static int COMPANY = 150;
        public final static int PHONE = 20;
    }
    public static class AdUser{

        public final static int EMAIL = 50;
        public final static int FIRST_NAME = 30 ;
        public final static int LAST_NAME = 30;
        public final static int SESSION_ID= 150 ;
        public final static int PASSWORD = 64;
    }

    public static class Ad{
        public final static int AD_PREVIEW_KEY = 64;
        public final static int AD_STATUS = 20;
        public final static int AD_EXTERNAL_URL = 250;
        public final static int AD_FB_URL = 250;
        public final static int AD_TWITTER_URL = 250;
        public final static int AD_NAME = 50;
    }

    public static class Campagin{
        public final static int CAMPAIGN_NAME = 50;
    }

    public static class VideoAd{
        public final static int VIDEO_LENGTH = 10;
    }

    public static class YoutubeRegex{
        public final static int LABEL = 20;
        public final static int VALUE = 80;
    }
}
