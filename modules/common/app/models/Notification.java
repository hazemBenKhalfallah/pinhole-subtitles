/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models;

import com.avaje.ebean.SqlRow;
import core.DateTimeUtils;
import models.user.User;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_notification")
public class Notification extends Model {

    /**
     * An Enum for the type of notification
     */
    public enum TriggerEnum {
        SUBSCRIPTION(1);
        private int value;

        private TriggerEnum(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    @Id
    @Column(name = "ntf_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "ntf_context_id", nullable = false)
    public Long contextObjectId;

    @Column(name = "ntf_creation_date", nullable = false)
    public Date creationDate;

    @Column(name = "ntf_trigger", nullable = false)
    public Integer trigger;

    @Column(name = "ntf_disabled", nullable = false, columnDefinition = "default false")
    public Boolean disabled;

    @ManyToOne()
    @JoinColumn(name = "usr_id", nullable = false)
    public User user;

    @Transient
    public Object contextObject;


    /**
     * Default constructor
     */
    public Notification() {
        this.creationDate = DateTimeUtils.now();
        this.disabled = false;
    }

    public Notification(Long id) {
        this.id = id;
    }

    public Notification(Long contextObjectId, User user, TriggerEnum trigger) {
        this();
        this.contextObjectId = contextObjectId;
        this.user = user;
        this.trigger = trigger.getValue();
    }
    //todo:hazem
    public static List<Notification> fromSql(List<SqlRow> rows) {
        return null;
    }
}
