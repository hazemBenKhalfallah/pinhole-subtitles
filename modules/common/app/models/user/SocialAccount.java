/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.JsonNode;
import core.DateTimeUtils;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_usr_social")
public class SocialAccount extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "soc_id")
    public Long id;

    @Version
    public int version;

    @ManyToOne
    @JoinColumn(name = "usr_id", nullable = false)
    public User user;

    @Enumerated(EnumType.STRING)
    @Column(name = "soc_provider", nullable = false)
    public ProviderEnum provider;

    @Column(name = "soc_user_id")
    public String userId;

    @Column(name = "soc_first_name")
    public String firstName;

    @Column(name = "soc_last_name")
    public String lastName;

    @Column(name = "soc_full_name")
    public String fullName;

    @Column(name = "soc_user_name")
    public String userName;

    @Column(name = "soc_email")
    public String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "soc_gender")
    public GenderEnum gender;

    @Column(name = "soc_birthday")
    @Temporal(TemporalType.DATE)
    public Date birthday;

    @Column(name = "soc_avatar_url")
    public String avatarUrl;

    @Column(name = "key_token", columnDefinition = "TEXT")
    public String token;

    @Column(name = "key_secret", columnDefinition = "TEXT")
    public String secret;

    @Column(name = "key_expire_in")
    public Integer expiresIn;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "soc_creation_date", nullable = false, columnDefinition = "default current_timestamp")
    public Date creationDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "soc_last_update", nullable = false, columnDefinition = "default current_timestamp")
    public Date lastAccountUpdate;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "soc_friend_last_update", nullable = false, columnDefinition = "default current_timestamp")
    public Date lastFriendsUpdate;

    @OneToMany(mappedBy = "socialAccount")
    @JsonManagedReference
    public Set<SocialAccountFriend> friends = new HashSet<SocialAccountFriend>();


    public SocialAccount() {
        this.lastAccountUpdate = DateTimeUtils.now();
        this.creationDate = DateTimeUtils.now();
        this.lastFriendsUpdate = DateTimeUtils.getMinDate();
    }

    public SocialAccount(User user, ProviderEnum provider) {
        this();
        this.user = user;
        this.provider = provider;
    }

    public static SocialAccount parseFromFacebook(JsonNode json) {
        final SocialAccount socialAccount = new SocialAccount();
        socialAccount.provider = ProviderEnum.FACEBOOK;
        socialAccount.userId = json.path("id").asText();
        socialAccount.fullName = json.path("name").asText();
        socialAccount.userName = json.path("username").asText();
        socialAccount.email = json.path("email").asText();
        socialAccount.gender = GenderEnum.fromFacebook(json.path("gender").asText());
        return socialAccount;
    }

    public static SocialAccount parseFromTwitter(JsonNode json) {
        final SocialAccount socialAccount = new SocialAccount();
        socialAccount.provider = ProviderEnum.TWITTER;
        socialAccount.userId = json.path("id").asText();
        socialAccount.fullName = json.path("name").asText();
        socialAccount.userName = json.path("screen_name").asText();
        socialAccount.email = json.path("email").asText();
        socialAccount.avatarUrl = json.path("profile_image_url").asText();
        return socialAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SocialAccount that = (SocialAccount) o;
        return !(id != null ? !id.equals(that.id) : that.id != null);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }


    /**
     * get friends list from json
     * todo:hazem refactor this
     *
     * @param json json node
     */
    public void parseFriends(JsonNode json) {
        if (json.has("friends")) {
            final JsonNode friendsNode = json.path("friends");
            if (friendsNode.has("data")) {
                for (Object o : friendsNode.path("data")) {
                    this.friends.add(new SocialAccountFriend((JsonNode) o));
                }
            }
        }
    }

    public enum GenderEnum {
        @EnumValue(value = "male")
        MALE,

        @EnumValue(value = "female")
        FEMALE;

        public static GenderEnum fromFacebook(String text) {
            if (text.trim().equalsIgnoreCase("male"))
                return GenderEnum.MALE;

            if (text.trim().equalsIgnoreCase("female"))
                return GenderEnum.FEMALE;

            return null;
        }
    }
}



