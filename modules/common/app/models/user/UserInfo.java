/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import com.avaje.ebean.SqlRow;
import models.Language;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_usr_info")
public class UserInfo extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "inf_id", nullable = false)
    public Long id;

    @Version
    public Long version;

    @OneToOne
    @JoinColumn(name = "usr_id", nullable = false)
    public User user;

    @Column(name = "inf_latitude")
    public String latitude;

    @Column(name = "inf_longitude")
    public String longitude;

    @Column(name = "inf_city")
    public String city;

    @Column(name = "inf_country")
    public String country;

    @Column(name = "inf_country_code")
    public String countryCode;

    @Column(name = "inf_region")
    public String region;

    @Column(name = "inf_language")
    public String language;

    @Column(name = "inf_timezone")
    public String timezone;

    public UserInfo() {
    }

    public UserInfo(User user, Location location, Language language) {
        set(user, location, language);
    }

    public void set(User user, Location location, Language language) {
        this.country = location.name;
        this.countryCode = location.code;
        this.timezone = location.timezone;
        this.language = language.code;
        this.user = user;
        this.user.timeZone = this.timezone;
    }

    public UserInfo(User user, SqlRow row) {
        this.id = row.getLong("inf_id");
        this.version = row.getLong("inf_version");
        this.country = row.getString("inf_country");
        this.countryCode = row.getString("inf_country_code");
        this.timezone = row.getString("inf_timezone");
        this.language = row.getString("inf_language");
        this.user = user;
        this.user.timeZone = this.timezone;
    }
}
