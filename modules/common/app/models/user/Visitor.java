package models.user;

import core.DateTimeUtils;
import core.HashUtils;
import models.Language;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_visitor")
public class Visitor extends Model {

    @Id
    @Column(name = "vis_uid", unique = true, nullable = false, length = 50)
    public String uid;

    @Version
    public Long version;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "lng_id", nullable = false)
    public Language language;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "loc_id", nullable = false)
    public Location location;

    @Column(name = "vis_timezone", nullable = false)
    public String timezone;

    @Column(name = "vis_ip", nullable = false)
    public String ipAddress;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "vis_creation_date", nullable = false)
    public Date creationDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "vis_update_date", nullable = false)
    public Date updateDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "vis_last_visit_date", nullable = false)
    public Date lastVisitDate;

    @Column(name = "vis_user_agent", nullable = true)
    public String userAgent;

    public Visitor() {
        this.updateDate = DateTimeUtils.now();
    }

    public Visitor(String ipAddress) {
        this();
        this.uid = HashUtils.hashUID(UUID.randomUUID().toString());
        this.ipAddress = ipAddress;
        this.creationDate = DateTimeUtils.now();
        this.lastVisitDate = DateTimeUtils.now();
    }

    public Visitor(User user, String ipAddress, String userAgent) {
        this();
        this.uid = user.uid;
        this.ipAddress = ipAddress;
        this.creationDate = DateTimeUtils.now();
        this.lastVisitDate = DateTimeUtils.now();
        this.language = user.language;
        this.location = user.location;
        this.timezone = user.timeZone;
        this.userAgent = userAgent;
    }
}
