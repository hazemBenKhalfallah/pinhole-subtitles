/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import core.DateTimeUtils;
import models.event.Event;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_usr_event_subs")
public class UserEventSubscription extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "evt_id", insertable = false, updatable = false)
    public Event event;

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "evs_endDate")
    public Date endDate;

    public UserEventSubscription(User user, Event event) {
        this.id = new Id(user, event, DateTimeUtils.now());
        this.user = user;
        this.event = event;

        this.event.subscriptions.add(this);
        this.user.subscriptions.add(this);
    }


    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "usr_id")
        public Long userId;

        @Column(name = "evt_id")
        public Long evendId;

        @Temporal(value = TemporalType.DATE)
        @Column(name = "evs_beginDate")
        public Date beginDate;

        public Id() {
        }

        public Id(User user, Event event, Date beginDate) {
            this.userId = user.id;
            this.evendId = event.id;
            this.beginDate = beginDate;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (beginDate != null ? !beginDate.equals(id.beginDate) : id.beginDate != null) return false;
            if (evendId != null ? !evendId.equals(id.evendId) : id.evendId != null) return false;
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = userId != null ? userId.hashCode() : 0;
            result = 31 * result + (evendId != null ? evendId.hashCode() : 0);
            result = 31 * result + (beginDate != null ? beginDate.hashCode() : 0);
            return result;
        }
    }
}
