/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import core.DateTimeUtils;
import models.event.Event;
import models.planning.ShowTime;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_usr_evt_share")
public class UserEventShare extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JoinColumn(name = "evt_id", insertable = false, updatable = false)
    public Event event;

    @ManyToOne
    @JoinColumn(name = "show_id", insertable = false, updatable = false)
    public ShowTime showTime;


    public UserEventShare() {
    }

    public UserEventShare(User user, Event event, ProviderEnum providerEnum) {
        this();
        this.id = new Id(user, event, DateTimeUtils.now(), providerEnum);
        this.user = user;
        this.event = event;
        this.showTime = event.currentShowTime;
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "usr_id")
        public Long userId;

        @Column(name = "evt_id")
        public Long eventId;

        @Column(name = "show_id")
        public Long showTimeId;

        @Temporal(value = TemporalType.DATE)
        @Column(name = "uvs_shareDate")
        @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
        public Date shareDate;

        @Enumerated(EnumType.STRING)
        @Column(name = "uvs_provider", nullable = false)
        public ProviderEnum providerEnum;

        public Id() {
        }

        public Id(User user, Event event, Date shareDate, ProviderEnum providerEnum) {
            this();
            this.userId = user.id;
            this.eventId = event.id;
            this.showTimeId = event.currentShowTime.id;
            this.shareDate = shareDate;
            this.providerEnum = providerEnum;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (eventId != null ? !eventId.equals(id.eventId) : id.eventId != null) return false;
            if (providerEnum != id.providerEnum) return false;
            if (shareDate != null ? !shareDate.equals(id.shareDate) : id.shareDate != null) return false;
            if (showTimeId != null ? !showTimeId.equals(id.showTimeId) : id.showTimeId != null) return false;
            //noinspection RedundantIfStatement
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = userId != null ? userId.hashCode() : 0;
            result = 31 * result + (eventId != null ? eventId.hashCode() : 0);
            result = 31 * result + (showTimeId != null ? showTimeId.hashCode() : 0);
            result = 31 * result + (shareDate != null ? shareDate.hashCode() : 0);
            result = 31 * result + (providerEnum != null ? providerEnum.hashCode() : 0);
            return result;
        }
    }
}
