/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import com.avaje.ebean.SqlRow;
import core.DateTimeUtils;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_usr_status")
public class UserStatus extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sts_id", nullable = false)
    public Long id;

    @Version
    public int version;

    @OneToOne
    @JoinColumn(name = "usr_id", nullable = false)
    public User user;

    /**
     * (UTC) - the date time that the user completed a successful Signup.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sts_became_member")
    public Date becameMemberDateTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sts_join_date")
    public Date joinDateTime;

    /**
     * (UTC) – the date time a user that had joined with email responded to Make it social.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sts_madeit_social")
    public Date madeItSocialDateTime;

    /**
     * (UTC) - the last date time of the user authentication prompt
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sts_last_auth_prompt")
    public Date lastAuthenticationPromptDateTime;


    /**
     * (UTC) – the last date time of the user category prompt
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sts_last_category_prompt")
    public Date lastCategoryPromptDateTime;

    /**
     * (UTC) – the last date time of the invite friends prompt
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sts_last_invite_prompt")
    public Date lastInvitePromptDateTime;

    /**
     * (UTC) – the last date time of the make it social prompt
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sts_last_makeit_social_prompt")
    public Date lastMakeItSocialPromptDateTime;

    /**
     * - +1 every time the user is prompted for category selection
     */
    @Column(name = "sts_category_prompt", nullable = false, columnDefinition = "default 0")
    public int categoryPromptCount;

    /**
     * - + 1 every time the user is prompted to invite friends
     */
    @Column(name = "sts_invite_friends_prompt", nullable = false, columnDefinition = "default 0")
    public int inviteFriendsPromptCount;

    /**
     * - +1 every time the user is prompted to make it social
     */
    @Column(name = "sts_makeit_social_prompt", nullable = false, columnDefinition = "default 0")
    public int makeItSocialPrompt;

    @Transient
    public Prompt prompt;

    public UserStatus() {
        this.prompt = new Prompt();
        this.joinDateTime = DateTimeUtils.now();
    }

    public UserStatus(User user) {
        this();
        this.user = user;
        this.prompt = new Prompt();
        this.joinDateTime = DateTimeUtils.now();
    }

    public UserStatus(User user, SqlRow row) {
        this();
        this.id = row.getLong("sts_id");
        this.becameMemberDateTime = row.getDate("sts_became_member");
        this.version = row.getInteger("sts_version");
        this.joinDateTime = row.getDate("sts_join_date") != null ? row.getDate("sts_join_date") : DateTimeUtils.now();
        this.lastMakeItSocialPromptDateTime = row.getUtilDate("sts_last_makeit_social_prompt");
        this.madeItSocialDateTime = row.getUtilDate("sts_madeit_social");
        this.user = user;
    }

    public void set(User user) {
        if (this.joinDateTime == null)
            this.joinDateTime = DateTimeUtils.now();
        this.prompt = new Prompt();
        this.becameMemberDateTime = DateTimeUtils.now();
        this.user = user;
    }

    public boolean hasDelayForLastAuthPromptBeenExceededSinceJoinDate(int minutes) {
        return this.joinDateTime == null || DateTimeUtils.isBefore(this.joinDateTime, DateTimeUtils.addMinutesFromNow(-minutes), true);
    }

    public boolean hasDelayForLastAuthPromptBeenExceeded(int minutes) {
        return this.lastAuthenticationPromptDateTime == null || DateTimeUtils.isBefore(this.lastAuthenticationPromptDateTime, DateTimeUtils.addMinutesFromNow(-minutes), true);
    }

    public boolean hasDelayForCategoryPromptBeenExceeded(int minutes) {
        return this.lastCategoryPromptDateTime == null || DateTimeUtils.isBefore(this.lastCategoryPromptDateTime, DateTimeUtils.addMinutesFromNow(-minutes), true);
    }

    public boolean hasDelayForLastInvitePromptBeenExceeded(int minutes) {
        return this.lastInvitePromptDateTime == null ||
                DateTimeUtils.isBefore(this.lastInvitePromptDateTime,
                        DateTimeUtils.addMinutesFromNow(-minutes),
                        true);
    }

    public boolean hasMadeItSocial() {
        return this.madeItSocialDateTime != null;
    }


    public class Prompt {


        public Prompt() {
            this.shouldShowAuth = true;
            this.shouldShowCategory = true;
            this.shouldShowFriendPrompt = true;
            this.shouldShowMakeItSocialPrompt = true;
        }

        /**
         * JoinDateTime < (now - X) -- X should be an app parameter -- compare hours.
         * visitor is not a member.
         * Visitor.LastAuthPrompt < (now - y) -- y should be an app parameter -- compare hours.
         */
        public boolean shouldShowAuth;


        public boolean shouldShowCategory;


        public boolean shouldShowFriendPrompt;


        public boolean shouldShowMakeItSocialPrompt;

    }
}
