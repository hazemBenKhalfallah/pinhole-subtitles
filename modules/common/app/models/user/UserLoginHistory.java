/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import core.DateTimeUtils;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Represent a Login information. Every time the user log to the application we track the time when his session begin
 * and the time when his session ends.
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_histlogin")
public class UserLoginHistory extends Model {

    @Id
    @Column(name = "hlo_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public int version;

    @ManyToOne
    @JoinColumn(name = "usr_id", nullable = false)
    public User user;

    @Column(name = "hlo_session_id", nullable = false, length = 150)
    public String sessionId;

    @Column(name = "hlo_ip_address", nullable = false, length = 50)
    public String ipAddress;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "hlo_login_date", nullable = false)
    public Date loginDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "hlo_logout_date", nullable = true)
    public Date logoutDate;

    /**
     * Default constructor
     */
    public UserLoginHistory() {
        this.loginDate = DateTimeUtils.now();
    }

    /**
     * Constructor with mandatory fields
     *
     * @param user      the user
     * @param ipAddress the ip address from where the user connected to the application
     */
    public UserLoginHistory(User user, String ipAddress) {
        this();
        this.ipAddress = ipAddress;
        this.user = user;
        this.sessionId = user.sessionId;
    }
}
