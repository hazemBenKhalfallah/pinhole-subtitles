/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import core.DateTimeUtils;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Represent a friendship between two user
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_user_friend")
public class UserFriend extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JoinColumn(name = "usr_friend_id", insertable = false, updatable = false)
    public User friend;


    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "ufr_since", nullable = false)
    public Date since;

    @Column(name = "ufr_blocked", nullable = false, columnDefinition = "default false not null")
    public Boolean blocked;


    /**
     * Default constructor
     */
    public UserFriend() {
        this.since = DateTimeUtils.now();
    }

    /**
     * Ebean.s
     * <p/>
     * Full constructor
     *
     * @param user   the current user
     * @param friend the friend
     */
    public UserFriend(User user, User friend) {
        this();
        this.id = new Id(user, friend);
        this.user = user;
        this.friend = friend;

        this.user.friends.add(this);
        this.friend.friends.add(this);
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "usr_id")
        public Long userId;

        @Column(name = "usr_friend_id")
        public Long friendId;

        public Id() {
        }

        public Id(User user, User friend) {
            this.userId = user.id;
            this.friendId = friend.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (friendId != null ? !friendId.equals(id.friendId) : id.friendId != null) return false;
            //noinspection RedundantIfStatement
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = userId != null ? userId.hashCode() : 0;
            result = 31 * result + (friendId != null ? friendId.hashCode() : 0);
            return result;
        }
    }
}
