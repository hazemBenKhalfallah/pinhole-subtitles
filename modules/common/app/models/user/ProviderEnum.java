/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import com.avaje.ebean.annotation.EnumValue;

/**
 * @author ayassinov
 */
public enum ProviderEnum {
    @EnumValue(value = "facebook")
    FACEBOOK,

    @EnumValue(value = "twitter")
    TWITTER;

    public static boolean isValidValue(String provider) {
        return provider != null && (provider.trim().toLowerCase().equals("facebook") || provider.trim().toLowerCase().equals("twitter"));
    }

    public static ProviderEnum get(String provider) {
        if (provider.trim().toLowerCase().equals("facebook"))
            return FACEBOOK;

        if (provider.trim().toLowerCase().equals("twitter"))
            return TWITTER;

        return null;
    }


    @Override
    public String toString() {
        if (this.equals(FACEBOOK))
            return "facebook";
        else if (this.equals(TWITTER))
            return "twitter";
        return "";
    }
}
