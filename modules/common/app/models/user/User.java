/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import com.avaje.ebean.SqlRow;
import com.google.common.base.Optional;
import core.DateTimeUtils;
import core.HashUtils;
import core.StringUtils;
import core.log.Log;
import models.Language;
import models.box.PinCode;
import models.box.TvBox;
import models.category.UserCategory;
import models.event.Event;
import models.event.UserEventWatch;
import models.extra.UserExtraWatch;
import models.tracking.AffiliateInvitation;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.*;

/**
 * Represent a User it can be an administration user or a user of the web site
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_user")
public class User extends Model {

    public static final String FB_IMAGE_URL = "https://graph.facebook.com/%s/picture?type=square";

    /**
     * Id for the PINHOLE USER. USED for system recommendation.
     */
    public static final Long PINHOLE_USER_ID = 0L;

    public static final String PINHOLE_USER_NAME = "Pinhole";

    /**
     * Represent the user state
     */
    public final static Integer NOT_ACTIVATED = 0;
    public final static Integer ACTIVATED = 1;
    public final static Integer BLOCKED = 2;


    @Id
    @Column(name = "usr_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(name = "usr_uid", unique = true, nullable = false)
    public String uid;

    @Version
    public Long version;

    @Column(name = "usr_session_id", unique = true, length = 150)
    public String sessionId;

    @Column(name = "usr_name", length = 150)
    public String name;

    /**
     * email is used in equality verification and need to be always provided
     */
    @Column(name = "usr_email", unique = true, length = 50)
    public String email;

    @Column(name = "usr_password", length = 64)
    public String password;

    @Column(name = "usr_account_type", length = 50)
    public String loginType;

    @Column(name = "usr_img_url", length = 200)
    public String imageUrl;

    @Column(name = "usr_timezone", length = 20, columnDefinition = "varchar(50) default E'GMT+01:00'")
    public String timeZone;

    @Column(name = "usr_firstLogin", columnDefinition = "boolean default true")
    public boolean firstLogin;

    @Column(name = "usr_isLogged", columnDefinition = "boolean default false")
    public boolean isLogged;

    @Column(name = "usr_isHidden", columnDefinition = "boolean default false")
    public boolean isHidden;

    @Column(name = "usr_isRegisterComplete", columnDefinition = "boolean default false")
    public boolean isRegistered;

    @Column(name = "usr_isadmin", columnDefinition = "boolean default false")
    public boolean isAdmin;

    @Column(name = "usr_status", columnDefinition = "Integer default 0")
    public Integer userStatus;

    @Column(name = "usr_email_opting", columnDefinition = "boolean default true")
    public boolean emailOptin;

    @Column(name = "usr_img_origin", nullable = false, columnDefinition = "default 'default'")
    public String imageOrigin;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "lng_id")
    public Language language;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "loc_id")
    public Location location;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usr_pinCode")
    public PinCode pinCode;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usr_tvBox")
    public TvBox tvBox;

    @ManyToOne
    @JoinColumn(name = "usr_affiliateInvitation")
    public AffiliateInvitation originalAffiliateInvitation;

    @ManyToOne
    @JoinColumn(name = "evt_id")
    public Event currentEvent;

    @OneToOne(mappedBy = "user", fetch = FetchType.EAGER)
    public UserInfo userInfo;

    @OneToOne(mappedBy = "user", fetch = FetchType.EAGER)
    public UserStatus status;

    @OneToOne(mappedBy = "user", fetch = FetchType.EAGER)
    public VideoPreferences videoPreferences;

    @OneToMany(mappedBy = "user")
    @MapKey(name = "provider")
    public Map<ProviderEnum, SocialAccount> socialAccounts = new HashMap<ProviderEnum, SocialAccount>();

    @OneToMany(mappedBy = "user")
    public List<UserFriend> friends = new ArrayList<UserFriend>();

    @OneToMany(mappedBy = "friend")
    public List<UserFriend> users = new ArrayList<UserFriend>();

    @OneToMany(mappedBy = "user")
    public List<UserCategory> categories = new ArrayList<UserCategory>();

    @OneToMany(mappedBy = "user")
    public List<UserEventWatch> watchedEvents = new ArrayList<UserEventWatch>();

    @OneToMany(mappedBy = "user")
    public List<FriendInvitation> invitations = new ArrayList<FriendInvitation>();

    @OneToMany(mappedBy = "user")
    public List<UserEventSubscription> subscriptions = new ArrayList<UserEventSubscription>();

    @OneToMany(mappedBy = "user")
    public List<UserExtraWatch> watchedExtras = new ArrayList<UserExtraWatch>();

    @Transient
    public boolean showWarning = true;

    @Transient
    public boolean isContentManager = false;

    @Transient
    public boolean isRtlName;

    @Transient
    public boolean hasFacebookAccount;

    @Transient
    public boolean hasTwitterAccount;

    @Transient
    public int friendsCount;


    /**
     * Default constructor that will generate a new uid
     */
    public User() {
        this.uid = HashUtils.hashUID(UUID.randomUUID().toString());
        this.isLogged = false;
        this.firstLogin = true;
        this.isRegistered = false;
        this.isHidden = false;
        this.userStatus = 0;
        this.showWarning = true;
        this.isContentManager = false;
        this.emailOptin = true;
        this.loginType = "none";
        this.imageOrigin = "default";
        this.timeZone = "GMT+01:00";
    }

    /**
     * Create a member user using the name, email and the clear password
     *
     * @param name          the name
     * @param email         the valid email address
     * @param clearPassword the clear password
     */
    public User(String name, String email, String clearPassword, Location location, Language language) {
        this();
        this.loginType = "email";
        this.name = name;
        this.password = HashUtils.hashPassword(clearPassword.trim());
        this.location = location;
        this.language = language;
        this.email = email.trim().toLowerCase();
    }

    /**
     * Create a member user the social account
     *
     * @param socialAccount the social account to be used for the membership
     */
    public User(SocialAccount socialAccount, Location location, Language language) {
        this();
        this.loginType = "facebook";
        this.email = socialAccount.email;
        this.name = socialAccount.fullName;
        this.location = location;
        this.language = language;
        this.updateSocialAccount(socialAccount);
    }

    /**
     * Create a user from a visitor cookie
     */
    public User(Language language, Location location) {
        this();
        this.isLogged = false;
        this.isRegistered = false;
        this.loginType = "none";
        this.language = language;
        this.location = location;
        this.timeZone = location.timezone;
        this.userInfo = new UserInfo(this, location, language);
        this.status = new UserStatus(this);
        this.videoPreferences = new VideoPreferences(this, VideoPreferences.DEFAULT_VOLUME_VALUE);
    }

    public User(SqlRow row) {
        this.id = row.getLong("usr_id");
        this.uid = row.getString("usr_uid");
        this.version = row.getLong("usr_version");
        this.sessionId = row.getString("usr_session_id");
        this.name = row.getString("usr_name");
        this.isRtlName = StringUtils.isRtl(this.name);
        this.loginType = row.getString("usr_account_type");
        this.email = row.getString("usr_email");
        this.password = row.getString("usr_password");
        this.emailOptin = row.containsKey("usr_email_opting") && row.getBoolean("usr_email_opting");
        this.isLogged = row.containsKey("usr_islogged") && row.getBoolean("usr_islogged");
        this.isRegistered = row.containsKey("usr_isregistercomplete") && row.getBoolean("usr_isregistercomplete");
        this.isAdmin = row.containsKey("usr_isadmin") && row.getBoolean("usr_isadmin");
        this.imageUrl = row.getString("usr_img_url");
        this.hasFacebookAccount = row.containsKey("has_fb") && row.getBoolean("has_fb");
        this.hasTwitterAccount = row.containsKey("has_tw") && row.getBoolean("has_tw");
        if (row.containsKey("friends"))
            this.friendsCount = row.getInteger("friends");
        if (row.containsKey("loc_id")) {
            this.location = new Location(row); //user.location;
            this.timeZone = this.location.timezone;
        }
        if (row.containsKey("lng_id"))
            this.language = new Language(row);
        if (row.containsKey("uvp_id"))
            this.videoPreferences = new VideoPreferences(row);
        if (row.containsKey("inf_id"))
            this.userInfo = new UserInfo(this, row);
        if (row.containsKey("sts_id"))
            this.status = new UserStatus(this, row);
    }

    public User(Long id) {
        this.id = id;
    }

    /**
     * Set the session information
     */
    public User setSession() {
        this.isLogged = true;
        this.isRegistered = true;
        this.sessionId = HashUtils.hashPassword(UUID.randomUUID().toString());
        return this;
    }

    /**
     * Get the user image URL that he uploaded or the fb image if his account is linked to facebook
     *
     * @return url to user profile image
     */
    public String getImageUrl() {
        if (this.socialAccounts.size() > 0) {
            SocialAccount socialAccount = this.socialAccounts.get(ProviderEnum.FACEBOOK);
            if (socialAccount != null)
                this.imageUrl = String.format(User.FB_IMAGE_URL, socialAccount.userId);
        }
        return this.imageUrl;
    }

    public void setImageUrl(String defaultImageUrl) {
        if (this.socialAccounts.containsKey(ProviderEnum.FACEBOOK)) {
            this.imageUrl = String.format(FB_IMAGE_URL, this.socialAccounts.get(ProviderEnum.FACEBOOK).userId);
            this.imageOrigin = "facebook";
        } else {
            if (defaultImageUrl == null) {
                Log.error(Log.Type.PARAMETER, "Default user image url was not found in parameters.");
            }
            this.imageUrl = defaultImageUrl;
            this.imageOrigin = "default";
        }
    }

    public Optional<SocialAccount> getSocialAccount(ProviderEnum providerEnum) {
        if (this.socialAccounts.size() == 0 || !this.socialAccounts.containsKey(providerEnum))
            return Optional.absent();
        return Optional.of(this.socialAccounts.get(providerEnum));
    }

    public SocialAccount updateSocialAccount(SocialAccount socialAccount) {
        if (this.socialAccounts.containsKey(ProviderEnum.FACEBOOK)) {
            //update social info
            final SocialAccount oldSocialAccount = this.socialAccounts.get(ProviderEnum.FACEBOOK);
            socialAccount.id = oldSocialAccount.id;
            socialAccount.user = this;
            socialAccount.version = oldSocialAccount.version;
            socialAccount.lastAccountUpdate = DateTimeUtils.now();
            socialAccount.lastFriendsUpdate = oldSocialAccount.lastFriendsUpdate;
            socialAccount.creationDate = oldSocialAccount.creationDate;
            socialAccount.friends = oldSocialAccount.friends;
        } else {
            socialAccount.user = this;
            socialAccount.creationDate = DateTimeUtils.now();
            socialAccount.lastAccountUpdate = DateTimeUtils.now();
            socialAccount.lastFriendsUpdate = DateTimeUtils.now();
        }
        this.socialAccounts.put(ProviderEnum.FACEBOOK, socialAccount);
        return socialAccount;
    }

    public void setInfo(Language language, Location location) {
        this.language = language;
        this.location = location;
        this.timeZone = this.location.timezone;

        //set user info
        if (this.userInfo == null || this.userInfo.id == null)
            this.userInfo = new UserInfo(this, location, language);
        else
            this.userInfo.set(this, location, language);

        //set user status
        if (this.status == null || this.status.id == null)
            this.status = new UserStatus(this);
        else
            this.status.set(this);

        //set video preferences
        if (this.videoPreferences == null || this.videoPreferences.id == null)
            this.videoPreferences = new VideoPreferences(this, 60);
        else
            this.videoPreferences.user = this;
    }

    public void setNoSession() {
        this.sessionId = null;
        this.currentEvent = null; //unset current event
        this.isLogged = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        User user = (User) o;

        return uid.equals(user.uid);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + uid.hashCode();
        return result;
    }

    public boolean isSamePassword(String password) {
        return this.password.equals(HashUtils.hashPassword(password.toLowerCase().trim()));
    }

    public void set(String accountType, String email, String fullName) {
        this.loginType = accountType;
        this.email = email;
        this.name = fullName;
    }

    public static List<User> listFromSql(List<SqlRow> rows) {
        final List<User> users = new ArrayList<User>();
        for (SqlRow row : rows)
            users.add(new User(row));
        return users;
    }

}

