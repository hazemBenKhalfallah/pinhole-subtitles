/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.user;

import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Friends related to a social account
 *
 * @author : hazem
 */
@Entity
@Table(name = "pin_usr_social_friend", uniqueConstraints = @UniqueConstraint(columnNames = {"frd_friend_id", "soc_id"}))
@JsonIgnoreProperties(value = {"id", "version"})
public class SocialAccountFriend extends Model {
    @Id
    @Column(name = "frd_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "frd_friend_id", length = 30, nullable = false)
    public String friendId;

    @Column(name = "frd_name", nullable = false, length = 150)
    public String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "frd_status", nullable = false)
    public StatusEnum status;

    @ManyToOne
    @JoinColumn(name = "soc_id", nullable = false)
    @JsonBackReference
    public SocialAccount socialAccount;

    public SocialAccountFriend() {
        this.status = StatusEnum.NOT_INVITED;
    }

    public SocialAccountFriend(JsonNode json) {
        this();
        this.friendId = json.path("id").asText();
        this.name = json.path("name").asText();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null || !(that instanceof SocialAccountFriend)) return false;

        SocialAccountFriend friend = (SocialAccountFriend) that;

        return !(friendId != null ? !friendId.equals(friend.friendId) : friend.friendId != null);


    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (friendId != null ? friendId.hashCode() : 0);
        return result;
    }

    /**
     * copies value of an other SocialAccountFriend object into this object
     *
     * @param that SocialAccountFriend object
     */
    public void copy(SocialAccountFriend that) {
        if (that == null) return;
        this.name = that.name;
    }

    public enum StatusEnum {
        @EnumValue(value = "not_invited")
        NOT_INVITED,
        @EnumValue(value = "invited")
        INVITED,
        @EnumValue(value = "friend")
        FRIEND,
        @EnumValue(value = "blocked")
        BLOCKED;

        /**
         * converts a String value to StatusEnum
         *
         * @param value String value
         * @return StatusEnum
         */
        public static StatusEnum convert(String value) {
            if (value.equalsIgnoreCase("not_invited")) {
                return NOT_INVITED;
            }
            if (value.equalsIgnoreCase("invited")) {
                return INVITED;
            }
            if (value.equalsIgnoreCase("friend")) {
                return FRIEND;
            }
            if (value.equalsIgnoreCase("blocked")) {
                return BLOCKED;
            } else return null;
        }

        public String getValue() {
            if (this.equals(NOT_INVITED)) return "not_invited";
            if (this.equals(BLOCKED)) return "blocked";
            if (this.equals(INVITED)) return "invited";
            return "friend";
        }
    }
}
