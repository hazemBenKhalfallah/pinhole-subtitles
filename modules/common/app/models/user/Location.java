package models.user;

import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_location")
public class Location extends Model {

    @Id
    @Column(name = "loc_id", nullable = false, unique = true, length = 10)
    public String code;

    @JsonIgnore
    @Version
    public Long version;

    @Column(name = "loc_name", nullable = false, length = 50)
    public String name;

    @Column(name = "loc_timezone", nullable = false, length = 10)
    public String timezone;

    @Column(name = "loc_dst", nullable = false, length = 10)
    public String dst;

    @JsonIgnore
    @Column(name = "loc_order", nullable = false, length = 10)
    public Integer order;

    @Column(name = "loc_default", nullable = false, columnDefinition = "default false")
    public boolean isDefault;


    public Location() {
    }

    public Location(String code, String name, String timezone) {
        this();
        this.code = code;
        this.name = name;
        this.timezone = timezone;
    }

    public Location(SqlRow row) {
        this.code = row.getString("loc_id");
        this.version = row.getLong("loc_version");
        this.name = row.getString("loc_name");
        this.timezone = row.getString("loc_timezone");
        this.dst = row.getString("loc_dst");
        this.isDefault = row.containsValue("loc_default") ? row.getBoolean("loc_default") : false;
    }
}
