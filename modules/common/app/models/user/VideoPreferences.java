/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author ayassinov
 */


@Entity
@Table(name = "pin_usr_video_pref")
public class VideoPreferences extends Model {

    public static final int DEFAULT_VOLUME_VALUE = 60;

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "uvp_id", nullable = false)
    public Long id;

    @JsonIgnore
    @Version
    public int version;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "usr_id", nullable = false)
    public User user;


    @Column(name = "uvp_volume", nullable = false, columnDefinition = "default 100")
    public int volume;


    public VideoPreferences(User user, int volume) {
        this.user = user;
        this.volume = volume;
    }

    public VideoPreferences(SqlRow row) {
        this.id = row.getLong("uvp_id");
        this.volume = row.getInteger("uvp_volume") != null ? row.getInteger("uvp_volume") : DEFAULT_VOLUME_VALUE;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        VideoPreferences that = (VideoPreferences) o;
        return !(id != null ? !id.equals(that.id) : that.id != null);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
