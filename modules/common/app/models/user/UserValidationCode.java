/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import core.DateTimeUtils;
import core.HashUtils;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;


/**
 * todo: ayassinov refactor this class
 *
 * @author zied
 */
@Entity
@Table(name = "pin_validation_code")
public class UserValidationCode extends Model {

    public static int EMAIL_VALIDATION_TYPE = 0;
    public static int PASSWORD_RECOVERY_TYPE = 1;

    private final static int NBR_HOURS_OF_VALIDITY = 48;

    @Id
    @Column(name = "val_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usr_id", nullable = false)
    public User user;

    @Column(name = "val_code", nullable = false)
    public String code;

    @Column(name = "val_endtime", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date endValidationDate;

    @Column(name = "val_type", nullable = false, columnDefinition = "default Integer 0")
    public Integer type;

    public UserValidationCode() {
    }

    public UserValidationCode(User user, int type) {
        this.user = user;
        this.code = HashUtils.hashUID(UUID.randomUUID().toString());
        this.type = type;
        this.endValidationDate = DateTimeUtils.addHoursFromNow(NBR_HOURS_OF_VALIDITY);
    }

    public UserValidationCode updateValidity() {
        this.code = HashUtils.hashUID(UUID.randomUUID().toString());
        this.endValidationDate = DateTimeUtils.addHoursFromNow(NBR_HOURS_OF_VALIDITY);
        return this;
    }
}
