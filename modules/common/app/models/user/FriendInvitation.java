/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.user;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * A friend invitation sent from a pinhole user.
 *
 * @author hazem
 */
@Entity
@Table(name = "pin_frd_invite")
public class FriendInvitation extends Model {

    @Id
    @Column(name = "inv_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "inv_email", nullable = false, length = 50)
    @Constraints.MaxLength(50)
    @Constraints.Required
    @Constraints.Email
    public String email;

    @Column(name = "inv_code", nullable = false, length = 6)
    @Constraints.MaxLength(6)
    public String code;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usr_id")
    public User user;


    /**
     * Default constructor
     */
    public FriendInvitation() {

    }

    /**
     * Full constructor
     *
     * @param email the destination email
     * @param code  the generated invitation code
     * @param user  the user how sent the invitation
     */
    public FriendInvitation(String email, String code, User user) {
        this();
        this.email = email;
        this.code = code;
        this.user = user;
    }
}
