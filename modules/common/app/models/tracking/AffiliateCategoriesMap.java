/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.tracking;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author ayassinov
 */
public class AffiliateCategoriesMap {

    private static final Map<String, String> AFFILIATE_CATEGORIES;

    static {
        AFFILIATE_CATEGORIES = new ImmutableMap.Builder<String, String>()
                .put("0", "None")
                .put("1", "Category1")
                .put("2", "Category2")
                .put("3", "Category3")
                .build();

    }

    public static Map<String, String> list() {
        return AFFILIATE_CATEGORIES;
    }

    public static String get(Integer key) {
        return AFFILIATE_CATEGORIES.get(key.toString());
    }
}
