/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.tracking;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author ayassinov
 */
public class AffiliateVerticalsMap {

    private static final Map<String, String> AFFILIATE_VERTICALS;

    static {
        AFFILIATE_VERTICALS = new ImmutableMap.Builder<String, String>()
                .put("1", "Partnership")
                .put("2", "FaceBook")
                .put("3", "twitter")
                .build();

    }

    public static Map<String, String> list() {
        return AFFILIATE_VERTICALS;
    }

    public static String get(Integer key) {
        return AFFILIATE_VERTICALS.get(key.toString());
    }

}
