/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.tracking;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_affiliate")
public class Affiliate extends Model {
    @Id
    @Column(name = "aff_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "aff_vertical", nullable = false)
    public Integer vertical;

    @Column(name = "aff_cat", columnDefinition = "smallint default 0")
    public Integer category;

    @Constraints.Required
    @Constraints.MaxLength(100)
    @Column(name = "aff_name", nullable = false, length = 100)
    public String name;

    @OneToMany(mappedBy = "affiliate", fetch = FetchType.LAZY)
    public List<AffiliateInvitation> affiliateInvitations;

}