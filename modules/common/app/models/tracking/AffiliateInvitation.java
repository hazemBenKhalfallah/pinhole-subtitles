/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.tracking;

import core.DateTimeUtils;
import models.user.User;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_affiliate_invitation")
public class AffiliateInvitation extends Model {
    @Id
    @Column(name = "aff_inv_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Constraints.Required
    @Constraints.MaxLength(100)
    @Column(name = "aff_inv_code", nullable = false, unique = true, length = 100)
    public String code;

    @Constraints.Required
    @Constraints.MaxLength(150)
    @Column(name = "aff_inv_shortDescription", nullable = false, length = 150)
    public String shortDescription;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "aff_inv_creationDate", nullable = false)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Constraints.Required
    public Date creationDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "aff_inv_activationDate")
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date activationDate;

    @ManyToOne
    @JoinColumn(name = "aff_id", nullable = false)
    public Affiliate affiliate;

    @OneToMany(mappedBy = "originalAffiliateInvitation", fetch = FetchType.LAZY)
    public List<User> users;

    public AffiliateInvitation() {
        this.creationDate = DateTimeUtils.currentDateWithoutSeconds();
    }
}
