/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.comment;

import models.user.User;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_comment_report")
public class AbuseReport extends Model {

    @Id
    @Column(name = "rep_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "reason")
    public String reason;

    @ManyToOne
    @JoinColumn(name = "cmt_id", nullable = false)
    public Comment comment;

    @ManyToOne
    @JoinColumn(name = "usr_id", nullable = false)
    public User user;

    public AbuseReport() {
    }

    public AbuseReport(String reason, Comment comment, User user) {
        this.reason = reason;
        this.comment = comment;
        this.user = user;
    }
}
