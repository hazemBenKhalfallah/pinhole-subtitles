/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.comment;

/**
 * @author : hazem
 */
public enum CommentSourceEnum {
    PINHOLE("pinhole"),
    TWITTER("twitter");
    private final String value;

    private CommentSourceEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
