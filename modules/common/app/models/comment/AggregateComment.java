/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.comment;


import com.avaje.ebean.*;
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

/**
 * doc:hazem
 *
 * @author : hazem
 */
@Entity
@Sql
public class AggregateComment {
    private static final String SQL_SELECT_COMMENTS = "select t0.cmt_id as c0, t0.evt_id as c1, t0.show_id as c2, t0.cmt_creationDate as c3, t0.cmt_replyTo as c4,  COUNT(t1.rep_id) as reports \n" +
            "from pin_comment t0 \n" +
            "left outer join pin_comment_report t1 on t1.cmt_id =   t0.cmt_id   \n" +
            "left outer join pin_showTime       t2 on t2.show_id =  t0.show_id  \n" +
            "WHERE t0.cmt_isDeleted = false \n" +
            "GROUP BY t0.cmt_id, t1.rep_id, t2.show_id \n" +
            "ORDER BY t2.show_sysBeginTime DESC, t0.cmt_creationDate DESC";

    public static final String SECTION_NAME = "Comments";
    public static final String PARAM_MAX_DISPLAY_COMMENTS = "MaxCommentsDisplay";

    @OneToOne
    public Comment comment;

    public Integer reports;

    public AggregateComment() {
    }

    public AggregateComment(Comment comment) {
        this();
        this.comment = comment;
        this.reports = comment.abuseReports.size();
    }

    /**
     * doc:hazem
     *
     * @param aggregateComments List of AggregateComment
     * @return List of Comment
     */
    public static List<Comment> extract(List<AggregateComment> aggregateComments) {
        final List<Comment> comments = new ArrayList<Comment>();
        for (AggregateComment aggregateComment : aggregateComments) {
            comments.add(aggregateComment.comment);
        }
        return comments;
    }

    /**
     * doc:hazem
     *
     * @param comments List of Comment
     * @return List of AggregateComment
     */
    public static List<AggregateComment> wrap(List<Comment> comments) {
        final List<AggregateComment> aggregateComments = new ArrayList<AggregateComment>();
        AggregateComment aggregateComment;
        for (Comment comment : comments) {
            aggregateComment = new AggregateComment(comment);
            aggregateComments.add(aggregateComment);
        }
        return aggregateComments;
    }


    /**
     * doc:hazem
     *
     * @param commentParser commentParser
     * @return Query<AggregateComment>
     */
    public static Query<AggregateComment> getQuery(CommentParser commentParser, int maxCommentsDisplay) {
        final RawSql rawSql = RawSqlBuilder
                .parse(SQL_SELECT_COMMENTS)
                .columnMapping("t0.cmt_id", "comment.id")
                .columnMapping("t0.cmt_creationDate", "comment.creationDate")
                .columnMapping("t0.show_id", "comment.showtime.id")
                .columnMapping("t0.evt_id", "comment.event.id")
                .columnMapping("t0.cmt_replyTo", "comment.replyTo.id")
                .create();

        //create configured query
        final Query<AggregateComment> query = Ebean.find(AggregateComment.class)
                .setRawSql(rawSql)
                .where()
                .add(getFilteringExpression(commentParser))
                .add(getPage(commentParser))
                .isNull("t0.cmt_replyTo")
                .setMaxRows(maxCommentsDisplay);

        //exclude reported abusive comments if param is set to true
        if (commentParser.excludeReported) {
            query.having().eq("reports", 0);
        }
        return query;
    }

    /**
     * doc:hazem
     *
     * @param commentParser commentParser
     * @return Expression
     */
    private static Expression getFilteringExpression(CommentParser commentParser) {
        return commentParser.isEmptyOrNull() ? Expr.eq("t0.evt_id", commentParser.eventId) : Expr.in("t0.show_id", commentParser.showtimeIds);
    }

    private static Expression getPage(CommentParser commentParser) {
        return commentParser.fromId < 0 ? Expr.raw("1 = 1") : Expr.lt("t0.cmt_id", commentParser.fromId);
    }
}