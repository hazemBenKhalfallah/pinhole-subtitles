/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.comment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Function;
import models.event.Event;
import models.planning.ShowTime;
import models.user.User;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_tweet")
public class Tweet implements Serializable {
    @Id
    @Column(name = "twt_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(name = "twt_tweet_id", nullable = false)
    public String tweetId;

    @Column(name = "twt_text", nullable = false, columnDefinition = "TEXT")
    public String text;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "twt_creationDate", nullable = false)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date createdAt;

    @Column(name = "twt_source")
    public String source;

    @Column(name = "twt_replyTo")
    public String inReplyToStatusId;

    @Column(name = "twt_user_id")
    public String twitterUserId;

    @Column(name = "twt_user_name")
    public String twitterUsername;

    @Column(name = "twt_user_screen_name")
    public String twitterUserScreenName;

    @Column(name = "twt_user_location")
    public String twitterUserLocation;

    @Column(name = "twt_user_image_url")
    public String twitterUserprofileImageUrl;

    @ManyToOne
    @JoinColumn(name = "usr_id")
    public User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "show_id", nullable = false)
    @Constraints.Required
    @JsonIgnore
    public ShowTime showtime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "evt_id", nullable = false)
    @Constraints.Required
    @JsonIgnore
    public Event event;

    @Transient
    public Boolean fromPinhole = false;

    @Override
    public String toString() {
        return "Tweet{" +
                "\n tweetId=" + tweetId +
                ",\n text='" + text + '\'' +
                ",\n createdAt=" + createdAt +
                ",\n source='" + source + '\'' +
                ",\n inReplyToStatusId=" + inReplyToStatusId + '\'' +
                "\n twitterUserId=" + twitterUserId +
                ",\n twitterUsername='" + twitterUsername + '\'' +
                ",\n twitterUserScreenName='" + twitterUserScreenName + '\'' +
                ",\n twitterUserLocation='" + twitterUserLocation + '\'' +
                ",\n twitterUserprofileImageUrl='" + twitterUserprofileImageUrl + '\'' +
                ",\n user=" + user +
                ",\n showtime=" + showtime.id +
                ",\n event=" + event.id +
                '}';
    }

    /**
     * retrieves Twitter user's id
     */
    final public static Function<Tweet, String> retrieveTwitterUserIds = new Function<Tweet, String>() {
        @Nullable
        @Override
        public String apply(@Nullable Tweet tweet) {
            return tweet.twitterUserId;
        }
    };

    /**
     * retrieves tweets ids
     */
    final public static Function<Tweet, String> retrieveTweetsIds = new Function<Tweet, String>() {
        @Nullable
        @Override
        public String apply(@Nullable Tweet tweet) {
            return tweet.tweetId;
        }
    };
}
