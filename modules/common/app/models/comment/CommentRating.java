/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.comment;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import core.DateTimeUtils;
import models.user.User;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * A rating of user on a comment.
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_comment_rating")
public class CommentRating extends Model {

    @EmbeddedId
    @javax.persistence.Id
    @JsonIgnore
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "cmt_id", insertable = false, updatable = false)
    @JsonBackReference
    public Comment comment;

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    @JsonIgnore
    public User user;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cmr_dateRating", nullable = false)
    public Date dateRating;

    /**
     * Default constructor with date creation set to now
     */
    public CommentRating() {
        this.dateRating = DateTimeUtils.now();
    }

    /**
     * A full constructor
     *
     * @param comment the comment
     * @param user    the user
     */
    public CommentRating(Comment comment, User user) {
        this();
        this.id = new Id(comment, user);
        this.comment = comment;
        this.user = user;
        this.comment.commentRatings.add(this);
    }

/*
    @JsonProperty("user")
    public UserJson getJsonUser() {
        return new UserJson(this.user);
    }
*/

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "cmt_id")
        public Long commentId;

        @Column(name = "usr_id")
        public Long userId;

        public Id() {
        }

        public Id(Comment comment, User user) {
            this.commentId = comment.id;
            this.userId = user.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (commentId != null ? !commentId.equals(id.commentId) : id.commentId != null) return false;
            //noinspection RedundantIfStatement
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = commentId != null ? commentId.hashCode() : 0;
            result = 31 * result + (userId != null ? userId.hashCode() : 0);
            return result;
        }
    }
}
