/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.comment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Optional;
import core.CommonError;
import core.log.Log;
import json.BaseBodyParser;
import models.planning.ShowTime;
import play.libs.Json;
import play.mvc.Http;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author : hazem
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentParser extends BaseBodyParser {
    private static final String showtimeKey = "showtime";
    private static final String excludeReportedKey = "excludeReported";
    private static final String eventKey = "event";
    private static final String fromIdKey = "fromId";

    public List<ShowTime> showTimes = new ArrayList<ShowTime>();

    public int total;

    public Boolean hasNext = false;

    @JsonIgnore
    public List<Comment> comments = new ArrayList<Comment>();

    @JsonIgnore
    public List showtimeIds = new ArrayList<Long>();

    @JsonIgnore
    public Long eventId;

    @JsonIgnore
    public boolean excludeReported = false;

    @JsonIgnore
    public Long fromId = -1l;

    @Override
    public void validate() {
        if (eventId == null && isEmptyOrNull())
            addError(CommonError.Codes.AT_LEAST_ONE_SET);
    }

    public boolean isEmptyOrNull() {
        return showtimeIds == null || showtimeIds.isEmpty();
    }

    @Override
    public Optional<ObjectNode> getSuccessNode() {
        final ObjectNode result = Json.newObject();
        result.put("total", this.total);
        result.put("hasNext", this.hasNext);
        result.put("showtimes", Json.toJson(this.showTimes));
        return Optional.of(result);
    }

    @Override
    @SuppressWarnings("unchecked")
    public CommentParser parseParameters() {
        final Map<String, String[]> queryString = Http.Context.current().request().queryString();
        try {
            if (queryString.containsKey(showtimeKey)) {
                this.showtimeIds = Json.fromJson(Json.parse(queryString.get(showtimeKey)[0]), List.class);
            }
            if (queryString.containsKey(excludeReportedKey)) {
                this.excludeReported = Boolean.valueOf(queryString.get(excludeReportedKey)[0]);
            }

            if (queryString.containsKey(eventKey)) {
                this.eventId = Long.valueOf(queryString.get(eventKey)[0]);
            }

            if (queryString.containsKey(fromIdKey)) {
                this.fromId = Long.valueOf(queryString.get(fromIdKey)[0]);
            }
        } catch (Exception ex) {
            Log.error(Log.Type.PARAMETER, ex, "Error while getting comments parameters");
            this.addError(CommonError.Codes.LIST_COMMENTS_EXCEPTION);
        }

        this.validate();
        return this;
    }
}
