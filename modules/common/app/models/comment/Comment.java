/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.comment;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import core.DateTimeUtils;
import models.event.Event;
import models.planning.ShowTime;
import models.user.User;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Represent a comment of a user on an event.
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_comment")
public class Comment extends Model implements Comparable<Comment> {

    @Id
    @Column(name = "cmt_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    @JsonIgnore
    public Long version;

    @Column(name = "cmt_twitterId")
    @Constraints.Required
    @JsonIgnore
    public String twitterId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usr_id", nullable = false)
    @Constraints.Required
    @JsonIgnore
    public User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "show_id", nullable = false)
    @Constraints.Required
    @JsonIgnore
    public ShowTime showtime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "evt_id", nullable = false)
    @Constraints.Required
    @JsonIgnore
    public Event event;

    @Column(name = "cmt_comment", nullable = false, columnDefinition = "TEXT")
    @Constraints.Required
    public String comment;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cmt_creationDate", nullable = false)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Constraints.Required
    public Date creationDate;

    @Column(name = "cmt_url")
    public String url;

    @Column(name = "cmt_onFacebook", columnDefinition = "boolean default false", nullable = false)
    public boolean onFacebook;

    @Column(name = "cmt_onTweeter", columnDefinition = "boolean default false", nullable = false)
    public boolean onTwitter;

    @Column(name = "cmt_isDeleted", columnDefinition = "boolean default false", nullable = false)
    public boolean deleted;

    @Transient
    public String source = CommentSourceEnum.PINHOLE.getValue();

    @Transient
    @JsonIgnore
    //used when this object is transformed to json to be used as a parameter
    public Boolean excludeReported = null;

    @ManyToOne
    @JoinColumn(name = "cmt_replyTo")
    @JsonBackReference
    public Comment replyTo;

    @OneToMany(mappedBy = "replyTo", fetch = FetchType.LAZY)
    @JsonManagedReference
    @JsonIgnore
    public List<Comment> replies = new ArrayList<Comment>();


    @OneToMany(mappedBy = "comment", fetch = FetchType.EAGER)
    @JsonIgnore
    public List<AbuseReport> abuseReports = new ArrayList<AbuseReport>();

    @OneToMany(mappedBy = "comment", fetch = FetchType.EAGER)
    @JsonManagedReference
    public List<CommentRating> commentRatings = new ArrayList<CommentRating>();

    /**
     * Default constructor with date creation set to now
     */
    public Comment() {
        this.creationDate = DateTimeUtils.now();
        this.onFacebook = false;
        this.onTwitter = false;
        this.deleted = false;
    }

    @Transient
    @JsonProperty(value = "reported")
    @SuppressWarnings("unused")
    public boolean isReported() {
        return !this.abuseReports.isEmpty();
    }

    @JsonProperty(value = "showtime")
    @SuppressWarnings("unused")
    public Long getShowTime() {
        return this.showtime == null ? 0l : this.showtime.id;
    }

    /*   @JsonProperty("user")
       @SuppressWarnings("unused")
       public UserJson getJsonUser() {
           return new UserJson(this.user);
       }
   */
    @JsonProperty("parent")
    public Long getParent() {
        return this.replyTo == null ? 0 : this.replyTo.id;
    }

    @JsonProperty(value = "replies")
    @SuppressWarnings("unused")
    public List<Comment> getFilteredReplies() {
        List<AggregateComment> aggregateComments = AggregateComment.wrap(this.replies);
        if (this.excludeReported == null) {
            this.excludeReported = (replyTo == null || replyTo.excludeReported == null) ? false : replyTo.excludeReported;
        }
        if (excludeReported)
            aggregateComments = Ebean.filter(AggregateComment.class)
                    .eq("comment.deleted", false) // not deleted comments
                    .eq("reports", 0)
                    .filter(aggregateComments);
        else
            aggregateComments = Ebean.filter(AggregateComment.class)
                    .eq("comment.deleted", false) // not deleted comments
                    .filter(aggregateComments);
        return sort(AggregateComment.extract(aggregateComments));
    }

    public void setShowtimeAndEvent(ShowTime showtime) {
        this.showtime = showtime;
        this.event = showtime.event;
    }


    /**
     * doc:hazem
     *
     * @param comments list of comments
     * @return sorted list of comments by creationDate
     */
    private List<Comment> sort(List<Comment> comments) {
        Collections.sort(comments);
        return comments;
    }

    @Override
    public int compareTo(Comment o) {
        return o.creationDate.compareTo(this.creationDate);
    }
}
