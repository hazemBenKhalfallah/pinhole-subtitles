package models.seo;

import core.DateTimeUtils;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Representing a html page that is rendered for the search bots
 * Use this class to track the status of the urls and to generate a sitemap file
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_prerendred")
public class PrerendredHtmlPage extends Model {

    @Id
    @Column(name = "pre_url", nullable = false, length = 200)
    public String url;

    @Column(name = "pre_status", nullable = false)
    public boolean status;

    @Column(name = "pre_last_update", nullable = false, length = 200)
    public Date lastUpdate;

    @Column(name = "pre_change_frequency", nullable = false)
    public String changeFrequency;

    @Column(name = "pre_priority", nullable = false)
    public Double priority;

    public PrerendredHtmlPage(String url, boolean status, String changeFrequency, double priority) {
        this.url = url;
        this.status = status;
        this.changeFrequency = changeFrequency;
        this.priority = priority;
        this.lastUpdate = DateTimeUtils.now();
    }

    public void setStatus(boolean status) {
        this.status = status;
        this.lastUpdate = DateTimeUtils.now();
    }

    public String getLastModificationDate() {
        return DateTimeUtils.format(this.lastUpdate, DateTimeUtils.SITEMAP_DATE_TIME_FORMAT);
    }
}
