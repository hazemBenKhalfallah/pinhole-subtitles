/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_parameter", uniqueConstraints = @UniqueConstraint(columnNames = {"sec_id", "prm_name"}))
public class Parameter extends Model {
    @Id
    @Column(name = "prm_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "prm_name", nullable = false, length = 30)
    @Constraints.Required
    public String name;

    @Column(name = "prm_value", nullable = false, length = 255)
    @Constraints.Required
    public String value;

    @ManyToOne
    @JoinColumn(name = "sec_id")
    @Constraints.Required
    public ParameterSection section;
}
