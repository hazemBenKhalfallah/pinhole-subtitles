package models;

import models.user.User;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author zied
 *         used to track xml file uplaod
 */

@Entity
@Table(name = "pin_upload_history")
public class HistoryUploadModel extends Model {

    @Id
    @Column(name = "hist_id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(name = "hist_date", nullable = false)
    public Date dateUpload;

    @Column(name = "hist_file_name", nullable = false)
    public String fileName;

    @Column(name = "hist_size", nullable = false)
    public long fileSize;

    @ManyToOne
    @JoinColumn(name = "hist_user", nullable = false)
    public User user;

    @Column(name = "hist_file_hash", nullable = false)
    public String fileCheckSum;

    @Column(name = "hist_file_state", nullable = false, columnDefinition = "default 0")
    public Integer fileState;


}
