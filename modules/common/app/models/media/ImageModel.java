package models.media;

/**
 * @author Zied
 *         this model is used to serialize Imgage within other models
 */

public class ImageModel {

    public String url;

    public String height;

    public String width;

    public String label;

    public String alt;

    public ImageModel() {
        this.alt = "";
    }
    public ImageModel(String label) {
        this.alt = "";
        this.label = label;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageModel imageModel = (ImageModel) o;
        if (url != null) if (!url.equals(imageModel.url)) return false;
        if (height != null) if (!height.equals(imageModel.height)) return false;
        if (width != null) if (!width.equals(imageModel.width)) return false;
        if (label != null) if (!label.equals(imageModel.label)) return false;
        if (alt != null) if (!alt.equals(imageModel.alt)) return false;
        return true;
    }
}
