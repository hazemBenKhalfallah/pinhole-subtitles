package models.media;

import com.avaje.ebean.SqlRow;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Zied
 */

@Entity
@Table(name = "pin_media_sizes")
public class MediaSize extends Model {

    @Id
    @Column(name = "media_type_id", nullable = false)
    public Long id;

    @Column(name = "media_height", nullable = false)
    public Long height;

    @Column(name = "media_width", nullable = false)
    public Long width;

    @Column(name = "media_label", nullable = false, length = 100)
    public String label;

    @Column(name = "media_class", nullable = false, length = 30)
    public String type;

    public MediaSize() {
    }

    public MediaSize(SqlRow row) {
        this();
        this.id = row.getLong("media_type_id");
        this.height = row.getLong("media_height");
        this.width = row.getLong("media_width");
        this.label = row.getString("media_label");
        this.type = row.getString("media_class");
    }

    public static List<MediaSize> from(List<SqlRow> rows) {
        final List<MediaSize> mediaSizes = new ArrayList<MediaSize>();
        for (final SqlRow row : rows) {
            mediaSizes.add(new MediaSize(row));
        }
        return mediaSizes;
    }
}
