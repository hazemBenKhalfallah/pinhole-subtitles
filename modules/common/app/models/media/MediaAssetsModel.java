package models.media;

import play.db.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name = "pin_media_assets")
public class MediaAssetsModel extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pin_media_id", nullable = false)
    public Long id;

    @Column(name = "media_relative_url", nullable = false, length = 255)
    public String mediaRelativeUrl;

    @Column(name = "media_object_id", nullable = false, length = 30)
    public String mediaObjectId;

    @OneToOne
    @JoinColumn(name = "media_size", nullable = false)
    public MediaSize mediaSize;

    public MediaAssetsModel() {

    }
}