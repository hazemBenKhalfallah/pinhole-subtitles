package models.media;

/**
 * todo: ayassinov delete object
 * @author Zied
 */

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.File;
import java.util.UUID;

@Table(name = "pin_amazon_History")
@Entity
public class S3File {

    @Id
    public UUID id;

    public String name;

    public String url;

    @Transient
    public File file;


}