/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.extra;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import core.DateTimeUtils;
import models.user.User;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_extra_rating")
public class ExtraRating extends Model {

    @EmbeddedId
    @javax.persistence.Id
    @JsonIgnore
    public Id id = new Id();

    @Version
    @JsonIgnore
    public Long version;

    @ManyToOne
    @JoinColumn(name = "ext_id", insertable = false, updatable = false)
    @JsonBackReference
    public Extra extra;

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    @JsonIgnore
    public User user;


    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "exr_ratingDate")
    public Date ratingDate;

    /*  @JsonProperty("user")
      @SuppressWarnings("unused")
      public UserJson getJsonUser() {
          return new UserJson(this.user);
      }
  */
    public ExtraRating() {
        this.ratingDate = DateTimeUtils.now();
    }

    public ExtraRating(Extra extra, User user) {
        this();
        this.id = new Id(extra, user);
        this.user = user;
        this.extra = extra;
        this.extra.ratings.add(this);
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "ext_id")
        public Long extraId;

        @Column(name = "usr_id")
        public Long userId;

        public Id() {
        }

        public Id(Extra extra, User user) {
            this.extraId = extra.id;
            this.userId = user.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (extraId != null ? !extraId.equals(id.extraId) : id.extraId != null) return false;
            //noinspection RedundantIfStatement
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = extraId != null ? extraId.hashCode() : 0;
            result = 31 * result + (userId != null ? userId.hashCode() : 0);
            return result;
        }
    }
}
