/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import models.event.Channel;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_extra_ads_channel")
public class ExtraAdsChannel extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "exa_id", insertable = false, updatable = false)
    public ExtraAds extraAds;

    @ManyToOne
    @JoinColumn(name = "cha_id", insertable = false, updatable = false)
    public Channel channel;

    public ExtraAdsChannel() {
    }

    public ExtraAdsChannel(ExtraAds extraAds, Channel channel) {
        this.id = new Id(extraAds, channel);
        this.extraAds = extraAds;
        this.channel = channel;
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "exa_id")
        public Long adsId;

        @Column(name = "cha_id")
        public Long channelId;

        public Id() {
        }

        public Id(ExtraAds extraAds, Channel channel) {
            this();
            this.adsId = extraAds.id;
            this.channelId = channel.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (adsId != null ? !adsId.equals(id.adsId) : id.adsId != null) return false;
            //noinspection RedundantIfStatement
            if (channelId != null ? !channelId.equals(id.channelId) : id.channelId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = adsId != null ? adsId.hashCode() : 0;
            result = 31 * result + (channelId != null ? channelId.hashCode() : 0);
            return result;
        }
    }
}
