/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import com.avaje.ebean.annotation.EnumValue;
import com.google.common.collect.ImmutableMap;
import core.DateTimeUtils;
import models.event.Channel;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;

/**
 * An advertisement related to an event <tt>TV Show</tt>.
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_ads")
public class Ads extends Model {

    @Id
    @Column(name = "ads_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "ads_name", nullable = false, length = 80)
    @Constraints.MaxLength(80)
    @Constraints.Required
    public String name;

    @Column(name = "ads_imageUrl", nullable = false, length = 200)
    @Constraints.Required
    @Constraints.MaxLength(200)
    public String imageUrl;

    @Column(name = "ads_externalUrl", nullable = false, length = 200)
    @Constraints.Required
    @Constraints.MaxLength(200)
    public String externalUrl;

    @Constraints.Required
    @Column(name = "ads_description", nullable = false, columnDefinition = "TEXT")
    public String shortDescription;

    /**
     * Concrete begin date calculated based on the minutes begin and the event date
     */
    @Constraints.Required
    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm:ss")
    @Column(name = "ads_beginTime", nullable = false)
    public Date beginTime;

    /**
     * Concrete begin date calculated based on the ads begin date and the minutes end values
     */
    @Constraints.Required
    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm:ss")
    @Column(name = "ads_endTime", nullable = false)
    public Date endTime;

    /**
     * If the ads is not in current time.
     * todo: ayassinov maybe we should remove this info as we can always use the endTime value
     */
    @Column(name = "ads_isold", nullable = false, columnDefinition = "boolean default false")
    public boolean isOld;

    @Constraints.Required
    @ManyToOne
    @JoinColumn(name = "cha_id")
    public Channel channel;

    @Constraints.Required
    @Enumerated(EnumType.STRING)
    @Column(name = "ads_type", nullable = false, columnDefinition = "varchar(30) default 'simple'")
    public TypeEnum adsType;

    @Transient
    public String adsTypeValue;

    @Constraints.Required
    @Constraints.Min(0)
    @Constraints.Max(100)
    @Column(name = "ads_timeout", nullable = false, columnDefinition = "default 0")
    public int timeout;

    @Constraints.Required
    @Constraints.Min(0)
    @Column(name = "ads_pos_x", nullable = false, columnDefinition = "default 0")
    public int posX;

    @Constraints.Required
    @Constraints.Min(0)
    @Column(name = "ads_pos_y", nullable = false, columnDefinition = "default 0")
    public int posY;


    /**
     * Default constructor
     */
    public Ads() {
    }

    /**
     * Constructor with only identity information
     * Be aware that the id is data base auto-generated and should not be set manually
     *
     * @param id the identity value
     */
    public Ads(Long id) {
        this();
        this.id = id;
    }

    /**
     * Full constructor
     *
     * @param channel          the channel
     * @param name             the ads name
     * @param imageUrl         the ads image url
     * @param shortDescription the ads short description
     * @param beginTime        the number of minute based on event begin date
     * @param endTime          the duration of the ads in minutes
     */
    public Ads(Channel channel, String name, String imageUrl, String shortDescription, TypeEnum adsType, int timeout, Date beginTime, Date endTime) {
        this();
        this.name = name;
        this.imageUrl = imageUrl;
        this.shortDescription = shortDescription;
        this.channel = channel;
        this.adsType = adsType;
        this.timeout = timeout;
        this.beginTime = beginTime;
        this.endTime = endTime;
    }

    /**
     * Constructor using an channel .Date and time information will not be defined
     *
     * @param channel the channel
     */
    public Ads(Channel channel) {
        this(channel, null, null, null, TypeEnum.SIMPLE, 0, DateTimeUtils.now(), DateTimeUtils.addHoursFromNow(1));
    }

    /**
     * Get the begin date converted from system timezone to the event timezone
     *
     * @return the begin date of ads with event timezone
     */
    public Date getBeginTimeZoned(String timeZone) {
        return DateTimeUtils.convertFromSystemTimeZone(this.beginTime, timeZone);
    }

    /**
     * Get the end date converted from system timezone to the event timezone
     *
     * @return the end date of ads with event timezone
     */
    public Date getEndTimeZoned(String timeZone) {
        return DateTimeUtils.convertFromSystemTimeZone(this.endTime, timeZone);
    }

    public void convertToSystemTimeZone(String timeZone) {
        this.beginTime = DateTimeUtils.convertToSystemTimeZone(this.beginTime, timeZone);
        this.endTime = DateTimeUtils.convertToSystemTimeZone(this.endTime, timeZone);
    }

    public enum TypeEnum {
        @EnumValue(value = "simple")
        SIMPLE("simple"),

        @EnumValue(value = "yesno")
        YES_NO("yesno");

        private String value;

        private TypeEnum(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public static TypeEnum fromValue(String value) {
            if (value.equalsIgnoreCase(SIMPLE.getValue()))
                return SIMPLE;
            else if (value.equalsIgnoreCase(YES_NO.getValue()))
                return YES_NO;
            else return SIMPLE;
        }
    }


    public static class Types {
        private static final Map<String, String> TYPES_MAP;

        static {
            TYPES_MAP = new ImmutableMap.Builder<String, String>()
                    .put("simple", "Simple")
                    .put("yesno", "With (Yes/No) Choices")
                    .build();
        }

        public static Map<String, String> list() {
            return TYPES_MAP;
        }
    }
}
