/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import core.DateTimeUtils;
import models.planning.ShowTime;
import models.user.User;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_extra_ads_watch")
public class ExtraAdsWatch extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "exa_id", insertable = false, updatable = false)
    public ExtraAds extraAds;

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JoinColumn(name = "show_id", insertable = false, updatable = false)
    public ShowTime showTime;

    @ManyToOne
    @JoinColumn(name = "ext_id", insertable = false, updatable = false)
    public Extra extra;

    public ExtraAdsWatch(User user, ExtraAds extraAds, Extra extra) {
        this.id = new Id(extraAds, user, extra, DateTimeUtils.now());
        this.extraAds = extraAds;
        this.user = user;
        this.extra = extra;
        this.showTime = extra.showtime;
    }


    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "exa_id")
        public Long adsId;

        @Column(name = "usr_id")
        public Long userId;

        @Column(name = "show_id")
        public Long showId;

        @Column(name = "ext_id")
        public Long extraId;

        @Temporal(value = TemporalType.DATE)
        @Column(name = "eaw_viewDate")
        @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
        public Date viewDate;

        public Id() {
        }

        public Id(ExtraAds extraAds, User user, Extra extra, Date viewDate) {
            this();
            this.adsId = extraAds.id;
            this.userId = user.id;
            this.extraId = extra.id;
            this.showId = extra.showtime.id;
            this.viewDate = viewDate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (adsId != null ? !adsId.equals(id.adsId) : id.adsId != null) return false;
            if (showId != null ? !showId.equals(id.showId) : id.showId != null) return false;
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;
            //noinspection RedundantIfStatement
            if (viewDate != null ? !viewDate.equals(id.viewDate) : id.viewDate != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = adsId != null ? adsId.hashCode() : 0;
            result = 31 * result + (userId != null ? userId.hashCode() : 0);
            result = 31 * result + (showId != null ? showId.hashCode() : 0);
            result = 31 * result + (viewDate != null ? viewDate.hashCode() : 0);
            return result;
        }
    }
}
