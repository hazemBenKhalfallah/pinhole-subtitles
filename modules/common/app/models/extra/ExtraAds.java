/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import com.avaje.ebean.SqlRow;
import com.avaje.ebean.annotation.Formula;
import com.fasterxml.jackson.annotation.JsonIgnore;
import core.DateTimeUtils;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_extra_ads")
public class ExtraAds extends Model {

    @Id
    @Column(name = "exa_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(name = "exa_title", length = 100, nullable = true)
    @Constraints.Required
    public String title;

    @Column(name = "exa_description", length = 100, nullable = true)
    public String description;

    @Column(name = "exa_url", length = 250)
    public String url;

    @Column(name = "exa_product_url", length = 250)
    public String productUrl;

    @Column(name = "exa_hashtag", length = 25)
    public String hashtag;

    @Column(name = "exa_length")
    public int length;

    @Column(name = "exa_everywhere", nullable = false, columnDefinition = "default false")
    @JsonIgnore
    public Boolean isOnAll = false;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "exa_creationdate", nullable = false)
    @JsonIgnore
    public Date aireDate = DateTimeUtils.now();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "adv_id")
    @JsonIgnore
    public Advertiser advertiser;

    @OneToMany(mappedBy = "extraAds", fetch = FetchType.EAGER)
    @JsonIgnore
    public List<ExtraAdsChannel> extraAdsChannels = new ArrayList<ExtraAdsChannel>();

    @OneToMany(mappedBy = "extraAds", fetch = FetchType.EAGER)
    @JsonIgnore
    public List<ExtraAdsEvent> extraAdsEvents = new ArrayList<ExtraAdsEvent>();

    @OneToMany(mappedBy = "extraAds", fetch = FetchType.EAGER)
    @JsonIgnore
    public List<ExtraAdsWatch> extraAdsWatches = new ArrayList<ExtraAdsWatch>();

    @Formula(select = "COALESCE(exaViews${ta}.viewCount, 0) as viewCount", join = "left join (select count(exa_id) as viewCount , exa_id  from  pin_extra_ads_watch group by exa_id) as exaViews${ta} on exaViews${ta}.exa_id = ${ta}.exa_id")
    public int viewCount;

    @Transient
    public int userViewCount;

    public ExtraAds() {
    }

    public ExtraAds(SqlRow row) {
        this.id = row.getLong("exa_id");
        this.url = row.getString("exa_url");
        this.productUrl = row.getString("exa_product_url");
        this.hashtag = row.getString("exa_hashtag");
        this.length = row.getInteger("exa_length");
        this.viewCount = row.getInteger("views");
        this.userViewCount = row.getInteger("userViews");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ExtraAds extraAds = (ExtraAds) o;
        return !(id != null ? !id.equals(extraAds.id) : extraAds.id != null);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
