/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import core.DateTimeUtils;
import models.user.User;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_extra_report")
public class ExtraReport extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "ext_id", insertable = false, updatable = false)
    public Extra extra;

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @Column(name = "exr_comment", columnDefinition = "TEXT")
    public String comment;

    public ExtraReport() {
    }

    public ExtraReport(Extra extra, User user, String comment) {
        this();
        this.id = new Id(extra, user);
        this.extra = extra;
        this.user = user;
        this.comment = comment;
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "ext_id")
        public Long extraId;

        @Column(name = "usr_id")
        public Long userId;

        @Temporal(value = TemporalType.DATE)
        @Column(name = "exr_reportDate")
        @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
        public Date reportDate;

        public Id() {
            this.reportDate = DateTimeUtils.now();
        }

        public Id(Extra extra, User user) {
            this();
            this.extraId = extra.id;
            this.userId = user.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (extraId != null ? !extraId.equals(id.extraId) : id.extraId != null) return false;
            if (reportDate != null ? !reportDate.equals(id.reportDate) : id.reportDate != null) return false;
            //noinspection RedundantIfStatement
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = extraId != null ? extraId.hashCode() : 0;
            result = 31 * result + (userId != null ? userId.hashCode() : 0);
            result = 31 * result + (reportDate != null ? reportDate.hashCode() : 0);
            return result;
        }
    }

}
