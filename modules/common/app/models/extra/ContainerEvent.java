/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.extra;

import models.event.Event;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_container_event")
public class ContainerEvent extends Model {
    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "evt_id", insertable = false, updatable = false)
    public Event event;

    @ManyToOne
    @JoinColumn(name = "ctn_id", insertable = false, updatable = false)
    public Container container;

    public ContainerEvent(Container container, Event event) {
        this.event = event;
        this.container = container;
        this.id = new Id(event.id, container.id);
    }

    @Embeddable
    public static class Id {
        @Column(name = "evt_id")
        public Long eventId;

        @Column(name = "ctn_id")
        public Long containerId;

        public Id() {
        }

        public Id(Long eventId, Long containerId) {
            this.eventId = eventId;
            this.containerId = containerId;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Id)) return false;

            Id id = (Id) o;

            if (containerId != null ? !containerId.equals(id.containerId) : id.containerId != null) return false;
            //noinspection RedundantIfStatement
            if (eventId != null ? !eventId.equals(id.eventId) : id.eventId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = eventId != null ? eventId.hashCode() : 0;
            result = 31 * result + (containerId != null ? containerId.hashCode() : 0);
            return result;
        }
    }

}
