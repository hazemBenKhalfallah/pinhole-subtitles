/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import core.DateTimeUtils;
import models.event.Event;
import models.planning.ShowTime;
import models.user.User;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_user_extra_watch")
public class UserExtraWatch extends Model {

    @Embedded
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JoinColumn(name = "ext_id", insertable = false, updatable = false)
    public Extra extra;

    @ManyToOne
    @JoinColumn(name = "show_id", insertable = false, updatable = false)
    public ShowTime showTime;

    @ManyToOne
    @JoinColumn(name = "evt_id", insertable = false, updatable = false, nullable = true)
    public Event event;

    /**
     * Default constructor
     */
    public UserExtraWatch() {
    }

    /**
     * A Constructor representing a user watching an event in the current time
     *
     * @param user  the user
     * @param extra the current extra
     */
    public UserExtraWatch(User user, Extra extra) {
        this(user, extra, DateTimeUtils.now());
    }

    /**
     * A Constructor representing a user watching an event starting from a given date
     *
     * @param user     the user
     * @param extra    the extra
     * @param viewDate the date when user start watching
     */
    public UserExtraWatch(User user, Extra extra, Date viewDate) {
        this();
        this.id = new Id(user, extra, viewDate);
        this.user = user;
        this.extra = extra;
        this.showTime = extra.showtime;
        this.event = extra.event;

        this.user.watchedExtras.add(this);
        this.extra.watchedUsers.add(this);
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "usr_id")
        public Long userId;

        @Column(name = "ext_id")
        public Long extraId;

        @Column(name = "show_id")
        public Long showTimeId;

        @Temporal(value = TemporalType.DATE)
        @Column(name = "uew_viewDate")
        @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
        public Date viewDate;

        public Id() {
        }

        public Id(User user, Extra extra, Date viewDate) {
            this();
            this.userId = user.id;
            this.extraId = extra.id;
            this.showTimeId = extra.showtime.id;
            this.viewDate = viewDate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (viewDate != null ? !viewDate.equals(id.viewDate) : id.viewDate != null) return false;
            if (extraId != null ? !extraId.equals(id.extraId) : id.extraId != null) return false;
            if (showTimeId != null ? !showTimeId.equals(id.showTimeId) : id.showTimeId != null) return false;
            //noinspection RedundantIfStatement
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = userId != null ? userId.hashCode() : 0;
            result = 31 * result + (extraId != null ? extraId.hashCode() : 0);
            result = 31 * result + (showTimeId != null ? showTimeId.hashCode() : 0);
            result = 31 * result + (viewDate != null ? viewDate.hashCode() : 0);
            return result;
        }
    }
}
