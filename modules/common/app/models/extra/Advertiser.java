/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_advertiser")
public class Advertiser extends Model {

    @Id
    @Column(name = "adv_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(name = "adv_name", length = 150)
    public String name;

    @Column(name = "adv_credit", length = 150)
    public int viewsCredit;

    List<ExtraAds> extraAdses = new ArrayList<ExtraAds>();

}
