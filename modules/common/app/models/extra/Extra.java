/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import com.avaje.ebean.SqlRow;
import com.avaje.ebean.annotation.EnumValue;
import com.avaje.ebean.annotation.Formula;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;
import core.DateTimeUtils;
import models.enums.DocumentTypeEnum;
import models.event.Event;
import models.planning.ShowTime;
import models.resources.SqlColumn;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * An extra information related to an event
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_extra")
public class Extra extends Model {

    @Id
    @Column(name = SqlColumn.Video.VIDEO_ID)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    @JsonIgnore
    public Long version;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SqlColumn.Event.EVENT_ID, nullable = false)
    @Constraints.Required
    @JsonIgnore
    public Event event;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SqlColumn.ShowTime.SHOW_TIME_ID, nullable = false)
    @Constraints.Required
    @JsonIgnore
    public ShowTime showtime;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "ext_beginTime")
    public Date beginTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "ext_endTime")
    public Date endTime;

    @Column(name = SqlColumn.Video.URL, nullable = false, columnDefinition = "TEXT")
    @Constraints.Required
    public String content;

    @Column(name = "ext_priority", nullable = false, columnDefinition = "bigint default 0")
    @Constraints.Required
    public Long priority;

    @Enumerated(EnumType.STRING)
    @Column(name = SqlColumn.Video.TYPE, nullable = false, columnDefinition = "varchar(30) default 'video'")
    public TypeEnum contentType;

    @Column(name = SqlColumn.Video.LENGTH)
    public String length;

    @Column(name = SqlColumn.Video.SPLASH, length = 250)
    public String splash;

    @Column(name = SqlColumn.Video.TITLE, length = 100)
    public String title;

    @Column(name = "ext_description", columnDefinition = "TEXT")
    public String description;

    @Column(name = "ext_publicationDate")
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date publicationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = SqlColumn.Video.PROVIDER, nullable = false, columnDefinition = "varchar(20) default 'youtube'")
    public ProviderEnum providerType;


    @Transient
    public String contentTypeValue;

    @Transient
    public String providerTypeValue;

    @Formula(select = "COALESCE(extcom${ta}.commentCount, 0) as commentCount", join = "left join (select count(exc_id) as commentCount, ext_id from pin_extra_comment group by ext_id) as extcom${ta} on extcom${ta}.ext_id = ${ta}.ext_id ")
    public int commentCount;

    @Formula(select = "COALESCE(extlike${ta}.likeCount, 0) as likeCount", join = "left join (select count(usr_id) as likeCount, ext_id from pin_extra_rating group by ext_id) as extlike${ta} on extlike${ta}.ext_id = ${ta}.ext_id ")
    public int likeCount;

    @Formula(select = "COALESCE(extview${ta}.viewUsersCount, 0) as viewUsersCount", join = "left join (select count(usr_id) as viewUsersCount, ext_id from pin_user_extra_watch group by ext_id) as extview${ta} on extview${ta}.ext_id = ${ta}.ext_id ")
    public int viewCount;

    @Formula(select = "COALESCE(extshare${ta}.shareCount, 0) as shareCount", join = "left join (select count(usr_id) as shareCount, ext_id from pin_usr_extra_share group by ext_id) as extshare${ta} on extshare${ta}.ext_id = ${ta}.ext_id ")
    public int shareCount;

    @Formula(select = "COALESCE(extreport${ta}.reportCount, 0) as reportCount", join = "left join (select count(usr_id) as reportCount, ext_id from pin_extra_report group by ext_id) as extreport${ta} on extreport${ta}.ext_id = ${ta}.ext_id ")
    public int reportCount;

    @Formula(select = "COALESCE(exttrend${ta}.trendingviews, 0) as trendingviews", join = "left join (select trendingviews, ext_id from pin_view_trending_extra) as exttrend${ta} on exttrend${ta}.ext_id = ${ta}.ext_id")
    public int viewCountForTrending;

    @Formula(select = "COALESCE(extpopular${ta}.popularviews, 0) as popularviews", join = "left join (select popularviews, ext_id from pin_view_popular_extra) as extpopular${ta} on extpopular${ta}.ext_id = ${ta}.ext_id")
    public int viewCountForPopular;

    //@Formula(select = "", join = "")
    @Transient
    public int userViewCount = 0;

    @Transient
    public int userReportCount = 0;

    @Transient
    public boolean hasImages = false;

    public Extra(SqlRow row) {
        this.id = row.getLong("ext_id");
        this.contentTypeValue = row.getString("ext_type");
        this.providerTypeValue = row.getString(SqlColumn.Video.PROVIDER);
        this.contentType = Extra.TypeEnum.fromValue(this.contentTypeValue);
        this.providerType = Extra.ProviderEnum.fromValue(this.providerTypeValue);
        this.event = new Event();
        this.event.id = row.getLong("evt_id");
        this.showtime = new ShowTime();
        this.showtime.id = row.getLong("show_id");
        this.showtime.beginTimeSystem = row.getDate("show_sysbegintime");
        this.showtime.endTimeSystem = row.getDate("show_sysendtime");
    }


    @JsonProperty(value = "showtime")
    @Transient
    public Long getShowTime() {
        return this.showtime == null ? 0l : this.showtime.id;
    }

    @OneToMany(mappedBy = "extra", fetch = FetchType.EAGER)
    @JsonManagedReference
    public List<ExtraComment> comments = new ArrayList<ExtraComment>();

    @OneToMany(mappedBy = "extra", fetch = FetchType.LAZY)
    @JsonManagedReference
    public List<ExtraRating> ratings = new ArrayList<ExtraRating>();

    @OneToMany(mappedBy = "extra", fetch = FetchType.LAZY)
    public List<UserExtraWatch> watchedUsers = new ArrayList<UserExtraWatch>();

    @OneToMany(mappedBy = "extra", fetch = FetchType.LAZY)
    public List<UserExtraShare> shares = new ArrayList<UserExtraShare>();

    /**
     * Default constructor. This will set timezone and begin/end time
     */
    public Extra() {
        //set default begin/end date to now
        final Date now = DateTimeUtils.currentDateWithoutSeconds();
        this.beginTime = now;
        this.endTime = now;
        this.contentType = TypeEnum.VIDEO;
        this.providerType = ProviderEnum.YOUTUBE;
        this.contentTypeValue = contentType.getValue();
        this.providerTypeValue = providerType.getValue();
        this.publicationDate = now;
    }

    /**
     * Constructor based on an showtime
     *
     * @param showtime the showtime
     */
    public Extra(ShowTime showtime) {
        this();
        this.showtime = showtime;
        this.event = showtime.event;
        this.priority = 1L;
        this.contentType = TypeEnum.EPISODE;
        this.providerType = ProviderEnum.YOUTUBE;
        this.title = "Episode " + showtime.showTimeRef;
    }

    public enum TypeEnum {
        @EnumValue(value = "video")
        VIDEO("video"),

        @EnumValue(value = "image")
        IMAGE("image"),

        @EnumValue(value = "episode")
        EPISODE("episode");

        private String value;

        private TypeEnum(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public static TypeEnum fromValue(String value) {
            if (value.equalsIgnoreCase(IMAGE.getValue()))
                return IMAGE;
            else if (value.equalsIgnoreCase(VIDEO.getValue()))
                return VIDEO;
            else if (value.equalsIgnoreCase(EPISODE.getValue()))
                return EPISODE;
            else return null;
        }

        public static TypeEnum getType(DocumentTypeEnum type) {
            if (type.equals(DocumentTypeEnum.EPISODE))
                return TypeEnum.EPISODE;
            else
                return TypeEnum.VIDEO;
        }
    }

    public enum ProviderEnum {
        @EnumValue(value = "youtube")
        YOUTUBE("youtube"),

        @EnumValue(value = "dailymotion")
        DAILYMOTION("dailymotion");

        private String value;

        private ProviderEnum(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public static ProviderEnum fromValue(String value) {
            if (YOUTUBE.getValue().equalsIgnoreCase(value))
                return YOUTUBE;
            else if (DAILYMOTION.getValue().equalsIgnoreCase(value))
                return DAILYMOTION;
            else return null;
        }
    }

    public static class Providers {
        private static final Map<String, String> PROVIDERS_MAP;

        static {
            PROVIDERS_MAP = new ImmutableMap.Builder<String, String>()
                    .put("youtube", "YOUTUBE")
                    .put("dailymotion", "DAILYMOTION")
                    .build();
        }

        public static Map<String, String> list() {
            return PROVIDERS_MAP;
        }
    }

    public static class Types {
        private static final Map<String, String> TYPES_MAP;

        static {
            TYPES_MAP = new ImmutableMap.Builder<String, String>()
                    .put("episode", "EPISODE")
                    .put("video", "VIDEO")
                    .put("image", "IMAGE")
                    .build();
        }

        public static Map<String, String> list() {
            return TYPES_MAP;
        }
    }
}
