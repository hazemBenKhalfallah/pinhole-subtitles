/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.extra;

import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonIgnore;
import core.DateTimeUtils;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_container")
public class Container extends Model {
    @Id
    @Column(name = "ctn_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    @JsonIgnore
    public Long version;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "ctn_creationDate")
    @JsonIgnore
    public Date creationDate;

    @Column(name = "ctn_inactive", nullable = false, columnDefinition = "boolean default false")
    @JsonIgnore
    public Boolean inactive = false;

    @Column(name = "ctn_title", nullable = false, length = 80)
    @Constraints.MaxLength(80)
    public String title;

    @Enumerated(EnumType.STRING)
    @Column(name = "ctn_type", nullable = false, columnDefinition = "varchar(30) default 'single'")
    public ContainerTypeEnum containerType;

    @Column(name = "ctn_priority")
    @Constraints.Required
    @JsonIgnore
    public int priority;

    @Transient
    public Integer order;

    @OneToMany(mappedBy = "container")
    @JsonIgnore
    public List<ContainerEvent> containerEvents = new ArrayList<ContainerEvent>();

    @OneToMany(mappedBy = "container")
    @JsonIgnore
    public List<ContainerExtra> containerExtras = new ArrayList<ContainerExtra>();


    public Container() {
        if (this.id == null)
            this.creationDate = DateTimeUtils.now();
    }

    public Container(ContainerTypeEnum containerType) {
        this();
        this.containerType = containerType;
    }

    public enum ContainerTypeEnum {
        @EnumValue(value = "event")
        EVENT,

        @EnumValue(value = "video")
        VIDEO;

        public static List<ContainerTypeEnum> getValues() {
            final List<ContainerTypeEnum> values = new ArrayList<ContainerTypeEnum>();
            values.add(EVENT);
            values.add(VIDEO);
            return values;
        }

        public static ContainerTypeEnum get(String value) {
            if (value.equalsIgnoreCase("EVENT"))
                return EVENT;
            else
                return VIDEO;
        }
    }
}
