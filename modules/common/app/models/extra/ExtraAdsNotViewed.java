/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import core.DateTimeUtils;
import models.planning.ShowTime;
import models.user.User;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_extra_ads_notviewed")
public class ExtraAdsNotViewed extends Model {

    @Id
    @Column(name = "ean_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usr_id")
    public User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "show_id")
    public ShowTime showTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ean_creationDate", nullable = false)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date creationDate;

    public ExtraAdsNotViewed() {
        this.creationDate = DateTimeUtils.now();
    }

    public ExtraAdsNotViewed(User user, ShowTime showTime) {
        this();
        this.user = user;
        this.showTime = showTime;
    }

}
