/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.extra;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import core.DateTimeUtils;
import models.user.User;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_extra_comment")
public class ExtraComment extends Model {

    @Id
    @Column(name = "exc_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    @JsonIgnore
    public Long version;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "exc_commentDate")
    public Date commentDate;

    @Column(name = "cmt_comment", nullable = false, columnDefinition = "TEXT")
    @Constraints.Required
    public String comment;

    @ManyToOne
    @JoinColumn(name = "usr_id", nullable = false)
    @JsonIgnore
    public User user;

    @ManyToOne
    @JoinColumn(name = "ext_id", nullable = false)
    @JsonBackReference
    public Extra extra;

/*    @JsonProperty("user")
    @SuppressWarnings("unused")
    public UserJson getJsonUser() {
        return new UserJson(this.user);
    }*/

    public ExtraComment() {
        this.commentDate = DateTimeUtils.now();
    }

    public ExtraComment(String comment, User user, Extra extra) {
        this();
        this.comment = comment;
        this.user = user;
        this.extra = extra;
    }
}
