/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import models.event.Event;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_extra_ads_event")
public class ExtraAdsEvent extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "exa_id", insertable = false, updatable = false)
    public ExtraAds extraAds;

    @ManyToOne
    @JoinColumn(name = "evt_id", insertable = false, updatable = false)
    public Event event;

    public ExtraAdsEvent() {
    }

    public ExtraAdsEvent(ExtraAds extraAds, Event event) {
        this.id = new Id(extraAds, event);
        this.extraAds = extraAds;
        this.event = event;
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "exa_id")
        public Long adsId;

        @Column(name = "evt_id")
        public Long eventId;

        public Id() {
        }

        public Id(ExtraAds extraAds, Event event) {
            this();
            this.adsId = extraAds.id;
            this.eventId = event.id;
        }

        public Id(Long extraAds, Long event) {
            this();
            this.adsId = extraAds;
            this.eventId = event;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (adsId != null ? !adsId.equals(id.adsId) : id.adsId != null) return false;
            //noinspection RedundantIfStatement
            if (eventId != null ? !eventId.equals(id.eventId) : id.eventId != null) return false;
            return true;
        }

        @Override
        public int hashCode() {
            int result = adsId != null ? adsId.hashCode() : 0;
            result = 31 * result + (eventId != null ? eventId.hashCode() : 0);
            return result;
        }
    }
}
