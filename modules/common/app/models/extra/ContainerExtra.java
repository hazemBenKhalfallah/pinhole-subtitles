/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.extra;

import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_container_extra")
public class ContainerExtra extends Model {
    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "ext_id", insertable = false, updatable = false)
    public Extra extra;

    @ManyToOne
    @JoinColumn(name = "ctn_id", insertable = false, updatable = false)
    public Container container;

    public ContainerExtra(Container container, Extra extra) {
        this.extra = extra;
        this.container = container;
        this.id = new Id(extra.id, container.id);
    }

    @Embeddable
    public static class Id {
        @Column(name = "ext_id")
        public Long extraId;

        @Column(name = "ctn_id")
        public Long containerId;

        public Id() {
        }

        public Id(Long extraId, Long containerId) {
            this.extraId = extraId;
            this.containerId = containerId;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Id)) return false;

            Id id = (Id) o;

            if (containerId != null ? !containerId.equals(id.containerId) : id.containerId != null) return false;
            //noinspection RedundantIfStatement
            if (extraId != null ? !extraId.equals(id.extraId) : id.extraId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = extraId != null ? extraId.hashCode() : 0;
            result = 31 * result + (containerId != null ? containerId.hashCode() : 0);
            return result;
        }
    }

}
