/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import core.DateTimeUtils;
import models.user.User;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * An association between the ads and user
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_ads_user")
public class AdsUser extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JoinColumn(name = "ads_id", insertable = false, updatable = false)
    public Ads ads;

    /**
     * The ads where sent to the TV box
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "adu_datesend", nullable = false)
    public Date dateSend;

    /**
     * The date when the user responded from his TV Box
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "adu_dateresponse")
    public Date dateResponse;

    /**
     * The date when the user viewed the ads on the web site
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "adu_dateviewed")
    public Date dateViewed;

    /**
     * true if user accepted the ads from the ui of his box
     */
    @Column(name = "adu_accepted", nullable = false, columnDefinition = "boolean default false")
    public boolean isAccepted;

    /**
     * Default constructor
     */
    public AdsUser() {
        this.dateSend = DateTimeUtils.now();
    }

    /**
     * Constructor with mandatory field
     *
     * @param user the user
     * @param ads  the ads
     */
    public AdsUser(User user, Ads ads) {
        this();
        this.id = new Id(user, ads);
        this.user = user;
        this.ads = ads;
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "ads_id")
        public Long adsId;

        @Column(name = "usr_id")
        public Long userId;

        public Id() {
        }

        public Id(User user, Ads ads) {
            this.userId = user.id;
            this.adsId = ads.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (adsId != null ? !adsId.equals(id.adsId) : id.adsId != null) return false;
            //noinspection RedundantIfStatement
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = adsId != null ? adsId.hashCode() : 0;
            result = 31 * result + (userId != null ? userId.hashCode() : 0);
            return result;
        }
    }
}
