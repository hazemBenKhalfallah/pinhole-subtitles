/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.extra;

import core.DateTimeUtils;
import models.user.ProviderEnum;
import models.user.User;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ayassinov
 */

@Entity
@Table(name = "pin_usr_extra_share")
public class UserExtraShare extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JoinColumn(name = "ext_id", insertable = false, updatable = false)
    public Extra extra;


    public UserExtraShare() {
    }

    public UserExtraShare(User user, Extra extra, ProviderEnum providerEnum) {
        this();
        this.id = new Id(user, extra, DateTimeUtils.now(), providerEnum);
        this.user = user;
        this.extra = extra;
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "usr_id")
        public Long userId;

        @Column(name = "ext_id")
        public Long extraId;

        @Temporal(value = TemporalType.DATE)
        @Column(name = "ues_shareDate")
        @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
        public Date shareDate;

        @Enumerated(EnumType.STRING)
        @Column(name = "ues_provider", nullable = false)
        public ProviderEnum providerEnum;

        public Id() {
        }

        public Id(User user, Extra extra, Date shareDate, ProviderEnum providerEnum) {
            this();
            this.userId = user.id;
            this.extraId = extra.id;
            this.shareDate = shareDate;
            this.providerEnum = providerEnum;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (extraId != null ? !extraId.equals(id.extraId) : id.extraId != null) return false;
            if (providerEnum != id.providerEnum) return false;
            if (shareDate != null ? !shareDate.equals(id.shareDate) : id.shareDate != null) return false;
            //noinspection RedundantIfStatement
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = userId != null ? userId.hashCode() : 0;
            result = 31 * result + (extraId != null ? extraId.hashCode() : 0);
            result = 31 * result + (shareDate != null ? shareDate.hashCode() : 0);
            result = 31 * result + (providerEnum != null ? providerEnum.hashCode() : 0);
            return result;
        }
    }
}
