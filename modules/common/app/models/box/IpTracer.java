/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.box;

import com.avaje.ebean.Ebean;
import com.google.common.base.Optional;
import core.DateTimeUtils;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Trace the box communication for pin code validation. Needed to block a user when he exceed
 * the number of authorized attempts
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_iptracer")
public class IpTracer extends Model {

    public static final int ATTEMPTS_NB = 5;
    public static final int TIME_WINDOW = 5; // in minutes

    @Id
    @Column(name = "ipr_ip")
    public String ip;

    @Version
    public Long version;

    @Column(name = "ipr_nbattempts", nullable = false)
    public int numberOfAttempts = 0;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ipr_lasttry")
    public Date lastTry;

    /**
     * Default constructor
     */
    public IpTracer() {
        this.lastTry = DateTimeUtils.now();
        this.numberOfAttempts = 1;
    }

    /**
     * Constructor based on the IP address
     *
     * @param ip the IP address value
     */
    public IpTracer(String ip) {
        this();
        this.ip = ip;
    }

    /**
     * Constructor with mandatory fields
     *
     * @param ip               the user ip address
     * @param lastTry          the last time when user tried to validate his pin code
     * @param numberOfAttempts the number of attempts
     */
    public IpTracer(String ip, Date lastTry, int numberOfAttempts) {
        this();
        this.ip = ip;
        this.lastTry = lastTry;
        this.numberOfAttempts = numberOfAttempts;
    }

    public boolean ipIsBanned() {
        return
                this.numberOfAttempts > IpTracer.ATTEMPTS_NB &&  //number of try is over authorisation and
                        DateTimeUtils.isAfter(     //the ban time window is after last try
                                DateTimeUtils.addMinutes(this.lastTry, IpTracer.TIME_WINDOW),
                                DateTimeUtils.now(),
                                false);
    }

    public static Optional<IpTracer> byIp(String ip) {
        return Optional.fromNullable(Ebean.find(IpTracer.class).setId(ip).findUnique());
    }

    public static void updateLastTry(IpTracer ipTracer) {
        if (ipTracer.numberOfAttempts >= ATTEMPTS_NB)  //reset number of retry
            ipTracer.numberOfAttempts = 0;

        ipTracer.numberOfAttempts++;
        ipTracer.lastTry = DateTimeUtils.now();
        Ebean.update(ipTracer);
    }

    public static void reset(IpTracer ipTracer) {
        ipTracer.numberOfAttempts = 0;
        Ebean.update(ipTracer);
    }
}
