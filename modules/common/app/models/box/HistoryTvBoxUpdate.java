/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.box;

import core.DateTimeUtils;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * History of tv box updates
 *
 * @author ayassinov
 */
@Entity
@Table(name = "box_hist_update")
public class HistoryTvBoxUpdate extends Model {

    @Id
    @Column(name = "his_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public int version;

    @ManyToOne
    @Constraints.Required
    @JoinColumn(name = "tvb_id", nullable = false)
    public TvBox tvBox;

    @ManyToOne
    @Constraints.Required
    @JoinColumn(name = "brd_id", nullable = false)
    public BroadcastConfig broadcastConfig;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "his_date_broadcast")
    public Date broadcastDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "his_date_update")
    public Date updateDate;

    @Column(name = "his_description", columnDefinition = "TEXT")
    public String description;

    @Column(name = "his_status", columnDefinition = " default false")
    public boolean status;

    public HistoryTvBoxUpdate() {
        this.status = false;
        this.broadcastDate = DateTimeUtils.now();
    }

    public HistoryTvBoxUpdate(TvBox tvBox, BroadcastConfig broadcastConfig, Date broadcastDate) {
        this();
        this.tvBox = tvBox;
        this.broadcastConfig = broadcastConfig;
        this.broadcastDate = broadcastDate;
    }
}
