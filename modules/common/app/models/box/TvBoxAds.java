/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.box;

import core.DateTimeUtils;
import models.extra.Ads;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_ads_tvbox")
public class TvBoxAds extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "tvb_id", insertable = false, updatable = false)
    public TvBox tvBox;

    @ManyToOne
    @JoinColumn(name = "ads_id", insertable = false, updatable = false)
    public Ads ads;

    /**
     * The ads where sent to the TV box
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "adt_sendDate", nullable = false)
    public Date sendDateTime;

    /**
     * The ads where sent to the TV box
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "adt_responseDate", nullable = false)
    public Date responseDateTime;


    /**
     * The ads where sent to the TV box
     */
    @Column(name = "adt_response", nullable = false)
    public String response;

    /**
     * Default constructor
     */
    public TvBoxAds() {
        this.sendDateTime = DateTimeUtils.now();
    }

    /**
     * Constructor with mandatory field
     *
     * @param ads the ads
     */
    public TvBoxAds(TvBox tvBox, Ads ads) {
        this();
        this.id = new Id(tvBox, ads);
        this.tvBox = tvBox;
        this.ads = ads;
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "ads_id")
        public Long adsId;

        @Column(name = "tvb_id")
        public Long tvBoxId;

        public Id() {
        }

        public Id(TvBox tvBox, Ads ads) {
            this.tvBoxId = tvBox.id;
            this.adsId = ads.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (adsId != null ? !adsId.equals(id.adsId) : id.adsId != null) return false;
            //noinspection RedundantIfStatement
            if (tvBoxId != null ? !tvBoxId.equals(id.tvBoxId) : id.tvBoxId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = adsId != null ? adsId.hashCode() : 0;
            result = 31 * result + (tvBoxId != null ? tvBoxId.hashCode() : 0);
            return result;
        }
    }
}
