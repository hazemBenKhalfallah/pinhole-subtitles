/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.box;

import core.DateTimeUtils;
import models.user.User;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * A PIN code used to link a user account with a TV box
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_code")
public class PinCode extends Model {

    @Id
    @Column(nullable = false)
    public int code;

    @Version
    public Long version;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm:ss ")
    public Date duration;

    @OneToOne(mappedBy = "pinCode")
    public User user;

    /**
     * Default constructor
     */
    public PinCode() {
    }

    /**
     * Constructor with only a pin code value, duration will be null
     *
     * @param code the pin code value
     */
    public PinCode(int code) {
        this(code, null);
    }


    /**
     * Full Constructor
     *
     * @param code     the pin code value
     * @param duration the duration of pin code validity
     */
    public PinCode(int code, Date duration) {
        this();
        this.code = code;
        this.duration = duration;
    }

    /**
     * Check if the pin code is expired based on the duration value
     *
     * @return true if expired
     */
    public boolean expired() {
        return duration == null || DateTimeUtils.isAfter(DateTimeUtils.now(), duration, true);
    }
}
