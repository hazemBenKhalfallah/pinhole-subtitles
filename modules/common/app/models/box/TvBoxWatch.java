/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.box;

import core.DateTimeUtils;
import models.event.Channel;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_channel_watch")
public class TvBoxWatch extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "tvb_id", insertable = false, updatable = false)
    public TvBox tvBox;

    @ManyToOne
    @JoinColumn(name = "cha_id", insertable = false, updatable = false)
    public Channel channel;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "bcw_endDate")
    public Date endDate;

    /**
     * Default constructor
     */
    public TvBoxWatch() {
    }

    /**
     * A Constructor representing a user watching an event in the current time
     */
    public TvBoxWatch(TvBox tvBox, Channel channel) {
        this(tvBox, channel, DateTimeUtils.now());
    }

    /**
     * A Constructor representing a user watching an event starting from a given date
     *
     * @param beginDate the date when user start watching
     */
    public TvBoxWatch(TvBox tvBox, Channel channel, Date beginDate) {
        this();
        this.id = new Id(tvBox, channel, beginDate);
        this.tvBox = tvBox;
        this.channel = channel;

        this.tvBox.watchedChannels.add(this);
        this.channel.watchedChannels.add(this);
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "tvb_id")
        public Long tvBoxId;

        @Column(name = "cha_id")
        public Long channelId;

        @Temporal(value = TemporalType.TIMESTAMP)
        @Column(name = "bcw_beginDate")
        public Date beginDate;

        public Id() {
        }

        public Id(TvBox tvBox, Channel channel, Date beginDate) {
            this();
            this.tvBoxId = tvBox.id;
            this.channelId = channel.id;
            this.beginDate = beginDate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (beginDate != null ? !beginDate.equals(id.beginDate) : id.beginDate != null) return false;
            if (channelId != null ? !channelId.equals(id.channelId) : id.channelId != null) return false;
            //noinspection RedundantIfStatement
            if (tvBoxId != null ? !tvBoxId.equals(id.tvBoxId) : id.tvBoxId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = tvBoxId != null ? tvBoxId.hashCode() : 0;
            result = 31 * result + (channelId != null ? channelId.hashCode() : 0);
            result = 31 * result + (beginDate != null ? beginDate.hashCode() : 0);
            return result;
        }
    }
}
