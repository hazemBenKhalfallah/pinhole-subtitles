/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.box;

import core.DateTimeUtils;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "box_broadcast")
public class BroadcastConfig extends Model {

    @Id
    @Column(name = "brd_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public int version;


    @OneToOne
    @JoinColumn(name = "ver_id", nullable = false)
    public BoxVersion boxVersion;

    /**
     * the number of the box to target in every broadcast update
     */
    @Constraints.Min(1)
    @Constraints.Max(500)
    @Constraints.Required
    @Column(name = "brd_boxes_count", nullable = false)
    public int numberOfBox;

    /**
     * In minutes
     */
    @Constraints.Min(1)
    @Constraints.Required
    @Column(name = "brd_frequency", nullable = false)
    public int frequency;

    /**
     * The date when the broadcast begin
     */
    @Constraints.Required
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "brd_date_begin", nullable = false)
    public Date beginDate;

    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "brd_date_end")
    public Date finishedDate;

    @Column(name = "brd_is_planned", nullable = false, columnDefinition = "default false")
    public boolean planned;

    @Column(name = "brd_is_started", nullable = false, columnDefinition = "default false")
    public boolean started;

    @OneToMany(mappedBy = "broadcastConfig", fetch = FetchType.LAZY)
    public List<HistoryTvBoxUpdate> historyTvBoxUpdates = new ArrayList<HistoryTvBoxUpdate>();

    @Transient
    public int boxesToUpdate;

    public BroadcastConfig() {
        this.planned = false;
        this.started = false;
        this.numberOfBox = 1;
        this.beginDate = DateTimeUtils.currentDateWithoutSeconds();
        this.frequency = 1;
        this.boxesToUpdate = 0;
    }

    public BroadcastConfig(int boxesToUpdate, BoxVersion boxVersion) {
        this();
        this.boxesToUpdate = boxesToUpdate;
        this.boxVersion = boxVersion;
    }

    public BroadcastConfig(BoxVersion boxVersion) {
        this();
        this.boxVersion = boxVersion;
    }


    public int startAfter() {
        return DateTimeUtils.getDifferenceInMinutes(DateTimeUtils.currentDateWithoutSeconds(), this.beginDate);
    }
}
