package models.box;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author hazem
 */
@Entity
@Table(name = "box_version")
public class BoxVersion extends Model {
    @Id
    @Column(name = "ver_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    /**
     * The type of the box should be an upper Case string value. search it on an internal list ?
     * exp: E2 for any system supporting Enigma2
     */
    @ManyToOne
    @JoinColumn(name = "typ_id", nullable = false)
    @Constraints.Required
    public TvBoxType tvBoxType;

    /**
     * The release number should be only numeric and without any symbol or space. Representing a major and a minor version
     * the minor version will be maximum to 9 and the major version is set to any number.
     */
    @Column(name = "ver_version", nullable = false, length = 4, scale = 1)
    @Constraints.Required
    //@Constraints.Pattern(value = "") //todo search for regex
    public float releaseVersion = 1.0f;


    @Column(name = "ver_url", nullable = false, length = 250)
    @Constraints.Required
    @Constraints.MaxLength(250)
    public String url;

    /**
     * 32-bit Hexadecimal character representing the the checksum of the file to be downloaded by the box.
     */
    @Column(name = "ver_checksum", nullable = false, length = 32)
    @Constraints.Required
    @Constraints.MinLength(32)
    @Constraints.MaxLength(32)
    //@Constraints.Pattern(value = "")  //todo check the regex for the hash
    public String checkSum;

    @Column(name = "ver_desc", columnDefinition = "TEXT")
    public String description;

    /**
     * The date when this version will be broadcasted
     */
    @Column(name = "ver_release_date")
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date releaseDate;

    @OneToMany(mappedBy = "boxVersion", fetch = FetchType.LAZY)
    public List<TvBox> tvBoxes = new ArrayList<TvBox>();

    @OneToOne(mappedBy = "boxVersion")
    public BroadcastConfig broadcastConfig;

    public BoxVersion() {
    }

    public BoxVersion(TvBoxType tvBoxType, float releaseVersion, String url, String checkSum, String description) {
        this();
        this.tvBoxType = tvBoxType;
        this.releaseVersion = releaseVersion;
        this.url = url;
        this.checkSum = checkSum;
        this.description = description;
    }

    public BoxVersion(float releaseVersion) {
        this();
        this.releaseVersion = releaseVersion;
    }

    public String formattedVersion() {
        if (this.releaseVersion == (int) this.releaseVersion)
            return String.format("%d", (int) this.releaseVersion) + ".0";
        else
            return String.format("%s", this.releaseVersion);
    }
}
