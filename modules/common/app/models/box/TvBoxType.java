/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.box;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represent a type of TvBox
 *
 * @author ayassinov
 */
@Entity
@Table(name = "box_type")
public class TvBoxType extends Model {

    @Id
    @Column(name = "typ_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "typ_name", length = 50, nullable = false)
    @Constraints.MaxLength(50)
    @Constraints.Required
    public String name;

    @Column(name = "typ_desc", columnDefinition = "TEXT")
    public String description;

    @OneToMany(mappedBy = "tvBoxType")
    public List<BoxVersion> versions = new ArrayList<BoxVersion>();

    public TvBoxType() {
    }

    public TvBoxType(String name, String description) {
        this();
        this.name = name.trim().toUpperCase();
        this.description = description;
    }
}
