/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.box;

import models.user.User;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A User TV BOX.
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_tvbox")
public class TvBox extends Model {

    @Id
    @Column(name = "tvb_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    /**
     * The TV box mac address
     */
    @Column(name = "tvb_mac", nullable = false, unique = true)
    public String mac;

    @Column(name = "tvb_name")
    public String name;

    @ManyToOne
    @JoinColumn(name = "ver_id", nullable = false)
    public BoxVersion boxVersion;

    @ManyToOne
    @JoinColumn(name = "typ_id", nullable = false)
    public TvBoxType tvBoxType;

    /**
     * The current TV box ip
     */
    @Column(name = "tvb_ip")
    public String ip;

    @OneToMany(mappedBy = "tvBox")
    public List<User> users;

    @OneToMany(mappedBy = "tvBox", fetch = FetchType.LAZY)
    public List<HistoryTvBoxUpdate> historyTvBoxUpdates = new ArrayList<HistoryTvBoxUpdate>();

    @OneToMany(mappedBy = "tvBox")
    public List<TvBoxWatch> watchedChannels = new ArrayList<TvBoxWatch>();

    /**
     * Default constructor
     */
    public TvBox() {
    }

    /**
     * Constructor with only mac address information
     *
     * @param mac the TV box mac address
     */
    public TvBox(String mac) {
        this(mac, null);
    }

    /**
     * Constructor with mandatory field
     *
     * @param mac the TV box mac address
     * @param ip  the ip address of the TV box
     */
    public TvBox(String mac, String ip) {
        this();
        this.mac = mac;
        this.ip = ip;
    }
}
