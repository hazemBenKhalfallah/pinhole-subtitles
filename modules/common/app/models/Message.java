package models;


import models.resources.SqlColumn;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author zied
 */
@Entity
@Table(name = "pin_message")
public class Message extends Model {

    @Id
    @Column(name = SqlColumn.Message.ID, unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;


    @Column(name = SqlColumn.Message.CODE, nullable = false)
    @Constraints.Required
    public String code;

    @Column(name = SqlColumn.Message.VALUE, nullable = false, columnDefinition = "TEXT")
    @Constraints.Required
    public String value;


    @ManyToOne
    @JoinColumn(name = SqlColumn.Message.LANGUAGE, nullable = false)
    public Language language;


}
