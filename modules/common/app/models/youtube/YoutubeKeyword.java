/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.youtube;

import com.avaje.ebean.SqlRow;
import models.event.Event;
import models.resources.FieldLength;
import models.resources.SqlColumn;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_youtube_keyword")
public class YoutubeKeyword extends Model {
    @Id
    @Column(name = "ytb_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "ytb_keyword", nullable = false, length = FieldLength.Event.TITLE, unique = true)
    @Constraints.MaxLength(FieldLength.Event.TITLE)
    @Constraints.Required
    public String keyword;

    @ManyToOne
    @JoinColumn(name = SqlColumn.YoutubeSetting.ID, nullable = false)
    @Constraints.Required
    public YoutubeSetting youtubeSetting;

    @ManyToOne
    @JoinColumn(name = SqlColumn.Event.EVENT_ID, nullable = false)
    @Constraints.Required
    public Event event;

    public YoutubeKeyword() {
    }

    public YoutubeKeyword(SqlRow row) {
        this();
        this.id = row.getLong(SqlColumn.YoutubeKeyword.ID);
        this.keyword = row.getString(SqlColumn.YoutubeKeyword.KEYWORD);
        this.youtubeSetting = new YoutubeSetting(row.getLong(SqlColumn.YoutubeSetting.ID));
        this.event = new Event(row.getLong(SqlColumn.Event.EVENT_ID));
        if (row.containsKey("version"))
            this.version = row.getLong("version");
    }

    public YoutubeKeyword(String keyword, Long eventId, Long youtubeSettingId) {
        this.keyword = keyword;
        this.event = new Event(eventId);
        this.youtubeSetting = new YoutubeSetting(youtubeSettingId);
    }

    public static List<YoutubeKeyword> fromSql(List<SqlRow> rows) {
        final List<YoutubeKeyword> list = new ArrayList<YoutubeKeyword>();
        for (SqlRow row : rows) {
            list.add(new YoutubeKeyword(row));
        }
        return list;
    }
}
