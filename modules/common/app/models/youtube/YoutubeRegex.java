/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.youtube;

import com.avaje.ebean.SqlRow;
import models.resources.FieldLength;
import models.resources.SqlColumn;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_youtube_regex")
public class YoutubeRegex extends Model {
    @Id
    @Column(name = SqlColumn.YoutubeRegex.ID)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = SqlColumn.YoutubeRegex.LABEL, nullable = false, length = FieldLength.YoutubeRegex.LABEL, unique = true)
    @Constraints.MaxLength(FieldLength.YoutubeRegex.LABEL)
    @Constraints.Required
    public String label;

    @Column(name = SqlColumn.YoutubeRegex.VALUE, nullable = false)
    @Constraints.Required
    public String value;

    @Column(name = SqlColumn.YoutubeRegex.TYPE, nullable = false)
    @Constraints.Required
    public Integer type;

    @Column(name = SqlColumn.YoutubeRegex.DATE_LOCAL)
    @Constraints.Required
    public String local;

    @Column(name = SqlColumn.YoutubeRegex.VALUE)
    @Constraints.Required
    public String dateFormat;

    public YoutubeRegex() {
    }

    public YoutubeRegex(Long id) {
        this.id = id;
    }


    public YoutubeRegex(SqlRow row) {
        this();
        this.id = row.getLong(SqlColumn.YoutubeRegex.ID);
        this.label = row.getString(SqlColumn.YoutubeRegex.LABEL);
        this.value = row.getString(SqlColumn.YoutubeRegex.VALUE);
        this.type = row.getInteger(SqlColumn.YoutubeRegex.TYPE);
        this.local = row.getString(SqlColumn.YoutubeRegex.DATE_LOCAL);
        this.dateFormat = row.getString(SqlColumn.YoutubeRegex.DATE_FORMAT);
        if (row.containsKey("version"))
            this.version = row.getLong("version");

    }

    public enum RegexTypeEnum {
        DATE(1),
        PARTS(2);
        private Integer value;

        private RegexTypeEnum(int value) {
            this.value = value;
        }

        public Integer getValue() {
            return this.value;
        }
    }
}
