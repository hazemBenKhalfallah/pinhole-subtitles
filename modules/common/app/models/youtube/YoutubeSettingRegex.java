/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.youtube;

import play.db.ebean.Model;

import javax.persistence.*;

/**
 * todo:hazem refactor this
 */
@Entity
@Table(name = "pin_youtube_setting_regex")
public class YoutubeSettingRegex extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "ys_id", insertable = false, updatable = false)
    public YoutubeSetting youtubeSetting;

    @ManyToOne
    @JoinColumn(name = "rg_id", insertable = false, updatable = false)
    public YoutubeRegex youtubeRegex;


    public YoutubeSettingRegex() {
    }

    public YoutubeSettingRegex(YoutubeSetting youtubeSetting, YoutubeRegex youtubeRegex) {
        this.youtubeSetting = youtubeSetting;
        this.youtubeRegex = youtubeRegex;
        this.id = new Id(youtubeSetting.id, youtubeRegex.id);
    }

    @Embeddable
    public static class Id {
        @Column(name = "ys_id")
        public Long youtubeSettingId;

        @Column(name = "rg_id")
        public Long youtubeRegexId;

        public Id() {
        }

        public Id(Long youtubeSettingId, Long youtubeRegexId) {
            this.youtubeSettingId = youtubeSettingId;
            this.youtubeRegexId = youtubeRegexId;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Id)) return false;

            Id id = (Id) o;

            if (youtubeRegexId != null ? !youtubeRegexId.equals(id.youtubeRegexId) : id.youtubeRegexId != null)
                return false;
            //noinspection RedundantIfStatement
            if (youtubeSettingId != null ? !youtubeSettingId.equals(id.youtubeSettingId) : id.youtubeSettingId != null)
                return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = youtubeSettingId.hashCode();
            result = 31 * result + youtubeRegexId.hashCode();
            return result;
        }
    }

}


