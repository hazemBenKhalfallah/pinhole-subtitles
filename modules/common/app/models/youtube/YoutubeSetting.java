/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models.youtube;

import com.avaje.ebean.SqlRow;
import models.event.Event;
import models.resources.SqlColumn;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_youtube_Setting")
public class YoutubeSetting extends Model {
    @Id
    @Column(name = SqlColumn.YoutubeSetting.ID)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = SqlColumn.YoutubeSetting.MIN_LENGTH, columnDefinition = "default 0")
    public Integer minLength;

    @Column(name = SqlColumn.YoutubeSetting.MAX_LENGTH, nullable = false, columnDefinition = "default 0")
    public Integer maxLength;

    @Column(name = SqlColumn.YoutubeSetting.HAS_DATE, columnDefinition = "default false")
    public Boolean hasDate;

    @Column(name = SqlColumn.YoutubeSetting.HAS_PARTS, columnDefinition = "default false")
    public Boolean hasParts;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SqlColumn.Event.EVENT_ID, nullable = false)
    @Constraints.Required
    public Event event;

    @Transient
    public Map<YoutubeRegex.RegexTypeEnum, List<YoutubeRegex>> youtubeRegexeList = new HashMap<YoutubeRegex.RegexTypeEnum, List<YoutubeRegex>>();

    public YoutubeSetting() {
    }

    public YoutubeSetting(SqlRow row) {
        this();
        this.id = row.getLong(SqlColumn.YoutubeSetting.ID);
        this.minLength = row.getInteger(SqlColumn.YoutubeSetting.MIN_LENGTH);
        this.maxLength = row.getInteger(SqlColumn.YoutubeSetting.MAX_LENGTH);
        this.hasDate = row.getBoolean(SqlColumn.YoutubeSetting.HAS_DATE);
        this.hasParts = row.getBoolean(SqlColumn.YoutubeSetting.HAS_PARTS);
        this.event = new Event(row.getLong(SqlColumn.Event.EVENT_ID));
        if (row.containsKey("version"))
            this.version = row.getLong("version");

    }

    public YoutubeSetting(Long id) {
        this.id = id;
    }

    public static List<YoutubeSetting> fromSql(List<SqlRow> rows) {
        final List<YoutubeSetting> list = new ArrayList<YoutubeSetting>();
        for (SqlRow row : rows) {
            list.add(new YoutubeSetting(row));
        }
        return list;
    }
}
