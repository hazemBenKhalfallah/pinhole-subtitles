package models.results;

import core.url.MediaTypesEnum;
import core.url.UrlBuilder;

/**
 * @author ayassinov
 */
public class BaseResourceResult extends BaseResult {

    public Long id;

    public String name;

    public String imageUrl;

    public BaseResourceResult() {
    }

    public BaseResourceResult(Long id, String name, MediaTypesEnum mediaTypesEnum) {
        this.id = id;
        this.name = name;
        this.imageUrl = UrlBuilder.getImageUrl(this.id, UrlBuilder.SIZE_NORMAL, mediaTypesEnum);
    }
}
