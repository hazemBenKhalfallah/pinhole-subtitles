package models.results.browse;

import models.results.category.CategoryResult;
import models.results.channel.ChannelResult;

import java.io.Serializable;
import java.util.List;

public class BrowseMenuResult implements Serializable {

    public List<ChannelResult> channels;
    public List<CategoryResult> categories;

    public BrowseMenuResult(List<ChannelResult> channelList, List<CategoryResult> categoryList){
        this.channels = channelList;
        this.categories  = categoryList;
    }
}
