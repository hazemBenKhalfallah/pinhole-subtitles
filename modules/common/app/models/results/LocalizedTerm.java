package models.results;

import com.avaje.ebean.SqlRow;
import models.resources.SqlColumn;

import java.io.Serializable;

public class LocalizedTerm implements Serializable {

    public static final String DEFAULT_LANGUAGE_CODE = "fr";

    public String term;
    public String languageCode;

    public LocalizedTerm(SqlRow row){
       this.term = row.getString(SqlColumn.LocalizedTerm.TERM);
       this.languageCode = row.getString(SqlColumn.LocalizedTerm.LANGUAGE_CODE);
    }
}
