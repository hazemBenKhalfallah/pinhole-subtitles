package models.results.contentItem;

import com.avaje.ebean.annotation.EnumValue;
import models.results.BaseResult;
import models.results.category.CategoryResult;
import models.results.channel.BaseChannelResult;
import models.results.event.BaseEventResult;
import models.user.User;

public class BaseContentItem extends BaseResult {
    public BaseResult entity;
    public ContentItemType type;

    // SEO related
//    public List<String> keywords;
//    public String metaDescription;

    public BaseContentItem(BaseResult entity){
        this.entity = entity;
        this.id = entity.id;
        if(entity instanceof BaseChannelResult){
            type = ContentItemType.CHANNEL;
        }else if(entity instanceof BaseEventResult){
            type = ContentItemType.EVENT;
        }else{
            type = ContentItemType.CATEGORY;
        }

    }

    public enum ContentItemType {
        @EnumValue(value = "event")
        EVENT,
        @EnumValue(value = "category")
        CATEGORY,
        @EnumValue(value = "channel")
        CHANNEL
    }

    public static ContentItem localizeForUser(ContentItem item, User user){
        if(item.type == ContentItemType.CATEGORY && item.entity != null && item.entity instanceof CategoryResult){
            item.entity = CategoryResult.localizeCategoryTitle((CategoryResult)item.entity, user.language.code);
        }
        return item;
    }

}
