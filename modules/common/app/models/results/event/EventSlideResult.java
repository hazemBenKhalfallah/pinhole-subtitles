package models.results.event;

import com.avaje.ebean.SqlRow;
import core.StringUtils;
import core.url.MediaTypesEnum;
import core.url.UrlBuilder;
import models.resources.SqlColumn;

import java.util.ArrayList;
import java.util.List;

/*
     **** NEEDS LAST VIDEO ****
    Used anywhere the event hero object is need such as:
    * home page hero
 */

public class EventSlideResult extends EventWithLastVideoResult {

    public String heroImageUrl;
    public String heroStatus;
    public String slideTitle;
    public String slideDescription;
    public Boolean isRtlSlideTitle;
    public Boolean isRtlSlideDescription;


    public EventSlideResult(SqlRow row) {
        super(row);
        this.heroStatus = row.getString(SqlColumn.Event.HERO_STATUS);
        if (StringUtils.isNullOrEmpty(row.getString(SqlColumn.Event.SLIDE_TITLE))) {
            this.slideTitle = row.getString(SqlColumn.Event.TITLE_BOX);
        } else {
            this.slideTitle = row.getString(SqlColumn.Event.SLIDE_TITLE);
        }

        if (StringUtils.isNullOrEmpty(row.getString(SqlColumn.Event.SLIDE_DESCRIPTION))) {
            this.slideDescription = row.getString(SqlColumn.Event.SHORT_DESCRIPTION);
        } else {
            this.slideDescription = row.getString(SqlColumn.Event.SLIDE_DESCRIPTION);
        }

        this.heroImageUrl = UrlBuilder.getImageUrl(id, UrlBuilder.SIZE_LARGE, MediaTypesEnum.HERO);
        setRtlFields();
    }

    public static List<EventSlideResult> slidesFromSqlRows(List<SqlRow> rows) {
        final List<EventSlideResult> results = new ArrayList<EventSlideResult>();

        if (rows == null || rows.isEmpty()) {
            return results;
        }

        for (final SqlRow row : rows) {
            results.add(new EventSlideResult(row));
        }

        return results;
    }

    private void setRtlFields() {
        this.isRtlSlideTitle = StringUtils.isRtl(this.slideTitle);
        this.isRtlSlideDescription = StringUtils.isRtl(this.slideDescription);
    }
}