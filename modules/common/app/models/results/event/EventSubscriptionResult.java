package models.results.event;

import java.io.Serializable;

public class EventSubscriptionResult implements Serializable {
    public Long eventId;
    public Boolean isSubscribed;

    public EventSubscriptionResult(Long eventId, Boolean isSubscribed) {
        this.eventId = eventId;
        this.isSubscribed = isSubscribed;
    }
}
