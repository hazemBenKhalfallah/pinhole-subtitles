package models.results.event;

import com.avaje.ebean.SqlRow;
import models.resources.SqlColumn;
import models.results.video.VideoResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
    **** NEEDS LAST VIDEO ****
    Used anywhere the light event object with the last video is need such as:
    * featured categories events
    * featured channels events
    * related events
 */
public class EventWithLastVideoResult extends EventResult {

    public VideoResult video;
    public Integer episodeCount;
    public Boolean isSubscribed;

    public EventWithLastVideoResult(SqlRow row) {
        super(row);
        video = new VideoResult(row);
        this.episodeCount = row.getInteger(SqlColumn.Event.EPISODE_COUNT);
    }
    public static List<EventWithLastVideoResult> fromSqlRows (List<SqlRow> rows){
        final List<EventWithLastVideoResult> results = new ArrayList<EventWithLastVideoResult>();

        if(rows == null || rows.isEmpty()){
           return results;
        }

        for(final SqlRow row : rows){
            results.add(new EventWithLastVideoResult(row));
        }
        return results;
    }

    public static List<EventWithLastVideoResult> setSubscriptionsForUser(List<EventWithLastVideoResult> eventList, Map<Long, EventResult> userSubscriptions) {
        if(eventList == null || eventList.isEmpty())
            return eventList;

        for(final EventWithLastVideoResult eventResult : eventList){
            eventResult.isSubscribed = userSubscriptions != null && userSubscriptions.containsKey(eventResult.id);
        }

        return eventList;
    }
}
