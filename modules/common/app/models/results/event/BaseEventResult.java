package models.results.event;

import com.avaje.ebean.SqlRow;
import core.StringUtils;
import core.url.MediaTypesEnum;
import core.url.UrlBuilder;
import models.resources.SqlColumn;
import models.results.BaseResult;

public class BaseEventResult extends BaseResult {

    public String title;
    public String shortDescription;
    public String description;
    public String hashtag;
    public String vanityUrl;
    public String largeImageUrl;
    public String normalImageUrl;
    public String smallImageUrl;
    public String miniImageUrl;
    public boolean isRtlTitle;
    public boolean isRtlDescription;
    public boolean isRtlShortDescription;
    public boolean isRtlHashTag;


    public BaseEventResult(SqlRow row) {
        super(row);
        this.id = row.getLong(SqlColumn.Event.EVENT_ID);
        this.title = row.getString(SqlColumn.Event.TITLE_BOX);
        this.shortDescription = row.getString(SqlColumn.Event.SHORT_DESCRIPTION);
        this.description = row.getString(SqlColumn.Event.DESCRIPTION);
        this.hashtag = StringUtils.formatTwitterTag(row.getString(SqlColumn.Event.HASHTAG));
        this.vanityUrl = row.getString(SqlColumn.Event.VANITY_URL);

        // set image fields
        setImageUrls();

        // set rtl fields
        setRtlFields();
    }


    private void setImageUrls() {
        this.largeImageUrl = UrlBuilder.getImageUrl(this.id, UrlBuilder.SIZE_LARGE, MediaTypesEnum.EVENT);
        this.normalImageUrl = UrlBuilder.getImageUrl(this.id, UrlBuilder.SIZE_NORMAL, MediaTypesEnum.EVENT);
        this.smallImageUrl = UrlBuilder.getImageUrl(this.id, UrlBuilder.SIZE_SMALL, MediaTypesEnum.EVENT);
        this.miniImageUrl = UrlBuilder.getImageUrl(this.id, UrlBuilder.SIZE_MINI, MediaTypesEnum.EVENT);
    }

    private void setRtlFields() {
        isRtlTitle = StringUtils.isRtl(this.title);
        isRtlDescription = StringUtils.isRtl(this.description);
        isRtlShortDescription = StringUtils.isRtl(this.shortDescription);
        isRtlHashTag = StringUtils.isRtl(this.hashtag);
    }
}