package models.results;

import com.avaje.ebean.SqlRow;
import core.log.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class BaseResult implements Serializable {
    public Long id;

    public BaseResult() {
    }

    public BaseResult(SqlRow row) {
    }

    public static <T> List<T> fromSqlRows(Collection<SqlRow> rows, Class<T> clazz) {
        final List<T> results = new ArrayList<T>();
        for (SqlRow row : rows)
            try {
                results.add(clazz.getDeclaredConstructor(new Class[]{SqlRow.class}).newInstance(row));
            } catch (Exception ex) {
                Log.error(Log.Type.UTILS, ex, "Error while calling fromSqlRows=%s should contain a constructor with SqlRow parameter", clazz.getName());
            }
        return results;
    }

}
