package models.results.channel;

import com.avaje.ebean.SqlRow;
import models.event.Channel;
import models.resources.SqlColumn;
import models.results.BaseResult;

public class BaseChannelResult extends BaseResult {

    public String name;
    public String vanityUrl;

    public BaseChannelResult() {
    }

    public BaseChannelResult(final Channel channel) {
        this.id = channel.id;
        this.name = channel.name;
        this.vanityUrl = channel.vanityUrl;
    }

    public BaseChannelResult(final SqlRow sqlRow) {
        this.id = sqlRow.getLong(SqlColumn.Channel.CHANNEL_ID);
        this.name = sqlRow.getString(SqlColumn.Channel.NAME);
        this.vanityUrl = sqlRow.getString(SqlColumn.Channel.VANITY_URL);
    }
}
