package models.results.channel;

import com.avaje.ebean.SqlRow;
import core.url.MediaTypesEnum;
import core.url.UrlBuilder;
import models.resources.SqlColumn;

import java.util.ArrayList;
import java.util.List;

public class ChannelResult extends BaseChannelResult {
    public String iconUrl;
    public String largeImageUrl;
    public Integer eventCount;

    public ChannelResult(){
    }
    public ChannelResult(final SqlRow sqlRow) {
        super(sqlRow);
        this.eventCount = sqlRow.containsKey(SqlColumn.Channel.EVENT_COUNT) ? sqlRow.getInteger(SqlColumn.Channel.EVENT_COUNT) : 0 ;
        setImageUrls();
    }

    public static List<ChannelResult> fromSqlRows(List<SqlRow> rows) {
        //    //TODO:akhezami normalize access to the channel sql row
            final List<ChannelResult> channels = new ArrayList<ChannelResult>();
            for (final SqlRow row : rows) {
                channels.add(new ChannelResult(row));
            }
            return channels;
    }

    public void setImageUrls(){
        this.iconUrl = UrlBuilder.getImageUrl(this.id, UrlBuilder.SIZE_SMALL, MediaTypesEnum.CHANNEL);
        this.largeImageUrl = UrlBuilder.getImageUrl(this.id, UrlBuilder.SIZE_LARGE, MediaTypesEnum.CHANNEL);
    }
}
