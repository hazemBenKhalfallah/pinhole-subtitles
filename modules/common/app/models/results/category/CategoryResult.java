package models.results.category;

import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.annotation.JsonIgnore;
import core.StringUtils;
import core.url.MediaTypesEnum;
import core.url.UrlBuilder;
import models.resources.SqlColumn;
import models.results.BaseResult;
import models.results.LocalizedTerm;

import java.util.*;

public class CategoryResult extends BaseResult {

    public String title;
    public String url;
    public String largeImageUrl;
    public String normalImageUrl;
    public Integer eventCount;

    @JsonIgnore
    public List<LocalizedTerm> names = new ArrayList<LocalizedTerm>();

    public CategoryResult(SqlRow row){
        this.id = row.getLong(SqlColumn.Category.CATEGORY_ID);
        this.url = row.getString(SqlColumn.Category.VANITY_URL);
        this.title = row.getString(SqlColumn.Category.TERM);
        this.eventCount = row.getInteger(SqlColumn.Category.EVENT_COUNT);
        setImageUrls();
    }

    // expects multiple rows per category because of i18n
    public static List<CategoryResult> categoryResultListFromSqlRows(List<SqlRow> rows){
        final Map<Long, CategoryResult> categories = new HashMap<Long, CategoryResult>();

        if(rows == null || rows.isEmpty()){
            return new ArrayList<CategoryResult>();
        }

        Long categoryId;
        CategoryResult category;

        for (final SqlRow row : rows){
            categoryId = row.getLong(SqlColumn.Category.CATEGORY_ID);
            if(categories.isEmpty() || !categories.containsKey(categoryId)){
               category = new CategoryResult(row);
               category.names.add(new LocalizedTerm(row));
               categories.put(categoryId, category);
            }else{
                categories.get(categoryId).names.add(new LocalizedTerm(row));
            }
        }

        return new ArrayList<CategoryResult>(categories.values());
    }

    //generalize fetching of a particular term frm a list of localized terms
    public static CategoryResult localizeCategoryTitle(CategoryResult category, String languageCode){
        languageCode = !StringUtils.isNullOrEmpty(languageCode) ? languageCode : LocalizedTerm.DEFAULT_LANGUAGE_CODE;

        if(category != null && category.names != null && !category.names.isEmpty()){
            for(LocalizedTerm name : category.names){
                if(name.languageCode.equalsIgnoreCase(languageCode)){
                    category.title = name.term;
                    break;
                }
            }
        }
        return category;
    }

    public static void SortCategories(List<CategoryResult> categories){
        Comparator<CategoryResult> comparator = new Comparator<CategoryResult>() {
            public int compare(CategoryResult c1, CategoryResult c2) {
                return c1.title.compareTo(c2.title); // sort categories
            }
        };

        Collections.sort(categories, comparator);
    }

    private void setImageUrls(){
        this.normalImageUrl = UrlBuilder.getImageUrl(this.id, UrlBuilder.SIZE_NORMAL, MediaTypesEnum.CATEGORY);
        this.largeImageUrl = UrlBuilder.getImageUrl(this.id, UrlBuilder.SIZE_LARGE, MediaTypesEnum.CATEGORY);
    }
}
