package models.results.ad;

import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import json.util.JsonDateSerializer;
import models.enums.ads.AdType;
import models.enums.ads.DiscountType;
import models.resources.SqlColumn;
import models.results.BaseResult;
import play.data.format.Formats;

import java.util.Date;

/**
 * @author ayassinov
 */
public class DiscountResult extends BaseResult {

    public Long id;
    public Double amount;
    public String adType;
    public String discountType;

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date startDateTime;

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date endDateTime;

    public DiscountResult(SqlRow row) {
        super(row);
        this.id = row.getLong(SqlColumn.AdDiscount.AD_DISCOUNT_ID);
        this.amount = row.getDouble(SqlColumn.AdDiscount.DISCOUNT_AMOUNT);
        this.adType = AdType.fromValue(row.getString(SqlColumn.AdDiscount.AD_TYPE)).toString();
        this.discountType = DiscountType.fromValue(row.getString(SqlColumn.AdDiscount.DISCOUNT_TYPE)).toString();
        this.startDateTime = row.getUtilDate(SqlColumn.AdDiscount.START_DATE_TIME);
        this.endDateTime = row.getUtilDate(SqlColumn.AdDiscount.END_DATE_TIME);
    }
}
