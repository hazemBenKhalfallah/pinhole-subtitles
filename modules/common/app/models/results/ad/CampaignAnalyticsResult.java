package models.results.ad;

import com.avaje.ebean.SqlRow;
import models.ad.Campaign;
import models.resources.SqlColumn;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ayassinov
 */
public class CampaignAnalyticsResult extends CampaignResult {

    public int views;
    public int clicks;
    public List<AdAnalyticsResult> ads = new ArrayList<AdAnalyticsResult>();

    public CampaignAnalyticsResult(SqlRow row) {
        super(row);
        this.views = row.getInteger("total_views");
        this.clicks = row.getInteger("total_clicks");
        addAds(row);
    }

    public void addAds(SqlRow row) {
        if (row.getLong(SqlColumn.Ad.AD_ID) != null) //ad exist ?
            ads.add(new AdAnalyticsResult(row));
    }

    public CampaignAnalyticsResult(Campaign campaign) {
        super(campaign);

    }
}
