package models.results.ad;

import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import json.util.JsonDateSerializer;
import models.ad.Ad;
import models.enums.ads.AdStatus;
import models.enums.ads.PricingUnit;
import models.resources.SqlColumn;
import play.data.format.Formats;

import java.util.Date;

/**
 * @author akhezami
 */
public class AdResult extends AdBaseResult {

    public AdStatus status;
    public Double allowance;
    public Double budget;
    public PricingUnit pricingUnit;

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date startDateTime;

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date endDateTime;

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date activateDateTime;

    public Status live_status;

    public CampaignResult campaign;

    public AdResult(SqlRow row) {
        super(row);
        this.status = AdStatus.fromValue(row.getString(SqlColumn.Ad.AD_STATUS));
        this.allowance = row.getDouble(SqlColumn.Ad.ALLOWANCE);
        this.budget = row.getDouble(SqlColumn.Ad.BUDGET);
        this.startDateTime = row.getDate(SqlColumn.Ad.START_DATE_TIME);
        this.endDateTime = row.getDate(SqlColumn.Ad.END_DATE_TIME);
        this.activateDateTime = row.getUtilDate(SqlColumn.Ad.ACTIVATE_DATE_TIME);
        this.live_status = Status.get(this.startDateTime, this.endDateTime);
        this.pricingUnit = PricingUnit.fromValue(row.getString(SqlColumn.AdPrice.UNIT));
        if (row.containsKey(SqlColumn.AdCampaign.CAMPAIGN_NAME)) {
            this.campaign = new CampaignResult(row);
        }
    }

    public AdResult(Ad ad) {
        super(ad);
        this.status = ad.status;
        this.allowance = ad.allowance;
        this.budget = ad.budget;
        this.startDateTime = ad.startDateTime;
        this.endDateTime = ad.endDateTime;
        this.live_status = Status.get(this.startDateTime, this.endDateTime);
        this.activateDateTime = ad.activateDateTime;
        this.pricingUnit = ad.pricingUnit;
        if (ad.campaign != null)
            this.campaign = new CampaignResult(ad.campaign);
    }
}