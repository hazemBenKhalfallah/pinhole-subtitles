package models.results.ad;

import core.DateTimeUtils;

import java.util.Date;

/**
 * @author ayassinov
 */
public enum Status {

    DONE, LIVE, PLANNED;

    public static Status get(Date startDateTime, Date endDateTime) {
        if (DateTimeUtils.isInTheFuture(startDateTime))
            return PLANNED;
        else if (DateTimeUtils.isBetween(DateTimeUtils.now(), startDateTime, endDateTime, true)) {
            return LIVE;
        } else if (DateTimeUtils.isInThePast(endDateTime))
            return DONE;


        throw new IllegalArgumentException(
                String.format("Cannot find the status of the ad/campaign based on the start %s and end dates %s",
                        startDateTime.toString(), endDateTime.toString())
        );
    }
}
