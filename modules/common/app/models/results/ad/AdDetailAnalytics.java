package models.results.ad;

import com.avaje.ebean.SqlRow;
import core.DateTimeUtils;

import java.io.Serializable;

/**
 * @author ayassinov
 */
public class AdDetailAnalytics implements Serializable {

    public String date;
    public int clicks;
    public int views;

    public AdDetailAnalytics(SqlRow row) {
        this.date = DateTimeUtils.convertTimeZoneAndFormat(row.getUtilDate("the_date"));
        this.clicks = row.getInteger("clicks");
        this.views = row.getInteger("views");
    }
}
