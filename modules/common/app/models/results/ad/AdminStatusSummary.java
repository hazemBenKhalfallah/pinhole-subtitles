package models.results.ad;

import com.avaje.ebean.SqlRow;

/**
 * @author: akhezami
 */
public class AdminStatusSummary {
    public Long pendingAccounts;
    public Long pendingAds;

    public AdminStatusSummary(SqlRow row){
        this.pendingAccounts = row.getLong("pendingAccounts");
        this.pendingAds = row.getLong("pendingAds");
    }
}
