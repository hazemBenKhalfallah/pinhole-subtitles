package models.results.ad;

import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import json.util.JsonDateSerializer;
import models.ad.Campaign;
import models.resources.SqlColumn;
import play.data.format.Formats;

import java.util.Date;

/**
 * @author akhezami
 */
public class CampaignResult extends CampaignBaseResult {

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date startDateTime;

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date endDateTime;

    public AccountResult account;

    public Status live_status;

    public CampaignResult(SqlRow row) {
        super(row);
        this.startDateTime = row.getUtilDate(SqlColumn.AdCampaign.START_DATE_TIME);
        this.endDateTime = row.getUtilDate(SqlColumn.AdCampaign.END_DATE_TIME);
        this.live_status = Status.get(this.startDateTime, this.endDateTime);
        if (row.containsKey(SqlColumn.Account.ACCOUNT_STATUS))
            this.account = new AccountResult(row);
    }

    public CampaignResult(Campaign campaign) {
        super(campaign);
        this.startDateTime = campaign.startDateTime;
        this.endDateTime = campaign.endDateTime;
        this.live_status = Status.get(this.startDateTime, this.endDateTime);
        if (campaign.account != null)
            this.account = new AccountResult(campaign.account);
    }
}
