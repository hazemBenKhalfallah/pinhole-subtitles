package models.results.ad;

import com.avaje.ebean.SqlRow;

/**
 * @author: akhezami
 */
public class PlayerSkinAdResult extends AdResult {

    public PlayerSkinAdResult(SqlRow row) {
        super(row);
    }
}
