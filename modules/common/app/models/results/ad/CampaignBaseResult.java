package models.results.ad;

import com.avaje.ebean.SqlRow;
import models.ad.Campaign;
import models.resources.SqlColumn;
import models.results.BaseResult;

/**
 * @author ayassinov
 */
public class CampaignBaseResult extends BaseResult {

    public String name;

    public CampaignBaseResult(SqlRow row) {
        this.id = row.getLong(SqlColumn.AdCampaign.AD_CAMPAIGN_ID);
        this.name = row.getString(SqlColumn.AdCampaign.CAMPAIGN_NAME);
    }

    public CampaignBaseResult(Campaign campaign) {
        this.id = campaign.id;
        this.name = campaign.name;
    }
}
