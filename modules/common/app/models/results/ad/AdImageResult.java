package models.results.ad;

import models.results.BaseResult;

/**
 * @author ayassinov
 */
public class AdImageResult extends BaseResult {

    public String url;

    public String previewKey;

    public AdImageResult(String url, String previewKey) {
        this.url = url;
        this.previewKey = previewKey;
    }
}
