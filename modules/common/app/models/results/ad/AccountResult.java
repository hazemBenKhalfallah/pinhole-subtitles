package models.results.ad;

import com.avaje.ebean.SqlRow;
import models.ad.Account;
import models.enums.ads.AdvertiserAccountStatus;
import models.resources.SqlColumn;
import models.results.BaseResult;
import play.data.format.Formats;

import java.util.Date;

/**
 * @author: akhezami
 */
public class AccountResult extends BaseResult {

    public AdvertiserAccountStatus accountStatus;

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    public Date ValidatedDateTime;

    public String company;
    public String primaryPhone;
    public String secondaryPhone;

    public AccountResult(SqlRow row) {
        this.id = row.getLong(SqlColumn.Account.ACCOUNT_ID);
        this.company = row.getString(SqlColumn.Account.COMPANY);
        this.primaryPhone = row.getString(SqlColumn.Account.PRIMARY_PHONE);
        this.secondaryPhone = row.getString(SqlColumn.Account.SECONDARY_PHONE);
        this.ValidatedDateTime = row.getUtilDate(SqlColumn.Account.VALIDATED_DATE_TIME);
        this.accountStatus = AdvertiserAccountStatus.fromValue(row.getString(SqlColumn.Account.ACCOUNT_STATUS));
    }

    public AccountResult(Account account) {
        this.id = account.id;
        this.company = account.company;
        this.primaryPhone = account.primaryPhone;
        this.secondaryPhone = account.secondaryPhone;
        this.ValidatedDateTime = account.validatedDateTime;
        this.accountStatus = account.accountStatus;
    }
}
