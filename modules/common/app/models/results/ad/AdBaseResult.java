package models.results.ad;

import com.avaje.ebean.SqlRow;
import models.ad.Ad;
import models.enums.ads.AdType;
import models.resources.SqlColumn;
import models.results.BaseResult;

/**
 * @author ayassinov
 */
public class AdBaseResult extends BaseResult {

    public String name;
    public String url;
    public String alternateUrl;
    public AdType type;
    public String previewKey;
    public String externalUrl;
    public String twitterUrl;
    public String fBUrl;
    public boolean enableTwitter;
    public boolean enableFB;

    public AdBaseResult(SqlRow row) {
        this.id = row.getLong(SqlColumn.Ad.AD_ID);
        this.name = row.getString(SqlColumn.Ad.AD_NAME);
        this.url = row.getString(SqlColumn.Ad.AD_URL);
        this.alternateUrl = row.getString(SqlColumn.Ad.AD_ALTERNATE_URL);
        this.type = AdType.fromValue(row.getString(SqlColumn.Ad.Ad_TYPE));
        this.previewKey = row.getString(SqlColumn.Ad.PREVIEW_KEY);
        this.externalUrl = row.getString(SqlColumn.Ad.EXTERNAL_URL);
        this.twitterUrl = row.getString(SqlColumn.Ad.TWITTER_URL);
        this.fBUrl = row.getString(SqlColumn.Ad.FB_URL);
        this.enableTwitter = row.getBoolean(SqlColumn.Ad.ENABLE_TWITTER);
        this.enableFB = row.getBoolean(SqlColumn.Ad.ENABLE_FB);
    }

    public AdBaseResult(Ad ad) {
        this.id = ad.id;
        this.name = ad.name;
        this.url = ad.url;
        this.alternateUrl = ad.alternateUrl;
        this.type = ad.adType;
        this.previewKey = ad.previewKey;
        this.externalUrl = ad.externalUrl;
        this.twitterUrl = ad.twitterUrl;
        this.fBUrl = ad.fBUrl;
        this.enableTwitter = ad.enableTwitter;
        this.enableFB = ad.enableFB;
    }
}
