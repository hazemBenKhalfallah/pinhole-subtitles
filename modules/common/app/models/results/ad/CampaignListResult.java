package models.results.ad;

import com.avaje.ebean.SqlRow;

/**
 * @author ayassinov
 */
public class CampaignListResult extends CampaignResult {

    public int nbEvents = 0;
    public int nbChannels = 0;
    public int nbAds = 0;

    public CampaignListResult(SqlRow row) {
        super(row);
        this.nbEvents = row.containsKey("events") ? row.getInteger("events") : 0;
        this.nbChannels = row.containsKey("channels") ? row.getInteger("channels") : 0;
        this.nbAds = row.containsKey("ads") ? row.getInteger("ads") : 0;
    }

}
