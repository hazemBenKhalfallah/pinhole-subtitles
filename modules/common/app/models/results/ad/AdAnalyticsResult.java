package models.results.ad;

import com.avaje.ebean.SqlRow;
import models.resources.SqlColumn;
import models.results.BaseResult;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ayassinov
 */
public class AdAnalyticsResult extends BaseResult {

    public String name;
    public int views;
    public int clicks;
    public List<AdDetailAnalytics> details = new ArrayList<AdDetailAnalytics>();

    public AdAnalyticsResult(SqlRow row) {
        this.id = row.getLong(SqlColumn.Ad.AD_ID);
        this.name = row.getString(SqlColumn.Ad.AD_NAME);
        this.views = row.getInteger("view_count");
        this.clicks = row.getInteger("click_count");
    }

    public void addDetail(SqlRow row) {
        if (row.getDate("the_date") != null)
            this.details.add(new AdDetailAnalytics(row));
    }
}
