package models.results.ad;

import com.avaje.ebean.SqlRow;
import core.url.MediaTypesEnum;
import models.ad.Campaign;
import models.results.BaseResourceResult;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ayassinov
 */
public class CampaignDetailResult extends CampaignResult {

    public List<BaseResourceResult> events;
    public List<BaseResourceResult> channels;
    public List<AdResult> ads;

    public CampaignDetailResult(SqlRow row) {
        super(row);
        this.events = new ArrayList<BaseResourceResult>();
        this.channels = new ArrayList<BaseResourceResult>();
    }

    public CampaignDetailResult(Campaign campaign) {
        super(campaign);
        this.events = new ArrayList<BaseResourceResult>();
        this.channels = new ArrayList<BaseResourceResult>();
    }

    public void addEvents(Long id, String name) {
        this.events.add(new BaseResourceResult(id, name, MediaTypesEnum.EVENT));
    }

    public void addChannels(Long id, String name) {
        this.channels.add(new BaseResourceResult(id, name, MediaTypesEnum.CHANNEL));
    }
}
