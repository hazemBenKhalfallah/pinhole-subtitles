package models.results.ad;

/**
 * @author: akhezami
 */

import com.avaje.ebean.SqlRow;
import models.ad.AdUser;
import models.resources.SqlColumn;
import models.results.BaseResult;

/**
 * @author: akhezami
 */
public class AdUserResult extends BaseResult {

    public String email;
    public String uid;
    public boolean isAdmin;
    public AccountResult account;

    public String languageCode;
    public String locationCode;

    public AdUserResult(SqlRow row) {
        super(row);
        this.id = row.getLong(SqlColumn.AdUser.AD_USER_ID);
        this.email = row.getString(SqlColumn.AdUser.EMAIL);
        this.uid = row.getString(SqlColumn.AdUser.AD_USER_UID);
        this.isAdmin = row.getBoolean(SqlColumn.AdUser.IS_ADMIN);
        this.locationCode = row.getString(SqlColumn.AdUser.LOCATION);
        this.languageCode = row.getString(SqlColumn.AdUser.LANGUAGE);
        if (row.containsKey(SqlColumn.Account.ACCOUNT_STATUS))
            this.account = new AccountResult(row);
    }

    public AdUserResult(AdUser adUser) {
        this.id = adUser.id;
        this.email = adUser.email;
        this.uid = adUser.uid;
        this.isAdmin = adUser.isAdmin;
        this.account = new AccountResult(adUser.account);
    }
}