package models.results.ad;

import com.avaje.ebean.SqlRow;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ayassinov
 */
public class CampaignAdResult {

    public CampaignBaseResult campaign;
    public List<AdBaseResult> ads = new ArrayList<AdBaseResult>();

    public CampaignAdResult() {
    }

    public CampaignAdResult(SqlRow row) {
        this.campaign = new CampaignBaseResult(row);
        this.ads.add(new AdBaseResult(row));
    }
}
