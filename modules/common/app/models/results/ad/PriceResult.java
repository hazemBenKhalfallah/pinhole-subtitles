package models.results.ad;

import com.avaje.ebean.SqlRow;
import models.enums.ads.AdType;
import models.enums.ads.PricingUnit;
import models.resources.SqlColumn;
import models.results.BaseResult;

/**
 * @author akhezami
 */
public class PriceResult extends BaseResult {

    public AdType type;
    public PricingUnit unit;
    public Double price;
    public Integer dependentAds;

    public PriceResult(SqlRow row) {
        this.dependentAds = row.getInteger(SqlColumn.AdPrice.DEPENDENT_ADS);
        this.id = row.getLong(SqlColumn.AdPrice.AD_PRICE_ID);
        this.type = AdType.fromValue(row.getString(SqlColumn.AdPrice.AD_TYPE));
        this.unit = PricingUnit.fromValue(row.getString(SqlColumn.AdPrice.UNIT));
        this.price = row.getDouble(SqlColumn.AdPrice.VALUE);
    }
}
