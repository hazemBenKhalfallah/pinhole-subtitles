package models.results.video;

import com.avaje.ebean.SqlRow;
import core.StringUtils;
import models.resources.SqlColumn;
import models.results.BaseResult;

public class BaseVideoResult extends BaseResult {

    public String splash;
    public String url;
    public String vanityUrl;
    public String title;
    public String length;
    public String eventTitle;

    public boolean isRtlTitle;


    public BaseVideoResult(SqlRow row){
        id = row.getLong(SqlColumn.Video.VIDEO_ID);
        url = row.getString(SqlColumn.Video.URL);
        splash = row.getString(SqlColumn.Video.SPLASH);
        title = row.getString(SqlColumn.Video.TITLE);
        length = row.getString(SqlColumn.Video.LENGTH);
        vanityUrl = row.getString(SqlColumn.Event.VANITY_URL);
        eventTitle = row.getString(SqlColumn.Event.TITLE_BOX);
        setRtl();
    }

    private void setRtl(){
      isRtlTitle = StringUtils.isRtl(this.title);
    }
}
