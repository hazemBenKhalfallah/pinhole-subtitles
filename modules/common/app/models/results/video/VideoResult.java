package models.results.video;


import com.avaje.ebean.SqlRow;

import java.util.ArrayList;
import java.util.List;

public class VideoResult extends BaseVideoResult {

    public VideoResult(SqlRow row) {
        super(row);
    }

    public static List<VideoResult> fromSqlRows(List<SqlRow> rows){
        final List<VideoResult> results = new ArrayList<VideoResult>();

        if(rows == null || rows.isEmpty()){
            return results;
        }
        for(final SqlRow row : rows){
            results.add(new VideoResult(row));
        }
        return results;
    }


}
