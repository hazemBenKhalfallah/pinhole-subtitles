package models.results.vanityUrl;

import com.avaje.ebean.SqlRow;
import models.resources.SqlColumn;

public class VanityUrlResult {

    public String url;
    public String redirectToUrl;
    public String type;

    public VanityUrlResult(SqlRow row){
        url = row.getString(SqlColumn.VanityUrl.URL);
        redirectToUrl = row.getString(SqlColumn.VanityUrl.REDIRET_URL);
        type = row.getString(SqlColumn.VanityUrl.TYPE);
    }
}
