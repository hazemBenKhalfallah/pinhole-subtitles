/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package models;

import com.avaje.ebean.SqlRow;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author : hazem
 */
@Entity
@Table(name = "pin_searchQuery")
public class SearchQuery extends Model {
    @Id
    @Column(name = "qry_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "qry_name", nullable = false, unique = true, length = 30)
    @Constraints.Required
    public String name;

    @Column(name = "qry_value", nullable = false, columnDefinition = "Text")
    @Constraints.Required
    public String value;

    @Column(name = "qry_desc", length = 80)
    public String description;


    public SearchQuery() {
    }

    public SearchQuery(SqlRow row) {
        this();
        this.id = row.getLong("qry_id");
        this.name = row.getString("qry_name");
        this.value = row.getString("qry_value");
        this.description = row.getString("qry_desc");
        this.version = row.getLong("version");

    }
}
