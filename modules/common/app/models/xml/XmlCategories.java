package models.xml;

import models.category.Category;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "pin_xml_categories")
public class XmlCategories extends Model {


    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "cat_id", insertable = false, updatable = false)
    public Category category;

    @ManyToOne
    @JoinColumn(name = "evt_id", insertable = false, updatable = false)
    public WorkLoaderModel workLoader;

    public XmlCategories() {
    }

    public XmlCategories(Category category, WorkLoaderModel workLoader) {
        this();
        this.id = new Id(category, workLoader);
        this.category = category;
        this.workLoader = workLoader;

    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "cat_id")
        public Long catId;

        @Column(name = "evt_id")
        public Long evtId;

        public Id() {
        }

        public Id(Category category, WorkLoaderModel workLoader) {
            this.catId = category.id;
            this.evtId = workLoader.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (catId != null ? !catId.equals(id.catId) : id.catId != null) return false;
            //noinspection RedundantIfStatement
            if (evtId != null ? !evtId.equals(id.evtId) : id.evtId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = catId != null ? catId.hashCode() : 0;
            result = 31 * result + (evtId != null ? evtId.hashCode() : 0);
            return result;
        }
    }


}

