package models.xml;

import models.HistoryUploadModel;
import models.OverlapModel;
import models.category.Category;
import models.event.Channel;
import models.event.Event;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "pin_workloader")
public class WorkLoaderModel extends Model {

    @Id
    @Column(name = "evt_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "evt_title", nullable = true)
    @Constraints.MaxLength(80)
    public String title;


    @Column(name = "evt_shortDescription", nullable = true)
    public String shortDescription;

    @Column(name = "evt_description", nullable = true, columnDefinition = "TEXT")
    public String description;


    @Column(name = "evt_timeZone", length = 10, nullable = true)
    public String timeZone;


    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "evt_sysBeginTime", nullable = true)
    public Date beginTimeSystem;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "evt_sysEndTime", nullable = true)
    public Date endTimeSystem;

    @Column(name = "evt_imageUrl", nullable = true)
    public String imageUrl;

    @Column(name = "evt_smallImageUrl", nullable = true)
    public String imageSmallUrl;

    @Constraints.MaxLength(16)
    @Column(name = "evt_twitterTag", nullable = true, length = 16)
    public String twitterTag;

    @Column(name = "evt_isRepetitive", nullable = true, columnDefinition = "boolean default false")

    public boolean isRepetitive;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cha_id", nullable = false)
    public Channel channel;


    @Column(name = "tmp_category", nullable = true)
    public String category;

    @Column(name = "evt_validity", nullable = false, columnDefinition = "default true")
    public boolean valid;

    @OneToMany(mappedBy = "workload", fetch = FetchType.LAZY)
    public List<OverlapModel> overlaps;

    @ManyToOne
    @JoinColumn(name = "file_origin", nullable = false)
    public HistoryUploadModel fileOrigin;

    @ManyToOne
    @JoinColumn(name = "evt_parent_id", nullable = true)
    public Event event;

    @OneToMany(mappedBy = "workLoader", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    public List<XmlCategories> categories;


    public boolean isCategoryExist(Category category) {
        for (XmlCategories categoryWl : categories) {
            if (categoryWl.category.id.equals(category.id))
                return true;
        }
        return false;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Event event = (Event) o;

        return !(id != null ? !id.equals(event.id) : event.id != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", version=" + version +
                ", title='" + title + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", description='" + description + '\'' +
                ", timeZone='" + timeZone + '\'' +
                ", beginTimeSystem=" + beginTimeSystem +
                ", endTimeSystem=" + endTimeSystem +
                ", imageUrl='" + imageUrl + '\'' +
                ", imageSmallUrl='" + imageSmallUrl + '\'' +
                ", twitterTag='" + twitterTag + '\'' +
                ", isRepetitive=" + isRepetitive +
                ", channel=" + channel.id +
                '}';
    }
}

