package models.xml;

import models.event.Channel;
import models.event.Event;
import models.planning.EventPlanning;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "pin_xmldataloader")
public class XmlProgramLoaderModel extends Model {

    @Id
    @Column(name = "evt_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    public Long version;

    @Column(name = "evt_title", nullable = true, length = 80)
    @Constraints.MaxLength(80)
    @Constraints.Required
    public String title;

    @Constraints.Required
    @Constraints.MaxLength(150)
    @Column(name = "evt_shortDescription", nullable = true, length = 150)
    public String shortDescription;

    @Column(name = "evt_description", nullable = true, columnDefinition = "TEXT")
    @Constraints.Required
    public String description;

    @Temporal(TemporalType.DATE)
    @Column(name = "evt_datetime", nullable = true)
    @Formats.DateTime(pattern = "dd/MM/yyyy")
    @Constraints.Required
    public Date dateTime;

    @Constraints.Required
    @Column(name = "evt_timeZone", length = 10, nullable = true)
    public String timeZone;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "evt_beginTime", nullable = true)
    @Formats.DateTime(pattern = "HH:mm")
    @Constraints.Required
    public Date beginTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "evt_endTime", nullable = true)
    @Formats.DateTime(pattern = "HH:mm")
    @Constraints.Required
    public Date endTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "HH:mm")
    @Column(name = "evt_sysBeginTime", nullable = true)
    public Date beginTimeSystem;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "HH:mm")
    @Column(name = "evt_sysEndTime", nullable = true)
    public Date endTimeSystem;

    @Column(name = "evt_imageUrl", nullable = true, length = 100)
    @Constraints.Required
    @Constraints.MaxLength(100)
    public String imageUrl;

    @Column(name = "evt_smallImageUrl", nullable = true, length = 100)
    @Constraints.Required
    @Constraints.MaxLength(100)
    public String imageSmallUrl;

    @Constraints.MaxLength(16)
    @Constraints.Required
    @Column(name = "evt_twitterTag", nullable = true, length = 16)
    public String twitterTag;

    @Column(name = "evt_isRepetitive", nullable = true, columnDefinition = "boolean default false")
    @Constraints.Required
    public boolean isRepetitive;

    @Column(name = "evt_buzz", columnDefinition = "default 0")
    public Double buzzScore;

    @Column(name = "evt_showRef", nullable = true)
    public Integer currentShowTimeRef;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cha_id", nullable = false)
    @Constraints.Required
    public Channel channel;

    @Column(name = "planning_id", nullable = true)
    public EventPlanning planning;


    @Column(name = "tmp_category", nullable = true)
    public String category;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Event event = (Event) o;

        if (id != null ? !id.equals(event.id) : event.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", version=" + version +
                ", title='" + title + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", description='" + description + '\'' +
                ", dateTime=" + dateTime +
                ", timeZone='" + timeZone + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", beginTimeSystem=" + beginTimeSystem +
                ", endTimeSystem=" + endTimeSystem +
                ", imageUrl='" + imageUrl + '\'' +
                ", imageSmallUrl='" + imageSmallUrl + '\'' +
                ", twitterTag='" + twitterTag + '\'' +
                ", isRepetitive=" + isRepetitive +
                ", currentShowTimeRef=" + currentShowTimeRef +
                ", channel=" + channel.id +
                '}';
    }
}

