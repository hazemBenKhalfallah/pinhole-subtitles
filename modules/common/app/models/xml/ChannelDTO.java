package models.xml;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zied
 */

public class ChannelDTO {
    // Channel ID
    String id;

    // Channel  Name
    String channelName;

    // Events
    Map<String, ChannelEvent> eventLists;

    ChannelDTO() {
        eventLists = new HashMap<String, ChannelEvent>();
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, ChannelEvent> getEventLists() {
        return eventLists;
    }

    public void setEventLists(Map<String, ChannelEvent> eventLists) {
        this.eventLists = eventLists;
    }
}