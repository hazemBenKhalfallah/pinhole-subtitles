/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models;

import core.DateTimeUtils;
import models.enums.JobIdEnum;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author hazem
 */
@Entity
@Table(name = "pin_Jobs_hist")
public class JobHistory extends Model {

    @Id
    @Column(name = "job_id")
    public Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "job_exec_time")
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    public Date executionTime;

    @Version
    public Long version;

    public JobHistory() {
        this.executionTime = DateTimeUtils.currentDateWithoutSeconds();
    }

    public JobHistory(JobIdEnum jobId) {
        this();
        this.id = jobId.getValue();
    }

}
