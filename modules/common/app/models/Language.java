package models;


import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zied
 */
@Entity
@Table(name = "pin_language")
public class Language extends Model {

    @JsonIgnore
    @Id
    @Column(name = "lng_id", nullable = false)
    public Long id;

    @Column(name = "lng_name", length = 50, nullable = false)
    public String name;

    @Column(name = "lng_locale", length = 10, nullable = false)
    public String code;

    @Column(name = "lng_default", nullable = false, columnDefinition = "default false")
    public boolean isDefault;

    public Language() {
    }

    public Language(Long id) {
        this();
        this.id = id;
    }

    public Language(SqlRow row) {
        this.id = row.getLong("lng_id");
        this.name = row.getString("lng_name");
        this.code = row.getString("lng_locale");
        this.isDefault = row.containsValue("lng_default") ? row.getBoolean("lng_default") : false;
    }

    public enum LanguageCodesEnum {
        FRENCH,
        ENGLISH,
        UNKNOWN;

        private static final Map<String, LanguageCodesEnum> TypesMap = new HashMap<String, LanguageCodesEnum>();
        private static final Map<LanguageCodesEnum, String> ValuesMap = new HashMap<LanguageCodesEnum, String>();

        static {
            TypesMap.put("fr", LanguageCodesEnum.FRENCH);
            TypesMap.put("en", LanguageCodesEnum.ENGLISH);
        }

        static {
            ValuesMap.put(LanguageCodesEnum.FRENCH, "fr");
            ValuesMap.put(LanguageCodesEnum.ENGLISH, "en");
        }

        public static LanguageCodesEnum getType(String value) {
            value = value.toLowerCase();
            if (TypesMap.containsKey(value))
                return TypesMap.get(value);
            else
                return UNKNOWN;
        }

        public static String getValue(LanguageCodesEnum type) {
            return ValuesMap.get(type);
        }

        public String getValue() {
            return ValuesMap.get(this);
        }
    }
}
