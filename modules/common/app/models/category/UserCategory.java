/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.category;

import com.fasterxml.jackson.annotation.JsonBackReference;
import core.DateTimeUtils;
import models.user.User;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_user_category")
public class UserCategory extends Model {

    /**
     * Represent the referrer for the relation between the user and category
     * A suggest category from friend, from pinhole or the user him self
     *
     * @author ayassinov
     */
    public static enum CategoryReferrerEnum {
        USER, PINHOLE, FRIEND, EVENT
    }

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JsonBackReference(value = "user-categories")
    @JoinColumn(name = "cat_id", insertable = false, updatable = false)
    public Category category;

    @Temporal(TemporalType.DATE)
    @Column(name = "uca_creation_date", nullable = false)
    public Date creationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "uca_referrer", nullable = false)
    public CategoryReferrerEnum categoryReferrerEnum;

    /**
     * Default constructor, the creation date is set to now
     */
    public UserCategory() {
        this.creationDate = DateTimeUtils.now();
    }

    /**
     * Constructor with CategoryReferrerEnum set to user
     *
     * @param user     the user
     * @param category the category
     */
    public UserCategory(User user, Category category) {
        this(user, category, CategoryReferrerEnum.USER);
    }

    /**
     * Full constructor
     *
     * @param user                 the user
     * @param category             the category
     * @param categoryReferrerEnum the referrer enum
     */
    public UserCategory(User user, Category category, CategoryReferrerEnum categoryReferrerEnum) {
        this();
        this.id = new Id(user, category);
        this.user = user;
        this.category = category;
        this.categoryReferrerEnum = categoryReferrerEnum;
        this.user.categories.add(this);
        this.category.users.add(this);
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "usr_id")
        public Long userId;

        @Column(name = "cat_id")
        public Long categoryId;

        public Id() {
        }

        public Id(User user, Category category) {
            this.userId = user.id;
            this.categoryId = category.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (categoryId != null ? !categoryId.equals(id.categoryId) : id.categoryId != null) return false;
            //noinspection RedundantIfStatement
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = userId != null ? userId.hashCode() : 0;
            result = 31 * result + (categoryId != null ? categoryId.hashCode() : 0);
            return result;
        }
    }
}
