/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.category;

import models.event.Event;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "pin_category_event")
public class CategoryEvent extends Model {

    private static final int DEFAULT_SCORE = 5;

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "cat_id", insertable = false, updatable = false)
    public Category category;

    @ManyToOne
    @JoinColumn(name = "evt_id", insertable = false, updatable = false)
    public Event event;

    @Column(name = "cte_score")
    @Constraints.Min(0)
    @Constraints.Max(5)
    public int score;

    /**
     * Default constructor, the creation date is set to now
     */
    public CategoryEvent() {
    }

    /**
     * Constructor with mandatory field, the score is set to default value (DEFAULT_SCORE)
     *
     * @param category the category
     * @param event    the event
     */
    public CategoryEvent(Category category, Event event) {
        this(category, event, DEFAULT_SCORE);
    }

    /**
     * Full constructor
     *
     * @param category the category
     * @param event    the event
     * @param score    the score of the relation from 1 to 10
     */
    public CategoryEvent(Category category, Event event, int score) {
        this();
        this.id = new Id(category, event);
        this.category = category;
        this.event = event;
        this.score = score;
        this.event.categories.add(this);
    }


    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "cat_id")
        public Long categoryId;

        @Column(name = "evt_id")
        public Long eventId;

        public Id() {
        }

        public Id(Category category, Event event) {
            this.categoryId = category.id;
            this.eventId = event.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (categoryId != null ? !categoryId.equals(id.categoryId) : id.categoryId != null) return false;
            //noinspection RedundantIfStatement
            if (eventId != null ? !eventId.equals(id.eventId) : id.eventId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = categoryId != null ? categoryId.hashCode() : 0;
            result = 31 * result + (eventId != null ? eventId.hashCode() : 0);
            return result;
        }
    }
}
