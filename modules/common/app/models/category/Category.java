/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package models.category;

import com.avaje.ebean.annotation.Formula;
import com.fasterxml.jackson.annotation.*;
import core.DateTimeUtils;
import models.media.HasMedia;
import models.media.ImageModel;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represent a category object, it's linked to user and event.
 * Needed to suggest to a user events based on the category that have been chosen
 *
 * @author ayassinov
 */
@Entity
@Table(name = "pin_category")
@JsonIgnoreProperties(ignoreUnknown = true, value = {"parentCategory"})
public class Category extends Model implements HasMedia {

    @Id
    @Column(name = "cat_id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Version
    @JsonIgnore
    public Long version;

    @Column(name = "cat_name", nullable = true, length = 50)
    @JsonProperty("default")
    public String name;

    @Transient
    @JsonIgnore
    @Formula(select = "categoryLanguages.name")
    public String categoryName;

    @Column(name = "cat_img_exists", length = 1, nullable = false, columnDefinition = "default false")
    @Constraints.Required
    @JsonIgnore
    public boolean imageExists;

//    @Column(name = "cat_icon_url", length = 255)
//    @Constraints.Required
//    @JsonProperty("icon")
//    public String categoryIcon;
//
//    @Constraints.Required
//    @Column(name = "cat_img_alt", nullable = false, length = 250)
//    public String altText;

    @Constraints.Required
    @Column(name = "cat_url", nullable = false, length = 255)
    public String vanityUrl;

    /**
     * a priority is unique for a same categories that have the same parent.
     * it will be used to sort list of categories
     */
    @Column(name = "cat_priority", nullable = false)
    @Constraints.Min(1)
    @JsonIgnore
    public int priority;

    @Column(name = "cat_is_featured", nullable = false, columnDefinition = "default false")
    public boolean isFeatured;

    @Column(name = "cat_inbrowse", nullable = false, columnDefinition = "default false")
    public boolean inBrowse;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "cat_last_modif_time", nullable = false, columnDefinition = "default current_timestamp")
    public Date lastModifiedTime;

    @ManyToOne
    @JoinColumn(name = "cat_parent_id", nullable = true)
    @JsonIgnore
    @JsonBackReference(value = "parent-category")
    public Category parentCategory;

    @OneToMany(mappedBy = "parentCategory")
    @JsonProperty("sub_categories")
    @JsonManagedReference(value = "parent-category")
    public List<Category> subCategories = new ArrayList<Category>();

    @OneToMany(mappedBy = "category")
    @JsonIgnore
    @JsonManagedReference(value = "user-categories")
    public List<UserCategory> users = new ArrayList<UserCategory>();

    @OneToMany(mappedBy = "category")
    @JsonIgnore
    @JsonManagedReference(value = "category-events")
    public List<CategoryEvent> events = new ArrayList<CategoryEvent>();


    @OneToMany(mappedBy = "category", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    @JsonProperty("names")
    @JsonManagedReference(value = "category-languages")
    public List<CategoryLanguage> categoryLanguages = new ArrayList<CategoryLanguage>();

    @Transient
    @JsonIgnore
    public boolean added;

    @Transient
    @JsonProperty("followers")
    public Long numberOfFollowers = 0l;


    @Transient
    @JsonProperty("images")
    public List<ImageModel> images;


    @Transient
    @JsonProperty("subscribed")
    public Boolean isSubscribedUser = false;


    /**
     * Default constructor
     */
    public Category() {
        this.lastModifiedTime = DateTimeUtils.currentDateWithoutSeconds();
    }


}
