package models.category;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import models.Language;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author zied
 */

@Entity
@Table(name = "pin_category_i18")
public class CategoryLanguage extends Model {

    @EmbeddedId
    @javax.persistence.Id
    @JsonIgnore
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "cat_id", insertable = false, updatable = false)
    @JsonBackReference(value = "category-languages")
    public Category category;

    @ManyToOne
    @JoinColumn(name = "lng_id", insertable = false, updatable = false)
    @JsonIgnore
    public Language language;

    @Column(name = "cat_name", length = 50, nullable = false)
    public String name;

    @JsonProperty("code")
    public String getLanguageCode() {
        return language.code;
    }

    public CategoryLanguage() {
    }

    public CategoryLanguage(long lgId) {
        this.id.languageId = lgId;
    }

    public CategoryLanguage(Category category, Language language) {
        this.id = new Id(language.id, category.id);
        this.category = category;
        this.language = language;
    }

    public CategoryLanguage(long catId, long lgId, String name) {
        this.id = new Id(lgId, catId);
        this.name = name;
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "lng_id")
        public Long languageId;

        @Column(name = "cat_id")
        public Long categoryId;

        public Id() {
        }

        public Id(Long languageId, Long categoryId) {
            this.languageId = languageId;
            this.categoryId = categoryId;
        }

        @Override
        public int hashCode() {
            int result = (languageId != null ? languageId.hashCode() : 0);
            result = 31 * result + (categoryId != null ? categoryId.hashCode() : 0);
            return result;

        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (languageId != null ? !languageId.equals(id.languageId) : id.languageId != null) return false;
            //noinspection RedundantIfStatement
            if (categoryId != null ? !categoryId.equals(id.categoryId) : id.categoryId != null) return false;
            return true;
        }
    }
}
