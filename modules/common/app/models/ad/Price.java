package models.ad;


import com.avaje.ebean.SqlRow;
import models.enums.ads.AdType;
import models.enums.ads.PricingUnit;
import models.resources.SqlColumn;
import play.db.ebean.Model;

import javax.persistence.*;

// AT LEAST 1 configuration per type with no date/time constraints is required at all times to ensure that everything is valid
@Entity
@Table(name = "ad_pricing")
public class Price extends Model {

    @Id
    @Column(name = SqlColumn.AdPrice.AD_PRICE_ID, unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = SqlColumn.AdPrice.AD_TYPE, nullable = false, columnDefinition = "default none")
    public AdType type;

    @Enumerated(EnumType.STRING)
    @Column(name = SqlColumn.AdPrice.UNIT, nullable = false, columnDefinition = "default none")
    public PricingUnit unit;

    @Column(name = SqlColumn.AdPrice.VALUE, nullable = false)
    public Double value; // -- base price is always in TND, eventually it can be converted per currency

    @Version
    public Long version;

    public Price() {
    }

    public Price(AdType type, PricingUnit unit, Double value) {
        this.type = type;
        this.unit = unit;
        this.value = value;
    }

    public Price(SqlRow row) {
        this.id = row.getLong(SqlColumn.AdPrice.AD_PRICE_ID);
        this.type = AdType.fromValue(row.getString(SqlColumn.AdPrice.AD_TYPE));
        this.unit = PricingUnit.fromValue(row.getString(SqlColumn.AdPrice.UNIT));
        this.value = row.getDouble(SqlColumn.AdPrice.VALUE);
    }
}
