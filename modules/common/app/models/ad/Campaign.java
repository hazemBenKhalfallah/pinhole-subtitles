package models.ad;

import com.avaje.ebean.SqlRow;
import core.DateTimeUtils;
import models.resources.SqlColumn;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author akhezami
 */

@Entity
@Table(name = "ad_campaign")
public class Campaign extends Model {

    @Id
    @Column(name = SqlColumn.AdCampaign.AD_CAMPAIGN_ID, unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(name = SqlColumn.AdCampaign.CAMPAIGN_NAME, nullable = false)
    public String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.AdCampaign.START_DATE_TIME)
    public Date startDateTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.AdCampaign.END_DATE_TIME)
    public Date endDateTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.AdCampaign.CREATED_DATE_TIME)
    public Date createdDateTime;

    @Column(name = SqlColumn.AdCampaign.CREATED_BY_ID)
    public String createdById;

    @Version
    public Long version;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SqlColumn.Account.ACCOUNT_ID)
    public Account account;

    @OneToMany(mappedBy = "campaign")
    public List<CampaignEvent> events = new ArrayList<CampaignEvent>();

    @OneToMany(mappedBy = "campaign")
    public List<CampaignChannel> channels = new ArrayList<CampaignChannel>();

    public Campaign() {
        this.createdDateTime = DateTimeUtils.now();
    }

    public Campaign(Account account, String name, String createdByUID, Date startDateTime, Date endDateTime) {
        this();
        this.account = account;
        this.name = name;
        this.createdById = createdByUID;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
    }

    public Campaign(SqlRow row) {
        this.account = new Account(row);
        this.id = row.getLong(SqlColumn.AdCampaign.AD_CAMPAIGN_ID);
        this.name = row.getString(SqlColumn.AdCampaign.CAMPAIGN_NAME);
        this.createdById = row.getString(SqlColumn.AdCampaign.CREATED_BY_ID);
        this.startDateTime = row.getUtilDate(SqlColumn.AdCampaign.START_DATE_TIME);
        this.endDateTime = row.getUtilDate(SqlColumn.AdCampaign.END_DATE_TIME);
    }

    public void addChannels(List<Long> channelsId) {
        this.channels.clear();
        for (final Long id : channelsId)
            this.channels.add(new CampaignChannel(id));
    }

    public void addEvents(List<Long> eventsId) {
        this.events.clear();
        for (final Long id : eventsId)
            this.events.add(new CampaignEvent(id));
    }

    public void updateChannels() {
        for (final CampaignChannel channel : channels) {
            channel.setCampaign(this);
        }
    }

    public void updateEvents() {
        for (final CampaignEvent event : events) {
            event.setCampaign(this);
        }
    }
}
