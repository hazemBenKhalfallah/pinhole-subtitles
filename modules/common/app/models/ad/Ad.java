package models.ad;

import com.avaje.ebean.SqlRow;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import core.DateTimeUtils;
import models.enums.ads.AdStatus;
import models.enums.ads.AdType;
import models.enums.ads.PricingUnit;
import models.resources.FieldLength;
import models.resources.SqlColumn;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;


@Entity
@Table(name = "ad")
public class Ad extends Model {
    @Id
    @Column(name = SqlColumn.Ad.AD_ID, unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(name = SqlColumn.Ad.AD_NAME, nullable = false)
    public String name;

    @Column(name = SqlColumn.Ad.AD_URL, nullable = false)
    public String url;

    @Column(name = SqlColumn.Ad.AD_ALTERNATE_URL)
    public String alternateUrl;

    @Column(name = SqlColumn.Ad.PREVIEW_KEY, unique = true, nullable = false, length = FieldLength.Ad.AD_PREVIEW_KEY)
    public String previewKey; // used for previews

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SqlColumn.AdCampaign.AD_CAMPAIGN_ID)
    public Campaign campaign;

    @Column(name = SqlColumn.AdDiscount.AD_DISCOUNT_ID)
    public Discount discount;

    @Column(name = SqlColumn.Ad.Ad_TYPE)
    public AdType adType;

    @Enumerated(EnumType.STRING)
    @Column(name = SqlColumn.Ad.AD_STATUS, nullable = false, length = FieldLength.Ad.AD_STATUS)
    public AdStatus status;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.Ad.START_DATE_TIME)
    public Date startDateTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.Ad.END_DATE_TIME)
    public Date endDateTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.Ad.ACTIVATE_DATE_TIME)
    public Date activateDateTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.Ad.CREATED_DATE_TIME)
    public Date createdDateTime;

    @Column(name = SqlColumn.Ad.BUDGET)
    public Double budget;

    @Column(name = SqlColumn.Ad.EXTERNAL_URL, nullable = false, length = FieldLength.Ad.AD_EXTERNAL_URL)
    public String externalUrl;

    @Column(name = SqlColumn.Ad.FB_URL, length = FieldLength.Ad.AD_FB_URL)
    public String fBUrl;

    @Column(name = SqlColumn.Ad.TWITTER_URL, length = FieldLength.Ad.AD_TWITTER_URL)
    public String twitterUrl;

    @Column(name = SqlColumn.Ad.ENABLE_FB)
    public boolean enableFB;

    @Column(name = SqlColumn.Ad.ENABLE_TWITTER)
    public boolean enableTwitter;

    @Enumerated(EnumType.STRING)
    @Column(name = SqlColumn.AdPrice.UNIT)
    public PricingUnit pricingUnit;

    @Column(name = SqlColumn.Ad.ALLOWANCE)
    public Double allowance;     // calculated threshold after which the ad will immediately expire/ be out of credits

    @Column(name = SqlColumn.Ad.CREATED_BY_ID, nullable = false)
    public String createdById;

    @Version
    public Long version;

    @Transient
    public int viewCount;

    @Transient
    public int clickCount;

    public static String getNewKeyPreview() {
        return UUID.randomUUID().toString();
    }

    public Ad() {
        this.createdDateTime = DateTimeUtils.now();
    }

    public Ad(String userUID, Campaign campaign, String previewKey, AdType adType, String name, String url, String alternateUrl,
              Optional<Discount> discount, PricingUnit pricingUnit, Double budget, Double allowance,
              Date startDateTime, Date endDateTime,
              String externalUrl, String fBUrl, String twitterUrl,
              boolean enableFB, boolean enableTwitter) {
        this();
        this.previewKey = Strings.isNullOrEmpty(previewKey) ? getNewKeyPreview() : previewKey;
        this.status = AdStatus.PENDING;

        this.createdById = userUID;
        this.campaign = campaign;
        this.adType = adType;
        this.name = name;
        this.url = url;
        this.discount = discount.orNull();
        this.pricingUnit = pricingUnit;
        this.budget = budget;
        this.allowance = allowance;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.externalUrl = externalUrl;
        this.fBUrl = fBUrl;
        this.twitterUrl = twitterUrl;
        this.enableFB = enableFB;
        this.enableTwitter = enableTwitter;
        this.alternateUrl = alternateUrl;
    }

    public Ad(SqlRow row) {
        this.id = row.getLong(SqlColumn.Ad.AD_ID);
        this.createdById = row.getString(SqlColumn.Ad.CREATED_BY_ID);
        this.createdDateTime = row.getUtilDate(SqlColumn.Ad.CREATED_DATE_TIME);
        this.previewKey = row.getString(SqlColumn.Ad.PREVIEW_KEY);
        this.adType = AdType.fromValue(row.getString(SqlColumn.Ad.Ad_TYPE));
        this.name = row.getString(SqlColumn.Ad.AD_NAME);
        this.url = row.getString(SqlColumn.Ad.AD_URL);
        this.status = AdStatus.fromValue(row.getString(SqlColumn.Ad.AD_STATUS));
        this.pricingUnit = PricingUnit.fromValue(row.getString(SqlColumn.AdPrice.UNIT));
        this.budget = row.getDouble(SqlColumn.Ad.BUDGET);
        this.allowance = row.getDouble(SqlColumn.Ad.ALLOWANCE);
        this.startDateTime = row.getUtilDate(SqlColumn.Ad.START_DATE_TIME);
        this.endDateTime = row.getUtilDate(SqlColumn.Ad.END_DATE_TIME);
        this.activateDateTime = row.getUtilDate(SqlColumn.Ad.ACTIVATE_DATE_TIME);
        this.externalUrl = row.getString(SqlColumn.Ad.EXTERNAL_URL);
        this.fBUrl = row.getString(SqlColumn.Ad.FB_URL);
        this.twitterUrl = row.getString(SqlColumn.Ad.TWITTER_URL);
        this.enableFB = row.getBoolean(SqlColumn.Ad.ENABLE_FB);
        this.enableTwitter = row.getBoolean(SqlColumn.Ad.ENABLE_TWITTER);
        if (row.containsKey(SqlColumn.AdDiscount.DISCOUNT_AMOUNT))
            this.discount = new Discount(row);
        if (row.containsKey(SqlColumn.AdCampaign.CAMPAIGN_NAME))
            this.campaign = new Campaign(row);

    }


}
