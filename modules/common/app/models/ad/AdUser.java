package models.ad;

import com.avaje.ebean.SqlRow;
import core.DateTimeUtils;
import core.HashUtils;
import core.StringUtils;
import models.Language;
import models.resources.FieldLength;
import models.resources.SqlColumn;
import models.user.Location;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "ad_user")
public class AdUser extends Model {
    @Id
    @Column(name = SqlColumn.AdUser.AD_USER_ID, unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(name = SqlColumn.AdUser.AD_USER_UID, length = FieldLength.AdUser.PASSWORD, unique = true, nullable = false)
    public String uid;

    @Version
    public Long version;

    @Column(name = SqlColumn.AdUser.SESSION_ID, unique = true, length = FieldLength.AdUser.SESSION_ID)
    public String sessionId;

    @Column(name = SqlColumn.AdUser.EMAIL, unique = true, length = FieldLength.AdUser.EMAIL)
    public String email;

    @Column(name = SqlColumn.AdUser.PASSWORD, length = FieldLength.AdUser.PASSWORD, nullable = false)
    public String password;

    @Column(name = SqlColumn.AdUser.FIRST_NAME, nullable = false, length = FieldLength.AdUser.FIRST_NAME)
    public String firstName;

    @Column(name = SqlColumn.AdUser.LAST_NAME, nullable = false, length = FieldLength.AdUser.LAST_NAME)
    public String lastName;

    @Column(name = SqlColumn.AdUser.IS_ADMIN, columnDefinition = "boolean default false")
    public boolean isAdmin;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.AdUser.CREATED_DATE_TIME, nullable = false)
    public Date createdDateTime;

    @ManyToOne
    @JoinColumn(name = SqlColumn.Account.ACCOUNT_ID, nullable = true)
    public Account account;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SqlColumn.AdUser.LANGUAGE)
    public Language language;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SqlColumn.AdUser.LOCATION)
    public Location location;

    public AdUser() {
        this.account = new Account();
        this.createdDateTime = DateTimeUtils.now();
        this.uid = HashUtils.hashUID(UUID.randomUUID().toString());
        this.isAdmin = false;
    }

    public AdUser(String firstName, String lastName, String email, String password,
                  String company, String primaryPhone, String secondaryPhone,
                  Location location, Language language) {
        this();
        this.account.company = company;
        this.account.primaryPhone = primaryPhone;
        this.account.secondaryPhone = secondaryPhone;

        this.firstName = firstName;
        this.lastName = lastName;
        this.password = HashUtils.hashPassword(password.trim());
        this.email = email.trim().toLowerCase();
        this.location = location;
        this.language = language;
    }

    public AdUser(SqlRow row) {
        this();
        this.account = new Account(row);
        this.id = row.getLong(SqlColumn.AdUser.AD_USER_ID);
        this.firstName = row.getString(SqlColumn.AdUser.FIRST_NAME);
        this.lastName = row.getString(SqlColumn.AdUser.LAST_NAME);
        this.uid = row.getString(SqlColumn.AdUser.AD_USER_UID);
        this.sessionId = row.getString(SqlColumn.AdUser.SESSION_ID);
        this.password = row.getString(SqlColumn.AdUser.PASSWORD);
        this.email = row.getString(SqlColumn.AdUser.EMAIL);
        this.isAdmin = row.getBoolean(SqlColumn.AdUser.IS_ADMIN);
        if(row.containsKey("loc_id")){
            this.location = new Location(row);
        }
        if(row.containsKey("lng_id")){
            this.language = new Language(row);
        }
    }

    /**
     * Generate a new session id
     */
    public void setNewSession() {
        this.sessionId = HashUtils.hashPassword(UUID.randomUUID().toString());
    }


    /**
     * return true if the given password match the user password.
     *
     * @param password the password to check, if it's null we return false
     * @return true/false
     */
    public boolean isPasswordMatch(String password) {
        return !(StringUtils.isNullOrEmpty(password) || StringUtils.isNullOrEmpty(this.password))
                && HashUtils.hashPassword(password.trim()).equals(this.password);
    }
}
