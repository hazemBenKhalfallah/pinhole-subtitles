package models.ad;

import com.avaje.ebean.SqlRow;
import models.enums.ads.AdType;
import models.enums.ads.DiscountType;
import models.resources.SqlColumn;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ad_discount")
public class Discount extends Model {

    @Id
    @Column(name = SqlColumn.AdDiscount.AD_DISCOUNT_ID, unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(name = SqlColumn.AdDiscount.DISCOUNT_AMOUNT, nullable = false)
    public Double amount;

    @Enumerated(EnumType.STRING)
    @Column(name = SqlColumn.AdDiscount.AD_TYPE, nullable = false, columnDefinition = "default none")
    public AdType adType;

    @Enumerated(EnumType.STRING)
    @Column(name = SqlColumn.AdDiscount.DISCOUNT_TYPE, nullable = false, columnDefinition = "default none")
    public DiscountType discountType;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.AdDiscount.START_DATE_TIME)
    public Date startDateTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.AdDiscount.END_DATE_TIME)
    public Date endDateTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = SqlColumn.Account.ACCOUNT_ID, nullable = false)
    @Constraints.Required
    public Account account;

    public Discount() {
    }

    public Discount(Double amount, AdType adType, DiscountType discountType, Date startDateTime, Date endDateTime, Account account) {
        this();
        this.amount = amount;
        this.adType = adType;
        this.discountType = discountType;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.account = account;
    }

    public Discount(SqlRow row) {
        if (row.containsKey(SqlColumn.Account.ACCOUNT_STATUS))
            this.account = new Account(row);
        this.id = row.getLong(SqlColumn.AdDiscount.AD_DISCOUNT_ID);
        this.amount = row.getDouble(SqlColumn.AdDiscount.DISCOUNT_AMOUNT);
        this.adType = AdType.fromValue(row.getString(SqlColumn.AdDiscount.AD_TYPE));
        this.discountType = DiscountType.fromValue(row.getString(SqlColumn.AdDiscount.DISCOUNT_TYPE));
        this.startDateTime = row.getUtilDate(row.getString(SqlColumn.AdDiscount.START_DATE_TIME));
        this.endDateTime = row.getUtilDate(row.getString(SqlColumn.AdDiscount.END_DATE_TIME));
    }
}
