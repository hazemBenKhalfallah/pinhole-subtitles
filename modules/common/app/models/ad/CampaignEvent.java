/**
 * @author: akhezami
 */
package models.ad;

import com.google.common.base.Preconditions;
import models.event.Event;
import models.resources.SqlColumn;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author: akhezami
 */
@Entity
@Table(name = "ad_campaign_event")
public class CampaignEvent extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = SqlColumn.AdCampaign.AD_CAMPAIGN_ID, insertable = false, updatable = false)
    public Campaign campaign;


    @ManyToOne
    @JoinColumn(name = SqlColumn.Event.EVENT_ID, insertable = false, updatable = false)
    public Event event;


    public CampaignEvent() {
    }

    public CampaignEvent(Long eventId) {
        this.event = new Event(eventId);
        this.id.eventId = eventId;
    }

    public void setCampaign(Campaign campaign) {
        Preconditions.checkNotNull(campaign.id);
        this.campaign = campaign;
        this.id.campaignId = campaign.id;
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = SqlColumn.AdCampaign.AD_CAMPAIGN_ID)
        public Long campaignId;

        @Column(name = SqlColumn.Event.EVENT_ID)
        public Long eventId;

        public Id() {
        }

        public Id(Campaign campaign, Event event) {
            this.campaignId = campaign.id;
            this.eventId = event.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || this.getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (campaignId != null ? !campaignId.equals(id.campaignId) : id.campaignId != null) return false;
            //noinspection RedundantIfStatement
            if (eventId != null ? !eventId.equals(id.eventId) : id.eventId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = campaignId != null ? campaignId.hashCode() : 0;
            result = 31 * result + (eventId != null ? eventId.hashCode() : 0);
            return result;
        }
    }
}

