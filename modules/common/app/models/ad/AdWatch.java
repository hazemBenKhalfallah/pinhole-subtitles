package models.ad;

import core.DateTimeUtils;
import models.event.Event;
import models.extra.Extra;
import models.planning.ShowTime;
import models.user.User;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ayassinov
 */
@Entity
@Table(name = "ad_watch")
public class AdWatch extends Model {

    @Embedded
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = "ad_id", insertable = false, updatable = false)
    public Ad ad;

    @ManyToOne
    @JoinColumn(name = "usr_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JoinColumn(name = "show_id", insertable = false, updatable = false)
    public ShowTime showTime;

    @ManyToOne
    @JoinColumn(name = "ext_id", insertable = false, updatable = false)
    public Extra extra;

    @ManyToOne
    @JoinColumn(name = "evt_id", insertable = false, updatable = false, nullable = true)
    public Event event;


    public AdWatch(Ad ad, User user, Extra extra) {
        this.id = new Id(ad, user, extra);
        this.ad = ad;
        this.user = user;
        this.extra = extra;
        this.showTime = extra.showtime;
        this.event = extra.event;
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "ad_id")
        public Long adId;

        @Column(name = "usr_id")
        public Long userId;

        @Column(name = "ext_id")
        public Long extraId;

        @Column(name = "show_id")
        public Long showTimeId;

        @Column(name = "evt_id")
        public Long eventId;

        @Temporal(value = TemporalType.TIMESTAMP)
        @Column(name = "ad_view_date")
        public Date viewDate;

        public Id() {
            this.viewDate = DateTimeUtils.now();
        }

        public Id(Ad ad, User user, Extra extra) {
            this();
            this.adId = ad.id;
            this.userId = user.id;
            this.extraId = extra.id;
            this.showTimeId = extra.showtime.id;
            this.eventId = extra.event.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (adId != null ? !adId.equals(id.adId) : id.adId != null) return false;
            if (eventId != null ? !eventId.equals(id.eventId) : id.eventId != null) return false;
            if (extraId != null ? !extraId.equals(id.extraId) : id.extraId != null) return false;
            if (showTimeId != null ? !showTimeId.equals(id.showTimeId) : id.showTimeId != null) return false;
            if (userId != null ? !userId.equals(id.userId) : id.userId != null) return false;
            if (viewDate != null ? !viewDate.equals(id.viewDate) : id.viewDate != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = adId != null ? adId.hashCode() : 0;
            result = 31 * result + (userId != null ? userId.hashCode() : 0);
            result = 31 * result + (extraId != null ? extraId.hashCode() : 0);
            result = 31 * result + (showTimeId != null ? showTimeId.hashCode() : 0);
            result = 31 * result + (eventId != null ? eventId.hashCode() : 0);
            result = 31 * result + (viewDate != null ? viewDate.hashCode() : 0);
            return result;
        }
    }
}
