package models.ad;

import models.enums.ads.AdTrackType;
import models.extra.Extra;
import models.resources.SqlColumn;
import models.user.User;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author: akhezami
 */
@Entity
@Table(name = "ad_track")
public class AdTrack  extends Model{

    @Enumerated(EnumType.STRING)
    @Column(name = SqlColumn.AdTrack.TYPE, nullable = false)
    public AdTrackType type;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.AdTrack.DATE_TIME, nullable = false)
    public Date timeStamp;

    public User user;

    public Ad ad;

    public Extra extra;
}
