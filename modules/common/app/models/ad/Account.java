package models.ad;

import com.avaje.ebean.SqlRow;
import models.enums.ads.AdvertiserAccountStatus;
import models.resources.FieldLength;
import models.resources.SqlColumn;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ad_account")
public class Account extends Model {

    @Id
    @Column(name = SqlColumn.Account.ACCOUNT_ID, unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(name = SqlColumn.Account.COMPANY, length = FieldLength.Account.COMPANY, nullable = true)
    public String company;

    @Column(name = SqlColumn.Account.PRIMARY_PHONE, length = FieldLength.Account.PHONE, nullable = false)
    public String primaryPhone;

    @Column(name = SqlColumn.Account.SECONDARY_PHONE, length = FieldLength.Account.PHONE, nullable = true)
    public String secondaryPhone;

    @Enumerated(EnumType.STRING)
    @Column(name = SqlColumn.Account.ACCOUNT_STATUS, nullable = false, columnDefinition = "default pending")
    public AdvertiserAccountStatus accountStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = SqlColumn.Account.VALIDATED_DATE_TIME, nullable = true)
    public Date validatedDateTime;

    @Version
    public Long version;

    public Account() {
        this.accountStatus = AdvertiserAccountStatus.pending;
    }

    public Account(SqlRow row) {
        this();
        this.id = row.getLong(SqlColumn.Account.ACCOUNT_ID);
        this.company = row.getString(SqlColumn.Account.COMPANY);
        this.primaryPhone = row.getString(SqlColumn.Account.PRIMARY_PHONE);
        this.secondaryPhone = row.getString(SqlColumn.Account.SECONDARY_PHONE);
        this.validatedDateTime = row.getDate(SqlColumn.Account.VALIDATED_DATE_TIME);
        this.accountStatus = AdvertiserAccountStatus.fromValue(row.getString(SqlColumn.Account.ACCOUNT_STATUS));
    }

    public Account(Long accountId) {
        this.id = accountId;
    }
}