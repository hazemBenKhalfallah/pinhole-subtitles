package models.ad;

import com.google.common.base.Preconditions;
import models.event.Channel;
import models.resources.SqlColumn;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author: akhezami
 */
@Entity
@Table(name = "ad_campaign_channel")
public class CampaignChannel extends Model {

    @EmbeddedId
    @javax.persistence.Id
    public Id id = new Id();

    @ManyToOne
    @JoinColumn(name = SqlColumn.AdCampaign.AD_CAMPAIGN_ID, insertable = false, updatable = false)
    public Campaign campaign;

    @ManyToOne
    @JoinColumn(name = SqlColumn.Channel.CHANNEL_ID, insertable = false, updatable = false)
    public Channel channel;

    public CampaignChannel() {
    }

    public CampaignChannel(Long channelId) {
        this.channel = new Channel(channelId);
        this.id.channelId = channelId;
    }

    public void setCampaign(Campaign campaign) {
        Preconditions.checkNotNull(campaign.id);
        this.campaign = campaign;
        this.id.campaignId = campaign.id;
    }

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = SqlColumn.AdCampaign.AD_CAMPAIGN_ID)
        public Long campaignId;

        @Column(name = SqlColumn.Channel.CHANNEL_ID)
        public Long channelId;

        public Id() {
        }

        public Id(Campaign campaign, Channel channel) {
            this.campaignId = campaign.id;
            this.channelId = channel.id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || this.getClass() != o.getClass()) return false;

            Id id = (Id) o;

            if (campaignId != null ? !campaignId.equals(id.campaignId) : id.campaignId != null) return false;
            //noinspection RedundantIfStatement
            if (channelId != null ? !channelId.equals(id.channelId) : id.channelId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = campaignId != null ? campaignId.hashCode() : 0;
            result = 31 * result + (channelId != null ? channelId.hashCode() : 0);
            return result;
        }
    }
}
