package models;

import models.planning.ShowTime;
import models.xml.WorkLoaderModel;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * @author zied
 */

@Entity
@Table(name = "pin_work_overlaps")
public class OverlapModel extends Model {

    @Id
    @Column(name = "overlap_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long overlapId;

    /**
     * workload id
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "evt_id")
    public WorkLoaderModel workload;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "show_id")
    public ShowTime showTime;

}
