package cookies;

import com.google.common.base.Optional;
import core.DateTimeUtils;
import core.log.Log;
import core.parameter.ApplicationParameter;
import models.user.User;
import play.mvc.Http;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 * @author ayassinov
 */
public class CookieUtils {

    public static final String SESSION_ID_NAME = "UID";
    public static final String LOGOUT_COOKIE_NAME = "PINLOGOUT";
    public static final String TRACK_COOKIE_NAME = "PINTRACK";
    public static final String USER_ID_CONTEXT_VAR_NAME = "USERID";

    /**
     * Get a cookie tracking class
     *
     * @return an optional tack cookie class
     */
    public static Optional<TrackCookie> getTrackCookie() {
        final Http.Cookie trackCookie = getCookieByName(TRACK_COOKIE_NAME);
        if (trackCookie == null)
            return Optional.absent();

        final String value = readCookieValue(trackCookie).orNull();
        if (value == null)
            return Optional.absent();

        return TrackCookie.parse(value);
    }

    public static void writeTrackCookie() {
        final Object userId = Http.Context.current().args.get(USER_ID_CONTEXT_VAR_NAME);
        if (userId == null || !(userId instanceof Long))
            return;

        final TrackCookie trackCookie = new TrackCookie((Long) userId, DateTimeUtils.now());
        saveCookie(trackCookie.toString(), TRACK_COOKIE_NAME, ApplicationParameter.COOKIE.trackingMaxAge);
    }

    public static void setUserIdToCurrentContext(Long userId) {
        if (userId == null)
            return;
        try {
            Http.Context.current().args.put(USER_ID_CONTEXT_VAR_NAME, userId);
        } catch (Exception ex) {
            Log.error(Log.Type.TRACKING, ex, "Failed to set the user id into the current context. user=%s", userId);
        }
    }

    public static Optional<Long> getUserIdFromCurrentContext() {
        try {
            final Object o = Http.Context.current().args.get(USER_ID_CONTEXT_VAR_NAME);
            if (o != null && o instanceof Long) {
                return Optional.of((Long) o);
            }
        } catch (Exception ex) {
            Log.error(Log.Type.TRACKING, ex, "Failed get user id from current context");
        }
        return Optional.absent();
    }

    /**
     * Get session id value from session cookie if exist
     *
     * @return null in case of exception or not found value
     */
    public static String getSessionId() {
        try {
            return Http.Context.current().session().get(SESSION_ID_NAME);
        } catch (Exception ex) {
            Log.error(Log.Type.COOKIE, ex, "Error on getting value from the session cookie with name=%s", SESSION_ID_NAME);
            return null;
        }
    }

    /**
     * Get a visitor cookie from request or
     * from response in case that it's the first time request
     *
     * @return a visitor cookie or null
     */
    public static VisitorCookie getVisitorCookie() {
        Http.Cookie visitorCookie = getCookieByName(ApplicationParameter.COOKIE.visitorName);
        if (visitorCookie == null)// try to find it from response
            visitorCookie = getVisitorCookieFromResponse();

        return VisitorCookie.parse(visitorCookie);
    }

    /**
     * remove the session cookie
     *
     * @return true if removed
     */
    public static boolean removeSessionCookie() {
        try {
            Http.Context.current().session().clear();
            return true;
        } catch (Exception ex) {
            Log.error(Log.Type.COOKIE, ex, "Error on clearing the session cookie");
            return false;
        }
    }

    public static void removeLogOutCookie() {
        try {
            Http.Context.current().response().discardCookie(LOGOUT_COOKIE_NAME);
        } catch (Exception ex) {
            Log.error(Log.Type.COOKIE, ex, "Error on clearing the Logout cookie");
        }
    }

    /**
     * check if we need to update the cookie information for a user. Update the value of the cookie if it's the case
     *
     * @param user the user
     */
    public static void updateVisitorCookie(User user) {
        final VisitorCookie oldCookie = CookieUtils.getVisitorCookie();
        final VisitorCookie newCookie = new VisitorCookie(user);
        if (!oldCookie.equals(newCookie)) { // update only when they are different
            CookieUtils.saveVisitorCookie(new VisitorCookie(user));
        }
    }

    /**
     * Save the session cookie
     *
     * @param sessionId the session id, should not be null!
     * @return true if saved
     */
    public static boolean saveSessionCookie(String sessionId) {
        try {
            Http.Context.current().session().put(SESSION_ID_NAME, sessionId);
            return true;
        } catch (Exception ex) {
            Log.error(Log.Type.COOKIE, ex, "Error on setting the session id into the cookie. sessionId=%s", sessionId);
            return false;
        }
    }

    /**
     * Save the visitor cookie
     *
     * @param visitorCookie teh visitor cookie
     * @return true if saved
     */
    public static boolean saveVisitorCookie(VisitorCookie visitorCookie) {
        return visitorCookie != null && saveCookie(visitorCookie.toJsonText(), ApplicationParameter.COOKIE.visitorName, ApplicationParameter.COOKIE.visitorMaxAge);
    }

    public static void setLogoutCookie() {
        saveCookie(UUID.randomUUID().toString(), LOGOUT_COOKIE_NAME, ApplicationParameter.COOKIE.visitorMaxAge);
    }

    /**
     * encode and serialize a message with the cookie name provided ans the max age.
     *
     * @param message    the value of the cookie
     * @param cookieName the cookie name
     * @param maxAge     the max age of the cookie
     * @return true if the cookie will be sent back to the browser
     */
    private static boolean saveCookie(String message, String cookieName, int maxAge) {
        if (message == null)
            return false;

        try {
            message = java.net.URLEncoder.encode(message, "UTF-8");
            Http.Context.current().response().setCookie(cookieName, message, maxAge);
            return true;
        } catch (UnsupportedEncodingException ex) {
            Log.error(Log.Type.COOKIE, ex, "Error while encoding and writing cookie value. message=%s, cookieName=%s, maxAge=%s", message, cookieName, maxAge);
            return false;
        }
    }


    /**
     * Get cookie by it's name
     *
     * @param cookieName the cookie name
     * @return the Http.cookie object
     */
    private static Http.Cookie getCookieByName(String cookieName) {
        return Http.Context.current().request().cookie(cookieName);
    }

    private static Optional<String> readCookieValue(Http.Cookie cookie) {
        try {
            return Optional.of(java.net.URLDecoder.decode(cookie.value(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            Log.error(Log.Type.TRACKING, e, "Exception on reading cookie value, cookie=%s, value=%s", cookie.name(), cookie.value());
            return Optional.absent();
        }
    }

    /**
     * get the visitor cookie from response
     *
     * @return return a Http.Cookie instance or null if not exists
     */
    private static Http.Cookie getVisitorCookieFromResponse() {
        for (final Http.Cookie cookie : Http.Context.current().response().cookies()) {
            if (cookie.name().equals(ApplicationParameter.COOKIE.visitorName)) {
                return cookie;
            }
        }
        return null;
    }
}
