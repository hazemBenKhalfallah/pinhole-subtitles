package cookies;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import core.DateTimeUtils;
import core.log.Log;

import java.util.Date;

/**
 * @author ayassinov
 */
public final class TrackCookie {
    private static final String splitChar = "@";
    private final Long id;
    private final Date date;

    public TrackCookie(final Long id, final Date date) {
        this.id = Preconditions.checkNotNull(id, "track cookie id");
        this.date = Preconditions.checkNotNull(date, "track cookie date");
    }

    public TrackCookie(final Long id) {
        this(id, DateTimeUtils.now());
    }

    public Long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public static Optional<TrackCookie> parse(String value) {
        try {
            Preconditions.checkNotNull(value, "tracking cookie value");
            String[] values = value.split(splitChar);
            Preconditions.checkArgument(values.length == 2, "{%s} is not a valid tacking cookie value", value);
            final Long id = Long.parseLong(values[1]);
            final Date date = DateTimeUtils.parse(values[0], DateTimeUtils.COOKIE_TRACKING_FORMAT);
            return Optional.of(new TrackCookie(id, date));
        } catch (Exception ex) {
            Log.error(Log.Type.TRACKING, ex, "Exception on parsing string value to the TrackCookie object value=%s", value);
            return Optional.absent();
        }
    }

    @Override
    public String toString() {
        return String.format("%s%s%s", DateTimeUtils.format(this.date, DateTimeUtils.COOKIE_TRACKING_FORMAT), splitChar, id);
    }
}
