/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package cookies;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import core.StringUtils;
import core.log.Log;
import json.JsonSerializer;
import models.user.User;
import play.mvc.Http;

import java.io.IOException;

/**
 * @author ayassinov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VisitorCookie {

    //visitor
    public String uid;
    public String visitor;
    public String language;
    public String location;

    public VisitorCookie() {
    }

    public VisitorCookie(User user) {
        this.uid = user.uid;
        this.visitor = "false";
        if (user.location != null)
            this.location = user.location.code;
        if (user.language != null)
            this.language = user.language.code;
    }


    public static VisitorCookie parse(Http.Cookie cookie) {
        if (cookie == null)
            return null;
        return parse(cookie.value());
    }


    public static VisitorCookie parse(String value) {
        if (StringUtils.isNullOrEmpty(value))
            return null;

        //do parsing !
        try {
            value = java.net.URLDecoder.decode(value, "UTF-8");
            return JsonSerializer.toObject(value, VisitorCookie.class);
        } catch (IOException ex) {
            Log.error(Log.Type.COOKIE, ex, "Error when parsing user cookie with value=%s", value);
            return null;
        }
    }

    public String toJsonText() {
        return JsonSerializer.serialize(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VisitorCookie that = (VisitorCookie) o;

        if (language != null ? !language.equals(that.language) : that.language != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        if (uid != null ? !uid.equals(that.uid) : that.uid != null) return false;
        //noinspection RedundantIfStatement
        if (visitor != null ? !visitor.equals(that.visitor) : that.visitor != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uid != null ? uid.hashCode() : 0;
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (visitor != null ? visitor.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }
}
