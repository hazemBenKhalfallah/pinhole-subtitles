package cookies;

import core.StringUtils;
import play.mvc.Controller;
import play.mvc.Http;

/**
 * @author ayassinov
 */
public class HeaderInfo {

    public String ipAddress;
    public String language;
    public String location;
    public String userAgent;

    public static final String DEFAULT_LOCATION = "TN";

    public HeaderInfo(String ipAddress, String language, String userAgent) {
        this.ipAddress = ipAddress;
        this.language = language;
        this.userAgent = userAgent;
    }

    public static HeaderInfo parse(Http.Request request) {
        final HeaderInfo headerInfo = new HeaderInfo(request.remoteAddress(), Controller.lang().code(), request.getHeader(Http.HeaderNames.USER_AGENT));
        headerInfo.location = getLocation(headerInfo.ipAddress);
        return headerInfo;
    }

    public static HeaderInfo parse() {
        return parse(Http.Context.current().request());
    }

    public static String getLocation(String ipAddress) {
        if (!StringUtils.isNullOrEmpty(ipAddress)) {
            return DEFAULT_LOCATION;
        }

        return DEFAULT_LOCATION; //todo search for location.
    }
}
