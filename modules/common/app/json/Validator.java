package json;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Optional;

/**
 */
public interface Validator {

    public void validate();

    public Optional<ObjectNode> getSuccessNode();
}
