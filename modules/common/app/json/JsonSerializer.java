/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */
package json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.common.base.Optional;
import core.log.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : hazem
 */
public class JsonSerializer {
    private final static ObjectMapper objectMapper = new ObjectMapper();

    public static String serialize(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (Exception ex) {
            Log.error(Log.Type.UTILS, ex, "Error when converting name=%s", object.getClass().getName());
        }
        return "";
    }

    public static <T> T toObject(String jsonText, Class<T> clazz) {
        try {
            return objectMapper.readValue(jsonText, clazz);
        } catch (IOException ex) {
            Log.error(Log.Type.UTILS, ex, "Exception on converting to class=%s", clazz.getName());
            return null;
        }
    }

    public static <T> List<T> toListObject(String jsonText, Class<T> clazz) {
        try {

            return objectMapper.readValue(jsonText, TypeFactory.defaultInstance().constructCollectionType(List.class, clazz));
        } catch (IOException ex) {
            Log.error(Log.Type.UTILS, ex, "Exception on converting to class=%s", clazz.getName());
            return new ArrayList<T>();
        }
    }

    public static Optional<JsonNode> parse(String jsonText) {
        try {
            return Optional.of(objectMapper.readTree(jsonText));
        } catch (IOException ex) {
            Log.error(Log.Type.UTILS, ex, "Exception on converting value=%s", jsonText);
            return Optional.absent();
        }
    }
}
