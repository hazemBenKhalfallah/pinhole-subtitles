package json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Optional;
import core.CommonError;
import core.log.Log;
import play.libs.F;
import play.libs.Json;
import play.mvc.Results;

public abstract class JsonResponse implements Validator {

    @JsonIgnore
    public ArrayNode errors = Json.newObject().arrayNode();

    @JsonIgnore
    private int globalError;

    @JsonIgnore
    public boolean isValid = true;

    public static <T extends JsonResponse> Results.Status parse(Class<T> clazz, F.Function<T, T> function) {
        try {
            T t = getInstance(clazz).orNull();
            if (t != null) {
                t = t.parseParameters();
                t.validate();
                if (t.isValid)
                    t = function.apply(t);
                return t.getResponse();
            }
            Log.error(Log.Type.UTILS, "Parsing failed for class=%s", clazz.getName());
            return T.getParsingErrorResponse();
        } catch (Throwable ex) {
            Log.error(Log.Type.UTILS, ex, "Parsing failed for class=%s", clazz.getName());
            return T.getParsingErrorResponse();
        }
    }

    private static <T extends JsonResponse> Optional<T> getInstance(Class<T> clazz) {
        try {
            final T instance = clazz.newInstance();
            instance.parseParameters();
            return Optional.of(instance);
        } catch (Exception e) {
            Log.error(Log.Type.UTILS, e, "Exception on instantiating an object from class=%", clazz.getName());
            return Optional.absent();
        }
    }

    public Results.Status getResponse() {
        if (isValid)
            return Response.with(Response.Status.OK, getSuccessBody());

        return Response.with(Response.Status.OK, getErrorBody());
    }

    private String getErrorBody() {
        final ObjectNode node = Json.newObject();
        node.put("error", !this.isValid);
        if (this.globalError > 0)
            node.put("global", this.globalError);
        else
            node.putNull("global");
        node.put("errors", this.errors);
        return Json.stringify(node);
    }

    protected String getSuccessBody() {
        final ObjectNode node = Json.newObject();
        node.put("error", !this.isValid);
        node.putNull("global");
        node.putNull("errors");
        if (getSuccessNode().isPresent())
            node.putAll(getSuccessNode().get());
        return Json.stringify(node);
    }

    public void addError(int code) {
        this.globalError = code;
        this.isValid = false;
    }

    public ObjectNode addError(String field, int code) {
        final ObjectNode node = Json.newObject();
        node.put(field, code);
        this.errors.add(node);
        this.isValid = false;
        return node;
    }

    public static Results.Status getParsingErrorResponse() {
        return Response.with(Response.Status.BAD_REQUEST, getParsingErrorBody());
    }

    private static String getParsingErrorBody() {
        final ObjectNode node = Json.newObject();
        node.put("error", true);
        node.put("global", CommonError.Codes.INVALID_JSON_TEXT);
        node.putNull("errors");
        return JsonSerializer.serialize(node);
    }

    @Override
    public Optional<ObjectNode> getSuccessNode() {
        return Optional.absent();
    }

    public <T extends JsonResponse> T parseParameters() {
        return null;
    }

}
