/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

package json;

import com.google.common.base.Optional;
import core.StringUtils;
import core.log.Log;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.SimpleResult;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ayassinov
 */
public class Response {

    public static class Head {
        public static final String HEAD_ETAG = "ETag";
        public static final String LAST_MODIFIED = "Last-Modified";
        public static final String CACHE_CONTROL = "Cache-Control";
        public static final String CONTENT_TYPE = "Content-Type";
        public static final String ACCESS_CONTROL = "Access-Control-Allow-Origin";

        private static final String MAX_AGE_ASSETS_CACHE = "3600";

        private Map<String, String> headers = new HashMap<String, String>();

        public Head setEtag(String etag) {
            if (!StringUtils.isNullOrEmpty(etag))
                this.headers.put(HEAD_ETAG, etag);
            return this;
        }

        public Head setLastModified(String modificationDate) {
            if (!StringUtils.isNullOrEmpty(modificationDate))
                this.headers.put(LAST_MODIFIED, modificationDate);
            return this;
        }

        public Head setDefaultCache() {
            this.headers.put(CACHE_CONTROL, MAX_AGE_ASSETS_CACHE);
            return this;
        }

        public Head setAccessControl() {
            this.headers.put(ACCESS_CONTROL, "*");
            return this;
        }

        public Head setCacheControlValue(String cacheControl) {
            if (!StringUtils.isNullOrEmpty(cacheControl))
                this.headers.put(CACHE_CONTROL, cacheControl);
            return this;
        }

        public Head setContentType(String contentType) {
            if (!StringUtils.isNullOrEmpty(contentType))
                this.headers.put(CONTENT_TYPE, contentType);
            return this;
        }

        protected Map<String, String> getHeaders() {
            return headers;
        }

        protected void setResponseHeaders() {
            if (headers.containsKey(HEAD_ETAG))
                Http.Context.current().response().setHeader(HEAD_ETAG, headers.get(HEAD_ETAG));

            if (headers.containsKey(LAST_MODIFIED))
                Http.Context.current().response().setHeader(LAST_MODIFIED, headers.get(LAST_MODIFIED));

            if (headers.containsKey(CONTENT_TYPE))
                Http.Context.current().response().setHeader(CONTENT_TYPE, headers.get(CONTENT_TYPE));

            //set cache control
            if (headers.containsKey(CACHE_CONTROL)) {
                final String cacheValue = headers.get(CACHE_CONTROL);
                if (cacheValue.equalsIgnoreCase("0"))
                    Http.Context.current().response().setHeader(CACHE_CONTROL, "no-cache");
                else
                    Http.Context.current().response().setHeader(CACHE_CONTROL, "max-age=" + headers.get(CACHE_CONTROL));
            } else {
                Http.Context.current().response().setHeader(CACHE_CONTROL, "no-cache");
            }
        }
    }

    /**
     * The default charset to be used for the json text response "application/json;charset=utf-8"
     */
    public static final String JSON_RESPONSE_CHARSET = "application/json;charset=utf-8";

    public static Head headers() {
        return new Head();
    }

    public static final String XML_RESPONSE_CHARSET = "text/xml;charset=utf-8";
    public static final String HTML_RESPONSE_CHARSET = "text/html;charset=utf-8";
    public static final String TEXT_RESPONSE_CHARSET = "text/plain;charset=utf-8";

    public final static int CACHE_MIN_AGE = 60; //seconds = 1 minutes
    public final static int CACHE_DEFAULT_AGE = 300; //seconds = 5 minutes
    public final static int CACHE_NO_CACHE = 0;
    public final static int CACHE_MAX_AGE = 3600; // 1 hour

    /**
     * The HTTP response status
     */
    public enum Status {
        OK, NOT_FOUND, BAD_REQUEST, INTERNAL_SERVER_ERROR, UNAUTHORIZED
    }

    /**
     * Set a HTTP 200 status with all the headers in config
     *
     * @param body   the body to return
     * @param config the headers to add to the response
     * @return HTTP Result
     */
    public static SimpleResult with(String body, Head config) {
        config.setResponseHeaders();
        return Results.ok(body);
    }

    /**
     * Get a optional value and return 404 HTTP status if not found. With a defined cache age
     *
     * @param maxAge        the value of cache age in seconds
     * @param optionalValue the json string value
     */
    public static Results.Status with(Optional<String> optionalValue, int maxAge) {
        if (optionalValue.isPresent())
            return with(Status.OK, optionalValue.get(), JSON_RESPONSE_CHARSET, maxAge);

        return with(Status.NOT_FOUND);
    }

    /**
     * Get a optional value and return 404 HTTP status if not found. With no cache and with json charset encoding
     *
     * @param optionalValue the json string value
     */
    public static Results.Status with(Optional<String> optionalValue) {
        return with(optionalValue, CACHE_NO_CACHE);
    }

    /**
     * Empty body without cache and with Json type charset
     *
     * @param status the HTTP status response
     */
    public static Results.Status with(Status status) {
        return with(status, "", JSON_RESPONSE_CHARSET, CACHE_NO_CACHE);
    }

    public static Result with(Optional<String> body, String responseType) {
        if (body.isPresent())
            return with(Status.OK, body.get(), responseType, CACHE_NO_CACHE);

        return with(Status.NOT_FOUND, "", responseType, CACHE_NO_CACHE);
    }

    /**
     * @param maxAge if the value is zero we force no cache. The value is in seconds.
     */
    public static Results.Status with(Status status, String body, int maxAge) {

        return with(status, body, JSON_RESPONSE_CHARSET, maxAge);
    }

    /**
     * response with no cache
     *
     * @param status the HTTP status
     * @param body   the json string body
     */
    public static Results.Status with(Status status, String body) {
        return with(status, body, JSON_RESPONSE_CHARSET, 0);
    }

    /**
     * A full method to return a HTTP response
     *
     * @param status       the HTTP Status for the response
     * @param body         the json text response
     * @param responseType the response charset
     * @param maxAge       the max age in seconds for the caching
     */
    public static Results.Status with(Status status, String body, String responseType, int maxAge) {
        //set cache
        if (maxAge == 0)
            Http.Context.current().response().setHeader("Cache-Control", "no-cache");
        else
            Http.Context.current().response().setHeader("Cache-Control", "max-age=" + maxAge);

        switch (status) {
            case OK:
                return Results.ok(body).as(responseType);
            case NOT_FOUND:
                return Results.notFound(body).as(responseType);
            case BAD_REQUEST:
                return Results.badRequest(body).as(responseType);
            case INTERNAL_SERVER_ERROR:
                return Results.internalServerError(body).as(responseType);
            case UNAUTHORIZED:
                return Results.status(403, body).as(responseType);
            default: {
                Log.error(Log.Type.UTILS, "Type response status not found. type=%s", status.toString());
                return Results.internalServerError(body).as(responseType);
            }
        }
    }
}
