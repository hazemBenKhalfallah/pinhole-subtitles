package json.util;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import core.DateTimeUtils;

import java.io.IOException;
import java.util.Date;

/**
 * @author ayassinov
 */
public class JsonDateSerializer extends JsonSerializer<Date> {

    @Override
    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        if (date != null) {
            final String formattedDate = DateTimeUtils.format(date, "dd/MM/yyyy");
            jsonGenerator.writeString(formattedDate);
        } else {
            jsonGenerator.writeNull();
        }

    }
}
