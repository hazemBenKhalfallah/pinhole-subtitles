package json.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import core.DateTimeUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author ayassinov
 */
public class JsonDateDeserializer extends JsonDeserializer<Date> {

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException,
            JsonProcessingException {
        final String dateText = jp.getText();
        return DateTimeUtils.parse(dateText, dateFormat).orNull();
    }
}
