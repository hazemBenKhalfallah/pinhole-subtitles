package json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cookies.CookieUtils;
import cookies.HeaderInfo;
import core.log.Log;
import core.user.BaseUserService;
import models.user.User;
import play.libs.F;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Results;

/**
 */
public abstract class BaseBodyParser extends JsonResponse {

    @JsonIgnore
    public HeaderInfo headerInfo;

    @JsonIgnore
    public String sessionId;

    @JsonIgnore
    public User userInSession;

    @JsonIgnore
    public boolean hasParsingErrors = false;


    protected BaseBodyParser() {
    }

    @SuppressWarnings("unchecked")
    public static <T extends BaseBodyParser> T parse(Http.Request request, Class<T> clazz, boolean withUser) {
        try {
            final T t = Json.fromJson(request.body().asJson(), clazz);
            if (t != null) {
                t.headerInfo = HeaderInfo.parse(request);
                t.sessionId = CookieUtils.getSessionId();
                if (withUser)
                    t.userInSession = BaseUserService.getUserOrCreate();
                t.isValid = true;
                t.validate();
                return t;
            }
            Log.error(Log.Type.UTILS, "Parsing failed for body=%s, class=%s", request.body().asJson(), clazz.getName());
            return getInstance(clazz, true);
        } catch (Exception ex) {
            Log.error(Log.Type.UTILS, ex, "Parsing failed for body=%s, class=%s", request.body().asJson(), clazz.getName());
            return getInstance(clazz, true);
        }
    }

    /**
     * Parse the json text body request to an instance of the Class provided by the clazz parameter.
     * If no error the function will be executed and the response will be returned
     *
     * @param request  the actual Http request
     * @param clazz    convert to this class
     * @param withUser get the user from the cookies
     * @param function the function to be executed
     * @param <T>      The class should extend {@link BaseBodyParser}
     * @return {@link play.mvc.Results.Status}
     */
    public static <T extends BaseBodyParser> Results.Status parse(Http.Request request, Class<T> clazz, boolean withUser, F.Function<T, T> function) {
        try {
            T t = Json.fromJson(request.body().asJson(), clazz);
            if (t != null) {
                t.headerInfo = HeaderInfo.parse(request);
                t.sessionId = CookieUtils.getSessionId();
                if (withUser)
                    t.userInSession = BaseUserService.getUserOrCreate();
                //t.isValid = true;
                t.validate();
                if (t.isValid) {
                    try {
                        t = function.apply(t);
                    } catch (Exception ex) {
                        // return t.getResponse();
                        Log.error(Log.Type.UTILS, ex, "Exception when execution a function from body parser class using class=%s", clazz.getName());
                    }
                }
                return t.getResponse();
            }
            Log.error(Log.Type.UTILS, "Parsing failed for body=%s, class=%s", request.body().asJson(), clazz.getName());
            return T.getParsingErrorResponse();
        } catch (Throwable ex) {
            Log.error(Log.Type.UTILS, ex, "Parsing failed for body=%s, class=%s", request.body().asJson(), clazz.getName());
            return T.getParsingErrorResponse();
        }
    }

    private static <T extends BaseBodyParser> T getInstance(Class<T> clazz, boolean isParsingError) {
        try {
            T t = clazz.newInstance();
            t.hasParsingErrors = isParsingError;
            return t;
        } catch (Exception ex) {
            Log.error(Log.Type.UTILS, ex, "Exception on instantiating an object from class=%s", clazz.getName());
            return null;
        }
    }

}
