import sbt._
import sbt.Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName = "pinhole-subtitles"

  val appVersion = "1.0-Snapshot"

  val commonDependencies = Seq(
    javaCore,
    javaEbean,
    cache,
    filters,
    //hash utils
    "org.apache.directory.studio" % "org.apache.commons.codec" % "1.8",
    // Mockito
    "org.mockito" % "mockito-all" % "1.9.5",
    //for email
    "com.typesafe" %% "play-plugins-mailer" % "2.1.0",
    //Guice
    "com.google.inject" % "guice" % "3.0",
    //for FB connection
    "org.scribe" % "scribe" % "1.3.3",
    //for amazon S3
    "com.amazonaws" % "aws-java-sdk" % "1.6.11",
    //Mustache for email templates
    "com.github.spullara.mustache.java" % "compiler" % "0.8.14",
    //for mongodb
    "org.reactivemongo" % "play2-reactivemongo_2.10" % "0.10.0",
    //for postgres database
    "org.postgresql" % "postgresql" % "9.3-1100-jdbc4",
    //for newrelic
    "com.newrelic.agent.java" % "newrelic-agent" % "3.3.2",
    "com.newrelic.agent.java" % "newrelic-api" % "3.3.2"
  )


  val mainAppDependency = Seq(
  )


  lazy val common = play.Project(appName + "-common", appVersion, commonDependencies, path = file("modules/common")).settings(
    sources in doc in Compile := List()
  )


  val aaMain = play.Project(appName, appVersion, mainAppDependency).settings(
    sources in doc in Compile := List()
  ).dependsOn(common).aggregate(common)


}
