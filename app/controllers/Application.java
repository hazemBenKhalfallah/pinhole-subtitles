package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render());
    }

    public static Result testGet() {
        final ObjectNode node = Json.newObject();
        node.put("name", "aze");
        node.put("age", 25);

        final ObjectNode node2 = Json.newObject();
        node2.put("attr1", "aze");
        node2.put("attr2", "qsd");


        node.put("sub", node2);

        return ok(node);
    }

    public static Result testPost() {
        final JsonNode node = request().body().asJson();
        String a = "", b = "";

        if (node.has("a"))
            a = node.get("a").asText();


        if (node.has("b"))
            b = node.get("b").asText();


        Logger.info("a= " + a + ", b = " + b);





        final ObjectNode result = Json.newObject();
        result.put("error", false);

        result.put("name", "aze");
        result.put("age", 25);

        final ObjectNode node2 = Json.newObject();
        node2.put("attr1", "aze");
        node2.put("attr2", "qsd");

        result.put("sub", node2);

        return ok(result);
    }

}
