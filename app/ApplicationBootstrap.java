/*
 * Copyright (c) 2013 Pinhole SA. All rights reserved. The content of this source may not
 * be copied, replaced, distributed, published, displayed, modified, or transferred in any
 * form or by any means except with the prior permission of Pinhole SA.
 * Copyright infringement is a violation of law subject to criminal and civil penalties.
 */

import core.DateTimeUtils;
import core.log.Log;
import play.Application;

/**
 * @author ayassinov
 */
public class ApplicationBootstrap {

    public static void start(Application application) {
        Log.info(Log.Type.BOOTSTRAP, "Pinhole has started at %s", DateTimeUtils.currentDateWithoutSeconds());
    }
}

