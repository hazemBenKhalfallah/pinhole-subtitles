import core.ResultWrapper;
import core.log.Log;
import core.parameter.ApplicationParameter;
import json.Response;
import play.Application;
import play.GlobalSettings;
import play.api.mvc.EssentialFilter;
import play.filters.gzip.GzipFilter;
import play.libs.F;
import play.mvc.Http;
import play.mvc.SimpleResult;

import static play.mvc.Results.badRequest;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;

/**
 * @author ayassinov
 */
public class Global extends GlobalSettings {

    @Override
    public void onStart(Application application) {
        ApplicationBootstrap.start(application);
    }

    @Override
    public void onStop(Application application) {
        Log.info(Log.Type.BOOTSTRAP, "Pinhole shutdown...");
        super.onStop(application);
        Log.info(Log.Type.BOOTSTRAP, "Pinhole has shutdown");
    }

    @Override
    public F.Promise<SimpleResult> onHandlerNotFound(Http.RequestHeader requestHeader) {
        if (requestHeader.method().equals("GET")) {
            return  ResultWrapper.promise(notFound(views.html.index.render()));
        }

        //404 template
        return ResultWrapper.promise(ok(views.html.index.render()));
    }

    @Override
    public F.Promise<SimpleResult> onBadRequest(Http.RequestHeader request, String error) {
        return ResultWrapper.promise(Response.with(Response.Status.BAD_REQUEST));
    }

    @Override
    public F.Promise<SimpleResult> onError(Http.RequestHeader requestHeader, Throwable throwable) {
        return ResultWrapper.promise(Response.with(Response.Status.INTERNAL_SERVER_ERROR));  //500
    }

    public <T extends EssentialFilter> Class<T>[] filters() {
        return new Class[]{GzipFilter.class};
    }

}
